
# We'll make the log directory owned by this user if we 'make perms'
UWWW=www-data

# TinyMCE is a WYSIWYG Javascript editor which prettifies our textboxes
# Visit http://tinymce.moxiecode.com/download/download.php to check 
# whether this is the latest version
TINYMCEVERSION=3.4.3.2

# JQueryUI makes things a bit prettier (we use it for pop-out menus)
# Visit http://jqueryui.com/download to check whether this is still
# the latest version
JQUERYUIVERSION=1.9.1

# JQuery makes things a bit easier to write JS (JQuery UI uses it)
# Visit http://docs.jquery.com/Downloading_jQuery to check whether this i
# still the latest version
## No longer needed: jquery-ui includes jquery
##JQUERYVERSION=1.6.2

# JQuery.hoverIntent helps out our pop-out menus.
# replaced with jquery menus
# Visit http://cherne.net/brian/resources/jquery.hoverIntent.html to check 
# for any updates
#HOVERURL=http://cherne.net/brian/resources/jquery.hoverIntent.minified.js

# Timepicker is a jQuery extension to help select times
# http://fgelinas.com/code/timepicker/ has more information
TIMEPICKVERSION=0.2.2

# pChart is a GPLd PHP graphing package
# Visit http://www.pchart.net/download to find the latest version
PCHARTVERSION=2.1.1

## That should be all you need to configure, really.

TMPDIR=./tmp

### Default target is to display some help
define SELFAWK
"\
/^[A-z0-9.%\-]*:/  && ! /^_/ && ! /^\./ {\
 if (line !~/xdoc/ && line !~/undoc/) {\
  print \"\",\$$1,line\
 }\
} ; \
 {line=\$$0}"
endef

# Display (this) help text
help:  
	@echo Valid targets are:
	@awk $(SELFAWK) Makefile

# Deal with helpers, symlinks and permissions
all: all-helpers symlinks perms

# Remove temporary files, helper scripts and symlinks
clean: clean-helpers symlinks-clean

.PHONY: doxy 

# Create our temporary directory
${TMPDIR}:
	mkdir -p ${TMPDIR}

# Create the directory for downloading helpers
helpers:
	mkdir -p helpers


# download tinyMCE
${TMPDIR}/tinymce.zip: ${TMPDIR}
	#wget -q -O ${TMPDIR}/tinymce.zip http://tinymce.moxiecode.com/track.php?url=http%3A%2F%2Fgithub.com%2Fdownloads%2Ftinymce%2Ftinymce%2Ftinymce_${TINYMCEVERSION}.zip
	wget -q -O ${TMPDIR}/tinymce.zip  http://github.com/downloads/tinymce/tinymce/tinymce_3.4.3.2.zip

# Unpack tinyMCE
helpers/tinymce: ${TMPDIR}/tinymce.zip helpers
	unzip -o -q ${TMPDIR}/tinymce.zip -d helpers

# Remove tinyMCE
clean-tinymce: ${TMPDIR}
	rm -Rf ${TMPDIR}/tinymce.zip helpers/tinymce

# Download jquery-ui
${TMPDIR}/jquery-ui.zip: ${TMPDIR}
	wget -q -O ${TMPDIR}/jquery-ui.zip http://jqueryui.com/resources/download/jquery-ui-${JQUERYUIVERSION}.custom.zip

# Unpack jquery-ui
helpers/jquery-ui: ${TMPDIR}/jquery-ui.zip helpers
	mkdir -p helpers/jquery-ui
	unzip -o -q ${TMPDIR}/jquery-ui.zip -d helpers/jquery-ui

# Remove jquery-ui
clean-jqueryui: ${TMPDIR}
	rm -Rf ${TMPDIR}/jquery-ui.zip helpers/jquery-ui

# Download timepicker
${TMPDIR}/timepicker.zip: ${TMPDIR}
	wget -q -O ${TMPDIR}/timepicker.zip http://fgelinas.com/code/timepicker/releases/jquery-ui-timepicker-${TIMEPICKVERSION}.zip

# Unpack timepicker
helpers/timepicker: ${TMPDIR}/timepicker.zip helpers
	mkdir -p helpers/timepicker
	unzip -o -q ${TMPDIR}/timepicker.zip -d helpers/timepicker

# Remove timepicker
clean-timepicker:
	rm -Rf ${TMPDIR}/timepicker.zip helpers/timepicker

# Download pChart
${TMPDIR}/pchart.zip: ${TMPDIR}
	wget -q -O ${TMPDIR}/pchart.zip http://www.pchart.net/release/pChart${PCHARTVERSION}.zip

# Unpack pChart
helpers/pchart: ${TMPDIR}/pchart.zip
	mkdir -p helpers/pchart
	unzip -o -q ${TMPDIR}/pchart.zip -d helpers/pchart

# Remove pChart
clean-pchart:
	rm -Rf helpers/pchart

# Create symlinks in js, css and classes
symlinks:
	make -C js links
	make -C css links
	make -C classes links

# Remove symlinks
symlinks-clean:
	make -C js clean
	make -C css clean
	make -C classes clean

# Correct permissions
perms:
	$(info Permitting the webserver user to write to log and incoming)
	$(info This may prompt for a sudo password)
	chown ${UWWW} log 2>/dev/null || sudo chown ${UWWW} log
	chown ${UWWW} incoming 2>/dev/null || sudo chown ${UWWW} incoming

# Create some docco
doxy:
	doxygen doxyfile

# Remove the docco
doxy-clean:
	rm -Rf doxy/html

### Database
database:
	echo Need to write this.
	echo 'For the moment, cat DB/01*/* | psql DBNAME'

### Tidying up
clean-helpers: clean-jquery clean-jqueryui clean-tinymce  clean-jquery-hover clean-timepicker clean-pchart

all-helpers: helpers/jquery-ui helpers/tinymce  helpers/timepicker helpers/pchart
	
