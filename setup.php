<html>
<head>
<title>Hotwire Setup</title>
</head>
<body>
<h2>Hotwire Setup</h2>

<p>This page will attempt to guide you through the setup of hotwire.</p>
<?php

error_reporting(E_ALL);
ini_set("display_errors",1);
$testcount=1;
assert_options(ASSERT_BAIL,true);
assert_options(ASSERT_ACTIVE,   true);
assert_options(ASSERT_WARNING,  true);

testcode('assert(file_exists("libchemdb.php"));',"Initialisation file exists");
require_once "preconfig.php";
testcode('assert(file_exists("config.php"));',"Configuration file exists");
require_once "config.php";
testcode('assert(file_exists("libchemdb.php"));',"Library of routines exists");
require_once "libchemdb.php";

# Check session name
$sessname=session_name();
info("Session name is set to '$sessname'. This ought to be unique to this database installation");

# Check database connection
testcode('$db=new dbBase();',"Create a dbBase object");
$db=new dbBase();
$_SESSION['dbhost']=$PSQL_HOST;
$_SESSION['dbname']=$PSQL_DB;
if (array_key_exists('_dbuser',$_REQUEST)) {
 $_SESSION['dbuser']=$_REQUEST['_dbuser'];
}
if (array_key_exists('_dbpasswd',$_REQUEST)) {
 $_SESSION['dbpasswd']=$_REQUEST['_dbpasswd'];
}

$dbh=$db->dbInit();
if (!$dbh) {
 print "</p>Need a userID and password to test against the database:</p>\n".
       "<form action=setup.php method=post>Username:<input type=text name=_dbuser>".
       "Password:<input type=password name=_dbpasswd>".
       "<input type='submit'/>".
       "</form>";
 exit;
} else {
 print "<p>Database connected correctly.</p><hr/>\n";
}

# Does the hotwire schema exist?
$schemacount=$db->sqltostring("select count(*) from pg_catalog.pg_namespace");
if ($schemacount<1) {
 print "<p class='error'>Sorry, it does not seem possible to query pg_catalog.pg_namespace with the current database user ID. Check that the database credentials are correct?</p>\n";
 exit;
} 
$schemacount=$db->sqltostring("select count(*) from pg_catalog.pg_namespace where nspname='hotwire';");
if ($schemacount<1) {
 print "<p class='warning'>Hmm, it seems that the <tt>hotwire</tt> schema hasn't been created. You might want to <tt>make database<tt></p>";
 exit;
} else {
 print "Found a hotwire schema, this is good.";
}


# Check the hotwire views exist.
if (!$db->viewExists('_view_data','hotwire')) {
 print "<p class='error'>The hotwire Schema needs the view '_view_data' creating.</p>\n";
 exit;
} else {
 print "<p class='success'>Checked that the view '_view_data' exists.</p>\n";
}

if (!$db->viewExists('_column_data','hotwire')) {
 print "<p class='error'>The hotwire Schema needs the view '_column_data' creating.</p>\n";
 exit;
} else {
 print "<p class='success'>Checked that the view '_column_data' exists.</p>\n";
}
if (!$db->viewExists('_preferences','hotwire')) {
 print "<p class='error'>The hotwire Schema needs the view '_preferences' creating.</p>\n";
 exit;
} else {
 print "<p class='success'>Checked that the view '_preferences' exists.</p>\n";
}
if (!$db->viewExists('_role_data','hotwire')) {
 print "<p class='error'>The hotwire Schema needs the view '_role_data' creating.</p>\n";
 exit;
} else {
 print "<p class='success'>Checked that the view '_role_data' exists.</p>\n";
}

if (array_key_exists('action',$_REQUEST)) {
 if ($_REQUEST['action']=='suggestsql') {
 $view=$_REQUEST['view'];
 $menu=$_REQUEST['menu'];
 $name=$_REQUEST['name'];
 $sql='select hotwire.suggest_view($1,$2,$3,$4);';
 $res=pg_query_params($sql,array($view,'public',$menu,$name));
 print "<p>Hotwire's suggested view definition for ".$view."</p>\n";
 print "<textarea cols=80 rows='40'>\n";
 while ($row=pg_fetch_row($res)) {
  print $row[0]."\n";
 }
 print "</textarea>\n";
 }
}


print "<p class='info'>Suggest view definitions: Select a view or table from the list to suggest a view definition which Hotwire could use.</p>\n";
$res=pg_query("select table_name from information_schema.tables where table_schema='public' and table_type in ('BASE TABLE','VIEW');");
print "<form action=setup.php method=post>\n";
print "<select name='view' />\n";
while ($row=pg_fetch_row($res)) {
  print "<option value='".$row[0]."'>".htmlspecialchars($row[0])."</option>\n";
}
print "</select>\n";
print "<input type='hidden' name='_dbuser' value='".$_REQUEST['_dbuser']."'>\n";
print "<input type='hidden' name='_dbpasswd' value='".$_REQUEST['_dbpasswd']."'>\n";
print "<input type='hidden' name='action' value='suggestsql'/>\n";
print "Menu name: <input type='text' name='menu' value='10_Menu'/>\n";
print "View name: <input type='text' name='name' value='Name Of View'>\n";
print "<input type='submit' value='Suggest definition'/>\n";
print "</form>\n";

?>

<p class='success'>Good. Your set-up seems good to go. Your next steps are to generate some views in the hotwire schema. Views will be placed into top-level menus. By default, hotwire has created some views in the '90_Action' menu (which appears at the top of the screen as 'Action': the '90_' is used for sorting then removed). If you were to create a view  called '05_Sales/Germany', the Sales menu would be created on the left hand side of the 'Action' menu and would contain one entry called 'Germany' which links to the view you created.</p>
<p class='success'>Where you have fields in a view which refer to another table, things are a little more complicated. These fields (which currently must be integers or big integers) must end with the name '_id' and there must be a corresponding view ending '_hid' in the hotwire schema. For example, if we have a table of rooms to which the 'person' table refers, we might have a field 'office' in the 'person' table. This 'office' field must contain the 'id' field of the 'rooms' table (a foreign key, in postgres parlance). To get Hotwire to display a list of offices from which to select this entry (rather than just accepting an integer which might get 
rejected by postgres if it doesn't match a value in rooms.id), we rename this field to 'office_id'.
Hotwire will then require a view called 'office_hid' to be present in the 'hotwire' schema. 
The 'office_hid' view must contain a column named 'office_id', which comes from the 'id' field of the 'room' table, and a column 'office_hid' which is the human-readable identifier for the room.
When hotwire presents data for the 'office_id' field, it will use the 'office_hid' view to
convert the integer values into human-readable strings. When presenting an editor for the 'office_id' 
field, Hotwire will present all possible valid values for the field from the 'office_hid' view. (Perhaps some rooms are broom-cupboards, so the office_hid view might have a "WHERE broomcupboard='f'" clause. Although postgres will accept a broom-cupboard as being an acceptable value
for an office_id (it's in the room table, after all), Hotwire will prevent this from being displayed as a choce for the field.
</p>
<p><b>Many-many joins</b> In a more complicated example, people might have more than one item which should maintain referential integrity. 
Take the example of telephone numbers: a person might have several numbers, all of which must exist
in our table of telephone numbers. To maintain integrity for an arbitrary number of telephoe
numbers, we create a mapping table mm_person_telephone_number with a column to store a 
reference to a person and a column to store a reference to a telephone number. Each column
has a foreign-key constraint to ensure that only valid references can be stored. Our view of
a person must then select an array of telephone number references from this table for each 
person:
select *, array(select telephone_number_id from mm_person_telephone_number where person_id=person.id) as telephone_id from person;
for example. Hotwire will then use the telephone_hid table (determined from the name of the
column containing references to telephone number) to display these fields.
</p>
<p>Meh, I'm too close to this to say whether that's helpful information or utterly useless. If you try this, mail me and give me some feedback with suggestions? Thanks! Frank (rl201@cam.ac.uk)</p>


<?php

exit;

## To create a list of views;
$sql=<<<END
SELECT pgns.nspname AS schema, pg_class.relname AS view, pg_class.relacl AS perms, has_table_privilege(((('"'::text || pgns.nspname::text) || '"."'::text) || pg_class.relname::text) || '"'::text, 'insert'::text) AS insert, has_table_privilege(((('"'::text || pgns.nspname::text) || '"."'::text) || pg_class.relname::text) || '"'::text, 'update'::text) AS update, has_table_privilege(((('"'::text || pgns.nspname::text) || '"."'::text) || pg_class.relname::text) || '"'::text, 'delete'::text) AS delete, has_table_privilege(((('"'::text || pgns.nspname::text) || '"."'::text) || pg_class.relname::text) || '"'::text, 'select'::text) AS "select", (( SELECT count(*) AS count                                                                                     FROM pg_rewrite r                                                                         JOIN pg_class c ON c.oid = r.ev_class                                                       LEFT JOIN pg_namespace n ON n.oid = c.relnamespace                                            WHERE r.ev_type = '4'::"char" AND r.rulename <> '_RETURN'::name AND c.relname = pg_class.relname)) > 0 AS delete_rule, (( SELECT count(*) AS count                                                     FROM pg_rewrite r                                                                         JOIN pg_class c ON c.oid = r.ev_class                                                       LEFT JOIN pg_namespace n ON n.oid = c.relnamespace                                            WHERE r.ev_type = '3'::"char" AND r.rulename <> '_RETURN'::name AND c.relname = pg_class.relname)) > 0 AS insert_rule, (( SELECT count(*) AS count                                                     FROM pg_rewrite r                                                                         JOIN pg_class c ON c.oid = r.ev_class                                                       LEFT JOIN pg_namespace n ON n.oid = c.relnamespace                                            WHERE r.ev_type = '2'::"char" AND r.rulename <> '_RETURN'::name AND c.relname = pg_class.relname)) > 0 AS update_rule                                                    FROM pg_class                                                                                  JOIN pg_namespace pgns ON pg_class.relnamespace = pgns.oid                                     JOIN pg_views ON pg_class.relname = pg_views.viewname WHERE pgns.nspname = ANY (ARRAY['hotwire'::name, 'public'::name]);


END;

$res=pg_query($sql);
if (!$res) {
 print "<p class='error'>Could not get a list of views</p>\n";
 print pg_last_error();
 exit;
}
while ($row=pg_fetch_assoc($res)){
 considerview($row);
}

function considerview($row) {
 $view=$row['view'];
 $schema=$row['schema'];
 print "<h3>Considering view $view in schema $schema</h3>\n";
 # Does this view have an 'id' field?
 $s="select * from information_schema.columns ".
    "where table_schema=$1 and table_name=$2 and column_name=$3";
 $tres=pg_prepare('',$s);
 $tres=pg_execute('',array($schema,$view,'id'));
 if (pg_num_rows($tres)>0) {
  print "<p>Good, we have an id field</p>\n";
 } else {
  print "<p>This view has no 'id' field and will not be considered further. Add an 'id' field to this view and repeat if you would like Hotwire to suggest how to incorporate it</p>\n";
  return;
 }
 # Okay, so by now we have an ID field.
 
}

function info($inf) {
 print "<p class='info'>$inf</p>\n<hr/>\n";
}

function testcode($code,$description) {
 global $testcount;
 print "<p class='test'>Running test: ".$description."</p>";
 print "<div id='div$testcount'>\n";
 print "<p class='code'><tt>$code</tt></p>\n<textarea cols='80' rows='8'>"; eval($code); 
 print "</textarea></div><script type='text/javascript'>".
       'document.getElementById("div'.$testcount.'").style.display="none";'."\n".
       '</script>'."\n";
 print "<p class='success' >That worked</p>\n";
 print "<hr>\n";
 $testcount++;
}
?>
