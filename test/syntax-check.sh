#!/bin/sh
CODE=$(dirname "$(readlink -f "$0")")
RESULT=0
for f in $(find ${CODE}/.. -path \*/www/helpers -prune -o -name \*.php -print)
do
    php -l "$f" || RESULT=1
done
exit $RESULT
