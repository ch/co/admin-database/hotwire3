<?php
header ('Content-type: text/html; charset=utf-8');
$_SESSION=array();

require './set_path.php';

require_once "preconfig.php";
include('libchemdb.php');
require_once('config.php');
$hw=new htmlBase();
$hw->prefs['MM_UI_TAG']=True;

# These are probably not necessary in list view
# Need to load JS/CSS for each type of field.
$js=array();
$css=array();
dbDispDate::prepareRecord($js,$css);
dbDispInt4::prepareRecord($js,$css);

# Inject those JS/CSS into the htmlBase object
global $HW_DEFAULT_JS_FILES;
global $HW_DEFAULT_CSS_FILES;
foreach ($js as $j) {
 $HW_DEFAULT_JS_FILES[]=($j.".js");
}
foreach ($css as $c) {
 $HW_DEFAULT_CSS_FILES[]=($c.".css");
}
$HW_DEFAULT_CSS_FILES[]='css/help-format.css';
#$HW_DEFAULT_JS_FILES=array_merge($HW_DEFAULT_JS_FILES,$js);
#$HW_DEFAULT_CSS_FILES=array_merge($HW_DEFAULT_CSS_FILES,$css);

$hw->echoHead();
$hw->echoTop();

?>

<div class='introduction'>Hotwire is a program to make databases accessible through a web browser. It is intended to be intuitive to use, but this page provides a brief introduction to its interface.</div>

<div class='explanation'>Hotwire List Screen</div>

<h1>View name</h1>

<div class='exemplar'>
 <div id='hw_menu'>
 <div id='hw_pagetitle' class='pageTitle'>
  <a href=''>Name of view</a>
  <img title="Remove re-ordering of columns" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAOCAYAAADABlfOAAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB9wGHhMkO8gttlMAAAClSURBVDjLnZNBDsMwCAQXWvXU/z8xL8i1poeUCDA4pCv5YAFjsNdAkACChgQgzRfgaWNcFFyC6YDJiuOY4guchu9Q0GjgTFqBQ2wCcwDa8ShexQBY9xrP6jkDVmACPr/9YzEpqHsn5iDVDuBdNPM3tBQXeaTLAF/mADIrq51eXoKxnbkTVwgurDVZaoTf0rFUZmztbhuLn3LH/OnIDXArkdFUBv0CIztstj1RTBIAAAAASUVORK5CYII="/>
  <img title="Drag this icon to bookmark this page" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAANCAMAAABBwMRzAAAAAXNSR0IArs4c6QAAAJlQTFRF/////v7+9v7+5PX97fX94/T84vP7yOn5v+D5tt/4vt/4td73rt/4rd73pNX2rN32o9z1o9T1nNX2mtP0kcv0f8nyfsjxdsjxd8HybcDxdcfwbL/wZLbwW7XvWr3uSazuUbPtP6rsNqnrLajqLqHrLKfpJaDqLJ/pJJ/pG5/pGp7oI57oCJTmAJTmAJPlAIvlAIvlAIrkAIjibG1MNgAAAAFiS0dEAIgFHUgAAAAJcEhZcwAACxMAAAsTAQCanBgAAAAHdElNRQfcBwUMJAY9R+chAAAAd0lEQVQI12NggABVBhTAZ8SLwteQVUeRVmZRRygQldERYhDWlRThZAbyjIyMxDiYmJh4xOU09YB8ESN2qDouQwEQJagEEeBQ4ofq1wZTRoIw05XAlBY3lC8tzcgrxcugIAHlK0kra0lrqcgpQvlG+iCdwoa6QBIA0ewILR2vgSgAAAAASUVORK5CYII="/>
 </div>
 <nav style='display:inline-block;'><ul></ul></nav>
 <div id="hw_navbuttons">
  <span class="navbuttons" title="Already on the first page">&Lt;</span><a class="navbuttons" title="Display all N records" href="#">All</a><a class="navbuttons" title="View results 51 - 67" href="">&Gt;</a>&nbsp;&nbsp;[1-50] of 67
 </div>
 </div>
</div>

<div class='explanation'>
 The top-left of the screen reminds the user of the name of their current view (in this case, the unimaginative &quot;Name of view&quot;. Clicking on this name will go back to the full view - i.e. without any user-defined sorting or searches. Two icons are also presented; the first to remove any <a href='#colshuffle'>column re-ordering</a>, the second can be dragged to save a bookmark to the current view.
</div>

<div class='explanation'>
 The top-right of the screen shows the user how many records are in the returned dataset and of their position within those records: the example shows that the first 50 records of a dataset containing 67 records are shown. <a href='#'>All</a> will display the full set (which, on very large datasets, might take some time). The arrows move between chunks of data. Hovering over these arrows will show which records will be shown when clicked.
</div>

<h1>Menu bar</h1>

<div class='exemplar'>
<div id="hw_menu">
 <nav id="primary_nav_wrap">
  <ul class='sm sm-clean'>
   <li>
    <a href='#Menu1'>Menu1</a>
    <ul>
     <li><a href='#'>Item</a></li>
     <li><a href='#'>Item</a></li>
     <li><a href='#'>Item</a></li>
     <li><a href='#'>Item</a></li>
     <li><a href='#Submenu'>Submenu</a>
      <ul>
       <li><a href='#'>Item</a></li>
       <li><a href='#'>Item</a></li>
       <li><a href='#'>Item</a></li>
       <li><a href='#'>Item</a></li>
      </ul>
     </li>
    </ul>
   </li>
   <li>
    <a href='#Menu2'>Menu2</a>
    <ul>
     <li><a href='#'>Item</a></li>
     <li><a href='#'>Item</a></li>
     <li><a href='#'>Item</a></li>
     <li><a href='#'>Item</a></li>
     <li><a href='#Submenu2'>Submenu</a>
      <ul>
       <li><a href='#'>Item</a></li>
       <li><a href='#'>Item</a></li>
       <li><a href='#'>Item</a></li>
       <li><a href='#'>Item</a></li>
      </ul>
     </li>
    </ul>
   </li>
   <li> 
    <a href='#Action'>Action</a>
    <ul>
     <li><a href='advsearch-help.php'>Advanced Search</a></li>
     <li><a href='#'>Flip records</a></li>
     <li><a href='#'>New record</a></li>
     <li><a href='#'>Clone record</a></li>
     <li><a href='#'>Export CSV (ISO-8859-1)</a></li>
     <li><a href='#'>Export CSV (UTF8)</a></li>
     <li><a href='#'>Import CSV</a></li>
     <li><a href='#'>Logout</a></li>
     <li><a href='#'>Report problem to ...</a></li>
    </ul>
   </li>
  </ul>
 </nav>
</div>
</div>

<div class='explanation'>The menu bar provides access to different selections of data, in a tree-like structure. Hover the mouse pointer over menus and submenus to see the items contained within.</div>

<div class='explanation'>The &quot;Action&quot; menu contains some special options:<ul>
<li><a href=''>Advanced Search</a> - builds a custom query to return specific records. </li>
<li><a href=''>Amend Search</a> - alters the parameters for the search. (Only shown when a search is taking place.) </li>
<li><a href=''>Flip records</a> - hides the records which were selected and displays the ones which were not.</li>
<li><a href=''>New record</a> - creates a new record with empty values</li>
<li><a href=''>Clone record</a> - creates a new record based on the current record</li>
<li><a href=''>Export CSV (ISO-8859-1)</a> - download the current dataset as a CSV file suitable for Windows machines</li>
<li><a href=''>Export CSV (UTF8)</a> - download the current dataset as a CSV file suitable for Mac/Linux machines</li>
<li><a href=''>Import CSV</a> - upload a CSV file to the dataset (appending new records or updating fields)</li>
<li><a href=''>Logout</a> - end your hotwire session</li>
<li><a href=''>Increase font size</a> - alter how large text appears</li>
<li><a href=''>Reset font size</a> - alter how large text appears</li>
<li><a href=''>Decrease font size</a> - alter how large text appears</li>
<li><a href=''>Report problem to ... </a> - send a bug report to your system administrator</li>
</ul>
</div>

(The CSV, font size and Report problem options may not be enabled on all databases.)

The &quot;Advanced Search&quot; tool has its own <a href='advsearch-help.php'>help page</a>.

<h1>Result box</h1>
<div class='exemplar'>
<div class="results">Update successful.</div>
</div>

<div class='explanation'>The result box shows the result of the last operation. If the last operation was intended to change the data, this box will confirm that the change was successful - or report any errors produced by the underlying database as a result of the attempted change.</div>

<h1>List of results</h1>

<div class='exemplar'>
<div class="hw_resulttable" id="Z__resulttable">
 <table class="hw_resulttable resizable hideable draggable" width="100%">
  <thead>
   <tr class="tableHeader">
    <th id="fieldname1" class="view fieldname1">
     <div class="hw_header" id="hw_colhead_fieldname1">
      <a id="sort_fieldname1" href="#">
       <input class="colname" value="fieldname1" type="hidden">
       <input class="direction" value="" type="hidden">
       <span class="columnname">Fieldname 1</span>&nbsp;
      </a>
     </div>
    </div>
   </th>
    <th id="fieldname2" class="view fieldname2">
     <div class="hw_header" id="hw_colhead_fieldname2">
      <a id="sort_fieldname2" href="#">
       <input class="colname" value="fieldname2" type="hidden">
       <input class="direction" value="" type="hidden">
       <span class="columnname">Fieldname 2</span>&nbsp;
      </a>
     </div>
   </th>
    <th id="fieldname3" class="view fieldname3">
     <div class="hw_header" id="hw_colhead_fieldname3">
      <a id="sort_fieldname3" href="#">
       <input class="colname" value="fieldname3" type="hidden">
       <input class="direction" value="" type="hidden">
       <span class="columnname">Fieldname 3</span>&nbsp;
      </a>
     </div>
   </th>
    <th id="fieldname4" class="view fieldname4">
     <div class="hw_header" id="hw_colhead_fieldname4">
      <a id="sort_fieldname4" href="#">
       <input class="colname" value="fieldname4" type="hidden">
       <input class="direction" value="" type="hidden">
       <span class="columnname">Fieldname 4</span>&nbsp;
      </a>
    </div>
   </th>
    <th id="fieldname5" class="view fieldname5">
     <div class="hw_header" id="hw_colhead_fieldname5">
      <a id="sort_fieldname5" href="#">
       <input class="colname" value="fieldname5" type="hidden">
       <input class="direction" value="" type="hidden">
       <span class="columnname">Fieldname 5</span>&nbsp;
      </a>
    </div>
   </th>
  </tr>
 </thead>
 <tbody class="tableTbody">
  <tr class="even  inactive">
   <td class="view">
    <div>
     <a href="record-help.php">Value1-Row1</a>
    </div>
   </td>
   <td class="view">
    <div>
     <a href="record-help.php">Value2-Row1</a>
    </div>
   </td>
   <td class="view">
    <div>
     <a href="record-help.php">Value3-Row1</a>
    </div>
   </td>
   <td class="view">
    <div>
     <a href="record-help.php">Value4-Row1</a>
    </div>
   </td>
   <td class="view">
    <div>
     <a href="record-help.php">Value5-Row1</a>
    </div>
   </td>
  </tr>
  <tr class="odd inactive">
   <td class="view">
    <div>
     <a href="record-help.php">Value1-Row2</a>
    </div>
   </td>
   <td class="view">
    <div>
     <a href="record-help.php">Value2-Row2</a>
    </div>
   </td>
   <td class="view">
    <div>
     <a href="record-help.php">Value3-Row2</a>
    </div>
   </td>
   <td class="view">
    <div>
     <a href="record-help.php">Value4-Row2</a>
    </div>
   </td>
   <td class="view">
    <div>
     <a href="record-help.php">Value5-Row2</a>
    </div>
   </td>
  </tr>
  <tr class="even  inactive">
   <td class="view">
    <div>
     <a href="record-help.php">Value1-Row3</a>
    </div>
   </td>
   <td class="view">
    <div>
     <a href="record-help.php">Value2-Row3</a>
    </div>
   </td>
   <td class="view">
    <div>
     <a href="record-help.php">Value3-Row3</a>
    </div>
   </td>
   <td class="view">
    <div>
     <a href="record-help.php">Value4-Row3</a>
    </div>
   </td>
   <td class="view">
    <div>
     <a href="record-help.php">Value5-Row3</a>
    </div>
   </td>
  </tr>
 </tbody>
</table>
</div>

<div class='explanation'>
The dataset is presented as a table, similar to a spreadsheet. Clicking on any of the rows takes the user to a page showing the <a href='record-help.php'>record screen</a> - in this help screen, clicking any of the rows shows the help for the Record screen.
</div>

<div class='explanation'>
Clicking on the header at the top of a column brings up a menu of options:
</div>

<div class='exemplar'>
<div class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable ui-resizable" style="outline: 0px none currentcolor; z-index: 1002; height: auto; width: 500px; display: block;" tabindex="-1" role="dialog" aria-labelledby="ui-id-1"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"><span id="ui-id-1" class="ui-dialog-title">

        
Fieldname 1&nbsp;
        
       </span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button"><span class="ui-icon ui-icon-closethick">close</span></a></div><div id="hw_searchAdjPopup_notes" style="display: block; width: auto; min-height: 106px; height: auto;" class="ui-dialog-content ui-widget-content" scrolltop="0" scrollleft="0"><div class="hw_searchAdjPopup"><ul><li class="clickable"><span>Sort ascending</span></li><li class="clickable"><span>Sort descending</span></li><li class="clickable disabled"><span>Remove sorting</span></li><li class="clickable"><span>Hide this column</span></li><li class="clickable"><span>Show all columns</span></li><li><input title="Search this field" class="newsearch" type="test"><button class="ui-button ui-state-default ui-corner-all ui-button-icon-only" title="Search now" aria-disabled="false" role="button" id="gosearch"><span class="ui-icon ui-icon-search"></span><span class="ui-button-text">Search now</span></button><button class="ui-button ui-state-default ui-corner-all ui-button-icon-only" title="Add search term" aria-disabled="false" role="button"><span class="ui-icon ui-icon-plusthick"></span><span class="ui-button-text">Add search term</span></button><button class="ui-button ui-state-default ui-corner-all ui-button-icon-only" title="Remove search term" aria-disabled="false" role="button"><span class="ui-icon ui-icon-minusthick"></span><span class="ui-button-text">Remove search term</span></button></li></ul></div></div><div class="ui-resizable-handle ui-resizable-n" style="z-index: 1000;"></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 1000;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 1000;"></div><div class="ui-resizable-handle ui-resizable-w" style="z-index: 1000;"></div><div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se ui-icon-grip-diagonal-se" style="z-index: 1000;"></div><div class="ui-resizable-handle ui-resizable-sw" style="z-index: 1000;"></div><div class="ui-resizable-handle ui-resizable-ne" style="z-index: 1000;"></div><div class="ui-resizable-handle ui-resizable-nw" style="z-index: 1000;"></div></div>
</div>

<div class='explanation'>
Sorting of the data may be controlled using the first three options. Columns may be hidden or revealed. Columns may also be searched by typing some text in the box and clicking the magnifying glass icon.
</div>

<div class='explanation'>
When a search is in effect on a view, the blue bar above the table shows the search currently in operation.
</div>

<div class='exemplar'>
<div class="hw_sortandsearchforjs"><div id="hw_sortandsearch"><form method="get" action="list.php" name="hw_sortandsearchforjs"><div class="hiddenfields"><div id="hw_hidden"><input name="_view" value="10_IT/10_Access_Group" type="text"><input name="_pagelen" value="50" type="text"></div></div>
<div class="sortedfields"><div id="hw_sort" class="invisible">
<span>Sorting records by: <ul id="hw_sort_id" class="sortable ui-sortable">

</ul></span></div></div>
<div class="searchfields"><div id="hw_search"><span>Showing records where: <ul id="hw_search_id">
<li id="hw_search_1" class="hw_clause">
<input class="hw_search_val" name="notes" value="CIA" size="3" type="text"><span class="clause_argument">Fieldname_3</span> <span class="clause_operator">like</span><span class="clause_value">Value 2</span> 
</li>
<li id="hw_search_2" class="hw_clause">
<input class="hw_search_val" name="person_hid" value="Smith" size="3" type="text"><span class="clause_argument">Fieldname 1</span> <span class="clause_operator">like</span><span class="clause_value">Value 1</span> 
</li>
<input name="_action" value="search" type="text">
</ul></span></div></div>
<div class="hw_sortandsearchsubmit">
<input value="Apply search/sort" type="submit">
</div>
</form>
</div>
</div>
</div>

<div class='explanation'>
This box also displays information about sorting records:
</div>

<div class='exemplar'>
<div class="hw_sortandsearchforjs"><div id="hw_sortandsearch"><form method="get" action="list.php" name="hw_sortandsearchforjs"><div class="hiddenfields"><div id="hw_hidden"><input name="_view" value="10_IT/10_Access_Group" type="text"><input name="_pagelen" value="50" type="text"></div></div>
<div class="sortedfields"><div id="hw_sort">
<span>Sorting records by: <ul id="hw_sort_id" class="sortable ui-sortable">
<li id="hw_sort_1" class="hw_sort">
<input name="_sort[]" class="sort" value="name" size="3" type="text">
<input name="_order[]" class="order" value="Asc" size="3" type="text">
<span class="hw_sort_field">Fieldname 1</span><span class="hw_sort_order">Ascending</span>
</li><li id="hw_sort_2" class="hw_sort">
<input name="_sort[]" class="sort" value="access_group_hid" size="3" type="text">
<input name="_order[]" class="order" value="Desc" size="3" type="text">
<span class="hw_sort_field">Fieldname 3</span><span class="hw_sort_order">Descending</span>
</li></ul></span></div></div>
<div class="searchfields"><div id="hw_search" class="invisible"><span>Showing records where: <ul id="hw_search_id">
</ul></span></div></div>
<div class="hw_sortandsearchsubmit">
<input value="Apply search/sort" type="submit">
</div>
</form>
</div>
</div>
</div>

<div class='explanation'>The sort may be altered by clicking on each sorting &quot;bubble&quot; The sort options may be re-ordered by dragging their &quot;bubbles&quot; around in the box.</div>

<div class='exemplar'>
<div class="ui-dialog ui-widget ui-widget-content ui-corner-all ui-draggable ui-resizable" style="outline: 0px none currentcolor; z-index: 1002; height: auto; width: 500px; display: block;" tabindex="-1" role="dialog" aria-labelledby="ui-id-1"><div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"><span id="ui-id-1" class="ui-dialog-title">Alter sort parameter 1</span><a href="#" class="ui-dialog-titlebar-close ui-corner-all" role="button"><span class="ui-icon ui-icon-closethick">close</span></a></div><div id="hw_sortAdjPopup_access_group_hid" style="display: block; width: auto; min-height: 106px; height: auto;" class="ui-dialog-content ui-widget-content" scrolltop="0" scrollleft="0"><div class="hw_sortAdjPopup">
<select class="hw_searchAdjPopup field">
 <option value="fieldname_1">Fieldname 1</option>
 <option selected="" value="access_group_hid">Fieldname 2</option>
 <option value="fieldname_3">Fieldname 3</option>
 <option value="fieldname_4">Fieldname 4</option>
 <option value="fieldname_5">Fieldname 5</option>
</select>
<select class="hw_sortAdjPopup direction"><option value="Desc" selected="">Descending</option><option value="Asc">Ascending</option></select><button class="ui-button ui-state-default ui-corner-all ui-button-icon-only" title="Sort now" aria-disabled="false" role="button"><span class="ui-icon ui-icon-circle-check"></span><span class="ui-button-text">Sort now</span></button><button class="ui-button ui-state-default ui-corner-all ui-button-icon-only" title="Sort later" aria-disabled="false" role="button"><span class="ui-icon ui-icon-circle-plus"></span><span class="ui-button-text">Sort later</span></button><button class="ui-button ui-state-default ui-corner-all ui-button-icon-only" title="Remove sort" aria-disabled="false" role="button"><span class="ui-icon ui-icon-circle-minus"></span><span class="ui-button-text">Remove sort</span></button></div></div><div class="ui-resizable-handle ui-resizable-n" style="z-index: 1000;"></div><div class="ui-resizable-handle ui-resizable-e" style="z-index: 1000;"></div><div class="ui-resizable-handle ui-resizable-s" style="z-index: 1000;"></div><div class="ui-resizable-handle ui-resizable-w" style="z-index: 1000;"></div><div class="ui-resizable-handle ui-resizable-se ui-icon ui-icon-gripsmall-diagonal-se ui-icon-grip-diagonal-se" style="z-index: 1000;"></div><div class="ui-resizable-handle ui-resizable-sw" style="z-index: 1000;"></div><div class="ui-resizable-handle ui-resizable-ne" style="z-index: 1000;"></div><div class="ui-resizable-handle ui-resizable-nw" style="z-index: 1000;"></div></div>
</div>


<?php
$hw->echoBottom();
