<?php
$GLOBALS['starttime']=microtime(true);
$t='$Id$';
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

 /** 
  * @file dbNavBar.php
  * @brief Contains default configuration which can be over-written by config.php
  * Part of Hotwire http://hotwire.sourceforge.net/.
  * @author Stuart Taylor
  * @author Frank Lee
  */

# String like "$Id$"
if (strlen($t)< 32) {
 $t='ID Dev'.microtime(true).' D';
}
# Split on spaces, take second string
$HOTWIRE_VERSION=explode(' ',$t)[1];

# Set up debugging flags
if (array_key_exists('_debugval',$_REQUEST)) {
 $GLOBALS['debugval']=$_REQUEST['_debugval'];
 $_SESSION['debugval']=$GLOBALS['debugval'];
} else {
 # No debug val, but we did get a _debugdepth, so don't restore from SESSION
 if (!array_key_exists('_debugdepth',$_REQUEST)) {
   if (array_key_exists('debugval',$_SESSION)) {
    $GLOBALS['debugval']=$_SESSION['debugval'];
   } else {
    $GLOBALS['debugval']=array();
   }
 } else {
  $GLOBALS['debugval']=array();
  $_SESSION['debugval']=array();
 }
}
#print "<pre>Debugval is ".join(",",$GLOBALS['debugval'])."</pre>\n";
if (array_key_exists('_debugobj',$_REQUEST)) {
 $GLOBALS['debugobj']=$_REQUEST['_debugobj'];
 $_SESSION['debugobj']=$GLOBALS['debugobj'];
} else {
  # No debug val, but we did get a _debugdepth, so don't restore from SESSION
 if (!array_key_exists('_debugdepth',$_REQUEST)) {
  if (array_key_exists('debugobj',$_SESSION)) {
   $GLOBALS['debugobj']=$_SESSION['debugobj'];
  } else {
   $GLOBALS['debugobj']=array();
  }
 } else {
  $GLOBALS['debugobj']=array();
  $_SESSION['debugobj']=array();
 }
}
#print "<pre>Debugobj is ".join(",",$GLOBALS['debugobj'])."</pre>\n";

error_reporting(E_ALL) ; // only give ugly errors if something really bad happens
#ini_set("display_errors",1);
mb_http_output("UTF-8") ;

# Set the host and database name here.
$PSQL_HOST      = 'localhost';
$PSQL_DB        = 'northwind';

# Default number of items on a page.
$PAGE_LENGTH    = 50;

# Default location of log file
$QUERY_LOG	= '/var/www/db/log/query';

# Instructions about logging in
$LOGINMSG = 'Log in using the default credentials: user=user,password=pass';
$LOGIN_ERROR_MESSAGE = "Failed to log in"; // set this to POSTGRES to use the rather technical postgres message

# Database title
$DBTITLE = 'Hotwire demonstration database';

// Javascript files to be loaded by default
$HW_DEFAULT_JS_FILES=array(
  'js/jquery/jquery.js',
  'js/jquery-ui/jquery-ui.js',
  'js/hotwire.js',
  'js/hw_objs.js',
  //'js/jquery.ui.menubar.js',
  'js/jquery.smartmenus.js',
  'js/jquery.smartmenus.keyboard.js',
  'js/hotwire.smartmenu.js',
  'js/jquery-cookie/jquery.cookie.js',
  'js/hw_debug.js',
  'js/hw_fontsize.js',
  'js/errorreport.js',
  // This for a clone of the older 'double-list' multiple-selects
  'js/multiselect/js/jquery.multi-select.js',
  // This to enable searching of the 'double-list' multiple-selects
  'js/quicksearch/jquery.quicksearch.js',
  // This for the tag-like multiple-selects
  'js/select2/select2.full.js'
);

$HW_DEFAULT_CSS_FILES=array(
  'css/style.css',
  'css/style-print.css',
  //'css/jquery.ui.menubar.css',
  'css/sm-core-css.css',
  'css/sm-clean.css',
  'css/jquery-ui.css',
  #// This for a clone of the older 'double-list' multiple-selects
  'css/multiselect/css/multi-select.css',
  #// And these are local adjustments for selected on the left
  'css/multiselect-hotwire-tweak.css',
  #// This for the tag-like multiple-selects
  #//'css/chosen/chosen.css'
  'css/select2/select2.css'
);

// Messages used on buttons
define("BTN_MSG_CREATE",'Save: Create new record');
define("BTN_MSG_UPDATE",'Save: Update this record');
define("BTN_MSG_UPDATE_SHOW_NEXT",'Save: Update this record, show next');
define("BTN_MSG_SHOW_NEXT",'Show next record');
define("BTN_MSG_DELETE",'Delete');
define("BTN_MSG_SEARCH",'Search');

////////////////////////////////////////////////////////////////////////////////
// Lots of debugging settings here. All are removable in production code ///////
////////////////////////////////////////////////////////////////////////////////

define("HW_DBG_OCRE",0x0001); # Debug object creation (and destruction, perhaps)
define("HW_DBG_OPUB",0x0002); # Debug public methods of objects
define("HW_DBG_OPRO",0x0004); # Debug protected methods of objects
define("HW_DBG_OPRI",0x0008); # Debug private methods of objects
define("HW_DBG_DBBG",0x0010); # Debug DB queries un-related to the view
define("HW_DBG_DBVI",0x0020); # Debug DB queries related to the view
define("HW_DBG_DANY",0x0040); # Debug DB queries of unknown origin
define("HW_DBG_BACK",0x0080); # Display backtrace
$GLOBALS['debug']=0;#HW_DBG_OPUB | HW_DBG_OPRI;

# An array of object types we wish to debug. If the array isn't defined, we debug everything...
$GLOBALS['debugobjects']=array();

// The menu under which logout/search/etc appears
define("HOTWIREMENU",'90_Action');

?>
