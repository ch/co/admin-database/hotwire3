<?php
// $Id$
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

 /** 
  * @file list.php
  * @brief To present a view to the user.
  * Part of Hotwire http://hotwire.sourceforge.net/.
  * @author Stuart Taylor
  * @author Frank Lee
  */

header ('Content-type: text/html; charset=utf-8');
session_start();

require './set_path.php';

require 'preconfig.php';
include 'config.php';
require 'libchemdb.php';
if (!isset($_SESSION['dbuser'])) {

 go_login();
 exit;
} else {
error_reporting(E_ALL);
  $list = new dbMetaList();
  $list->echoHead();
  $list->echoTop();
  $list->echoNavBar();
  $list->displaylist();
  $list->echoBottom();
}
#print "<div class='runtime'>Runtime: ".sprintf('%.3f ms',1000*(microtime(true)-$GLOBALS['starttime']))."</div>\n";

# Present some resource utilisation
$dat=getrusage();
$dat['sql_timing']=$hw_sql_timing;
if (array_key_exists('_dbpasswd',$_POST)) { $_POST['_dbpasswd']='PASSWORD_REMOVED';};
$data=array('GET'=>$_GET,'POST'=>$_POST,'COOKIES'=>$_COOKIE,'FILES'=>$_FILES);
if (isset($_SESSION['dbuser'])) {
 $data['SERVER']=$_SERVER;
}
print "<div class='runtime'><span class='rusage json'>".json_encode($dat)."</span><span class='request json'>".json_encode($data)."</span></div>\n";
exit;

?>
