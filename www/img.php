<?php
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

/*
 * This file is part of Hotwire
 * http://hotwire.sourceforge.net/
 * and presents a single image to the user
 */

 /** 
  * @file image.php
  * @brief Presents an image.
  * @date 2011-09-22
  * @version 1
  **/

error_reporting(E_ALL);
session_start();
require './set_path.php';

require 'preconfig.php';
include 'config.php';
require 'libchemdb.php';

if (!isset($_SESSION['dbuser'])) {
 // Perhaps this should be replaced by a passthrough to a blank image 
 // of the same type?
 go_login();

} else {
  $record = new dbRecord();
  $record->dbInit();
  $record->displayField();
}
exit;
?>
