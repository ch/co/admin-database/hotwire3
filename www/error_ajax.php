<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '/usr/share/php/libphp-phpmailer/src/Exception.php';
require '/usr/share/php/libphp-phpmailer/src/PHPMailer.php';
require '/usr/share/php/libphp-phpmailer/src/SMTP.php';


$error_recipient=null;
include ('../local/config/error_reporting.php');

# Block access to this script unless we have established a user session.
session_start();
if(!isset($_SESSION['dbuser'])) {
  http_response_code(403);
  die();
}

# Permit a "enabled" query to check whether we should use this
if (array_key_exists('enabled',$_REQUEST) && $_REQUEST['enabled']) {
 $result=[];
 $result['enabled']=($error_recipient===NULL?'false':'true');
 $result['recipient']=$error_recipient;
 header('Content-Type: application/json');
 print json_encode($result);
 exit;
}

$result=[];
$result['status']='No-Op';
if (array_key_exists('send',$_POST)) {
 $result['status']='';
 $result['recipient']=$error_recipient;
 
 $to=$error_recipient;  # Change this to help@maths
 
 $text=[];
 $text[]='<html><body><ul>';
 if (array_key_exists('type',$_POST) && $_POST['type']) {
  $text[]='<li>'.$_POST['type'].'</li>';
 }
 if (array_key_exists('comment',$_POST) && $_POST['comment']) {
  $text[]='<li>Comment from user: '.htmlspecialchars($_POST['comment'])."</li>";
 }
 if (array_key_exists('url',$_POST) && $_POST['url']) {
  $text[]='<li>Affected URL <a href="'.$_POST['url'].'">'.$_POST['url'].'</a></li>';
 }
 if (array_key_exists('user',$_POST) && $_POST['user']) {
  $text[]='<li>Affected user '.$_POST['user']."</li>";
 }
 if (array_key_exists('formdata',$_POST) && $_POST['formdata']) {
  $text[]='<li>Form data was '.myprint_r($_POST['formdata'])."</li>";
 }
 if (array_key_exists('rusage',$_POST) && $_POST['rusage']) {
  $text[]='<li>Resource usage: '.myprint_r($_POST['rusage'])."</li>";
 }
 if (array_key_exists('request',$_POST) && $_POST['request']) {
  $text[]='<li>Request: '.myprint_r($_POST['request'])."</li>";
 }
 
 if (array_key_exists('message',$_POST) && $_POST['message']) {
  $msg=str_replace(array("\r", "\n", "%0a", "%0d"), '', stripslashes($_POST['message']));
  $text[]='</li>Message reported was '.$_POST['message']."</li>";
 }
 if (array_key_exists('screenshot',$_POST) && $_POST['screenshot']!='None') {
  $text[]='<li>Screenshot: <img src="'.$_POST['screenshot'].'"></li>';
 }
 $text[]='</ul></body></html>';
 
 
 $mail = new PHPMailer();
 $mail->isSendmail();
 $mail->setFrom($to);
 $mail->addAddress($to);
 $mail->Subject = 'Hotwire3 error report from user';
 $mail->msgHTML(join("",$text));
 
 $r = $mail->send();
 
 if ($r === TRUE) {
  $result['status']='OK';
 } else {
  $result['status']='Bad';
 }

}
header('Content-Type: application/json');
print json_encode($result);

function myprint_r($my_array) {
    return '<pre>'.print_r($my_array,true).'</pre>';
    $res=[];
    if (is_array($my_array)) {
        $res[]="<table border=1 cellspacing=0 cellpadding=3 width=100%>";
        $res[]='<tr><td colspan=2 style="background-color:#333333;"><strong><font color=white>ARRAY</font></strong></td></tr>';
        foreach ($my_array as $k => $v) {
                $res[]='<tr><td valign="top" style="width:40px;background-color:#F0F0F0;">';
                $res[]='<strong>' . $k . "</strong></td><td>";
                $res=array_merge($res,myprint_r($v));
                $res[]="</td></tr>";
        }
        $res[]="</table>";
        return join("\n",$res);
    }
    return $my_array;
}
