<?php
// $Id$
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

 /** 
  * @file list.php
  * @brief To present a view to the user.
  * Part of Hotwire http://hotwire.sourceforge.net/.
  * @author Stuart Taylor
  * @author Frank Lee
  */



# Set our includepath to include:
# ../local/config - for local config files
# ./classes - for `our' classes
# ../local/classes - for local classes (extending others perhaps)
$ds=DIRECTORY_SEPARATOR;
set_include_path(implode(PATH_SEPARATOR,array(
                      get_include_path(),
                      realpath(implode($ds,array(__DIR__,'..','local','config'))),
                      realpath(implode($ds,array(__DIR__,'..','local','classes'))),
                      realpath(implode($ds,array(__DIR__,'classes')))
                 )));
         
unset($ds);

