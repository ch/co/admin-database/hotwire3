<?php
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

/*
 * This file is part of Hotwire
 * http://hotwire.sourceforge.net/
 * and is used by AJAX calls to populate drop-down lists on demand rather than
 * sending all the options to each page every time.
 */

 /**
  * @file ajaxchecksum.php
  * @author Frank Lee
  * @date 2011-07-11
  * @version 11
  */

session_start();
require './set_path.php';

require 'preconfig.php';
include 'config.php';
require 'libchemdb.php';

if ( !isset($_SESSION['dbuser'])) {

?>
[{"optVal":"Login","optDisp":"Not logged in"}]
<?php

} else {
 if (array_key_exists('_id',$_REQUEST) && $_REQUEST['_id']) {
  $record = new dbRecord();
  if (method_exists($record,'calculateChecksum')) {
   $record->calculateChecksum();
   print '[{"checksum":"'.$record->checksum.'"}]';
  } else {
   print '[{"checksum":"No method"}]';
  }
 } else {
  $record = new dbList();
  if (method_exists($record,'calculateChecksum')) {
   $record->calculateChecksum();
  }
  #print "Checksum :".$record->checksum."\n";
  print '[{"checksum":"'.$record->checksum.'"}]';
 }

}

?>
