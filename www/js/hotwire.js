/**
 * @file hotwire.js
 * @brief Changes colour of page when data changes.
 * Part of Hotwire http://hotwire.sourceforge.net/ .
 * @author Frank Lee
 */

/* Some timing information */
var hw_js_start_time_ms=Date.now();
var hw_js_end_time_ms=0;

// Handle missing empty select[multiple] results
function addemptytexts() {
 $('form#itemdata select[multiple]').filter(function(){return $(this).val() ===null;}).each(function(i,e){
  $(this).parent().append($('<input>').attr('name',$(this).attr('name')).attr('type','text'));
 });
}

/* Because internet explorer still hasn't died. */
// https://tc39.github.io/ecma262/#sec-array.prototype.includes
if (!Array.prototype.includes) {
  Object.defineProperty(Array.prototype, 'includes', {
    value: function(valueToFind, fromIndex) {

      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      // 1. Let O be ? ToObject(this value).
      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If len is 0, return false.
      if (len === 0) {
        return false;
      }

      // 4. Let n be ? ToInteger(fromIndex).
      //    (If fromIndex is undefined, this step produces the value 0.)
      var n = fromIndex | 0;

      // 5. If n ≥ 0, then
      //  a. Let k be n.
      // 6. Else n < 0,
      //  a. Let k be len + n.
      //  b. If k < 0, let k be 0.
      var k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

      function sameValueZero(x, y) {
        return x === y || (typeof x === 'number' && typeof y === 'number' && isNaN(x) && isNaN(y));
      }

      // 7. Repeat, while k < len
      while (k < len) {
        // a. Let elementK be the result of ? Get(O, ! ToString(k)).
        // b. If SameValueZero(valueToFind, elementK) is true, return true.
        if (sameValueZero(o[k], valueToFind)) {
          return true;
        }
        // c. Increase k by 1. 
        k++;
      }

      // 8. Return false
      return false;
    }
  });
}

function setTitle() {
  var d = document.getElementById('pageTitle');
  if (d != null) {
    document.title = d.innerHTML + ' :: ' + document.title;
  }
}

// Horrible hack:
if (!('browser' in $)) {
  $.browser='';
}

/**
 * @brief Function to format metaviews by coverting
 * Part of Hotwire http://hotwire.sourceforge.net/ .
 * @author Frank Lee
 **/
function meta_view_format() {
 if ($('body div.hw_resulttable:visible').length>1) {
  $('div.results').hide();
  $('span.hw_meta_view_separator').each(function() { 
   var label=$(this).clone(); 
   $(this).closest('div.hw_resulttable:visible').addClass('hw_meta_view_separator');
   var link=$(this).parent().next();
   label.append(link);
   $(this).closest('div.hw_resulttable').find('div.hw_sortandsearchforjs').remove();
   $(this).closest('div.hw_resulttable').find('div.floatHeader').remove();
   $(this).closest('table').replaceWith(label); 
  });
 }
}

// To build the replacement multi-select UI
var multiselect_args={
    selectableHeader: "<span>Available</span><input type='text' class='search-input' autocomplete='off' placeholder='search, enter to select'>",
    selectionHeader: "<span>Selected</span>",
    afterInit: function(ms){
      var that = this,
          $selectableSearch = that.$selectableUl.prev(),
          selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)';

      that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
      .on('keydown', function(e){
        if (e.which === 40){
          that.$selectableUl.focus();
          return false;
        }
        if (e.which == 13) {
          e.preventDefault();
          $(that.$selectableUl).find('li:visible').click();
          return false;
        }
      });
    },
    afterSelect: function(){
      this.qs1.cache();
    },
    afterDeselect: function(){
      this.qs1.cache();
    }
  };

/* To extend the options in a list */
function ajax_sel_list_extend(list,id) {
 sel=$('select#'+id);
 selection=sel.val();
 //h=Object.keys(list).map( u => $('<option>').attr('value',u).text(list[u]));
 h=list.map(function(item,index){ 
  var o=$('<option>').attr('value',item['id']).text(item['text']).attr('sortpos',index);
  if (item['disabled']) { o.attr('disabled','true'); }
  return o;
 });
 if (('MM_UI_TAG' in hw_prefs)) {
  // Don't label the 'blank' option for TAG interface
  h.unshift($('<option>').text((hw_prefs['MM_UI_TAG']==true && sel.attr('notnull')=='0')?'':'blank'));
 }
 // Can't use Spread (...) operator in Internet Explorer
 //sel.empty().append(...h);
 sel.empty();
 h.map(function(e,i){
  sel.append(e);
 });
 
 if (selection) {
   if (Array.isArray(selection)) {
     selection.forEach(function(k,i){
       $('option[value="'+k+'"]',sel).attr('selected','selected');
     });
   } else {
     tstamp('setting selection of '+id+' to '+selection);
     $('option[value="'+selection+'"]',sel).attr('selected','selected');
   }
 }
 sel.val(selection);
}

/* A function to populate the select elements by ajax or cache */
function ajax_select_generic(disable_flag,callback) {
 sel=$('select.select_ajax_multi, select.select_ajax').first();
 if (! (sel && sel.length>0)) {
   tstamp('No further selects to consider');
   return;
 }
 if (! window.hasOwnProperty('hid_cache')) {
   window.hid_cache=JSON.parse(localStorage.getItem('hid_cache')||'{}');
 }
 if (sel.hasClass('select_ajax_multi')) {
   sel.removeClass('select_ajax_multi').addClass('select_ajax_multi_done');
 }
 if (sel.hasClass('select_ajax')) {
   sel.removeClass('select_ajax').addClass('select_ajax_done');
 }
 md5=sel.attr('md5');
 if (window.hid_cache && md5 in hid_cache && Array.isArray(hid_cache[md5]['opts'])) {
   tstamp('Cache hit for '+sel.attr('id'));
   // Mark this cache as being useful for this view and column
   hid_cache_allocate(md5,sel.attr('view'),sel.attr('column'));
   // Extend the 'options'
   ajax_sel_list_extend(hid_cache[md5]['opts'],sel.attr('id'));
 } else {
   tstamp('Loading data from '+sel.attr('src')+' for '+sel.attr('id'));
   $.ajax({
     method: "GET",
     type: "GET",
     url: sel.attr('src'),
     dataType: 'json',
     data: {
       dom_id:sel.attr('id')
     }
   }).done(function(d){
      // Die unless we have important data sent
      if (! ('md5' in d && d.md5
         && 'view' in d && d.view
         && 'column' in d && d.column
         && 'dom_id' in d && d.dom_id)) {
         tstamp('Interesting data back from AJAX request:');
         console.log(d);
         return;
      }
      // Stash this in the hid_cache
      hid_cache_save(d.md5,d.view,d.column,d.opts);
      // Extend the 'options'
      ajax_sel_list_extend(d.opts,d.dom_id);
   });
 }
 // Perform the magic
 if (sel.attr('disabled_by')==disable_flag) {
   sel.removeAttr('disabled').removeAttr('disabled_by');
 }
 callback(sel);
 // Do another one
 setTimeout(ajax_select_generic,10,disable_flag,callback);
}

// To save some data into the hid cache
function hid_cache_save(md5,view,column,options) {
  window.hid_cache[md5]={opts:options,used_for:{}};
  window.hid_cache[md5]["used_for"][view]=[column];
  hid_cache_remove_others(md5,view,column);
  // should catch exceptions here or we get form elements that don't function
  try {
    localStorage.setItem('hid_cache',JSON.stringify(window.hid_cache));
  }
  catch(err) {
    // But I don't know what to do with the exception...however continuing is better than stopping
  }
}

// To ensure the given MD5 is the only cache kept for 
// this view and column combination
function hid_cache_remove_others(md5,view,column) {
  // Take this opportunity to remove any entries where 'opts' 
  // isn't an array
  Object.keys(window.hid_cache).forEach(function(k,i) {
    if (!Array.isArray(window.hid_cache[k]['opts'])) {
      delete window.hid_cache[k];
    }
  });
  // remove other matches for view and column from other caches
  //console.log(view+'/'+column+' is '+md5+' - looking for others');
  //Object.keys(window.hid_cache).filter(m => (m!=md5)).forEach(function(k,i) {
  Object.keys(window.hid_cache).filter(function(m) {if (m!=md5) { return m; }}).forEach(function(k,i) {
    if (view in window.hid_cache[k]["used_for"]) {
      // Remove this column from the cols for this view 
      //console.log(window.hid_cache[k]["used_for"][view]);
      if (window.hid_cache[k]["used_for"][view].includes(column)) {
        //console.log('Cache '+k+' has '+view+'/'+column+' : Deleting');
        //console.log(window.hid_cache[k]["used_for"][view]);
        // Okay, so this /doesn't/ work.
        // delete window.hid_cache[k]["used_for"][view][column];
        // Nut? Meet Mr. Sledgehammer...
        window.hid_cache[k]["used_for"][view]=window.hid_cache[k]["used_for"][view].filter(function(v,a,r){return v!=column;});
        //console.log(window.hid_cache[k]["used_for"][view]);
      }
      // Remove the entry for this view if empty
      if (window.hid_cache[k]["used_for"][view].length==0) {
        delete window.hid_cache[k]["used_for"][view];
      }
    }
    // Delete cache if no longer required
    if (Object.keys(window.hid_cache[k]["used_for"]).length==0) {
      delete window.hid_cache[k];
    }
  });
}

// To mark this cache as being useful for a particular view and column
function hid_cache_allocate(md5,view,column) {
  if (!(view in window.hid_cache[md5]['used_for'])) {
     window.hid_cache[md5]['used_for'][view]=[];
  }
  cols=window.hid_cache[md5]['used_for'][view];
  if (cols.indexOf(column) === -1) { cols.push(column);};
  window.hid_cache[md5]['used_for'][view]=cols
  hid_cache_remove_others(md5,view,column);
  // should catch exceptions here or we end up with form elements that don't function
  try {
    localStorage.setItem('hid_cache',JSON.stringify(window.hid_cache));
  }
  catch (err) {
    // Don't know what to do here. But we don't want the exception. 
  }
}


function hw_changemonitor_init() {
 // Configure change-monitoring
 $('form#itemdata input, form#itemdata textarea, form#itemdata select').change(function() {
   $('div.resultstable').removeClass('resultstable').addClass('resultstablechanged');
   changes = true;
 });
}

function hw_menupath_highlight() {
 var view=$('input#_view').first().val();
 var e=$('a[href="list.php?_view='+view+'"]').first();
 e.addClass('menuCurrentItem');
 while (e.closest('ul').parent().children('a').length) {
  e=e.closest('ul').parent().children('a').first();
  e.addClass('menuCurrentItem');
 }
 tstamp('Ended menu path');
}

function hw_addcancelbutton() {
  if (typeof hw_prefs == "undefined") { return; }
  if (hw_prefs==null) { return ; }
  // To add a cancel button for people who have been told never to touch
  // the most basic component of a web browser's user interface
  // Only display if 'SHOWCANCEL' is set to TRUE
  if (window.history.length>1 && 'SHOWCANCEL' in hw_prefs && hw_prefs['SHOWCANCEL']==true) {
  $('div.tableFooter').prepend(
    $('<button>').click(function(e) {
      e.preventDefault()
      history.back(-1);
    }).text('Cancel')
  );
  $('div.dbRecord_hints').append(
    $('<span>').addClass('cancel').text("'Cancel' to go back.")
  );
  tstamp('Added cancel button');
  }
}

function hw_phonydelete_button() {
  // Add a replica 'Delete' button to tidy up the UI a little
  // But only if we have a real one (-:
  if ($('form#deleteform input[type="submit"]').length > 0) {
    $('div.tableFooter').first().prepend(
      $('<button>').attr('id','formdel').text('Delete')
    );
    $('button#formdel').prop('type','button').click(function() {
      $('form#deleteform input[type="submit"]').click();
    });
    $('form#deleteform').parent().hide();
    tstamp('Hidden old deletion form');
  } else {
    $('span.hint_delete').hide();
  }
}

function hw_tallfields_init() {
 // Tooltip / title on 'big' fields
 // Much slower to do it on "big" fields (.offsetHeight>.scrollHeight) so
 // do it on all.
 $('table.hw_resulttable td div a').slice(0,1000000).each( 
  function(i,e){
   e.title=e.text;
 });
 tstamp('Ended mouseover');

 // Adjust height of 'big' rows
 $('div.hw_resulttable').append(
  $('<menu>').attr('type','context').attr('id','mymenu').append(
   $('<menuitem>').attr('label','Auto-height row').click(function(){
     $($('#Z__resulttable')[0].cte.target).closest('tr').find('td div').css('max-height','inherit');
   }),
   $('<menuitem>').attr('label','Auto-height all').click(function(){
     $('div.hw_resulttable table tr td div').css('max-height','inherit');
   })
  )
 );
 $('div.hw_resulttable table').attr('contextmenu','mymenu');
 if ($('#Z__resulttable').length>0) {
   document.getElementById('Z__resulttable').addEventListener("contextmenu",function(){
     this.cte=arguments[0];
   });
 };
 tstamp('Ended context menu');
};

function hw_dropcache_init() {
 $('div.tableResults').first().append(
  $('<menu>').attr('id','tableResults_ctxt_menu').attr('type','context').append(
   $('<menuitem>').attr('label','Clear cached data').click(function() {
    console.log('Context menu clicked');
    localStorage.clear();
    alert('Reload page');
   })
  )
 );
 console.log('Doing Dropcache');
 $('div.tableResults').attr('contextmenu','tableResults_ctxt_menu');
}

$(document).ready(function(){
  window.hw_js_start=Date.now();
  tstamp('hotwire.js starting');
  setTitle();              // Set the page title
  meta_view_format();      // Format any meta-views
  hw_changemonitor_init(); // Enable monitoring for changes
  hw_menupath_highlight(); // Highlight the menu path
  hw_tallfields_init();    // Sort out tall rows
  hw_dropcache_init();     // A right-click option to delete cached data
  hw_addcancelbutton();    // Add a 'cancel' button.
  hw_phonydelete_button(); // Add a better-placed 'delete' button.
  hw_reformatselect();     // Reformat the select lists.
  $('form#itemdata').on("submit",addemptytexts); // Ensure that empty lists are still submitted.
  // Tidy up unused UI elements.
  if ($('div.tableFooter input.update').length == 0) {
    $('span.hint_update').hide();
  }

  // Set up the dateTimePicker args:
  dateTimePickerArgs={inline:true, format:'Y-m-d H:i:s'};
  // Have we a DATEFORMAT?
  if (typeof hw_prefs !== "undefined" && hw_prefs !== null && 'DATEFORMAT' in hw_prefs) {
    dateTimePickerArgs['format']=hw_prefs['DATEFORMAT']+' H:i';
  }
  // Have we a DAYSTART?
  if (typeof hw_prefs !== "undefined" && hw_prefs !== null && 'JS_UI_DAYSTART' in hw_prefs && ! isNaN(hw_prefs['JS_UI_DAYSTART'])) {
    dateTimePickerArgs['dayOfWeekStart']=hw_prefs['JS_UI_DAYSTART'];
  }
  // Add further arguments:
  if (typeof dateTimePickerArguments=='object') {
    dateTimePickerArgs=Object.assign(dateTimePickerArgs,dateTimePickerArguments);
  }
  if ('datetimepicker' in jQuery && typeof(jQuery.datetimepicker)=='object') {
    $('div.tableResults div.timestamp div.recordvalue input').datetimepicker(dateTimePickerArgs);
  }

  // Make table-width adjustable
  // Set width to ensure that the plugin knows the starting point.
  if (typeof hw_prefs !== "undefined" && hw_prefs !== null && 'RESIZECOLS' in hw_prefs && hw_prefs['RESIZECOLS']) {
    $('table.hw_resulttable.resizable th').each(function(i,e){
      $(e).css('width',$(e).width()+'px' );
    });
    $("table.hw_resulttable.resizable").colResizable({
	    liveDrag:true, 
	    gripInnerHtml:"<div class='grip'></div>", 
	    draggingClass:"dragging", 
      resizeMode:'overflow',
      postbackSafe:true,
      partialRefresh: true
    });
    tstamp('Table now draggable');
  };

  tstamp('Ended');
  hw_js_end_time_ms=Date.now();
});


function hw_pref(p,def) {
 if (typeof hw_prefs == "undefined") { return def; }
 if (hw_prefs == null) { return def; }
 if (!(p in hw_prefs)) {
   return def;
 }
 return hw_prefs[p];
}

// To sort our select2 interfaces
var customSort = function(data) {
  tstamp('In customSort');
  data.sort(function(a,b) {
    if (a.element.attributes.getNamedItem('sortpos')=== null) {
      return null;
    }
    if (b.element.attributes.getNamedItem('sortpos')=== null) {
      return null;
    }
    return parseInt(a.element.attributes.getNamedItem('sortpos').value) - parseInt(b.element.attributes.getNamedItem('sortpos').value);
  });
  tstamp('sorted');
  return data;
}

function omitBlank(field) {
  if (field.text == 'blank' && field.id== 'blank') {
   return null; 
  }
  return field.text;
}

function hw_select2_cb(sel) {
  var args={sorter: customSort, templateResult: omitBlank};
  if (sel.attr('notnull')=='0') {
    args['allowClear']=true;
  }
  if (sel.attr('multiple')) {
      if (hw_pref('MM_UI_TAG',false)==true) {
      args['placeholder']='Type to search and add options';
    } else {
      args['placeholder']='Select one or more';
    }
  } else {
    if (hw_pref('MM_UI_TAG',false)==true) {
      args['placeholder']='Type to search and select a single option';
    } else {
      args['placeholder']='Select a single entry';
    }
  }
  // Set up a read-only attribute

  if ((sel.attr('id').substring(0,3)=='ro_') && (sel[0].hasAttribute('disabled'))) {
    args['disabled']=true;
  }
  sel.select2(args);
  // TODO
  // Add a context menu to this item to reload the list.
}

function hw_doublelist_cb(sel) {
  if ($(sel)[0].hasAttribute('multiple')) {
    sel.multiSelect(multiselect_args);
  }
}

// Adjust the UI of select lists 
function hw_reformatselect() {
  // Return early if MM_UI_NOJS is set to true
  if (hw_pref('MM_UI_NOJS',false)==true) {
    return;
  }
  tstamp('Format MM lists using JS');
  if (hw_pref('MM_UI_TAG',false)==true) { // Set pref to use tag-like select2 interface
    if (hw_pref('MM_UI_AJAX',false)==true) {
      $('select.select_ajax, select.select_ajax_multi').not('[disabled]').attr('disabled','disabled').attr('disabled_by','select_select2');
      ajax_select_generic('select_select2',hw_select2_cb);
      tstamp('Initiated select2');
    } else {
      $('select').select2({ sorter: customSort });
      // Do all at once
    } 
    tstamp('Set up select2');
  } else { // Use double-list UI for multi-select options.
    if (hw_pref('MM_UI_AJAX',false)==true) {
      $('select.select_ajax, select.select_ajax_multi').not('[disabled]').attr('disabled','disabled').attr('disabled_by','select_doublelist');
      ajax_select_generic('select_doublelist',hw_doublelist_cb);
      tstamp('Initiated double-list');
    } else {
      $('select[multiple]').multiSelect(multiselect_args);
    }
    tstamp('Added double-list context menus');
  }
}

function tstamp(msg) {
 var n=Date.now();
 console.log(n+': ['+(n-window.hw_js_start)+'] '+msg);
}

function hw_domName(ident) {
  ident=ident.replace(/_/,"__");
  ident=ident.replace(/[^-_A-Za-z0-9:\.]/g,"_");
  return ident;
}
