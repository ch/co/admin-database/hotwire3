/** 
 * @file timepicker.js
 * @brief Produces UI for a time field.
 * @author Frank Lee
 */

$(document).ready(function(){
 $('input.time').timepicker({showSecond: true, timeFormat: 'hh:mm:ss'});
});

