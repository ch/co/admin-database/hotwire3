/* To set up the smart menu */
	$(function() {
		$('#primary_nav_wrap ul').first().smartmenus({
			mainMenuSubOffsetX: -1,
			mainMenuSubOffsetY: 4,
			subMenusSubOffsetX: 6,
			subMenusSubOffsetY: -6,
		});
                $('#primary_nav_wrap ul').first().smartmenus('keyboardSetHotkey', 123, 'shiftKey');
	});
