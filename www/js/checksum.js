/** 
 * @file checksum.js
 * @brief Helps produce the checksum for a dataset/record.
 * @author Frank Lee
 */

function comparechecksum(ident,goodsum,view,id) {
 //787f2643b45987a2d629530eb6acce5c
 if (id==null ) {
  $.getJSON("ajaxchecksum.php",{_view: view}, function(data) {
   if (goodsum != data[0].checksum) {
    $('#'+ident).show();
    $('#'+ident).html("Warning: Data has changed. Reload this view");
    //$('#'+ident).html("Checksum mismatch: "+"Old checksum "+goodsum+" New checksum "+data[0].checksum+" ");
   } else {
    // var now=new Date();
    // $('#'+ident).html("Old checksum "+goodsum+" New checksum "+data[0].checksum+" "+now.getSeconds());
    $('#'+ident).hide();
   }
  });
 } else {
  $.getJSON("ajaxchecksum.php",{_view: view, _id: id},function(data) {
   if (goodsum != data[0].checksum) {
    $('#'+ident).show();
    $('#'+ident).html("Warning: Data has changed. Reload this record");
    //$('#'+ident).html("Checksum mismatch: "+"Old checksum "+goodsum+" New checksum "+data[0].checksum+" ");
   } else {
    // var now=new Date();
    // $('#'+ident).html("Old checksum "+goodsum+" New checksum "+data[0].checksum+" "+now.getSeconds());
    $('#'+ident).hide();
   }
  });
 }
 setTimeout(function() {
  comparechecksum(ident,goodsum,view,id);
    }, 5000);
}

