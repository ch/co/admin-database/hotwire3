/** 
 * @file advsearch.js
 * @brief Helps with the 'advanced search' view.
 * Displays 'Show all' and 'Hide all' buttons. Converts 
 * checkbox logic from 'click to hide' (better for HTTP 
 * POST logic) to 'click to show' (better UI).
 * Part of Hotwire http://hotwire.sourceforge.net/ .
 * @author Frank Lee
 */

$(document).ready(function(){
 $('div.suppress').hide();
 $('div.suppressall').hide();
 $('div.displayall').html('<button id=\'displayall\'>Show all</button><button id=\'undisplayall\'>Hide all</button>');
 $('div.displayall button#displayall').click(function() {
  $('div.display [type="checkbox"][name^="dsp"]').prop('checked',true); 
  $('div.suppress [type="checkbox"][name^="hde"]').prop('checked',false);
  return false; // prevent form submission by this button
 });
 $('div.displayall button#undisplayall').click(function() {
  $('div.suppress [type="checkbox"][name^="hde"]').prop('checked',true);
  $('div.display [type="checkbox"][name^="dsp"]').prop('checked',false);
  return false; // prevent form submission by this button
 });
 $('div.display [type="checkbox"][name^="dsp"]').each(function() {
  $(this).change(function () {
    var orig_id=$(this).attr('id');
    var hde_id=orig_id.replace('dsp_','hde_');
   $('div.suppress input#'+hde_id).prop('checked',!$(this).prop('checked'));
  });
 });
});
