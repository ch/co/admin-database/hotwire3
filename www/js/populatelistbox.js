/**
 * @file populatelistbox.js
 * @brief Performs AJAX calls to populate a list-box.
 * List boxes can be left unpopulated and populated when
 * they're entered - saves time when there are long lists 
 * of options which don't get changed often.
 * Part of Hotwire http://hotwire.sourceforge.net/ .
 * @author Frank Lee
 */

function FAJ_load(field,view) {
  var ident=field;
// Replace non-DOM compatible characters with underscores, as in
// htmlBase->domName

  //ident=ident.replace(/_/g,"__");
  //ident=ident.replace(/[^-_A-Za-z0-9:\.]/g,"_");
document.body.style.cursor='wait';
console.log('View is '+view);
console.log('Looking for ident '+ident);
var faj_box=document.getElementById('FAJ_replacement_'+ident);
faj_box.value='Loading options...';
faj_box.removeAttribute('onfocus');
var faj_dropdown=document.getElementById(ident);
var val=null;
if (typeof(faj_dropdown.options[faj_dropdown.selectedIndex]) !== 'undefined') {
 val=faj_dropdown.options[faj_dropdown.selectedIndex].value;
}

$.getJSON("ajaxdropdown.php",{column: field.replace(/__/g,"_"), _view: view},function(data) {
  var actid=document.activeElement.id;
  $('#'+ident+' options').remove();
  var options='<option value=""'+(val?'':' selected')+'>blank</option>';
  for (var i=0; i<data.length; i++) {
    options += '<option value="'+data[i].optVal+'"'+(data[i].optVal==val?' selected':'')+'>'+data[i].optDisp+'</option>';
  }
  $('#'+ident).html(options);
  $('#'+ident).val(val);
  // Hide our extra input
  var faj_box=document.getElementById('FAJ_replacement_'+ident);
  faj_box.style.display="none";
  var faj_dd=document.getElementById(ident);
  faj_dd.style.display="block";
  faj_dd.style.visibility='visible'; 

  if (actid=='FAJ_replacement_'+ident) {
    // Set original active
    // Meh, this doesn't cause it to drop down, which is a bit dull.
    faj_dd.focus();
  }
  document.body.style.cursor='auto';
});

} 

function FAJ_build(field) {
 var ident=field;
 view=$('div.tableResults').find('input#_view').first().val();
 //ident=ident.replace(/_/g,"__");
 //ident=ident.replace(/[^-_A-Za-z0-9:\.]/g,"_");
 // Hide existing drop-down
 var sel=document.getElementById(ident);
 if (!sel) {return;}
 var parent=sel.parentNode;
 //sel.style.display='none';
 sel.style.visibility='hidden'; 
 var newSel=sel.cloneNode(true);

 // Create new textbox
 var newText=document.createElement('input');
 newText.id='FAJ_replacement_'+ident
 newText.setAttribute('readonly','true');
 newText.setAttribute('class','pseudodd');
 if (typeof(sel.options[sel.selectedIndex])!=='undefined') {
  newText.value=sel.options[sel.selectedIndex].text;
 }
 newText.setAttribute('onfocus','FAJ_load("'+field+'","'+view+'");');
 newText.style.position='absolute';
 newText.style.top=0;
 newText.style.left=0;
// newText.style.height='100%';
 //newText.style.zIndex=100;
 // Create new div to contain both elements
 // ? should this be a span ?
 var newDiv=document.createElement('div');
 newDiv.style.position='relative';
 newDiv.appendChild(newSel);
 newDiv.appendChild(newText);
 // Replace with textbox
 parent.replaceChild(newDiv,sel);
}

