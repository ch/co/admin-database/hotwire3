$(document).ready(function(){
 $('textarea.hw_htmlview').each(function(i,v) {
  tinyMCE.execCommand('mceAddControl',false,$(this).attr('id'));
 });
});

tinyMCE.init({
        mode : "none",
        theme : "advanced",
        plugins : "safari",
	width : "90%",
        height : 70,
        valid_elements : "b/strong,i,sup,sub,div,ul,li,p,img,em",
        theme_advanced_buttons1 : "bold,italic,|,charmap,|,sub,sup,code",
        theme_advanced_buttons2 : "",
        theme_advanced_buttons3 : "",
        convert_fonts_to_spans : false,
extended_valid_elements : "a[class|name|href|target|title|onclick|rel],div[class],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],$elements"
});

