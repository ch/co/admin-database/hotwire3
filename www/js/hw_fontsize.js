/**
 * @file hw_fontsize.js
 * @brief Enables font-size changing 'menu' entries.
 * TODO - Need to move to icons when we do that.
 * Part of Hotwire 
 * @author Frank Lee
 */

function hw_font_increase() {
 // Get current font size
 size=1.1*($.cookie('hw_font_scale') || '1');
 $.cookie('hw_font_scale',size,20*365);
 console.log(size);
 $('body').css('text-shadow','0px 0px 10px #000000');
 $('body').css('font-size',Math.round(100.0*size)+'%');
 $('body').css('text-shadow','');
}

function hw_font_decrease() {
 // Get current font size
 size=0.9*($.cookie('hw_font_scale') || '1');
 $.cookie('hw_font_scale',size,20*365);
 console.log(size);
 $('body').css('text-shadow','0px 0px 10px #000000');
 $('body').css('font-size',Math.round(100.0*size)+'%');
 $('body').css('text-shadow','');
}

function hw_font_reset() {
 $.cookie('hw_font_scale',1);
 $('body').css('font-size','100%');
}

function hw_font_init() {
 // Provide font-changing menu options
 $('nav#primary_nav_wrap > ul > li:last-child ul').first().append(
   $('<li>').append(
     $('<a href="#">').attr('id','hw_font_increase').text('Increase font size')
   )
  ,$('<li>').append(
     $('<a href="#">').attr('id','hw_font_reset').text('Reset font size')
   )
  ,$('<li>').append(
     $('<a href="#">').attr('id','hw_font_decrease').text('Decrease font size')
   )
 );
 $('#hw_font_increase').click(hw_font_increase);
 $('#hw_font_reset').click(hw_font_reset);
 $('#hw_font_decrease').click(hw_font_decrease);
 if ($.cookie('hw_font_scale')) {
   $('body').css('font-size',Math.round(100.0*$.cookie('hw_font_scale'))+'%');
 }
}

$(document).ready(function(){
 hw_font_init();
});
