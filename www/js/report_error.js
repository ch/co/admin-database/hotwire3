// To report errors directly back
$(function() {
  // If we have any errors, we should load html2canvas for screenshots
  if (
    ($('div.tableTitle div.msg') && $('div.tableTitle div.msg').first().text().includes('ERROR'))
    ||
    ($('._debug_sqlerror'))
  ) {
    $.getScript('js/html2canvas.min.js', function() {
      window.can_screenshot = 1;
      console.log('Loaded html2canvas');
      html2canvas(document.body).then(function(canvas) {
        window.screenshot = canvas.toDataURL();
        console.log('Screenshot taken');
      });
    });
  }

  // Do we have anything configured to receive these reports?
  $.ajax({
    method: "POST",
    type: "POST",
    url: "error_ajax.php",
    data: {
      enabled: 1
    }
  }).done(function(d) {
    if (d.enabled) {
      console.log(d);
      // To detect an error from the database resulting from a failed insert/update
      $('span#_debug_report_database_error').remove();
      if ($('div.tableTitle div.msg').first().text().includes('ERROR')) {
        var msg = $('div.tableTitle div.msg').first().text();
        $('div.tableTitle').append(
          $('<span>').attr('id', '_debug_report_database_error').append(
            $('<span>').text('If you would like help with this error please '),
            $('<button>').append('tell '+d.recipient).click(function() {
              $("body").css("cursor", "progress");
              $('span#_debug_report_database_error').text('Reporting error to '+d.recipient);
              var summary = [];
              $('form#itemdata').find(':input').each(function(i, e) {
                summary.push({"name":$(e).attr('name'),"value": e.value});
              });
              $.ajax({
                method: "POST",
                type: "POST",
                url: "error_ajax.php",
                data: {
                  url: window.location.href,
                  user: $('span#_debug_dbuser').text(),
                  formdata: summary,
                  message: msg,
                  screenshot: (('screenshot' in window) ? window.screenshot : "None"),
                  type: "Error message from database"
                }
              }).done(function(d) {
                if (d.status == 'OK') {
                  $('span#_debug_report_database_error').text('Error reported to '+d.recipient);
                } else {
                  $('span#_debug_report_database_error').text('Failed to report error to '+d.recipient);
                }
              }).fail(function(a, b, c) {
                $('span#_debug_report_database_error').text('Unable to report error to '+d.recipient +': ' + a.statusText);
              });
            })
          )
        );
      }

      // To detect an sqlError 
      $('div#_debug_report_database_error').remove();
      if ($('._debug_sqlerror')) {
        $('._debug_sqlerror').append(
          $('<div>').attr('id', '_debug_report_database_error').append(
            $('<span>').text('If you would like help with this error please '),
            $('<button>').append('Report this error').click(function() {
              console.log('Button pressed');
              $('div#_debug_report_database_error').text('Reporting error to helpdesk...');
              var summary = ['Form status:'];
              $('form#itemdata').find(':input').each(function(i, e) {
                summary.push('Input named "' + $(e).attr('name') + '" has value "' + e.value + '"');
              });
              $.ajax({
                method: "POST",
                type: "POST",
                url: "error_ajax.php",
                dateType: 'json',
                data: {
                  url: window.location.href,
                  user: $('span#_debug_dbuser').text(),
                  formdata: summary.join('\n'),
                  message: msg,
                  screenshot: (('screenshot' in window) ? window.screenshot : "None"),
                  send: 1
                }
              }).done(function(d) {
                if (d.status == 'OK') {
                  $('div#_debug_report_database_error').text('Error reported to '+d.recipient);
                } else {
                  $('div#_debug_report_database_error').text('Failed to report error to '+d.recipient);
                }
              }).fail(function(a, b, c) {
                $('div#_debug_report_database_error').text('Unable to report error to '+d.recipient +': ' + a.statusText);
              });
            })
          )
        );
      }
    }
  });
});
