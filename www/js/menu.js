/**
 * @file menu.js
 * @brief Produces 'delay' on CSS-based menu.
 * Without this class, the CSS-based menu disappears immediately
 * the pointer exits a parent. Unhelpful unless your mouse-
 * control is perfect, so this javascript introduces delays.
 * Part of Hotwire http://hotwire.sourceforge.net/ .
 * @author Frank Lee
 */

function flipMenu(td) {
/*  var d = document.getElementById(menuName + 'Menu');*/
  var ds = td.getElementsByTagName('div');

  for (i = 0; i < ds.length; i++) {
    if (ds[i].className.search('Menu') > 0) {
      if (ds[i].className.search('Hover') > 0) {
        $(ds[i]).fadeTo('fast', 0, 
                         function () { 
                           ds[i].className = ds[i].className.slice(
                             0, ds[i].className.indexOf('MenuHover')) 
                               + 'Menu';alert('ok'); });
      } else {
        ds[i].className = ds[i].className.slice(
                        0, ds[i].className.indexOf('Menu')) 
                          + 'MenuHover';
        ds[i].style.opacity = 0;
        $(ds[i]).fadeTo('fast', 0.9);
      }
    }
  }
}

function init() {
  var changes = false;
}

function checkChanges() {
  if (changes==true) {
    alert('Changed!');
  }
}

function getInput(td) {
  var tdsubid = td.id.replace('Item', 'Sub');
  var tdsub = document.getElementById(tdsubid);
  tdsub.style.display = 'inline';
}

function span_hl_on(sp) {
  /*sp.className = 'labelHover';*/
}

function span_hl_off(sp) {
  /*sp.className = 'label';*/
}

function setNavHeader(dTitle, dValue, dFormatValue) {
  var d = document.getElementById(dTitle + 'Header');
  d.innerHTML = dFormatValue;
}

function setNavSearch(eName) {
  var e = document.getElementById('navSearch');
  e.name = eName;
}

function setTitle() {
  var d = document.getElementById('pageTitle');
  if (d != null) {
    document.title = d.innerHTML + ' :: ' + document.title;
  }
}

function expand(tdid) {

  td = document.getElementById(tdid);

  var ds = td.getElementsByTagName('div');

  for (i = 0; i < ds.length; i++) {
    var d = ds[i];
    if (d.className == 'menuExpander' && d.parentNode == td) {

      d.className = d.className.replace('menuExpander', 'menuExpanderHover');

      /*d.style.opacity = 0;
        $(d).fadeTo('fast', 0.9);
        how to get around the mouseout on cell transition?*/
    }
  }
}

function collapse(tdid) {

  td = document.getElementById(tdid);

  var ds = td.getElementsByTagName('div');

  /*  $(d).fadeTo('fast', 0, function () {
                           d.className = d.className.slice(
                                         0, d.className.indexOf('MenuHover'))
                                         + 'Menu';
                         }
  ); */

  for (i = 0; i < ds.length; i++) {
    var d = ds[i];
    if (d.className == 'menuExpanderHover' && d.parentNode == td) {

      d.className = d.className.replace('menuExpanderHover', 'menuExpander');

    }
  }
}

function expandMulti(b) {
  var n = b.name;
  n = n.replace('btnAdd', 'div') + '_avail';

  var d = document.getElementById(n);
  d.className = 'multiVisible';

  n = n.replace('div', '');
  var s = document.getElementById(n);
  s.focus();

  d.style.opacity = 0;

  $(d).fadeTo('fast', 1);

}

function collapseMulti(s) {
  var n = 'div' + s.name;

  var d = document.getElementById(n);
  $(d).fadeTo('fast', 0, function () { this.className='multiHidden'; });

}


function check_submit(fname, btn) {
  //var s = document.getElementsByTagName('select');
  // Need to test whether '_'.s.'_avail' exists
  // for that's the old way of doing MM fields.
  //if (document.getElementsByTagName('_'+s+'_avail')) {
    //for (var i = 0; i < s.length; i++) {
      //if (s[i].multiple && s[i].name.substr(0,1) != '_') {
        //var l = s[i];
        //for (var j = 0; j < l.length; j++) {
          //l.options[j].selected = true;
        //}
      //}
    //}
  //}
  document.forms[fname]._action.value = btn.value;
  changes = false;
  document.forms[fname].submit();
}

