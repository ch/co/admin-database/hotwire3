/* To set the xdebug cookie */
$(function() {
 $('span.enable-debug').click(function() {
  $.cookie('XDEBUG_PROFILE',1);
  $('span.enable-debug').hide();
  $('span.disable-debug').css('display','inline-block').show();
  $('span.debug-info').remove();
  $('div#hw_sortandsearch').append(
   $('<span>').addClass('debug-info').text('Debugging now enabled ').append(
    $('<a>').attr('href','/webgrind').text('Webgrind').attr('target','_blank')
   )
  );
 });
 $('span.disable-debug').click(function() {
  $.cookie('XDEBUG_PROFILE',null);
  $('span.disable-debug').hide();
  $('span.enable-debug').show();
  $('span.debug-info').remove();
  $('div#hw_sortandsearch').append(
   $('<span>').addClass('debug-info').text('Debugging now disabled ').append(
    $('<a>').attr('href','/webgrind').text('Webgrind').attr('target','_blank')
   )
  );
 });
 /* Are we already debugging? */
 if ($.cookie('XDEBUG_PROFILE')) {
  $('span.enable-debug').hide();
  $('span.disable-debug').css('display','inline-block').show();
 }
});

