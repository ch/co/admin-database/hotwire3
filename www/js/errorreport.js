/**
 * @file hw_errorreport.js
 * @brief Enables error reporting via menus.
 * TODO - Need to move to icons when we do that.
 * Part of Hotwire 
 * @author Frank Lee
 */



function hw_send_help() {
  // Hide the menu option
  $(this).parent().parent().hide();
  // Expand any hidden SQL blocks by setting the appropriate CSS class on body
  $('body').addClass('errorreporting');
  if (typeof(html2canvas)=='function') {
    html2canvas(document.body).then(function(canvas){
      window.screenshot=canvas.toDataURL();
      var summary=[];
      $('form#itemdata').find(':input').each(function(i,e) {
        summary.push({'name':$(e).attr('name'),'value':e.value});
      });
      msg=prompt('Please describe the problem','');
      if (msg!==null) {
      $.ajax({
        method: "POST",
        type: "POST",
        url: "error_ajax.php",
        data: { 
          url: window.location.href,
          user: $('span#_debug_dbuser').text(),
          comment: msg,
          formdata: summary,
          message: $('div.tableTitle div.msg').first().text(),
          screenshot: (('screenshot' in window)? window.screenshot: 'None'),
          type: 'Error reported',
          send: '1',
          rusage: JSON.parse($('div.runtime span.rusage').text()||'{}'),
          request: JSON.parse($('div.runtime span.request').text()||'{}')
        }
      }).done(function(d){
        if(d.status == 'OK') {
          $('span#_debug_report_error').text('Error reported to '+d.recipient);
        } else {
          $('span#_debug_report_error').text('Failed to report error to '+d.recipient);
        }
        $('body').removeClass('errorreporting');
        // Re-enable the menu option
        $(this).parent().parent().show();
      }).fail(function() {
        $('span#_debug_report_error').text('Unable to report error!');
        $('body').removeClass('errorreporting');
        // Re-enable the menu option
        $(this).parent().parent().show();
      });
      }
    });
  } else {
   alert('Unable to request help');
  }
}


function hw_help_init() {
 $.ajax({
   url: "error_ajax.php",
   method: 'POST',
   data: {enabled: true},
   success:	 function(data){
   if (('enabled' in data) && data.enabled) {
     $('div.tableTitle').append(
       $('<span>').attr('id','_debug_report_error')
     );
     $.getScript('js/html2canvas.min.js',function(){
       $('nav#primary_nav_wrap > ul > li:last-child ul').first().append(
         $('<li>').append(
          $('<a href="#">').attr('id','hw_send_help_request').text('Report problem to ').append(
            $('<span>').attr('id','hw_help_email').text(data.recipient)
          )
         )
       );
       // Include the external script
       $('#hw_send_help_request').click(hw_send_help);
     });
     tstamp('Set up error reporting');
   } 
  }
 });
};

function tstamp(msg) {
 var n=Date.now();
 console.log(n+': ['+(n-window.hw_js_start)+'] '+msg);
}

$(document).ready(function(){
 hw_help_init();
});
