// To tweak the multiselect UI into a more 'hotwire-like' form.
// As a stepping stone to using the 'tag' UI which is a bit more 21st C

$('select[multiple]').multiSelect({
  selectableHeader: "<span>Available</span><input type='text' class='search-input' autocomplete='off' placeholder='type to search, enter to select'>",
  selectionHeader: "<span>Selected</span>",
  afterInit: function(ms){
    var that = this,
        $selectableSearch = that.$selectableUl.prev(),
        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)';

    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
    .on('keydown', function(e){
      if (e.which === 40){
        that.$selectableUl.focus();
        return false;
      }
      if (e.which == 13) {
        e.preventDefault();
        $(that.$selectableUl).find('li:visible').click();
        return false;
      }
    });
  },
  afterSelect: function(){
    this.qs1.cache();
  },
  afterDeselect: function(){
    this.qs1.cache();
  }
});
