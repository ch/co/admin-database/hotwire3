/*
 Stuff to help with Hotwire stuff
*/

/* An object to reimplement some common hotwire methods */
hotwireobj = {
 init: function() {
 },
  /* Returns a DOM-friendly representation of a string.
   * Must start [A-z] then have [-A-z0-9_:\.]* */
 domName: function(text) {
  // Ensure we start with A-z
  text=text.replace(/^([^A-Za-z])/, 'Z\1');
  // Escape underscores with underscores
  text=text.replace(/_/g,'__');
  // Replace non [-A-z0-9_:\.] characters with underscores
  text=text.replace(/[^-_A-Za-z0-9:\.]/g,'_');
  return text;
 },

 hdrFormat: function(text) {
  if (text.match(/(.*)_(id|hid|subview|view|report|ro|ssi|html)$/)) {
   text=text.replace(/_(id|hid|subview|view|report|ro|ssi|html)$/,'');
  }
  return text;
 },
 // Fields as a list of <option>s for a select
 fieldlistforselect: function(mycol) {
  var optionlistarr=[];
  if (mycol.substr(0,4)=='max_' || mycol.substr(0,4)=='min_') {
    mycol=mycol.substr(4);
  }
  $(document).find('tr.tableHeader input.colname').each(function(index,element) {
   if (mycol==element.value) {
    optionlistarr.push("<option selected value='"+element.value+"'>"+($(element).parent().find('span.columnname').text())+"</option>");
   } else {
    optionlistarr.push("<option value='"+element.value+"'>"+($(element).parent().find('span.columnname').text())+"</option>");
   }
  });
  return optionlistarr; 
 },
 // To format a name nicely
 fieldname: function(mycol) {
   var table=$('table.hw_resulttable');
   var searchval=table.find('input.colname[value="'+mycol+'"]');
   //var searchval=table.find('input.colname');
   return searchval.closest('a').find('span.columnname').text().trim();
  //return form.find('tr.tableHeader th#'+hotwireobj.domName(mycol)+' span.columnname').text().trim();
 },
 // To format a direction nicely
 sortname: function(short) {
  var ops=[];
  ops["Desc"]='Descending';  
  ops["Asc"]='Ascending';
  return ops[short];
 },

}
 

/* An object to deal with the search items */
searchobj = {
 init: function() {
  /* Hide the traditional search interface - more room for menu structure */
  $('ul#hw_quicksearch_form_hide').hide();
  /* Bind to "ul#hw_search_id li": Handle clicks on search clauses */ 
  $('ul#hw_search_id').on('click','li.hw_clause',function(event){
   // if ( event.target != this) return;
   var id=this.parentNode.id;  // Identify the clause
   var mycol='';               // Column
   var myval='';               // Value
   var myop='';                // Operator

   // Read column and value from the hidden input field
   var li=$(this).closest('li');
//console.log('INPUT');
//console.log(li.find('input.hw_search_val').size());
   myval=li.find('input.hw_search_val').val();
   mycol=li.find('input.hw_search_val').attr('name');
   myop=li.find('span.clause_operator').text();
   var form=$('div#hw_sortandsearch').find('form[name="hw_sortandsearchforjs"]');
  // Build a dialog box
   var $dialog = $('<div id="hw_searchAdjPopup_'+mycol+'">')
		 .append(
                  $('<div>')
                  .addClass('hw_searchAdjPopup')
                  .append(searchobj.jq_searchfielddropdown(mycol,''))
                  .append(searchobj.jq_searchoperatordropdown(myop,''))
                  .append(searchobj.jq_searchinputbox(myval,''))
                  .append(searchobj.jq_searchbutton('search','Search now',
                   function(event) {
                    if ($(this).parent().find('select.field').val()!=mycol) {
                      columnobj.delsearch(form,mycol,$(this).parent().find('input').val());
                    }
                    columnobj.dosearch(form,
                     $(this).parent().find('select.field').val(),
                     $(this).parent().find('input.value').val(),
                     $(this).parent().find('select.operator').val());
                   },'gosearch')
                  )
                  .append(searchobj.jq_searchbutton('plusthick','Add search term',
                   function(event) {          
                    columnobj.altersearch(form,
                     $(this).parent().find('select.field').val(),
                     $(this).parent().find('input.value').val(),
                     $(this).parent().find('select.operator').val()
                    );
                    $(this).closest('div.ui-dialog-content').dialog('close');
                   })
                  )
                  .append(searchobj.jq_searchbutton('minusthick','Remove search term',
                   function(event) {       
                    columnobj.delsearch(form,mycol,$(this).parent().find('input').val());
                    $(this).closest('div.ui-dialog-content').dialog('close');
                   })
                  )
                 )
                 .dialog({
                        width: 500,
                        modal: true,
			autoOpen: false,
                        show: "slide",
			hide: 'slide',
			title: "Alter search parameter",
			open: function(e,u) { $('.value').focus().select();},
                        }
		 );
   $dialog.dialog('open');

  });
 },

 // Building up the UI for things
 jq_searchfielddropdown: function(mycol,id) {
  return $('<select>')
         .addClass('hw_searchAdjPopup')
         .addClass('field')
         .append(hotwireobj.fieldlistforselect(mycol).join(''));
 },
 jq_sortoperatordropdown: function(myop,id) {
  return $('<select>')
         .addClass('hw_sortAdjPopup')
         .addClass('direction')
         .append(searchobj.jq_sortoperatorlist(myop).join(''));
 },
 jq_sortoperatorlist: function(myop,id) {
  var ops=[];
  ops["Desc"]='Descending';  
  ops["Asc"]='Ascending';
  var optionlistarr=[];
  for (var dir in ops) {
   optionlistarr.push("<option value='"+dir+"' "+(dir==myop?'selected':'')+">"+ops[dir]+"</option>");
  }
  return optionlistarr;
 },
 // TODO: 'selected' should be set on one of these
 jq_searchoperatordropdown: function(myop,id) {
   if (myop=='like' || myop=='not like' || myop=='=' || myop=='!=') {
   operatoropt='<option value="=" '+(myop=='='?'selected':'')+'>Equals</option>'+
               '<option value="like" '+(myop=='like'?'selected':'')+'>Matches</option>'+
               '<option value="!=" '+(myop=='!='?'selected':'')+'>Does not equal</option>'+
               '<option value="not like" '+(myop=='not like'?'selected':'')+'>Does not match</option>';
   } else if (myop=='before' || myop=='after') {
     if (myop=='before') {
     operatoropt='<option value="before" selected="selected">Before</option>'+
                 '<option value="after">After</option>';
     } else {
     operatoropt='<option value="before">Before</option>'+
                 '<option value="after" selected="selected">After</option>';
     }
   } else {
   operatoropt='<option value="'+myop+'">'+myop+'</option>';
   }
   return $('<select>')
            .addClass('hw_searchAdjPopup')
            .addClass('operator')
            .append(operatoropt);
 },
 jq_searchinputbox: function(myval,id) {
  return $('<input>')
         .attr('type','text')
         .addClass('hw_searchAdjPopup')
         .addClass('value')
         .attr('size','10')
         .attr('value',myval)
         .keydown(function(e){
                      if(e.which == 13) {
                       $(this).closest('div.ui-dialog-content').find('button#gosearch').click();
                      }});
 },
 jq_searchbutton: function(icon,text,fn,id) {
  var bu=$('<button>')
         .addClass('ui-button')
         .addClass('ui-state-default')
         .addClass('ui-corner-all')
         .addClass('ui-button-icon-only')
         .attr('title',text)
         .attr('aria-disabled','false')
         .attr('role','button')
         .append(
          $('<span>')
          .addClass('ui-icon')
          .addClass('ui-icon-'+icon)
         )
         .append(
          $('<span>')
          .addClass('ui-button-text')
          .html(text)
         )
         .on('click',fn);
  if (id) {
   bu.attr('id',id);
  }
  return bu;
 },

 /* A method to remove the search item */
 destroySearch: function(targetId) {
  // Is this the last of the line? If so, remove the parent.  
  var sibs=$("#"+targetId).siblings().length;
  var parent=$("#"+targetId).closest("div");
  $('#'+targetId).remove();
  if (sibs==0) {
  // alert('Will destroy '+parent);
   parent.remove();
  }
 },

 /* A method to close the given dialogue box */
 closeDialogue: function(dId) {
  // Is this the last of the line? If so, remove the parent.
  $("#"+dId).dialog("destroy"); 
 }, 
}

/* sortobj deals with the sorting */
sortobj = {
 init: function() {
  /* Hide the static HTML sorting information */
  $("caption.tableHeader span.sort").hide();

  /* Bind to "ul#hw_sort_id li " */ 
  $('ul#hw_sort_id').on('click','li.hw_sort',function(event){
   // Read column and value from the hidden input field
   mycol=$(this).closest('li').find('input.sort').val();
   myval=$(this).closest('li').find('input.order').val();
   myid=$(this).closest('li').attr('id').split(/_/)[2];
   var form=$('div#hw_sortandsearch').find('form[name="hw_sortandsearchforjs"]');

   // Put together a dialog box: Change direction, apply now, apply later, remove sort
   var $dialog = $('<div id="hw_sortAdjPopup_'+mycol+'">')
		 .append(
                  $('<div>')
                  .addClass('hw_sortAdjPopup')
                  .append(searchobj.jq_searchfielddropdown(mycol,''))
                  .append(searchobj.jq_sortoperatordropdown(myval,''))
                  .append(searchobj.jq_searchbutton('circle-check','Sort now',
                   function(event) {
                    columnobj.dosort(myid,form,$(this).parent().find('select.field').val(),
                                          $(this).parent().find('select.direction').val());
                    $(this).closest('div.ui-dialog-content').dialog('close');
                   }))
                  .append(searchobj.jq_searchbutton('circle-plus','Sort later',
                   function(event) {          
                    columnobj.altersort(myid,form,$(this).parent().find('select.field').val(),
                                          $(this).parent().find('select.direction').val());
                    $(this).closest('div.ui-dialog-content').dialog('close');
                   }))

                  .append(searchobj.jq_searchbutton('circle-minus','Remove sort',
                   function(event) {       
                    columnobj.delsort(myid,form,$(this).parent().find('select.field').val(),
                                          $(this).parent().find('select.direction').val());
                    $(this).closest('div.ui-dialog-content').dialog('close');
                   })
                  )
                 )
                 .dialog({
                        width: 500,
                        modal: true,
			autoOpen: false,
                        show: "",
			title: "Alter sort parameter "+myid,
                        }
		 );
   $dialog.dialog('open');

  });
 },
 dummt: function() {
   var mycol=this.innerHTML;
   var id=this.parentNode.id;
   var myval='';

   /* Find the direction in which we sort */
   $(this).closest('li').find('span.hw_sort_order').first().each(function(index,element){myval=element.text;});

  alert('Myval2 is '+myval);
  /* To find a complete list of fields */
  var fields=[];
  $('table#hw_resulttable tr#tableHeader input#colname').each(function(index,element) {
   fields.push(element.value)
  });
  alert('Fields '.fields.join(', '));
  
   /* Produce a pop-up */
   text="<div class='hw_sortAdjPopup'><select class='hw_sortAdjPopup field'><option value='hw_preference_hid'>HW Preference</option><option value='preference_name' selected>Preference Name</option></select> <select class='hw_sortAdjPopup operator'><option value='DESC'>Descending</option><option value='ASC'>ascending</option></select> <button class='ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only' role='button' aria-disabled='false' title='Button with icon only'><span class='ui-button-icon-primary ui-icon ui-icon-circle-check'></span><span class='ui-button-text'>Button with icon only</span></button> <button class='ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only' role='button' aria-disabled='false' title='Button with icon only'><span class='ui-button-icon-primary ui-icon ui-icon-circle-close'></span><span class='ui-button-text'>Button with icon only</span></button>  <button class='ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only' role='button' aria-disabled='false' title='Button with icon only'><span class='ui-button-icon-primary ui-icon ui-icon-trash'></span><span class='ui-button-text'>Button with icon only</span></button></div>";
   var fields=$('#hw_qs_form').clone();
   var $dialog = $('<div id="adjustSortParams_'+id+'"></div>')
		.html(text)
		.dialog({
                        width: 500,
                        modal: true,
			autoOpen: false,
			title: 'Sort item',
		});
   $dialog.dialog('open');
  
 },

}

/* An object to deal with hotwire records */
recordobj = {
 init: function() {
  /* Bind to all div.recordkey objects */
  $('body').on('click','div.clearwidth:not(._hwsubviewb):not(._hwsubviewc) div.recordkey',function(){
console.log('Clicked');
   /* We have a field. Offer to search the view for its contents, default is current */
   d=$(this).parent().find('div.recordvalue').children().last();
   fld='';

   while (!fld && d.children().length>0) {
    d=d.children().first();
    fld=d.attr('name');
   }
   if (!fld && d.children().length==0) {
    fld=d.attr('name');
   }
   fld=fld.replace(/\[\]$/,'');
   if (fld.match(/_id$/)) {
    fld=fld.replace(/_id$/,"_hid");
    //fld=fld.replace(/^ro_/,"");
   }
   if (d.is('select')) {
    val=d.find('option[selected]').text();
   } else {
    val=d.val();
   }
   var $dialog = $('<div id="hw_searchField_'+fld+'">')
		 .append(
                  $('<div>')
                  .addClass('hw_searchField')
                  .append(
                   $('<ul>')
                   .append(
                    $('<li>')
                    .append(
                     $('<input title="Search this field" class="newsearch" type="test"/>')
                     .val(val)
		     .attr('name',fld)
                    )
                    .append(searchobj.jq_searchbutton('search','Search now',
                     function(event) {
                      recordobj.dosearch(fld,$(this).parent().find('input').first().val());
                     })
                    )
                   )
                  )
                 )
                
                .dialog({
                        width: 500,
                        modal: true,
			autoOpen: false,
                        show: "",
			title: "Find "+$(this).text()+" in other records",
		});
   $dialog.dialog('open');;
   return false;
  });
  
 },

 dosearch: function(field, value) {
  url=$("div#hw_pagetitle a").attr('href');
  if (url.search(/\?/)!=-1) {
   url=url+'&';
  } else {
   url=url+'?';   
  }
  url=url+"_action=search&"+field+'='+encodeURI(value);
  //console.log(url);
  $("*").css("cursor","progress");
  window.location.href = url;
 },

}

/* An object to deal with the columns */
columnobj = {
 /* Hotwire's data table */
 hwt:null,
 /* To replay relevant [hidecols, sizecols*, swapcols*] cookies on a table */
 replaycookies: function() {
 //  alert('Replaying all table cookies on '+this.hwt.attr('id'));
  if (this.hwt.hasClass('hideable' )) { this.replayHideCookie();}
  if (this.hwt.hasClass('draggable')) { this.replayDragCookie();}
  //if (this.hwt.hasClass('resizable')) { this.replaySizeCookie();}
 },

 replayHideCookie: function(hwt) {
   c=$.cookie(this.hwt.attr('id')+'-hide');
   if (c) {
     h=c.split(/\+/);
     for (var col in h) {
       this.rehidecol(h[col]);
     }
   }
 },

 rehidecol: function(colid) {
  idx=this.hwt.find('th.'+colid).index()+1;
  this.hwt.find('td:nth-child('+idx+'),th:nth-child('+idx+')').hide();
 },

 showallcols: function(form) {
  /* Un-hide any hidden columns */
  var tbl=form.closest('div.hw_resulttable').find('table.hw_resulttable');
  $.cookie(this.hwt.attr('id')+'-hide',null);
  this.hwt.find('th:hidden,td:hidden').show();
 },

 hidecol: function(form,fld) {
  // Hmm, this ought to be client-side in a cookie, really //
  /* Find in that form a div for hiding fields,  */
  /* Actually do the hide */
  //var form=$('div#hw_sortandsearch').find('form[name="hw_sortandsearchforjs"]');
  var tbl=form.closest('div.hw_resulttable').find('table.hw_resulttable');
  var idx=tbl.find('th.'+hotwireobj.domName(fld)).index()+1;
  tbl.find('td:nth-child('+idx+'),th:nth-child('+idx+')').hide();
  c=$.cookie(this.hwt.attr('id')+'-hide');
  if (c) {
   y=jQuery.grep(c.split(/\+/),function(n,i){n != hotwireobj.domName(fld);});
   y.push(hotwireobj.domName(fld));
   h=y.join('+');

  } else {
   h=hotwireobj.domName(fld);
  }
  $.cookie(this.hwt.attr('id')+'-hide',h,{expires: 365});
 },

 replayDragCookie: function(hwt) {

 },

 replaySizeCookie: function(hwt) {
   c=$.cookie(this.hwt.attr('id')+'-width');
   if (c) {
     widths=c.split(/\+/);
     for (var element in widths) {
       items=widths[element].split(/:/);
       $('th#'+items[0]).width(items[1]);
//       document.getElementById(items[0]).style.width=items[1]+'px'
     }
   }
 },

 init: function() {
  /* Hide columns */
  this.hwt=$('table.hw_resulttable');
  columnobj.replaycookies();
 
  /* Display search clause if we have some */

  /* Bind to "div.hw_header" and set up a dialog box when clicked*/ 
  $('body').on('click','div.hw_header',function(event){
   var form=$('div#hw_sortandsearch').find('form[name="hw_sortandsearchforjs"]');
   var mycol=$(this).find('input.colname').val();
   var searchval=form.find('input[name="'+mycol+'"]').val();
   var title=$(this).text();
   var sortclause=form.find('input[value="'+mycol+'"]').closest('li');
   var order=sortclause.find('input.order').val();
   var $dialog = $('<div id="hw_searchAdjPopup_'+mycol+'">')
		 .append(
                  $('<div>')
                  .addClass('hw_searchAdjPopup')
                  .append(
                   $('<ul>')
                   .append(columnobj.clickableli(
                    'Sort ascending',
                    function(event) {
                     columnobj.addoraltersort(form,mycol,'Asc');
                     form.submit();
                     $(this).closest('div.ui-dialog-content').dialog('close');
                     }).addClass((order=='Asc'?'disabled':''))
                   )
                   .append(columnobj.clickableli(
                    'Sort descending',
                    function(event) {
                     columnobj.addoraltersort(form,mycol,'Desc');
                     form.submit();
                     $(this).closest('div.ui-dialog-content').dialog('close');
                    }).addClass((order=='Desc'?'disabled':''))
                   )
                   .append(columnobj.clickableli(
                    'Remove sorting',
                    function(event) {
                     columnobj.removeSort(form,mycol);
                     form.submit();
                     $(this).closest('div.ui-dialog-content').dialog('close')
                    }).addClass((order==undefined?'disabled':''))
                   )
                   .append(columnobj.clickableli(
                    'Hide this column',
                    function(event) { 
                     columnobj.hidecol(form,mycol);
                     $(this).closest('div.ui-dialog-content').dialog('close');
                    })
                   )
                   .append(columnobj.clickableli(
                    'Show all columns',
                    function(event) {
                     columnobj.showallcols(form);
                     $(this).closest('div.ui-dialog-content').dialog('close')
                    })
                   )
                   .append(
                    $('<li>')
                    .append(
                     $('<input title="Search this field" class="newsearch" type="test"/>')
                     .val(searchval)
                     .focus(function(){if (this.value == searchval) { this.select();}})
                     .keydown(function(e){
                      if(e.which == 13) {
                       $(this).closest('div.ui-dialog-content').find('button#gosearch').click();
                      }})
                    )
                    .append(searchobj.jq_searchbutton('search','Search now',
                     function(event) {
                      columnobj.dosearch(form,mycol,$(this).parent().find('input').val());
                     },'gosearch')
                    )
                    .append(searchobj.jq_searchbutton('plusthick','Add search term',
                     function(event) {
//console.log('Operator is '+$(this).parent().find('select.operator').val());
                      columnobj.addoraltersearch(form,mycol,$(this).parent().find('input').val(),$(this).parent().find('select.operator').val());
                      $(this).closest('div.ui-dialog-content').dialog('close');
                     })
                    )
                    .append(searchobj.jq_searchbutton('minusthick','Remove search term',
                     function(event) {       
                      columnobj.delsearch(form,mycol,$(this).parent().find('input').val());
                      $(this).closest('div.ui-dialog-content').dialog('close');
                     })
                    )
                   )
                  )
                 )
                
                .dialog({
                        width: 500,
                        modal: true,
			autoOpen: false,
                        show: "",
			title: $(this).text(),
		});
   $dialog.dialog('open');;
   return false;
  });
 },

 /* To give a clickable li */
 clickableli: function(text,fn) {
  return $('<li>').addClass('clickable').append($('<span>').text(text)).on('click',fn);
 },
 
 dosort: function(id,form,fld,val) {
  columnobj.altersort(id,form,fld,val);
  form.submit();
 },
 
 delsort: function(id,form,fld) {
  form.find('li#hw_sort_'+id).remove();
  $('div#hw_sortandsearch').removeClass('unchanged');  
  $('div#hw_sortandsearch').addClass('changed');
  columnobj.displaysort(form);
 },

 dosearch: function(form,fld,val,op) {
  columnobj.addoraltersearch(form,fld,val,op);
  $("*").css("cursor","wait");
  form.submit();
 },

 delsearch: function(form,fld) {
  $('div#hw_sortandsearch').find('div.searchfields').find('input[name="'+fld+'"]').closest('li').remove();
  $('div#hw_sortandsearch').removeClass('unchanged');  
  $('div#hw_sortandsearch').addClass('changed');
  columnobj.displaysearch(form);
 },

 addoraltersearch: function(form,fld,val,op) {
  if($('div#hw_sortandsearch').find('div.searchfields').find('input[name="'+fld+'"]').length==0) {
   columnobj.addsearch(form,fld,val,op);
  } else {
   columnobj.altersearch(form,fld,val,op);
  }
 },

 displaysearch: function(form) {
  if (0==form.find('ul#hw_search_id').children('li').length) {
   form.find('div#hw_search').addClass('invisible');
   form.find('input[name="_action"]').remove();
  } else {
   form.find('div#hw_search').removeClass('invisible');   
   form.append($('<input type="text" value="search" name="_action">'));
  }
 },
 
 displaysort: function(form) {
  if (0==form.find('ul#hw_sort_id').children('li').length) {
   form.find('div#hw_sort').addClass('invisible');
  } else {
   form.find('div#hw_sort').removeClass('invisible');
  }
 },

 altersort: function(id,form,fld,val) {
  // Find the containing li
  li=form.find('li#hw_sort_'+id);
  li.find('input.sort').val(fld);
  li.find('input.sort').attr('value',fld);
  li.find('input.order').val(val);
  li.find('span.hw_sort_field').text(hotwireobj.fieldname(fld));
  li.find('span.hw_sort_order').text(hotwireobj.sortname(val));
  $('div#hw_sortandsearch').removeClass('unchanged');  
  $('div#hw_sortandsearch').addClass('changed');
  columnobj.displaysort(form);
 },

 addsort: function(form,fld,val) {
  /* Remove any previous search on this field */
  form.find('div.sortedfields').find('input[value="'+fld+'"]').closest('li').remove();
  /* Add a new set of sort data */
  id=1+form.find('ul#hw_sort_id').children('li').length;
  form.find('ul#hw_sort_id').append(
   $('<li>').attr('id','hw_sort_'+id).addClass('hw_sort')
 .append($('<input>').attr('type','text').attr('name','_sort[]').attr('value',fld).addClass('sort'))
   .append($('<input>').attr('type','text').attr('name','_order[]').attr('value',val).addClass('order'))
   .append($('<span>').addClass('hw_sort_field').text(hotwireobj.fieldname(fld)))
   .append($('<span>').addClass('hw_sort_order').text(hotwireobj.sortname(val)))
  );
  $('div#hw_sortandsearch').removeClass('unchanged');  
  $('div#hw_sortandsearch').addClass('changed');
  columnobj.displaysort(form);
 },

 altersearch: function(form,fld,val,op) {
//console.log('Saving search');
  ofld=fld;
  li=form.find('div.searchfields').find('input[name="'+ofld+'"]').closest('li');
  if (fld.substr(0,4)=='max_' || fld.substr(0,4)=='min_') {
    fld=fld.substr(4);
  }
  sfld=fld;
  if (op=='before')  {fld='max_'+fld;}
  if (op=='after')  {fld='min_'+fld;}
  if (op==undefined) {op='like';}
  li.find('input.hw_search_xcl').remove();
  if (op=='not like') {
    var li=form.find('div.searchfields').find('input[name="'+ofld+'"]').closest('li');
    form.find('div.searchfields').find('input[name="'+ofld+'"]').closest('li').append(
     $('<input>').addClass('hw_search_xcl').attr('checked','checked').attr('type','checkbox').attr('name','xcl_'+ofld).attr('value','on')
    );
  }
  if (op=='=' || op=='!=') {
    li.find('input.hw_search_xct').attr('value','on');
  } else {
    li.find('input.hw_search_xct').attr('value','off');
  }
  // Do not remove ro_ from field names
  // if (fld.substr(0,3)=='ro_') { fld=fld.substr(3);}
  // Find the containing li
  li.find('input.hw_search_val').val(val);
  li.find('input.hw_search_val').attr('value',val);
  li.find('input.hw_search_val').attr('name',fld);
  li.find('span.clause_argument').text(hotwireobj.fieldname(sfld));

  li.find('span.clause_operator').text(op);
  li.find('span.clause_value').text(val);
  $('div#hw_sortandsearch').removeClass('unchanged');  
  $('div#hw_sortandsearch').addClass('changed');
  columnobj.displaysearch(form);
 },


 /* To add a search term */
 addsearch: function(form,fld,val,op) {
  if (op=='before')  {fld='max_'+fld;}
  if (op=='after')  {fld='min_'+fld;}
  if (op==undefined) {op='like';}
  /*if (fld.substr(0,3)=='ro_') { fld=fld.substr(3);}*/
  /* Remove any previous search on this field */
  form.find('div.searchfields').find('input[name="'+fld+'"]').closest('li').remove();
  /* Add a new set of search data */
  id=1+form.find('ul#hw_search_id').children('li').length;
  form.find('ul#hw_search_id').append(
   $('<li>').attr('id','hw_search_'+id).addClass('hw_clause')
   .append($('<input>').attr('type','text').attr('name',fld).attr('value',val).val(val))
   .append($('<span>').addClass('clause_argument').text(hotwireobj.fieldname(fld)))
   .append($('<span>').addClass('clause_operator').text(op))
   .append($('<span>').addClass('clause_value').text(val))
  );
  $('div#hw_sortandsearch').removeClass('unchanged');  
  $('div#hw_sortandsearch').addClass('changed');
  columnobj.displaysearch(form);
 }, 

 addoraltersort: function(form,fld,dir) {
  if(form.find('div.sortedfields').find('input[value="'+fld+'"]').length==0) {
   columnobj.addsort(form,fld,dir);
  } else {
   id=form.find('div.sortedfields').find('input[value="'+fld+'"]').closest('li').attr('id').split(/_/)[2];
   columnobj.altersort(id,form,fld,dir);
  }
 },

 removeSort: function(form,fld) {
  id=form.find('div.sortedfields').find('input[value="'+fld+'"]').closest('li').attr('id').split(/_/)[2];
  columnobj.delsort(id,form,fld);
 },




}

cloneobj={
 init:function() {
  /* Look for a 'Clone this entry' menu item */
  $('div#hw_menu ul.sub-menu li a').each(function(i){
   if (this.innerHTML=='Clone this entry') {
    //console.log('Found');
    $(this).parent().click(cloneobj.doclone);
   }
  });
 },

 doclone:function() {
  // User has clicked the 'clone me' button.
  //console.log($(this));
  $('form#cloneform').remove();
  var cloneform=$('<form>').attr('id','cloneform').attr('method','POST').attr('target','_blank').attr('action','edit.php');
  var view=$('ul#hw_quicksearch_form_hide').find('input[name="_view"]').val();
 //console.log('View '+view);
  // Loop over all fields appending data to that form
  $('div.recordvalue input,select,textarea').each(function(i){
    if ($(this).attr('name') && $(this).val()) {
      cloneform.append($('<input>')
               .attr('type','hidden')
               .attr('name',$(this).attr('name'))
               .val($(this).val()));
    }
  });
  cloneform.append($('<input>').attr('type','hidden').attr('name','_view').val(view))
           .append($('<input>').attr('type','hidden').attr('name','_action').val('new'));
  $(this).after(cloneform);
  // Submit the form
  $('form#cloneform').submit();
  return false;
 }
};

// An object to present debugging code
debugobj={
 init: function() {
  var dbgctl=$('span#hw_control_debug').first(); // first and only, methinks
//console.log('Checking for debug option');
  if (dbgctl) {
//console.log('Debug control located. Binding');
   dbgctl.click(debugobj.displaycontrol);
   dbgctl.css('cursor','pointer');
  }

 },

 displaycontrol: function() {
  //console.log('Displaying debugging control');
  var basevars=$('div#hw_sortandsearch').find('input');
  dbg_vals=new Array('Object creation', 'Public method calls', 
                     'Protected method calls', 'Private method calls',
                     'Non-view related SQL', 'View-related SQL', 
                     'Unknown SQL', 'Display backtrace'
);
  dbg_vals_inputs='';
  for (var i=0; i<dbg_vals.length; i++) {
   u=Math.pow(2,i);
   selected=($('span#hw_control_debug input#_debugval_'+u).length>0);
   dbg_vals_inputs+='<input type="checkbox" name="_debugval[]" '
                   +(selected?'checked="checked"':'')
                   +'value="'+u+'">'
                   +dbg_vals[i]+'</input>';
  }
  // grep ^class *.php | awk ' { print $2 } '
  dbg_objs=new Array('dbBase', 'dbColumn', 'dbCSVimport', 'dbCSV', 'dbDispBase',
                     'dbDispBlobtype', 'dbDispBool', 'dbDispDate', 'dbDispFloat4',
                     'dbDisp_hwgraph', 'dbDisp_hwsubview', 'dbDispInt4',
                     'dbDispInt8', 'dbDispMacaddr', 'dbDispNumeric', 'dbDispOid',
                     'dbDispText', 'dbDispTime', 'dbDispVarchar', 'dbGraph', 
                     'dbList', 'dbMetaList', 'dbNavBar', 'dbRecord', 'dbSearch',
                     'dbSortList', 'dbSort', 'dbView', 'htmlBase', 'hwFunctions');
  dbg_objs_inputs='';
  for (var i=0; i<dbg_objs.length; i++) {
    // If we find input#_debug_OBJECT, set 
    selected=($('span#hw_control_debug input#_debugobj_'+dbg_objs[i]).length>0);
    dbg_objs_inputs+='<input type="checkbox" name="_debugobj[]" class="_hw_debugobj" '
                    +(selected?'checked="checked':'')
                    +'value="'+dbg_objs[i]+'" >'
                    +dbg_objs[i]+'</input>';
  }
  var $dialog = $('<div id="hw_debug_control_popup">')
                .append(
                 $('<div>')
                 .append('Debug control - todo: load already set parameters from page')
                )
                .append(
                 $('<form>')
                 .append('<div>Type of debugging</div>')
                 .append( dbg_vals_inputs)
                 .append('<div>Objects to debug</div>')
                 .append('<input type="checkbox" id="_debugobj_all" />All objects</input>')
                 .append($('<div style="display:none;">').append(basevars))
                 .append(dbg_objs_inputs)
                 .append('<br/>Max call depth:<input name="_debugdepth" value="0" type="text" width="1">')
                 .append('<button id="hw_setdebug">Apply</button>')
                )
  		.dialog({
                        width: 500,
                        modal: true,
                        autoOpen: false,
                        show: "slide",
                        hide: 'slide',
                        title: "Alter debugging parameters",
                        }
                 );
  $dialog.find('input#_debugobj_all').change(debugobj.selectallobjs);
  $dialog.find('button#hw_setdebug').click(debugobj.apply);
  $dialog.dialog('open');
  

 },
 selectallobjs: function() {
   tval=$('div#hw_debug_control_popup').find('input#_debugobj_all').prop('checked');
   $('div#hw_debug_control_popup').find('input._hw_debugobj').each(function(i,v) {
     $(this).prop('checked',tval);
   });
 },
 apply:  function() {
console.log('Clicked');
  
 },

}

// An object to handle related records
relrecobj={

 init: function() {
   var rr=  $('a[href="AJAXRELREC"]');
   //console.log('RR');
   //console.log(rr);
   rr.click(function() {
    rr.off('click');
    rr.html('Loading');
    //console.log('clicked');
    var i=$('input[id="id"]');
    var v=$('input[name="_view"]').first();
    //console.log(v);
    $.ajax({url: "ajaxrelrec.php?_id="+i.val()+"&_view="+v.val()}).done(function(msg) {
     var ul=$('<ul>').attr('class','sub-menu');
     var obj=jQuery.parseJSON(msg);
     if (obj) {
      jQuery.each(obj,function(i,e){
       ul.append(
                 $('<li>').append($('<a>').attr('href',e.url).html(e.name)));
      });
     }
     rr.html('Related Records').append(ul);
     
    });
    return false;
   });
   //console.log('relrecobj initialised');
 },
};


/* Create the searchobj when the page has loaded */
$(document).ready(function(){
/* Bring page to front as loading */
  //$('div#jqmenu > ul').menu(); // Set up menu
  /* Don't use this for menu bar any more
  $('div#jqmenubar > ul').menubar({autoExpand: true,menuIcon: true, buttons: true,
			position: {
				within: $("#demo-frame").add(window).first()
			},
*/
//});
  
/* - This still seems to be called from tinymceinit.js
  $('textarea.hw_htmlview').each(function(i) {
   tinyMCE.execCommand('mceAddControl',false,$(this).attr('id'));
  });
  */
  // Handle dates
  
  // DATEFORMAT is specified according to https://www.php.net/manual/en/function.date.php
  // We need formats for jquery.datepicker
  var date_arg_map={
    "d": "dd",
    "D": "D",
    "j": "d",
    "l": "DD",
    "z": "o",
    //   : "oo",
    "n": "m",
    "m": "mm", 
    "M": "M",
    "F": "MM",
    "y": "y",
    "Y": "yy",
    "U": "@"
  };
  //var this_date_format=('DATEFORMAT' in hw_prefs?hw_prefs['DATEFORMAT']:'d-m-Y');
  var this_date_format=hw_pref('DATEFORMAT','d-m-Y');
  var fields=this_date_format.split(/[-/]/);
  var delim=this_date_format.match('[/-]')[0];
  delim=(delim?delim:'-');
  this_date_format=fields.map(function(f){return(f in date_arg_map?date_arg_map[f]:'')}).join(delim);
  $('input.date').datepicker({
                              changeMonth: true, 
                              changeYear: true,
                              validateOnBlur: false,
                              dateFormat: this_date_format,
                              lazyInit: true,
                              constrainInput: false,
                              closeOnDateSelect: true,
                              closeOnWithoutClick: true,
                              mask:false,
                              todayButton: true,
                              yearRange: '-100:+50'
});

  // This seems to be non-date related. 
  var d = document.getElementById('hw_pagetitle');
  if (d != null) {
    document.title = d.textContent+ ' :: ' + document.title;
  }

  // Do we want to apply this to everything?!
  if (document.itemdata && typeof document.itemdata.elements!=="undefined") {
   for (i = 0; i < document.itemdata.length; i++) {
    document.itemdata.elements[i].onchange = function() {
     changes = true;
    };
   }
  }

  console.log('Loaded v1');
  debugobj.init();
  searchobj.init();
  sortobj.init();
  columnobj.init();
  hotwireobj.init();
  recordobj.init();
  cloneobj.init();
  relrecobj.init();
  $(".sortable").sortable();
  $(".sortable" ).disableSelection();
  $("div#hw_sort ul.sortable").bind("sortupdate",function(event,ui) {  
console.log('Sort drop');
   $('div#hw_sortandsearch').removeClass('unchanged');  
   $('div#hw_sortandsearch').addClass('changed');
  });
  // Handle exit without save
  $(window).bind('beforeunload', function() {
   if (changes) { 
    return 'You have changed data on this form. Lose these changes?';
   }
 });

});

function confirm_delete(e) {
  return confirm('Are you sure you wish to delete this record?');
}

