<?php
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

/*
 * This file is part of Hotwire
 * http://hotwire.sourceforge.net/
 * and is used by AJAX calls to get list of views which might reference this record
 */

 /**
  * @file ajaxrelrec.php
  * @author Frank Lee
  * @date 2011-07-11
  * @version 11
  */

session_start();

require 'preconfig.php';
include 'config.php';
include 'libchemdb.php';

if ( !isset($_SESSION['dbuser'])) {

 ?>
[{"optVal":"Login","optDisp":"Not logged in"}]
 <?php

} else {
 // What possible views could relate to this record?
 $record = new dbRecord();
 $views=$record->relatedrecords();
 print json_encode($views);
}

?>
