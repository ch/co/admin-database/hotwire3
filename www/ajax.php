<?php
// $Id$
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

/*
 * This file is part of Hotwire
 * http://hotwire.sourceforge.net/
 * and is used by AJAX calls to interact with the database
 */

 /**
  * @file ajax.php
  * @author Frank Lee
  * @date 2012-11-22
  * @version 1
  */

session_start();
error_reporting(E_ALL);
require './set_path.php';

require 'preconfig.php';
include 'config.php';
require 'libchemdb.php';

if (!isset($_SESSION['dbuser'])) {
 $result=new dbAjaxResult();
 $result->fail('No longer logged in to the database');
 print $result->JSON();
} else {
 $result=new dbAjax;
 print $result->JSON();
}

?>
