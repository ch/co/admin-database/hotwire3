<?php
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

/*
 * This file is part of Hotwire
 * http://hotwire.sourceforge.net/
 * and is used to present the login screeen for users.
 */

/**
 * @brief Displays the login form
 * @todo Make the login form appear from dbBase
 * @date 2011-07-26
 * @version 1
 */

header ('Content-type: text/html; charset=utf-8');
session_start();
session_unset();
session_destroy();
# Include files from ../local/config/ by default
include './set_path.php';

require 'preconfig.php';
include 'config.php';
require 'libchemdb.php';

$db = new dbBase();
 $db->echoHead();
 $db->echoTop();
?>
<div class='hw_login'>
 You have been logged out; to return to the database, please <a href='login.php'><button>login again</button></a>.
</div>
<?php
 $db->echoBottom();

 exit;


?>
