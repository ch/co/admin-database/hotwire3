<?php
header ('Content-type: text/html; charset=utf-8');
$_SESSION=array();

require './set_path.php';

require_once "preconfig.php";
include('libchemdb.php');
require_once('config.php');
$hw=new htmlBase();
$hw->prefs['MM_UI_TAG']=True;

# Need to load JS/CSS for each type of field.
$js=array();
$css=array();
dbDispDate::prepareRecord($js,$css);
dbDispInt4::prepareRecord($js,$css);

# Inject those JS/CSS into the htmlBase object
global $HW_DEFAULT_JS_FILES;
global $HW_DEFAULT_CSS_FILES;
foreach ($js as $j) {
 $HW_DEFAULT_JS_FILES[]=($j.".js");
}
foreach ($css as $c) {
 $HW_DEFAULT_CSS_FILES[]=($c.".css");
}
$HW_DEFAULT_CSS_FILES[]='css/help-format.css';
#$HW_DEFAULT_JS_FILES=array_merge($HW_DEFAULT_JS_FILES,$js);
#$HW_DEFAULT_CSS_FILES=array_merge($HW_DEFAULT_CSS_FILES,$css);

$hw->echoHead();
$hw->echoTop();

?><h1>Hotwire Record Screen</h1>
<div>A record in the database is displayed in the browser using some common elements. Those presented here can be used much like the elements on the live database - except the data will not be stored, of course.</div>
<div class='fields'>
<div class='explanation'>Individual records are displayed with a label for each field.</div>
<div>Click into the box, and edit any existing data to be correct.</div>
<div class='field1'>
<?php
# Produce a basic text field

$r=giveui(array(array('val'=>'Field value','attname'=>'field name','typname'=>'varchar','relname'=>'help','attnotnull'=>'f','atthasdef'=>'f','array'=>'f')));
$r->displayData();
?>

<div class='explanation'>Some fields are read-only and cannot be edited.</div>
<div>Clicking into the box has no effect.</div>
<div class='field2'>
<?php

$r=giveui(array(array('val'=>'Some read-only data','attname'=>'ro_A read-only field','typname'=>'varchar','relname'=>'help','attnotnull'=>'f','atthasdef'=>'f','array'=>'f')));
$r->displayData();

?>

<div class='explanation'>Some types of data don&apos;t need to be typed.</div>
<div>For example, a yes/no question can be answered by selecting the option from the drop-down list. Here, &apos;-&apos; is used to indicate &apos;Unknown&apos;.</div>
<div>When setting a date field, clicking into the box brings up a calendar from which a day can be chosen - or one can type in the date.
<div class='field3'>
<?php

function giveui($l) {
$r=new dbRecord();
$r->id=array(0);
foreach ($l as $a) {
 $r->data[0][$a['attname']]=$a['val'];
 $c=new dbColumn($a,$r);
 $r->colList[]=$c->name();
 $r->colobjs[]=$c;
}
#$r->displayData();
return $r;
}
$r=giveui(array(
 array('val'=>'t','attname'=>'A yes or no field','typname'=>'bool','relname'=>'help','attnotnull'=>'t','atthasdef'=>'f','array'=>'f'),
 array('val'=>'2022-01-01','attname'=>'A date field','typname'=>'date','relname'=>'help','attnotnull'=>'t','atthasdef'=>'f','array'=>'f')
));
$r->displayData();
?>

<div class='explanation'>Some boxes can handle more than one line of text.</div>
<div>There's quite a lot of space already, but if you run out, you can grab the bottom-right of the box and drag it to get more.</div>
<?php
$r=giveui(array(
 array('val'=>'It\'s quite possible to have long text in a box. This can be useful when recording addresses and other information which can go on and on','attname'=>'A notes field','typname'=>'text','relname'=>'help','attnotnull'=>'t','atthasdef'=>'f','array'=>'f'),
));
$r->displayData();
?>

<div class='explanation'>Some fields must refer to a list of values.</div>
<div>Here, one can click to select a value from the list or type to search within the list and narrow the choice, whether one is restricted to a single choice (your favourite colour) or able to choose a selection of options. In either case, if the list has been narrowed to a single choice, pressing enter will select it.</div>
<!-- Statically-generated HTML here alas -->
<div class='resultstable'>
  <input type='hidden' name='_changed' id='_changed' value='0' />
  <input type='hidden' name='_view' id='_view' value='' />
  <input type='hidden' name='_id[]' id='Z__id' value='0'/>

  <div class='itemdata'>
   <!-- Displaying an entry of type text -->
   <!-- Class dbDispText -->
   <div class="clearwidth text" id="A_notes_field__row">
    <div class="record">
     <div class="recordkey"><p>Favourite colour</p></div>
     <div class="recordvalue">
      <select name="fav_col" column="fav_col" id="fav__col">
       <option value='red'>Red</option>
       <option value='green'>Green</option>
       <option value='blue' selected>Blue</option>
      </select>
     </div><!-- recordvalue -->
    </div><!-- record -->
   </div><!-- clearwidth -->

   <!-- Displaying an entry of type text -->
   <!-- Class dbDispText -->
   <div class="clearwidth text" id="A_notes_field__row">
    <div class="record">
     <div class="recordkey"><p>Lunch choices</p></div>
     <div class="recordvalue">
      <select name="lunch_col[]" multiple='multiple' column="lunch_col" id="lunch__col" class="select_ajax_multi_invisible">
       <option value='soup'>Soup</option>
       <option value='baguette'>Baguette</option>
       <option value='samosa' selected>Samosa</option>
       <option value='cheese'>Cheese</option>
       <option value='salad' selected>Salad</option>
      </select>
     </div><!-- recordvalue -->
    </div><!-- record -->
   </div><!-- clearwidth -->
  </div>

</div> <!-- tableResults -->
</div>
</div> <!-- fields -->

<div class='explanation'>Saving the record - or deleting it.</div>
<div>If you have the rights, you can save the record you have edited by using the &quot;Save: Update this record&quot; button at the bottom of the page. You might also see a &quot;Delete&quot; button to erase the record. These buttons will only be presented if the database knows you have the right to save or delete the record. (Neither button is functional on this documentation page.) If you do delete the record, you will be returned to the <a href='list-help.php'>list screen</a>.</div>
<div class='resultstable'>
 <div class="tableFooter">
  <button id="formdel" type="button">Delete</button>
  <button id="update" type="button">Save: Update this record</button>
 </div>
</div>

<?php
$hw->echoBottom();
