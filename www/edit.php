<?php
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

/*
 * This file is part of Hotwire
 * http://hotwire.sourceforge.net/
 * and presents a single record to the user for editing.
 */

 /** 
  * @file edit.php
  * @brief Presents a single record to the user for editing
  * @date 2011-07-11
  * @version 11
  **/

header ('Content-type: text/html; charset=utf-8');
error_reporting(E_ALL);
ini_set("display_errors",1);
session_start();

include './set_path.php';

require 'preconfig.php';
include 'config.php';
include 'libchemdb.php';

if (!isset($_SESSION['dbuser'])) {

 go_login();

} else {
  $record = new dbRecord();
  $record->echoHead();
  $record->echoTop();
  $record->echoNavBar();
  $record->display();

  # Present some resource utilisation
  $dat=getrusage();
  $dat['sql_timing']=$hw_sql_timing;
  if (array_key_exists('_dbpasswd',$_POST)) { $_POST['_dbpasswd']='PASSWORD_REMOVED';};
  $data=array('GET'=>$_GET,'POST'=>$_POST,'COOKIES'=>$_COOKIE,'FILES'=>$_FILES,'SERVER'=>$_SERVER);
  print "<div class='runtime'><span class='rusage json'>".json_encode($dat)."</span><span class='request json'>".json_encode($data)."</span></div>\n";

  $record->echoBottom();
}

exit;
?>
