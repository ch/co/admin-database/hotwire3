<?php
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

/*
 * This file is part of Hotwire
 * http://hotwire.sourceforge.net/
 * and is used by AJAX calls to populate drop-down lists on demand rather than
 * sending all the options to each page every time.
 */


/**
 * @brief Provide the data for the many-many dropdown UI tool
 * @author Frank Lee
 * @date 2011-07-11
 * @version 12
 * @todo Prevent this from breaking when debug is turned on (i.e. 
 *  turn it off for this session!
 **/
session_start();
require './set_path.php';

require 'preconfig.php';
include 'config.php';
require 'libchemdb.php';


if ( !isset($_SESSION['dbuser'])) {

?>
Login=Not logged in
<?php

} else {
 $record = new dbRecord();
 $options=$record->ajaxmmdropdown($_REQUEST['column']);
 $options['usage']=getrusage();
 $options['dom_id']=$_REQUEST['dom_id'];
 header('Content-Type: application/json');
 print json_encode($options);
}

?>
