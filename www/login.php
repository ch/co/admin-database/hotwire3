<?php
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

/*
 * This file is part of Hotwire
 * http://hotwire.sourceforge.net/
 * and is used to present the login screeen for users.
 */

/**
 * @brief Displays the login form
 * @todo Make the login form appear from dbBase
 * @date 2011-07-26
 * @version 1
 */

header ('Content-type: text/html; charset=utf-8');
session_start();
#session_unset();
#session_destroy();
# Include files from ../local/config/ by default
include './set_path.php';

require 'preconfig.php';
include 'config.php';
require 'libchemdb.php';

$db = new dbBase();

$append="";
if ($_SERVER['QUERY_STRING']) {
 $append="?".$_SERVER['QUERY_STRING'];
 $append=preg_replace('/landing=[^&]*&/','',$append);
}

# Handle the case where we already have all the credentials necessary.
if ($db->dbInit()) {
  # Remove any remaining 'landing=...'
  if (array_key_exists('defaultview',$_SESSION) && $_SESSION['defaultview']) {
    if (isset($_GET['landing'])) {
      header('Location: '.$_GET['landing'].$append);
    } else {
      header('Location: list.php'.$append);
    }
  } else {
    if (isset($_GET['landing'])) {
      header('Location: '.$_GET['landing'].$append);
    } else {
      header('Location: index.php'.$append);
    }
  }
  exit;
}

if ($db->authenticate()) {
  # Remove any remaining 'landing=...'
  if (array_key_exists('defaultview',$_SESSION) && $_SESSION['defaultview']) {
    if (isset($_GET['landing'])) {
      header('Location: '.$_GET['landing'].$append);
    } else {
      header('Location: list.php'.$append);
    }
  } else {
    if (isset($_GET['landing'])) {
      header('Location: '.$_GET['landing'].$append);
    } else {
      header('Location: index.php'.$append);
    }
  }
  exit;
} else {
  # We have $db, so we might as well use it!
  $db->echoHead();
  $db->echoTop();
  $append="";
  if ($_SERVER['QUERY_STRING']) {
   $append="?".$_SERVER['QUERY_STRING'];
  }
  if (file_exists('../local/config/motd.html')) {
    echo "<div class='motd'>";
    readfile('../local/config/motd.html');
    echo "</div>";
  }
  echo "
 <div class='hw_login'>
  <form name='hw_login' action='login.php$append' method='post'>
   <input type='hidden' id='hw_javascript' name='hw_javascript' value='false'>
   <div class='hw_login_msg'>$LOGINMSG</div>
   <div class='hw_login_boxes'>
    <div class='username'><label for='hw_username'>Username</label><input id='hw_username' type='text' name='_dbuser'/></div>
    <div class='hw_password'><label for='hw_password'>Password</label><input id='hw_password' type='password' name='_dbpasswd'/></div>
   </div>
";
  if ($_POST) {
    echo '<div class="hw_login_err">Login failed</div>'."\n";
  }
  echo "<input type='submit' value='Login' class='btn'>
   </form>
  </div> ";
  if ($db->message) {
   echo '<div class="hw_login_error">'.$db->message.'</div>';
  }
?>
<script type='text/javascript'>
 window.onload = function() {
  document.getElementById("hw_username").focus();
  document.getElementById("hw_javascript").value='true';
};
</script>
<?php

 $db->echoBottom();

 exit;

}

?>
