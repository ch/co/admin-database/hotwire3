<?php
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

/*
 * This file is part of Hotwire
 * http://hotwire.sourceforge.net/
 * and displays a graph 
 */

session_start();
require './set_path.php';

require 'preconfig.php';
include 'config.php';
require 'libchemdb.php';

error_reporting(E_ALL);
ini_set('display_errors','On');
if (!isset($_SESSION['dbuser'])) {
 # Umm, can't do much about this 'cos we're being called to provide an image
 go_login();

} else {
 
  $base=new dbGraph();
  $base->plot();
}

exit;

?>
