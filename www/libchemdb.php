<?php
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

 /** 
  * @file libchemdb.php
  * @brief To provide functions which don't fit into objects.
  * Part of Hotwire http://hotwire.sourceforge.net/.
  * @author Stuart Taylor
  * @author Frank Lee
  */

# We now do this with spl_autoload_register for 7.2+ compatibility
/**
 * @todo: Some debug info here would be good
 * @todo Do we need to override __autoload?
 *       Can we just use include_path instead?
 **/
#function __autoload($className) {
 #if (file_exists('classes/'.$className.'.php')) {
  #include_once 'classes/' . $className . '.php';
 #} else {
  #throw new Exception('No such file for '.$className);
 #}
#}
spl_autoload_register(function($class) {
  require_once $class.'.php';
});

/**
 * @todo Remove this
 **/
if (in_array('debug', array_keys($_REQUEST))) {
  $_SESSION['debug'] = TRUE;
} elseif (in_array('nodebug', array_keys($_REQUEST))) {
  unset($_SESSION['debug']);
}

$hw_sql_timing=array();

/**
 * Redirects us to the login page
 **/ 

function go_login() {
 ## TODO: this should be a 302 moved, surely?
 $append="";
 if ($_SERVER['QUERY_STRING']) {
  $append="?landing=".basename($_SERVER['SCRIPT_NAME'])."&".$_SERVER['QUERY_STRING'];
 }
 echo '
 <html>
 <head>
 <meta http-equiv="refresh" content="0;url=login.php'.$append.'">
 </head>
 </html>';
}

?>
