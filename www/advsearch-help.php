<?php
header ('Content-type: text/html; charset=utf-8');
$_SESSION=array();

require './set_path.php';

require_once "preconfig.php";
include('libchemdb.php');
require_once('config.php');
$hw=new htmlBase();
$hw->prefs['MM_UI_TAG']=True;

function giveui($l) {
$r=new dbRecord();
$r->id=array(0);
foreach ($l as $a) {
 $r->data[0][$a['attname']]=$a['val'];
 $c=new dbColumn($a,$r);
 $r->colList[]=$c->name();
 $r->colobjs[]=$c;
}
#$r->displayData();
return $r;
}

# Need to load JS/CSS for each type of field.
$js=array();
$css=array();
dbDispDate::prepareRecord($js,$css);
dbDispInt4::prepareRecord($js,$css);

# Inject those JS/CSS into the htmlBase object
global $HW_DEFAULT_JS_FILES;
global $HW_DEFAULT_CSS_FILES;
foreach ($js as $j) {
 $HW_DEFAULT_JS_FILES[]=($j.".js");
}
foreach ($css as $c) {
 $HW_DEFAULT_CSS_FILES[]=($c.".css");
}
$HW_DEFAULT_CSS_FILES[]='css/help-format.css';
#$HW_DEFAULT_JS_FILES=array_merge($HW_DEFAULT_JS_FILES,$js);
#$HW_DEFAULT_CSS_FILES=array_merge($HW_DEFAULT_CSS_FILES,$css);

$hw->echoHead();
$hw->echoTop();

?><h1>Hotwire Advanced Search Screen</h1>
<div>Basic searches are easily done from the <a href='list-help.php'>list screen</a>, but more advanced searches are possible - for example, returning all records which don't match a parameter or excluding some fields from the results.</div>

<div class='buttons'>
<div class='explanation'>Show / hide all buttons</div>
<div>When choosing which columns will be returned by a search, sometimes we wish to start with none and build up: press 'Hide all' to un-tick all the 'Show in results' boxes.</div>
<?php
# Produce a basic text field

$r=giveui(array(array('val'=>'Field value','attname'=>'dummy','typname'=>'varchar','relname'=>'help','attnotnull'=>'f','atthasdef'=>'f','array'=>'f')));
$r->displaySearch();

?>
</div> <!-- buttons -->

<div class='fields'>

<div class='explanation'>Searching a text field.</div>
<div>Records will be filtered; those matching the text in the box will be displayed: tick the &quot;Exclude&quot; box to only show records where the field does not match. If only exact matches are required, tick the &quot;Exact match&quot; box: that will prevent a search for &quot;alum&quot; matching &quot;aluminium&quot;. If the &quot;Show in results&quot; box is unticked, this column will not appear in the results (which makes it hard to see whether your search has been successful.)</div>

<?php
$r=giveui(array(array('val'=>'Field value','attname'=>'Field name','typname'=>'varchar','relname'=>'help','attnotnull'=>'f','atthasdef'=>'f','array'=>'f')));
$r->displaySearch();
?>

<div class='explanation'>Searching dates</div>
<div>Date fields offer from -- to searching: if the from date (the first box) is left empty, all records matching up to the to date (the second box) are returned. If both from and to dates are specified, records within the range are returned. If only the from date is specified, records after that date will be returned. (Subject to the usual operation of tha &quot;Exclude&quot; option.)</div>
<div>The from and to dates may be entered in the same way as on the <a href='record-help.php'>Record screen</a> - by typing or by selection from the calendar widget.
</div>
<?php

$r=giveui(array(
 #array('val'=>'t','attname'=>'A yes or no field','typname'=>'bool','relname'=>'help','attnotnull'=>'t','atthasdef'=>'f','array'=>'f')
 array('val'=>'2022-01-01','attname'=>'A date field','typname'=>'date','relname'=>'help','attnotnull'=>'t','atthasdef'=>'f','array'=>'f')
));
$r->displaySearch();
?>

<div class='explanation'>Searching lists</div>
<div>When a field is restricted to a list of options, whether a single selection or multiple selections, when searching multiple options may always be selected. This allows us to search for people whose favourite colour is either red or green easily.</div>

<div>Here, one can click to select a value from the list or type to search within the list and narrow the choice, whether one is restricted to a single choice (your favourite colour) or able to choose a selection of options. In either case, if the list has been narrowed to a single choice, pressing enter will select it.</div>
<!-- Statically-generated HTML here alas -->
<div class='searchtable'>
  <div class="tableHeader">
    <div class="tableTitle">
       <span id="_debug_report_error"></span>
    </div> <!-- tableTitle -->
    <div class="tableActions">
      <form name="altview" action="edit.php" method="get">
        <input type="hidden" name="_id[]" id="Z__id" value="0">
      </form>
    </div> <!-- tableActions -->
    <div class="clearwidth"> </div> <!-- clearwidth -->
  </div>
  <form class='itemdata' name='itemdata'>
    <div class='actionbuttontop'></div>
    <div class='clearwidth'></div>
    <div class='tableResults'>
      <div class='itemdata'></div>
      <div class='clearwidth'>
        <div class="searchkey"></div>
        <div id='favcol_id__row' class='clearwidth int4'>
          <div class='search'>
            <div class='searchkey'>
              <p>Favourite colour</p>
              <div class="suppress" id="hde__favcol__id" >
                <input type="checkbox" value="off" name="hde_switch_blade_model_hid" id="hde__switch__blade__model__hid">
              </div> <!-- suppress -->
            </div><!-- Searchkey -->
            <div class="searchvalue">
              <select name="switch_blade_model_id[]" view="dummy" column="switch_blade_model_id" class="" size="10" id="favcol__id" multiple="multiple" tabindex="-1" aria-hidden="true">
                <option value="red" sortpos="0">red</option>
                <option value="green" sortpos="1">green</option>
                <option value="blue" sortpos="2">blue</option>
              </select>
              <span class="searchxcl">
                Exclude <input type="checkbox" name="xcl_switch_blade_model_hid" id="xcl__switch__blade__model__hid">
              </span><!-- searchxcl -->
            </div><!-- SearchValue -->
          </div> <!-- search -->
        </div> <!-- clearwidth,int4 -->
      </div> <!-- clearwidth -->
    </div> <!-- tableResults -->
  </form>
  <div class="postFooter"></div>
</div> <!-- searchtable -->

<div>Exclude has the usual meaning</div>
</div> <!-- fields -->

<div class='explanation'>Executing the search</div>
<div>To find results matching the search criteria, use the &quot;Search&quot; button at the lower right of the form. From the results of the search, the parameters may be altered by following &quot;Amend Search&quot; from the Action menu.</div>
<div class='resultstable'>
 <div class="tableFooter">
  <button id="search" type="button">Search</button>
 </div>
</div>

<?php
$hw->echoBottom();
