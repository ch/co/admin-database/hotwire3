<?php
 /**
  * @brief A class with methods to display a time field
  * @author Frank Lee
  * @date 2011-11-09
  * @version 12
  **/
class dbDispTime extends dbDispBase {

 /**
  * @brief Code placed in the HEAD of the page to load CSS/JS for time pickers
  * @date 2011-07-11
  * @version 11
  **/
 public function prepareRecord(&$js,&$css) {
  $js[]="js/timepicker.js";
  $js[]='jquery-timepicker/jquery-ui-timepicker-addon.js';
 }
 
 /**
  * @brief Generate a UI for editing a time field
  * @date 2011-07-11
  * @version 11
  **/
 public function displayEdit(){
   if (substr($this->dbColumn->name(),0,1)=='_') { return;}
   $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
   $classes=array();
   if (array_key_exists('readonly',$this->attrs)){
     $classes[]='text';
     $classes[]='readonly';
   } else {
     $classes[]='time';
   }
   return '<input type="text" size="10" name="'.htmlspecialchars($this->key).'" '.
          (array_key_exists('readonly',$this->attrs)?' readonly="readonly" ':'').
          'class="'.join(' ',$classes).'" '.
          'value="'.htmlspecialchars($this->val).'" />';
 }
 

}
?>
