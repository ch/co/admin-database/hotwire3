<?php
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

 /** 
  * @file dbDispBase.php
  * @brief Handles display of a field (base class)
  * Part of Hotwire http://hotwire.sourceforge.net/. 
  */

 /**
  * @brief A class with basic methods to display a field
  * @author Frank Lee
  * @date 2011-07-11
  * @version 12
  **/
class dbDispBase extends dbBase {
 protected $attrs=array();       //!<@brief Attributes about this field
 protected $fieldname;           //!<@brief Name of this field
 protected $dbColumn;            //!<@brief dbColumn object
 protected $objRecord=NULL;      //!<@brief dbRecord object 
 protected $classes=NULL;        //!<@brief Cache of classes
 private   $getparams=NULL;      //!<@brief Cache of parameters
 protected $getparamstring=null; //!<@brief String cache of parameters
 protected $colname=null;        //!<@brief cache of key in $row
 protected $view=null;		 //!<@brief Cache of name of view
 private   $isarray=null;        //!<@brief Cache of if we're an array
 private   $issubview=null;      //!<@brief Cache of whether we're in a subview
 private   $inhtml=null;         //!<@brief Cache of the dbColumn in_html flag

 public function getid($offset=NULL) {
  return $this->dbColumn->getid($offset);
 }


 function __construct(&$dbColumn) {
  $this->hwDebugCreateStart();
  $this->loadPreferences();
  $this->getHWSchema();
  $this->dbColumn=&$dbColumn;
  $this->fieldname=$this->dbColumn->name();
  # If we're about to display a dbRecord:
  if (is_a($dbColumn->parentobj,'dbRecord')) {
    $this->objRecord=&$dbColumn->parentobj;
    $this->key=$this->fieldname;
    // Individual RO field
    $this->ro = (preg_match('/^ro_|_ro_|_ro$/', $this->dbColumn->name()));
    if ($this->objRecord->propertyEqual('action','new')) {
     # Field is read-only if we're trying to create a record we can't insert.
     if (!$this->objRecord->perms['insert']) { $this->ro=TRUE;}
    } else {
     # We're not inserting, so we're displaying
     if (array_key_exists('update',$this->objRecord->perms) && !$this->objRecord->perms['update']) {$this->ro=TRUE;}
    }
  }
  // Build caches: Classes
  $classes[]=$this->dbColumn->parentobj->listClass; // View or Report
  $classes[]=$this->dbColumn->parentobj->domName($this->dbColumn->parentobj->view);
  $classes[]=$this->dbColumn->parentobj->domName($this->dbColumn->parentobj->view."_".$this->dbColumn->name());
  $this->classes=join(' ',$classes);
  // Build caches: Array?
  $this->isarray=$this->dbColumn->is_array();
  // Build caches: view?
  $this->view=$this->dbColumn->parentobj->view;
  // Build caches: subview?
  $this->issubview=$this->dbColumn->parentobj->is_subview;
  // Build caches: GET parameters
  $args=array('_view='.htmlentities($this->view));
  $args[]=$this->dbColumn->parentobj->usortobjs->asGET();
  if (method_exists($this->dbColumn->parentobj,'pagerAsGET')) {
    $args=array_merge($args,$this->dbColumn->parentobj->pagerAsGET());
  }
  if (method_exists($this->dbColumn->parentobj,'searchAsGET')) {
    $args=array_merge($args,$this->dbColumn->parentobj->searchAsGET());
  }
  $this->getparams=$args;
  $this->getparamstring=join('&',$args);
  // Build caches: column name
  $this->colname=($this->dbColumn->ent_hid()?$this->dbColumn->ent_hid():$this->dbColumn->name());
  // Build caches: Are we 'in HTML'?
  $this->inhtml=$this->dbColumn->inhtml();

  $this->hwDebugCreateStop();
 }

 function get_data_value($colname) {
   # Or we could raise an exception if the column doesn't exist
   $ok=false;
   $val=null;
   $id=$this->getid()[0];
   $data=$this->objRecord->data[$id];
   if (array_key_exists($colname,$data)) {
     $val=$data[$colname];
     $ok=true;
   }
   return array($ok,$val);
 }
 
 /**
  * @brief Code placed in the HEAD of the page to load CSS/JS.
  * This should be over-written by child classes
  * @date 2011-07-11
  * @version 11
  **/
 public static function prepareRecord(&$js,&$css) {    
 }

 public static function prepareList(&$js,&$css) {
 }

  /**
   * @brief To return a DOM-compatible ID
   * @date 2012-11-09
   * @version 12
   **/
  public function id() {
    return $this->domName($this->dbColumn->name());
  }

  protected function htmlesc() {
    return htmlspecialchars($this->getval(),ENT_QUOTES);
  }
 
 /**
  * @brief To display a simple edit UI for the general case
  * @date 2011-07-11
  * @version 11
  **/
 public function displayEdit() {
   // Don't display fields named _FOO by convention
   if (substr($this->dbColumn->name(),0,1)=='_') { return;}
   return '<input type="text" size="10" name="'.htmlspecialchars($this->dbColumn->name()).'" '.
          ($this->ro?' readonly="readonly" class="readonly" ':'').
          'value="'.htmlspecialchars($this->getval()).'" />';
 }

 public function preDisplayEdit(&$ui) {
   $ui[]='<div class="clearwidth '.$this->dbColumn->type().'" id="'.$this->domName($this->dbColumn->name().'_row').'">';
   $ui[]='<div class="record">';
   $ui[]='<div class="recordkey"><p>'.$this->hdrFormat($this->dbColumn->name()).'</p></div>';
   $ui[]='<div class="recordvalue">';
 }

 public function postDisplayEdit(&$ui) {
    $ui[]='</div><!-- recordvalue -->';
    $ui[]='</div><!-- record -->';
    $ui[]='</div><!-- clearwidth -->';
 }

 public function preDisplaySearch(&$ui) {}
 public function postDisplaySearch(&$ui) {}
 
 /**
  * @brief To display a simple search box
  * @date 2011-07-11
  * @version 11
  **/
 public function displaySearch() {
   $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
   return '<input type="text" class="varchar" size="10" '.
          'name="'.htmlspecialchars($this->dbColumn->name()).'" value="'.$this->getval().'"/>';
 }

 /**
  * @brief To display a simple 'exact match?' option
  * @date 2011-07-11
  * @version 11
  **/
 public function displayExact() {
   $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
   return "Exact match<input type='checkbox' name='xct_".$this->dbColumn->name()."' id='xct_".$this->dbColumn->name()."'/>";
 }

 /**
  * @brief To give the value of this object (used by subview);
  * @author Frank Lee
  * @version 1
  * @date 2012-09-10
  */
 public function getval() {
  # On failure, present the user with the data they sent to avoid losing typing
  if ($this->objRecord->updatefailed) {
   return $this->objRecord->post[$this->dbColumn->name()];
  } else {
   if (array_key_exists(0,$this->objRecord->id)) {
    return $this->objRecord->data[$this->objRecord->id[0]][$this->dbColumn->name()];
   } else {
    if (array_key_exists($this->dbColumn->name(),$this->objRecord->post)) {
      return $this->objRecord->post[$this->dbColumn->name()];
    }
   }
  }
 }

 public function ro() {
  return $this->dbColumn->ro();
 }

 public function getkey() {
  return $this->dbColumn->name();
 }
/*
 public function classes() {
  if ($this->classes) {
   return $this->classes;
  }
  $classes=array();
  $classes[]=$this->dbColumn->parentobj->listClass; // View or Report
  $classes[]=$this->dbColumn->parentobj->domName($this->dbColumn->parentobj->view);
  $classes[]=$this->dbColumn->parentobj->domName($this->dbColumn->parentobj->view."_".$this->dbColumn->name());
  $this->classes=$classes;
  return $classes;
 }
*/

 public function viewcellcsv(&$row) {
   if (!$this->inhtml) { return ; }
   if (!$this->dbColumn->visible()) { return ; }
   $value=$this->valuefromrow($row);
   // This was unhelpful; exporting a CSV and importing it again is necessary, so we need the brackets
   /*
   if ($this->dbColumn->isarray()) {
     $value=trim($value,'{}"');
   }
   */
   return $value;
 }
/*
 public function c_getview() {
   if (is_null($this->view)) {
     $this->view=$this->dbColumn->parentobj->view;
   }
   return $this->view;
 }

 public function c_isarray() {
   if (is_null($this->isarray)) {
     $this->isarray=$this->dbColumn->isarray();
   }
   return $this->isarray;
 }

 public function c_issubview() {
   if (is_null($this->issubview)) {
     $this->issubview=$this->dbColumn->parentobj->is_subview;
   }
   return $this->issubview;
 }
*/
 public function viewcell(&$row) {
   if (!$this->inhtml) { return; }
   if (!$this->dbColumn->visible()) { return ; }
   //$view=$this->dbColumn->parentobj->view;
   //$field='id';
   // Replace with _hid?
   $value=$this->valuefromrow($row);
   if ($this->isarray) {
    # Remove initial/final {}
    $value=trim($value,'{}');
    # Remove intermediate '","'
    $value=str_replace('","','; ',$value);
    # Remove initial/final "
    $value=trim($value,'"');
   }
   // Handle subviews differently
   if ($this->issubview) {
    $view=$this->dbColumn->parentobj->subviewtarget();
    // Is that a real view or reference to a field?
    if (count(explode('/',$view))<2) {
     $view=$row[$view];
    }
    $svref=$this->dbColumn->parentobj->subviewref();
    $tfield=$this->dbColumn->parentobj->subviewtargetfield();
# TODO
# Well, that's a helpful comment, isn't it... I wonder what I was going todo?
    $ref=$row['id'];
    // What's the value of the field we're going to search>
    if (substr($svref,-3)=='_id') {

    } 
    if ($svref) {
     $ref=$row[$svref];
    }
    
    if (! $tfield) {
     $tfield="id";
    }
    // Construct target for links
    if ($this->pref('OWNTAB')) {
     $target="target='".(array_key_exists('id',$row)?$view.'_'.$row['id']:'_blank')."' ";
    } else {
     $target="";
    }
    // If we're in a subview, and we're not searching on the field 'id' (which is assumed unique)
    // we display a list. Otherwise, assume we're going to an individual record and display it.
    $url=(($svref && ($tfield!='id')) ?'list.php':'edit.php')."?"
        ."_view=".htmlentities($view)
        .(($svref && ($tfield!='id')) ?"&_action=search&".urlencode($tfield)."=".urlencode($ref):"&_id=".urlencode($ref));
    return "<td class='".$this->classes."'><div>"
          .($view?"<a ".$target."href='".$url."'>":'')
          .$value
          .($view?"</a>":"")."</div></td>\n";
   } else {
    // Construct target for links
    if ($this->pref('OWNTAB')) {
     $target="target='".(array_key_exists('id',$row)?$this->dbColumn->parentobj->view.'_'.$row['id']:'_blank')."' ";
    } else {
     $target="";
    }
    return "<td class='".$this->classes."'><div>"
          ."<a ".$target."href='edit.php?"
//.$this->join($args,'&')
          .$this->getparamstring.'&_id='.$row['id']
          ."'>".$value."</a></div></td>\n";
   }
 }
/*
 public function colname() {
   if (is_null($this->colname)) {
     $this->colname=($this->dbColumn->ent_hid()?$this->dbColumn->ent_hid():$this->dbColumn->name());
   }
   return $this->colname;
 }
*/
 protected function valuefromrow(&$row) {
  # Hmm, some fields are ro_ but not flagged readonly
  /*
  if (is_null($this->colname)) {
    $this->colname=($this->dbColumn->ent_hid()?$this->dbColumn->ent_hid():$this->dbColumn->name());
  }
  */
  return $row[$this->colname];
/*
  return ($this->dbColumn->ent_hid()
                                      #ro_
         ?$row[$this->dbColumn->ent_hid()]
         :$row[$this->dbColumn->name()]);
*/
 }

 protected function displaySearchKeyStart(&$ui) {
   $ui[]= '<div class="searchkey"><p>'.$this->hdrFormat($this->colname).'</p>';
 }
 
 protected function displaySearchKeyEnd(&$ui) {
   $ui[]= '</div><!-- Searchkey -->';
 }

 protected function displaySearchHideStart(&$ui) {
   $ui[]= '<div class="suppress" id="'.$this->domName('hde_'.$this->colname).'" >';
 }

 protected function displaySearchHideBox(&$ui) {
   $ui[]= '<input type="checkbox" value="off" name="hde_'.$this->colname.'" id="'.$this->domName('hde_'.$this->colname).'" />';
 }

 protected function displaySearchHideEnd(&$ui) {
   $ui[]= '</div> <!-- suppress -->';
 }

 protected function displaySearchClearWidthStart(&$ui) {
   $ui[]='<div class="clearwidth '.$this->dbColumn->type().'" id="'.$this->domName($this->colname.'_row').'">';
 }

 protected function displaySearchClearWidthEnd(&$ui) {
   $ui[]='</div> <!-- clearwidth -->';
 }

 protected function displaySearchDisplay(&$ui) {
   #$ui[]= '<div method="displaySearchDisplay" class="display" id="'.$this->domName("dsp_".$this->colname).'"></div>';
  $ui[]= '<div class="display" id="'.$this->domName("dsp_".$this->colname).'">Show in results <input type="checkbox" checked name="dsp_'.$this->colname.'" id="'.$this->domName('dsp_'.$this->colname).'" /></div>';
 }

 protected function displaySearchExcludeStart(&$ui) {
   $ui[]= '<span class="searchxcl">';
 }

 protected function displaySearchExcludeBox(&$ui) {
    $ui[]=  "Exclude <input type='checkbox' name='xcl_".$this->colname."' ".((! is_null($this->dbColumn->parentobj->colAttrs[$this->colname]))&&(array_key_exists('exclude',($this->dbColumn->parentobj->colAttrs[$this->colname])))?'checked="" ':'')." id='".$this->domName("xcl_".$this->colname)."'/>";
 }

 protected function displaySearchExcludeEnd(&$ui) {
    $ui[]= '</span ><!-- searchxcl -->';
 }

 protected function displaySearchDivStart(&$ui) {
    $ui[]='<div class="search">';
 }
 
 protected function displaySearchDivEnd(&$ui) {
    $ui[]='</div> <!-- search -->';
 }

 protected function displaySearchValueStart(&$ui) {
$ui[]='<!-- Calling displaySearch -->';
    $ui[]='<div class="searchvalue">'.$this->displaySearch();
$ui[]='<!-- Returned from displaySearch -->';
 }

 protected function displaySearchValueEnd(&$ui) {
    $ui[]= '</div><!-- SearchValue -->';
 }

 protected function displaySearchExact(&$ui) {
    $ui[]= '<span class="searchxct">'.$this->displayExact().'</span>';
 }

 public function displaySearchField(&$ui) {
    $ui[]=$this->preDisplaySearch($ui);
    $this->displaySearchClearWidthStart($ui);
    $this->displaySearchDivStart($ui);
    $this->displaySearchKeyStart($ui);
    $this->displaySearchHideStart($ui);
    $this->displaySearchHideBox($ui);
    $this->displaySearchHideEnd($ui);
    $this->displaySearchDisplay($ui);
    $this->displaySearchKeyEnd($ui);
    $this->displaySearchValueStart($ui);
    $this->displaySearchExact($ui);
    $this->displaySearchExcludeStart($ui);
    $this->displaySearchExcludeBox($ui);
    $this->displaySearchExcludeEnd($ui);
    $this->displaySearchValueEnd($ui);
    $this->displaySearchDivEnd($ui);
    $this->displaySearchClearWidthEnd($ui);
    $ui[]=$this->postDisplaySearch($ui);
 }

 /**
  * A function to display the UI with some information 
  **/
 public function printFieldDocco() {
  $ui=array();
  $this->preDisplayEdit($ui);
  print join("\n",$ui)."\n";
  print $this->displayEdit();
  $ui=array();
  $this->postDisplayedit($ui);
  print join("\n",$ui)."\n";
 }

}
?>
