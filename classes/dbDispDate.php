<?php
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

 /** 
  * @file dbDispDate.php
  * @brief Handles display of Date fields
  * Part of Hotwire http://hotwire.sourceforge.net/.
  * @author Stuart Taylor
  * @author Frank Lee
  */

 /**
  * @brief A class with basic methods to display a date field
  * @author Frank Lee
  * @date 2012-11-09
  * @version 12
  **/
class dbDispDate extends dbDispBase {

 private $infmt;
 private $outfmt;

 public function __construct($args) {
   parent::__construct($args);
   $this->infmt=$this->pref('DBDATEFORMAT','Y-m-d');
   $this->outfmt=$this->pref('DATEFORMAT','Y-m-d');
 }

 /**
  * @brief Ensure we include the correct JS file for our UI
  * @return Some HTML to include in our HEAD
  * @version 11
  * @date 2011-07-11
  **/
 public static function prepareRecord(&$js,&$css) {
  $js[]="js/jquery.ui.timepicker";
 }
 
 /**
  * @brief Display a UI to edit a date field
  * @return Some HTML to include in a form
  * @date 2011-07-11
  * @version 11
  **/
 public function displayEdit() {
   if (substr($this->dbColumn->name(),0,1)=='_') { return;}
   $classes=array();
   if ($this->ro()) {
     $classes[]='text';
     $classes[]='readonly';
   } else {
     $classes[]='date';
   }
   $text_date=$this->dateAsString($this->getval());
   return '<input type="text" size="10" name="'.htmlspecialchars($this->getkey()).'" '.
          ($this->ro()?' readonly="readonly" ':'').
          'class="'.join(' ',$classes).'" '.
          'value="'.htmlspecialchars($text_date).'" />';
 }

 public function dateAsString($v) {
   $date=DateTime::createFromFormat($this->infmt,$v);
   $text_date=($date===FALSE?$v:$date->format($this->outfmt));
   return $text_date;
 }

 /**
  * @brief Display a UI to search a date field
  * @return Some HTML to include in a form
  * @date 2011-07-11
  * @version 12
  **/
 public function displaySearch() {
   $min=(array_key_exists('min_'.$this->getkey(),$this->objRecord->post)?
         $this->objRecord->post['min_'.$this->getkey()]:NULL);
   $max=(array_key_exists('max_'.$this->getkey(),$this->objRecord->post)?
         $this->objRecord->post['max_'.$this->getkey()]:NULL);
    
   return '<input type="text" size="10" name="min_'.htmlspecialchars($this->getkey()).
          '" class="date" '.($min!==NULL?'value="'.$min.'"':'').'/> to '.
          '<input type="text" size="10" name="max_'.htmlspecialchars($this->getkey()).
          '" class="date" '.($max!==NULL?'value="'.$max.'"':'').'/> ';
 }

 /**
  * @brief Displays the 'Exact' option (which we don't use for date fields)
  * @return empty
  * @date 2011-07-11
  * @version 12
  **/
 public function displayExact() {
   return;
 }

  protected function valuefromrow(&$row) {
     $value=$row[$this->colname];
     if (is_null($value)) {
       return '';
     }
     $text_date=$this->dateAsString($value);
  return ($text_date);
 }

}
?>
