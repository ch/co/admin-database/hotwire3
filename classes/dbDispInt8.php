<?php
 /**
  * @brief A class with methods to display an Integer field.
  * Inherits everything from dbDispInt8 (BigInt) class
  * @author Frank Lee
  * @date 2011-07-11
  * @version 11
  **/
class dbDispInt8 extends dbDispInt4 {
}
?>
