<?php
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

 /** 
  * @file dbAjax.php
  * @brief Handles ajax requests
  * Part of Hotwire http://hotwire.sourceforge.net/. 
  */

 /**
  * @brief A class to handle ajax requests
  * @author Frank Lee
  * @date 2012-11-21
  * @version 11
  **/
class dbAjax {
 public $results=array(); // array of dbAjaxResult objects
 private $json;   // Posted JSON object
 private $index;  // Current object index
 
 /**
  * @brief To construct the handler for ajax requests
  * @author Frank Lee
  * @date 2012-11-24
  * @version 1
  **/
  public function __construct() {
   // Did we have a request?
   if (! array_key_exists('json',$_REQUEST)) {
    return $this->fail('No json data send to server');
   }
   // We had a request. Process it.
   $this->json=json_decode(stripslashes($_REQUEST['json']));
   if (is_array($this->json)) {
    for ($this->index=0; $this->index<count($this->json); $this->index++) {
     $this->handlerequest();
    }
   } 
  }

  /**
   * @brief To return failure from this object
   **/
  private function fail($msg) {
   $result=new dbAjaxResult;
   $result->fail($msg);
   return $result->JSON();
  }

  /** 
   * @brief Returns the current JSON object
   * @author Frank Lee
   * @date 2012-11-24
   * @version 1
   **/
  public function currentreq() {
   return $this->json[$this->index];
  }
  
  /**
   * @brief Handles the JSON request
   * @author Frank Lee
   * @date 2012-11-24
   * @version 1
   **/
  private function handlerequest() {
   $this->results[$this->index]=new dbAjaxResult;
   $req=$this->json[$this->index];
   if (!array_key_exists('action',$req)) {
    $this->results[$this->index]->fail('No action specified');
   }
   if (!array_key_exists('view',$req)) {
    $this->results[$this->index]->fail('No view specified for '.$req->action);
   }
   $_REQUEST['_view']=$req->view;
   $dummy=NULL;
   $view=new dbList($dummy,$req->view);
   switch($req->action) {
     case 'update':
       $this->ajaxupdate();
       break;
     case 'delete':
       $this->ajaxdelete();
       break;
     case 'insert':
       $this->ajaxinsert();
       break;
     case 'select':
       $this->ajaxselect();
       break;
     case 'search':
       $this->ajaxsearch();
       break;
     default:
       $this->results[$this->index]->fail('Unknown action '.$req->action);
   }
   //$this->results[$this->index]->succeed('Stub OK');
  }

 /**
  * @brief: Retrieves a particular value from the current AJAX request
  * @date 2012-11-24
  * @author Frank Lee
  * @version 1
  **/
  private function getfield($name,&$data) {
   if (!array_key_exists($name,$this->json[$this->index])) {
    return false;
   }
   $data=$this->json[$this->index]->$name;
   return true;
  }

 /** 
  * @brief: Handles a JSON update request
  * @date 2012-11-24
  * @author Frank Lee
  * @version 1
  **/
  private function ajaxupdate() {
    if (!$this->getfield('id',$id)) { 
      $this->currentfail('Need an ID field for update');
      return;
    }
    if (!$this->getfield('newrecord',$newrecord)) {
      $this->currentfail('Need a newrecord field for update');
      return;
    }
    $_REQUEST['_id[]']=$id;
    $_REQUEST['_action']='Update';
    $_REQUEST=array_merge($_REQUEST,$newrecord);
    $record=new dbRecord();
    $this->results[$this->index]=$record->ajaxresult();
  }

 /** 
  * @brief: Handles a JSON delete request
  * @date 2012-11-24
  * @author Frank Lee
  * @version 1
  **/
  private function ajaxdelete() {
    if (!$this->getfield('id',$id)) {
      $this->currentfail('Need an ID field for delete');
      return;
    }
    $_REQUEST['_id']=$id;
    $_REQUEST['_action']='Delete';
    $record=new dbRecord();
    $this->results[$this->index]=$record->ajaxresult();
  }

 /** 
  * @brief: Handles a JSON insert request
  * @date 2012-11-24
  * @author Frank Lee
  * @version 1
  **/
  private function ajaxinsert() {
    if (!$this->getfield('newrecord',$newrecord)) {
      $this->currentfail('Need a newrecord field for insert');
      return;
    }
    $_REQUEST['_id[]']=$id;
    $_REQUEST['_action']='Insert';
    $_REQUEST=array_merge($_REQUEST,$newrecord);
    $record=new dbRecord();
    $this->results[$this->index]=$record->ajaxresult();
  }

 /** 
  * @brief: Handles a JSON search request
  * @date 2012-11-24
  * @author Frank Lee
  * @version 1
  **/
  private function ajaxsearch() {
    if (!$this->getfield('search',$search)) {
      $this->currentfail('Need a search field for search');
      return;
    }
    $_REQUEST=$search;
    $_REQUEST['action']='search';
    $view=new dbList();
    $this->results[$this->index]=$view->ajaxresultsearch();
  }

 /** 
  * @brief: Handles a JSON update request
  * @date 2012-11-24
  * @author Frank Lee
  * @version 1
  **/
  private function ajaxselect() {
    if (!$this->getfield('id',$id)) {
      $this->currentfail('Need an id field for select');
    }
    if (!$this->getfield('fields',$fields)) {
      $this->currentfail('Need fields field for select');
    }
    $_REQUEST['_id[]']=$id;
    $_REQUEST['action']='search';
    $view=new dbList();
    $this->results[$this->index]=$view->ajaxresultselect($fields);
  }

  public function currentfail($msg) {
    $this->results[$this->index]->fail($msg);
  }

  public function currentsucceed($msg) {
    $this->results[$this->index]->succeed($msg);
  }

  public function JSON() {
   $results=array();
   for ($this->index=0; $this->index<count($this->json); $this->index++) {
    $results[$this->index]=$this->results[$this->index]->asphparray();
   }
   return json_encode($results);
  }
}
?>
