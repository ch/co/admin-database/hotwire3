<?php
 /**
  * @brief A class with methods to display a MAC address field
  * @author Frank Lee
  * @date 2011-07-11
  * @version 11
  **/
class dbDispMacaddr extends dbDispBase {

 /**
  * @brief Code placed in the HEAD of the page to load CSS/JS.
  * We use JS and CSS to provide a nice interface for MAC address lists
  * @return HTML for the HEAD to load appropriate JS files. 
  * @date 2011-07-11
  * @todo Bind the various click functions on whizzymacs by the macaddress.js script
  * @version 11
  **/
 public function prepareRecord(&$js,&$css) {
  $js[]="macaddress";
 }

 /**
  * @brief Displays an edit UI for MAC address fields.
  * @date 2011-07-11
  * @version 11
  * @return HTML input element for a form
  **/
 public function displayEdit() {
  if (substr($this->dbColumn->name(),0,1)=='_') { return;}
  $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
  $val=$this->getval();
  $key=$this->getkey();
  $jkey='MAC'.preg_replace('/[^A-Za-z0-9:_-]/', '_', $key);
  $ro = $this->ro;
  $result='<div class="whizzymac">';
  $mac=($val?explode(':',$val):'');
  $focusfs='';
  $changefs='';
  if (!$ro) {
   for ($i = 0; $i < 6; $i++) {
    $result.=($i ? ":" : "");
    $result.="<select id='$jkey"."_$i' name='$key" . "_$i'" . ($ro ? " readonly>" : ">")."\n";
    $result.="<option selected>".($mac?$mac[$i]:"--")."</option>\n"; 
    if ($_SESSION['javascript']) {
     $result.="</select>\n";
     //$focusfs.='$("#'.$jkey.'_'.$i.'").focus(function() {'.
               //"populatemac('".$jkey.'_'.$i."')});\n";
     //$changefs.='$("#'.$jkey.'_'.$i.'").change(function() {'.
               //"displaymac('".$jkey."')});\n";
    } else {
     // Build options server-side
     for ($j=0; $j<256; $j++) {
      $result.="<option ".(hexdec($mac[$i])==$j?'selected':'').">".sprintf('%02x',$j)."</option>\n";
     }
     $result.="</select>\n";

    }
   } 
  }
  if ($_SESSION['javascript']) {
  $result.="&nbsp;&nbsp;<input class='whizzymacinput' type=text id='".$jkey."_text' " .
            " value='".htmlentities($val)."' /></div>";

  //$result.="<script type='text/javascript'>\n".
           //'$(document).ready(function() {'."\n".
           //$focusfs."\n".
           //$changefs."\n".
           //'$("#'.$jkey.'_text").'.
           //"focus(function() {macboxfocus('".$jkey."')});\n".
           //'});'."\n".
           //"</script>\n";
  }
  return $result;
 }

}
?>
