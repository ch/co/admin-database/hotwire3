<?php
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

 /** 
  * @file dbNavBar.php
  * @brief To provide a class to display the navigation bar.
  * Part of Hotwire http://hotwire.sourceforge.net/.
  * @author Stuart Taylor
  * @author Frank Lee
  */

/**
 * @class dbNavBar
 * @brief Displays a header with menus, record selectors and the like.
 */
class dbNavBar extends htmlBase {
  private $dbResource; //!< @brief Object to hold the database record

  /**
   * @brief To create the dbNavBar object 
   * @param[in] dbObject Object representing the database connection (dbList or dbRecord)
   * @todo Move the constant definition elsewhere
   * @todo Add the pref['Default view'] link to this?
   * @todo Re-work the constructor routine to use functions for readability!
   * @date 2011-07-11
   * @version 11
   **/
  function __construct($dbObject) {
    $this->hotwireDebug(HW_DBG_OCRE,'Creation of a '.__CLASS__.' object');
    define("HOTWIREMENU",'90_Action'); // The menu under which logout/search/etc appears
    $this->dbResource=$dbObject;
    $viewMenu=array();
    $actionMenu=array();
    $reportMenu=array();
    ?>
 <!-- echoNav() begin -->
  <div id='menu'>
   <ul>
    <?php
// TODO: add the pref['Default view'] link here //
      if (property_exists($this->dbResource,'view') 
       && $this->dbResource->view) {
         // Display a refresh-form link
         $view=$this->dbResource->view;
         $jview=$this->domName($view);
         echo "<li><h2>".
              "<div class='pageTitle' id='hw_pageTitle'>".
              "<a href='list.php?".$this->dbResource->setGetVars(array('view'))."'>".
	      $this->dbResource->hdrFormat($this->dbResource->view)."</a>".
              '</div>'.
              "</h2></li>\n";
       } else {
         echo "<li>No current view</li>\n";
       }
     ?>
   </ul>

  <?php

  // Build the menu structure:
  $viewnames=array_unique(preg_replace('/\/\/.*/','//*',array_keys($this->dbResource->views)));
  
  $menuarray=array();
  $vs=array();
  foreach ($viewnames as $n) {
    $a=preg_replace('/\/\/./','',$n);
   $vs[$a]='list.php?_view='.htmlspecialchars($n);
  }
  // If we don't have HOTWIREMENU defined, these won't get displayed
  if (defined('HOTWIREMENU')) {
   $vs[HOTWIREMENU.'/99_Logout']='login.php';
   if (property_exists($this->dbResource,'view') && $this->dbResource->view) {
    $vs[HOTWIREMENU.'/99_Advanced Search']='edit.php?_view='.$this->dbResource->view.
                                  '&_action=search';
   }
   if (property_exists($this->dbResource,'action') &&
         $this->dbResource->action == 'search') {
# RFL - I think this is unused now.
    $vs[HOTWIREMENU.'/99_Amend Search']='edit.php?'.
                               $this->dbResource->setGetVars(array('action','view')).'&'.
                               $this->dbResource->getSearchVars();
   }
   if (property_exists($this->dbResource,'listClass') 
       && $this->dbResource->listClass == 'view' 
       && $this->dbResource->data) {
     $ids = array_keys(array_slice($this->dbResource->data,
                                   0+$this->dbResource->offset,
                                   0+$this->dbResource->pagelen,
                                   TRUE));
     $vs[HOTWIREMENU.'/99_Flip records']='list.php?'.
             $this->dbResource->setGetVars(array('view','action','filter')).'&'.
             '_id[]='.join('&_id[]=',$ids);
   }
   if (property_exists($this->dbResource,'listClass') 
    && ($this->dbResource->listClass != 'report') 
    && array_key_exists('insert',$this->dbResource->perms) 
    && $this->dbResource->perms['insert']) {
     $vs[HOTWIREMENU.'/99_New '.$this->dbResource->entity]='edit.php?_action=new&'.
                                              '_view='.$this->dbResource->view;  
   }

   if (in_array('csv', $_SESSION['dbroles']) &&
       is_a($this->dbResource, 'dbMetaList')) {  
    if (property_exists($this->dbResource,'view') && $this->dbResource->view) {
     $vs[HOTWIREMENU.'/99_Export CSV']='csv.php?_view='.$this->dbResource->view.'&'.$this->dbResource->setGetVars(array('pagelen'));
    }
    if (array_key_exists('insert',$this->dbResource->perms) &&
        $this->dbResource->perms['insert']) {
     $vs[HOTWIREMENU.'/99_Import CSV']='csvin.php?_view='.$this->dbResource->view;
    }
   }
  } // if defined(HOTWIREMENU)
      
  foreach ($vs as $k => $v) {
   $this->buildmenuarray(explode('/',$k),$v,$menuarray);
  }
  ksort($menuarray);

  $menulist=$this->buildmenuformat($menuarray);
  $data = array();

  print $menulist;
  // Search form
  if (isset($this->dbResource->view) 
   && !preg_match('/\bsearch/', $this->dbResource->view) 
   && !(is_a($this->dbResource, 'dbRecord') 
   && property_exists($this->dbResource,'action')
   && in_array($this->dbResource->action, array('new', 'search')))) { 
    echo "
    <ul class='menutopinactive' id='quicksearch'><li" . $this->navSearch() . "</li></ul>\n";
  }
  // Nav controls
  $navctrls=$this->navControls();
  if ($navctrls) {
   echo "<ul class='menutopinactive'><li>".$navctrls."</li></ul>\n";
  }

  // Create a URL for bookmarking
  $url='http';
  if (array_key_exists('HTTPS',$_SERVER) && 
      $_SERVER["HTTPS"] == "on") {$url .= "s";}
  $url.="://".$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME']."?";
  //
  $url.=$this->dbResource->setGetVars(array('view', 'pagelen', 'id',
                                            'filter', 'action','search'));
  $url.="&".$this->dbResource->getSearchVars();
  $title=$this->dbResource->getTitle();
  echo "<div class='right'>";
  echo " <ul class='menutopinactive'><li> <a class='bookmark' target='_blank'  href='$url'>Bookmark</a></li></ul>";
  echo "</div>";
  ?>
  </div><!-- id=menu -->
<?php if ($this->dbResource->msg) { ?>
  <div class='results'>
<?php
  // Present results of query if any
if (method_exists($this->dbResource,'calculateChecksum')) {
 $this->dbResource->calculateChecksum();
} else {
 print "No checksum method\n";
}
if (property_exists($this->dbResource,'checksum') &&
    $this->dbResource->pref('CHECKSUM')) {
?>
<?php
 print '<script type="text/javascript" src="js/checksum.js"></script>';
 ?>
<script type="text/javascript">
$(document).ready(function(){
    setTimeout(function() {
<?php if (property_exists($this->dbResource,'id')) {?>
        var id=["<?php print join('","',$this->dbResource->id);?>"];
<?php } else { ?>
        var id=null;
<?php } ?>
        comparechecksum('checksum','<?php print $this->dbResource->checksum;?>','<?php print $this->dbResource->view;?>',id);
    }, 1000);
    $('#checksum').hide();
});
</script>
<?php
 echo '<div id="checksum" class="checksum">';
 echo 'Javascript is not enabled. Unable to check for datachanges.';
 echo '</div>';
}
 echo $this->dbResource->msg; ?>

  </div>
<?php } ?>
  <div class='clearwidth'></div>
  <?php
  $this->hotwireDebug(HW_DBG_OCRE,"Ended creation of a ".get_class($this)." object",$this);
  }


  /**
   * @brief Produces some hidden input entities to save the search variables.
   * Saves the search variables as hidden html entities.
   * @todo Isn't there a method to create hidden inputs?
   * @date 2011-07-11
   * @version 11
   */
  private function _setSearchVars() {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__);
    $html = '';
    foreach (array_values(preg_grep('/^id$|^_|_hid$/',
                          array_keys($this->dbResource->colAttrs),
                          PREG_GREP_INVERT)) as $key) {
      if ($this->dbResource->colAttrs[$key][nodisplay]) {
        $html .= "<input type=hidden name='hde_$key' value=on>";
      }
      if ($this->dbResource->colAttrs[$key][exact]) {
        $html .= "<input type=hidden name='xct_$key' value=on>";
      }
      if ($this->dbResource->colAttrs[$key][exclude]) {
        $html .= "<input type=hidden name='xcl_$key' value=on>";
      }

    }
    return $html;
  }

  /**
   * @brief Generates the search box.
   * @todo Use a method to create hidden inputs and clean this up a bit
   * @date 2011-07-11
   * @version 11
   */
  private function navSearch() {
  $this->hotwireDebug(HW_DBG_OPRI,'Called [private] '.get_class($this).'->'.__METHOD__);
    $fields = array_values(preg_grep('/^id$|_oid$|\bmm_|^_/',
                           array_keys($this->dbResource->colAttrs), 
                           PREG_GREP_INVERT));
    # Replace any foo_id fields with foo_hid fields
    $fields = preg_replace('/(.*)_id$/','${1}_hid',$fields);
    # Replace any ro_foo fields with foo fields
    $fields = preg_replace('/ro_(.*)$/','${1}',$fields);
$this->echoDebug($fields,'FIELDS');
    $fields = array_unique($fields);
    #print "RFL DEBUG Fields are ".join(",",$fields);
    if ($this->dbResource->search) {
     $selected=$this->dbResource->search;
    } elseif (array_key_exists(0,$fields)) {
     $selected=$fields[0];
    } else {
     $selected=NULL;
    }
    
    $html = "<form name=qs_form action='list.php' method=get>".
      $this->dbResource->setPostVars(array('view', 'sort', 'pagelen')) .
      "<input type=hidden name='_action' value='search'>" .
      "<input type=hidden name='_pagelen' value='0'>".
      $this->genSelect('_search', $fields, $selected) . 
      "&nbsp;matches&nbsp;". 
      "<input type=text name='_filter' class='navSearch' " .
      "value='" .(property_exists($this->dbResource,'filter')?
                  $this->dbResource->filter:''). "'>" .
      "<input type=submit value='Search' class='btn'>&nbsp;" .
      $this->dbResource->setSearchVars() . "
     </form>";
     return $html;
  }

  /**
   * @brief Displays the navigation controls.
   * @todo Check that the nav controls retain sort and search info
   * @todo This is getting a bit unreadable...
   * @date 2011-07-11
   * @version 11
   */
  private function navControls() {
  $this->hotwireDebug(HW_DBG_OPRI,'Called [private] '.get_class($this).'->'.__METHOD__);
    $html = "";
    //   <div class='" . $this->dbResource->listClass . "NavItem'>
    # dbResource should have a "count" attribute
    $count = count($this->dbResource->data);
    if ($count > $this->dbResource->pagelen) {
    $html .= "
       <form name=nav_form action='" . (is_a($this->dbResource, 'dbRecord') 
          ? "edit" : "list") . ".php' method=post>";
      // only display page controls if we need to
      $html .=
         "<input type=hidden name='_offset' value='" .
         $this->dbResource->offset . "'>" .
         "<input type=submit value='&laquo;' " .
         "onClick=\"document.nav_form._offset.value=" .
         ($this->dbResource->offset - $this->dbResource->pagelen) . ";\"" .
         ($this->dbResource->offset ? "" : " disabled") . " class='btn'>" .
         (!is_a($this->dbResource, 'dbRecord')
           ? "<input type=submit value='All' " .
             "onClick=\"document.nav_form._pagelen.value='$count'; " .
             "document.nav_form._offset.value='0';\" class='btn'>" : "") .
         "<input type=submit value='&raquo;' " .
         "onClick=\"document.nav_form._offset.value=" .
         ($this->dbResource->offset + $this->dbResource->pagelen) . ";\"" .
         ($count <= $this->dbResource->offset + $this->dbResource->pagelen
           ? " disabled" : "") . " class='btn'>" .
         $this->dbResource->setPostVars(array('view', 'pagelen', 'id',
                                              'filter', 'action','search')) .
         $this->dbResource->setSearchVars();
    }

    if ($count > $this->dbResource->offset) {
      $html .= "&nbsp;&nbsp;[" . 
        ($count > 1 && $this->dbResource->pagelen > 1 ? "" : "") .
        ($this->dbResource->offset + 1) . 
        ($count > 1 && $this->dbResource->pagelen > 1
          ? "-" .
          ($count < ($this->dbResource->offset + $this->dbResource->pagelen) ?
           $count : ($this->dbResource->offset + $this->dbResource->pagelen))
          : "") . "] of $count" . 
        ((property_exists($this->dbResource,'action') 
         && $this->dbResource->action == 'search') 
         ? " (search results)" : "");
    }

    if ($html) {$html .= "\n         </form>";}
    return $html;
  }

  /**
   * @brief Creates a selection box from a list.
   * @todo Is this compatible with another, similarly named, routine?
   * @date 2011-07-11
   * @version 11
   */
  private function genSelect($name, $items, $selected, $sort = TRUE) {
    $this->hotwireDebug(HW_DBG_OPRI,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($name,$items,$selected,$sort));
    $html = "<select name=$name>";
    if ($sort) { sort ($items); }
    foreach ($items as $item) {
      $html .= "<option value=$item" . 
               ($item == $selected ? " selected>" : ">") .
               $this->dbResource->hdrFormat($item) . "</option>";
    }
    $html .= "</select>";
    return $html;
  }

  /**
   * @brief Build the menu array structure from a flat array
   * @param[in] path Path to menu item
   * @param[in] value Name of menu item
   * @param[out] r Pointer to array to contain tree structure
   * @date 2011-07-11
   * @version 11
   */  
  private function buildmenuarray($path,$value,&$r) {
    $this->hotwireDebug(HW_DBG_OPRI,'Called [private] '.get_class($this).'->'.__METHOD__,
                        array($path,$value,$r));
   $first=array_shift($path);
   if (count($path)==0) {
    $r[$first]=$value;
   } else {
    if (! array_key_exists($first,$r)) {
     $r[$first]=array();
    }
    $this->buildmenuarray($path,$value,$r[$first]);
   }
  }

  /**
   * @brief Returns the HTML equivalent of the menu array structure
   * @param[in] menuarray Array containing the menu
   * @param[in] top Boolean indicating we're at the top of the tree
   * @return HTML formatted nested-UL list representing menu
   * @date 2011-07-11
   * @version 11
   **/
  private function buildmenuformat($menuarray,$top=true) {
    $this->hotwireDebug(HW_DBG_OPRI,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($menuarray,$top));
    $formatted='';
    $class=($top?'menutop':'menu');
    foreach ($menuarray as $k =>$v) {
      $nicek=preg_replace('/^\d+_/','',$k);
      $nicek=$this->hdrFormat($nicek);
      if (is_array($v)) {
        ksort($v);
        $formatted.="<li class='item'>".$nicek.$this->buildmenuformat($v,false)."</li>\n";
      } else {
        $formatted.="<li class='item'><a href='$v'>".$nicek."</a></li>\n";
      }
    }
    return "<ul class='$class'>".$formatted."\n</ul>";
  }
 
}

?>
