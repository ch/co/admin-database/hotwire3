<?php
 /**
  * @brief A class with methods to display a numeric field.
  * Actually just inherits everything...
  * @author Frank Lee
  * @date 2011-07-11
  * @version 11
  **/
class dbDispBaseWide extends dbDispBase {

 public function preDisplayEdit(&$ui) {
  $ui[]='</div><!-- Exit itemdata -->';
  $ui[]='<div class="wideitemdata">';
  parent::preDisplayEdit($ui);
  #$ui[]='<div class="recordvalue">';
 }

 public function postDisplayEdit(&$ui) {
  parent::postDisplayEdit($ui);
  $ui[]='</div> <!-- class=wideitemdata -->';
  $ui[]='<div class="itemdata">';
 }

}
?>
