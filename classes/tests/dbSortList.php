<?php
 include 'tests/common.php';
 # Unit test for the dbSort class
 include 'dbSort.php';
 include 'dbSortList.php';

 # Instatiation: Complex
 $dbs=new dbSortList(array(
                 array('col'=>'ColumnName1','dir'=>true, 'nul'=>true),
                 array('col'=>'ColumnName2','dir'=>false, 'nul'=>true),
                ));

 $got=$dbs->asSQL();
 $expect='"ColumnName1" ASC NULLS FIRST,"ColumnName2" DESC NULLS LAST';
 ok('asSQL method',$got,$expect);

 $expect='_sort[]=ColumnName1&_order[]=ASC+NULLS+FIRST&_sort[]=ColumnName2&_order[]=DESC+NULLS+LAST';
 $got=$dbs->asGET();
 ok('asGET method',$got,$expect);

 $expect='<input type="hidden" name="_sort[]" value="ColumnName1"/><input type="hidden" name="_order[]" value="ASC NULLS FIRST"/><input type="hidden" name="_sort[]" value="ColumnName2"/><input type="hidden" name="_order[]" value="DESC NULLS LAST"/>';
 $got=$dbs->asPOST();
 ok('asPOST method',$got,$expect);
 
 $dbs=new dbSortList('person.surname, title_hid.title_hid, person.known_as');
 $expect='"surname" ASC,"title_hid" ASC,"known_as" ASC';
 $got=$dbs->asSQL();
 ok('Multi-Sort string splitting',$got,$expect);

 $dbs=new dbSortList('');
 $expect='';
 $got=$dbs->asSQL();
 ok('Empty string splittin',$got,$expect);

?>
