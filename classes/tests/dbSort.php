<?php
 # Unit test for the dbSort class
 include 'dbSort.php';
 include 'tests/common.php';

 # Instantiation: Simple
 $dbs=new dbSort(array('col'=>'ColumnName'));
 
 # Instatiation: Complex
 $dbs=new dbSort(array('col'=>'ColumnName2','dir'=>true, 'nul'=>true));

 $expect='"ColumnName2" ASC NULLS FIRST';
 $got=$dbs->asSQL();
 ok('asSQL method',$got,$expect);

 $expect='_sort[]=ColumnName2&_order[]=ASC+NULLS+FIRST';
 $got=$dbs->asGET();
 ok('asGET method',$got,$expect);

 $expect='<input type="hidden" name="_sort[]" value="ColumnName2"/><input type="hidden" name="_order[]" value="ASC NULLS FIRST"/>';
 $got=$dbs->asPOST();
 ok('asPOST method',$got,$expect);

 $dbs=new dbSort('');
 $expect=NULL;
 $got=$dbs->asSQL();
 ok('Parsing empty string',$got,$expect);

?>
