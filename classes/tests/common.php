<?php

function ok($test,$got,$expect) {
 print "$test:\n";
 if ($got===$expect) {
  print "Passed correctly\n";
 } elseif ($got==$expect) {
  print "Passed with a type conversion problem:\n";
  print "Expected argument with type ".gettype($expect)." but got ".gettype($got)."\n";
 } else {
  print "Failed.\nExpected '$expect' but got '$got'\n";
 }
}

?>
