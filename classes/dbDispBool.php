<?php
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

 /** 
  * @file dbDispBool.php
  * @brief Handles display of Boolean fields
  * Part of Hotwire http://hotwire.sourceforge.net/.
  * @author Stuart Taylor
  * @author Frank Lee
  */

 /**
  * @brief A class with basic methods to display a boolean field
  * @author Frank Lee
  * @date 2012-11-09
  * @version 12
  **/
class dbDispBool extends dbDispBase {
 /**
  * @brief Displays an edit control for the boolean field
  * @date 2011-07-09
  * @version 11
  **/
 public function displayEdit() {
  if (substr($this->dbColumn->name(),0,1)=='_') { return;}
 $val=$this->getval();
  return "<select name='".$this->getkey()."'" 
         . ($this->dbColumn->ro() ? " disabled>" : ">") .
         "<option value=''"
         .(($val===NULL || $val==='')?" selected>" : ">")
         ."-</option>" .
         "<option value='FALSE'" .
          (($val==='f' || $val==='FALSE')? " selected>" : ">") . "No</option>" .
         "<option value='TRUE'" .
          (($val==='t' || $val==='TRUE')? " selected>" : ">") . "Yes</option>" .
         "</select>";
 }

 /**
  * @brief Displays a search control for the boolean field
  * @date 2011-07-09
  * @version 11
  **/
 public function displaySearch() {
  $return="<select name='".htmlspecialchars($this->getkey())."'>" .
         "<option value=''>-</option>" .
         "<option value='FALSE'".
         (($this->getval()== 'FALSE' && $this->getval()!==NULL)?' selected >':' >').
          "No</option>" .
         "<option value='TRUE'".
         ($this->getval()=='TRUE'?' selected >':' >').
          "Yes</option>" .
         "</select>";
  return $return;
 }


 protected function valuefromrow(&$row) {
  $val=$row[$this->dbColumn->name()];
  switch (true) {
    case ($val===NULL):
      $return='';
      break;
    case ($val=='t'):
      $return='Yes';
      break;
    case ($val=='f'):
      $return='No';
      break;
    default:
      $return='??';
  }
  return $return;
  //return ($row[$this->dbColumn->name()]?'Yes':'No');
 }

}
?>
