<?php 
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

 /** 
  * @file dbNavBar.php
  * @brief Provides the data contained within the dbList object as a CSV.
  * Part of Hotwire http://hotwire.sourceforge.net/.
  * @author Stuart Taylor
  * @author Frank Lee
  */
 
 /**
  * @class dbCSV
  * @brief Provides a CSV representation of the data contained in a @ref dbList object
  * @date 2011-07-11
  * @version 11
  */
class dbCSV extends dbList {

  /**
   * @brief Dumps the data to the client as a CSV file.
   * @date 2011-10-24
   * @version 11
   **/
  public function display() {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
    $csv=array();
    $chunk=$this->data;
    $outstream = fopen("php://output",'w');
    // Output headers
    $headers=array();
    // Find booleans fields to remap, fields to hide
    $boolfields=array();
    $hidefields=array();
    foreach (array_keys($this->colAttrs) as $field) {
      if (preg_match('/_id$|^id$/',$field) ||
          array_key_exists('nodisplay',$this->colAttrs[$field])) {
        $hidefields[]=$field;
        continue; // Don't bother with the bool test
      } else {
       array_push($headers,$this->hdrFormat($field));
      }
      $this->echoDebug($this->colAttrs[$field],$field);
      if ($this->colAttrs[$field]['type']=='bool') {
        $boolfields[]=$field;
      }
    }
    fputcsv($outstream,$headers);
    foreach ($chunk as $row) {
      // Adjust boolean fields
      foreach ($boolfields as $bfield) {
        $row[$bfield]=($row[$bfield]=='t' ? 'Yes' : 'No');
      }
      // Remove hidden fields
      foreach ($hidefields as $hfield) {
        unset($row[$hfield]);
      }
      // Output row
      fputcsv($outstream,array_values($row));
    }
    fclose($outstream);
  }

}

?>
