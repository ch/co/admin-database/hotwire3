<?php
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

 /** 
  * @file dbAjaxResult.php
  * @brief Handles the result of an ajax request
  * Part of Hotwire http://hotwire.sourceforge.net/. 
  */

 /**
  * @brief A class with methods to return an Ajax result
  * @author Frank Lee
  * @date 2012-11-09
  * @version 12
  **/
class dbAjaxResult {

 private $result; // Boolean result. True=OK, false=fail
 private $resulttext; // Human readable result string
 private $id=NULL; // Array of returned IDs
 private $data=NULL; // Array of returned fields
 
 public function setdata($data) {
  $this->data=$data;
 }

 public function setid($id) {
  $this->id=$id;
 }

 /**
  *
  **/
 public function succeed($msg) {
  $this->result=true;
  $this->resulttext=$msg;
 }

 /**
  *
  **/
 public function fail($msg) {
  $this->result=false;
  $this->resulttext=$msg;
 }

 public function JSON() {
  $return=array();
  $return['result']=$this->result;
  $return['resulttext']=$this->resulttext;
  if ($this->data!==NULL) {
   $return['data']=$this->data;
  }
  if ($this->id!==NULL) {
   $return['id']=$this->id;
  }
  return json_encode($return);
 }

 public function asphparray() {
  $return=array('result'=>$this->result,
                'resulttext'=>$this->resulttext,
               );
  $return['data']=$this->data;
  $return['id']=$this->id;
  return $return;
 }

}
?>
