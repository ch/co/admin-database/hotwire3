<?php
 /**
  * @brief A class with methods to manage a list of sort objects
  * @author Frank Lee
  * @date 2012-11-12
  * @version 1
  **/
class dbSortList extends hwFunctions {

  private $sortobjs=array();
  private $fieldlist=array();
  private $sortasget;         // Cache

  public function __construct($args=NULL) {
   if (is_array($args)) {
    foreach ($args as $arg) {
      $o=new dbSort($arg);
      $this->sortobjs[]=$o;
      $this->fieldlist[]=$o->fieldname();
    }
   } elseif (! is_null($args)) {
    foreach (explode(',',trim($args)) as $arg) {
     if ($arg!='') {
      $o=new dbSort($arg);
      $this->fieldlist[]=$o->fieldname();
      $this->sortobjs[]=$o;
     }
    }
   }
  }

  // To return an array of dbSorts
  public function export() {
    return $this->sortobjs;
  }
 
  public function append($arg) {
    if (is_array($arg)) {
      foreach ($arg as $o) {
        if (get_class($o)!='dbSort') {
          $o=new dbSort($o);
        }
        $tfield=$o->fieldname();
        if (! in_array($tfield,$this->fieldlist)) {
          $this->fieldlist[]=$tfield;
          $this->sortobjs[]=$o;
        }

      }
    } else {
      $o=new dbSort($arg);
      $tfield=$o->fieldname();
      if(! in_array($tfield,$this->fieldlist)) {
        $this->fieldlist[]=$tfield;
        $this->sortobjs[]=$o;
      }
    }
    // Destroy cache
    $this->sortasget=null;
  }

  public function prepend($arg) {
    $sortobj=new dbSort($arg);
    array_push($this->sortobjs,$sortobj);
  }

  public function asGET() {
    if (is_null($this->sortasget)) {
      $html=array();
      foreach ($this->sortobjs as $obj) {
        $html[]=$obj->asGET();
      }
      $this->sortasget=$html;
    }
    return join('&',$this->sortasget);
  }

  public function asPOST() {
    $html=array();
    foreach ($this->sortobjs as $obj) {
      $html[]=$obj->asPOST();
    }
    return join('',$html);
  }

  public function asSQL() {
   $sql=array();
   foreach ($this->sortobjs as $obj) {
     $sql[]=$obj->asSQL();
   }
   return join(',',$sql);
  }

  public function nextAsGet($field) {
    $clauses=array();
    foreach ($this->sortobjs as $obj) {
      if ($obj->fieldname()==$field) {
        array_unshift($clauses,$obj->nextAsGet());
      } else {
        $clauses[]=$obj->asGET();
      }
    }
    return join('&',$clauses);
  }

  public function count() {
    return count($this->sortobjs);
  }

  public function fieldForJS() {
    $return=array();
    $i=1;
    foreach ($this->sortobjs as $obj) {
      $return[]='<li id="hw_sort_'.$i.'" class="hw_sort">';
      $return[]=$obj->fieldForJS();
      $i++;
    }
    return join("\n",$return);
  }

  /**
   * @brief Produces nice human-readable summary of sorting options.
   * @param[in] key Field to sort by
   * @date 2011-07-11
   * @version 11
   **/
  public function sortFormat($key) {
   //$this->hotwireDebug(HW_DBG_OPRI,'Called [public] '.get_class($this).'->'.__METHOD__,
                       //array($key));
   $sortinfo='&nbsp;';
   // Loop over all sort objects in case we have one of this type
   foreach ($this->sortobjs as $obj) {
     if ($key==$obj->fieldname()) {
       $sortinfo.='<span class="sortarrow">'.$obj->directionAsArrow()."</span>";
     }
   }
   $field=$key;
   $return='<span class="columnname">'.$this->hdrFormat($key).'</span>';
   return $return.$sortinfo;
  }


}
?>
