<?php
 // $Id$
 
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

 /* @file dbBase.php
  * @brief To provide a class with database interaction
  * dbBase - an extension of htmlBase  
  * Part of Hotwire http://hotwire.sourceforge.net/.
  * @author Stuart Taylor
  * @author Frank Lee
  * @date 2009-2011
  */

 /**
  * @class dbBase
  * @brief Provides access to the database. 
  */
class dbBase extends htmlBase {

  public $dbh;              //!<@brief handle to the database
  public $hwschema;         //!<@brief The schema Hotwire finds its configuration in
  public $views = array(); //!<@brief Array of views we're allowed to use
  public $post = array();  //!<@brief data sent back by the browser
  public $prefs = array(); //!<@brief User preferences
  public $sort = array();  //!<@brief How to sort the data
  public $usortobjs = array(); //!<@brief A list of sort objects set by the user
  public $vsortobjs = array(); //!<@brief A list of sort objects set by the view
  public $perms = array(); //!<@brief Permissions on the current view
  public $roles= array();  //!<@brief Roles the user has
  public $defsort = array(); //!<@brief Sort as defined by the view
  public $connected = false; //!<@brief Are we connected to the database?
  public $order;
  public $action;

  /** 
   * @brief Connects to the database, obtains any form results, obtains the available views.
   * @date 2011-07-11
   * @version 10
   */
  public function __construct() {
    $this->hwDebugCreateStart();
    $this->getPostVars();
    $this->dbInit(); 
    if ($this->connected) {
      $this->loadPreferences();
      $this->getViews();
    }
    $this->hwDebugCreateStop();
  }

  /**
   * @brief Obtains any form results.
   * Forms (often sent through javascript) encode the information required by Hotwire
   * to determine which view to display, which fields should be shown etc. In fact, it's
   * the user-interface part of this user interface.
   * @date 2012-12-09
   * @version 12
   */
  public function getPostVars() {
    $this->hwDebugPubStart(__METHOD__);
    $request=$this->getRealRequest();
    # Parse $request['sort'] into $this->usortobjs;
    $this->parseSortObjs($request);
    $this->echoDebug($request,'Posted variables are');
    foreach (array_keys($request) as $key) {
      if ($key==session_name()) { continue;} // Skip any posted session data
      if (preg_match('/^_/', $key)) {
       # The namespace starting with _ indicates information for Hotwire
       if ($key=='_action') {
        // Rewrite some actions so we can use friendly-named buttons.
        if ($request['_action']==constant("BTN_MSG_CREATE")) { $request['_action']='insert';}
        if ($request['_action']==constant("BTN_MSG_UPDATE")) { $request['_action']='update';}
        if ($request['_action']==constant("BTN_MSG_UPDATE_SHOW_NEXT")) { $request['_action']='update';}
        if ($request['_action']==constant("BTN_MSG_DELETE")) { $request['_action']='delete';}
        if ($request['_action']==constant("BTN_MSG_SEARCH")) { $request['_action']='search';}
       }
       $this->{ltrim($key,'_')} = $request[$key];
      } else {
       $this->post[$key] = $request[$key];
      }
    }
    // Explicitly sanitize a couple of items
    if (property_exists($this,'offset') && $this->{'offset'}!==null) {
      $this->{'offset'}=filter_var($this->{'offset'},FILTER_SANITIZE_NUMBER_INT);
    }
    if (property_exists($this,'pagelen') && $this->{'pagelen'}!==null) {
      $this->{'pagelen'}=filter_var($this->{'pagelen'},FILTER_SANITIZE_NUMBER_INT);
    }
    // If a new _debugval was sent, use it. If not, use saved value
    if (array_key_exists('_debugval',$request)) { 
      $_SESSION['debugval']=$request['_debugval'];
      $GLOBALS['debugval']=$_SESSION['debugval'];
    } else {
      if (array_key_exists('debugval',$_SESSION)) {
      $this->post['_debugval']=$_SESSION['debugval'];
      $GLOBALS['debugval']=$_SESSION['debugval'];
      } else {
        $GLOBALS['debugval']=array();
      }
    }
    // Likewise for debugobj
    if (array_key_exists('_debugobj',$request)) {
      $_SESSION['debugobj']=$request['_debugobj'];
      $GLOBALS['debugobj']=$_SESSION['debugobj'];
    } else {
      if (array_key_exists('debugobj',$_SESSION)) {
      $this->post['_debugobj']=$_SESSION['debugobj'];
      $GLOBALS['debugobj']=$_SESSION['debugobj'];
      } else {
        $GLOBALS['debugval']=array();
      }
    }
    # Sanitise $this->sort;
    $this->sanitiseSort();
    if (!empty($this->filter)) {
      // RFL: remove trailing space on filter
      $this->filter=rtrim($this->filter);
      $this->post[$this->search] = $this->filter;
    }
    $this->hwDebugPubStop(__METHOD__,$this->post);
  }

  /** 
   *@brief Parse $this->sort into objects
   *@todo - this seems to be called twice
   **/
  private function parseSortObjs(&$request) {
   $sort=(array_key_exists('_sort',$request)?$request['_sort']:array());
   $orde=(array_key_exists('_order',$request)?$request['_order']:array());
   $this->usortobjs=new dbSortList;
   for ($i=0;$i<min(count($sort),count($orde));$i++) {
     $this->usortobjs->append($sort[$i].' '.$orde[$i]);
   }
  }

  /**
   * @brief Fix up POST/GET variable names.
   * According to http://php.net/manual/en/language.variables.external.php
   * PHP replaces various characters with underscores. This is unhelpful in 
   * our use case.
   * @date 2011-07-25
   * @version 1
   **/
  protected function getRealRequest() {
    $this->hwDebugProStart(__METHOD__);
    # php://input doesn't work for multipart/form-data, so we work around that
    if (array_key_exists('CONTENT_TYPE',$_SERVER) 
     && substr($_SERVER['CONTENT_TYPE'],0,20)=='multipart/form-data;') {
      foreach ($_POST as $key=>$value) {
        // Don't Explicitly remove empty arrays /unless/ we're searching.
        // if (array(0=>'')!==$value) {
        if (!(array_key_exists('_action',$_POST) && $_POST['_action']==constant("BTN_MSG_SEARCH"))
         || ((array(0=>'')!==$value))) {
          $vars[$key]=$value;
        }
        // }
        // How else are we to indicate that the column is empty?
      }     
    } else {
      # This is better for if we might end up renaming fields.
      $pairs = explode("&", file_get_contents("php://input"));
      $vars = array();
      # $_COOKIES includes stuff about column widths, which we don't want.
      if ($_POST) {
        $vars=$_POST;
      }
      foreach ($pairs as $pair) {
        $nv = explode("=", $pair);
        if (array_key_exists(1,$nv)) {
          $name = urldecode($nv[0]);
          $value = urldecode($nv[1]);
          $vars[$name] = $value;
        }
      }
    }
    # And now process any GET variables: We do this by hand 'cos PHP removes 
    # spaces. Bad PHP!
    if (gettype($_SERVER) !== "undefined" && array_key_exists('QUERY_STRING',$_SERVER)) {
      $pairs=explode('&',$_SERVER['QUERY_STRING']);
    } else {
      $pairs=array();
    }
    foreach ($pairs as $pair) {
      if (strpos($pair,'=')!==FALSE) {
        list($k, $v) = array_map("urldecode", explode("=", $pair));
      } else {
        $k=$pair;
        $v=NULL;
      }
      // '[]' is taken as the end of a variable name. 
      if (strpos($k,'[]')!==FALSE) {
        $k=preg_replace('/\[\].*/','',$k);
        $vars[$k][]=$v;
      } else {
        $vars[$k] = $v;
      }
    } 
    $this->hwDebugProStop(__METHOD__);
    return $vars;
  }

  /** 
   * @brief Sanitise sorting arguments.
   * Ensure that we're sorting by ASC or DESC, nothing else.
   * @date 2011-07-11
   * @version 11
   */
  private function sanitiseSort() {
    $this->hwDebugPriStart(__METHOD__);
    if (!is_array($this->sort)) {
      $this->sort=array($this->sort);
    }
    $fixedsort=array();
    //foreach ($this->sort as $sort) {
    for ($i=0;$i<count($this->sort);$i++) {
      $sort=$this->sort[$i];
      $sort=rtrim($sort);
      preg_match('/^(.*?) {0,1}([ADE]+SC)?$/',$sort,$matches);
      if (array_key_exists(1,$matches) && $matches[1]) {
        $field=$matches[1];
        if (array_key_exists(2,$matches)) {
         $order=$matches[2];
        } else {
          if (array_key_exists($i,$this->order) &&
              ((strtoupper($this->order[$i])=='ASC') ||
               (strtoupper($this->order[$i])=='DESC'))) {
            $order=strtoupper($this->order[$i]);
          }
        }
      }
    }
    $this->sort=$fixedsort;
    $this->hwDebugPriStop(__METHOD__);
  }

 /**
  * @brief Determines whether a given table, schema or field exists
  * @date 2011-08-08
  * @version 1
  **/
  protected function database_entity_exists($schema,$table=NULL,$column=NULL) {
    $this->hwDebugProStart(__METHOD__);
    $sql='select count(*) from information_schema.columns where table_schema=$1';
    $args=array($schema);
    if ($table) {
      $sql.=' AND table_name=$2';
      $args[]=$table;
      if ($column) {
        $sql.=' AND column_name=$3';
        $args[]=$column;  
      }
    }
    $result=pg_query_params($this->dbh,$sql,$args);
    $count=NULL;
    if ($result) {
      $count=pg_fetch_result($result,0,0);
    }
    $this->hwDebugProStop(__METHOD__);
    return $count>0;
  }

 /** 
  * @brief Obtains the permissions for the view, stores them in appropriate 
  * properties of this object.
  * The constant 'WARNRULES' may be set to true to report on which permissions
  * are being granted.
  * @todo replace 'WARNRULES' by a user-option
  * @date 2012-11-09
  * @version 12
  */
  protected function getPerms() {
    // If we have no view, this is all a bit meaningless.
    if (!$this->view) { return ; }
    if (!array_key_exists($this->view,$this->views)) { return; }
    if (!defined('WARNRULES')) { 
     define('WARNRULES',false);
    }
    $this->hwDebugProStart(__METHOD__);
    // Transfer permissions into this object
    $this->perms['select']=true; // Kind of by definition of _view_data
    $permlist=array('insert','update','delete');
    foreach ($permlist as $perm) {
      $this->perms[$perm]=$this->views[$this->view][$perm];
      if (WARNRULES && $this->perms[$perm] && ! $this->perms[$perm.'_rule']) {
        print "<p class='error'>Warning: you have $perm rights on this view "
             . "but there is no $perm rule. Ask your system administrator to "
             . "write one for ".$this->view."</p>\n";
        $this->perms[$perm]=false;
      }
    }
    $this->hwDebugProStop(__METHOD__);
  }

  /**
   * @brief Load the name of the hotwire schema
  **/
  protected function getHWSchema() {
   if (array_key_exists('hwschema',$_SESSION) && $_SESSION['hwschema']) {
    $this->hwschema=$_SESSION['hwschema'];
    return;
   }
   if (array_key_exists('PSQL_HOTWIRE_SCHEMA',$GLOBALS)){
    $_SESSION['hwschema']=$GLOBALS['PSQL_HOTWIRE_SCHEMA'];
   } else {
    $_SESSION['hwschema']='hotwire';
   }
   $this->hwschema=$_SESSION['hwschema'];
  }

  /** 
   * @brief Load preferences from the back-end.
   * Preferences are stored in the hotwire schema. Load them into a hash.
   * @date 2011-07-11
   * @version 11
   **/
  protected function loadPreferences() {
   // Swiftly and silently return if cached
   if (array_key_exists('prefs',$_SESSION) && $_SESSION['prefs']) {
    $this->prefs=$_SESSION['prefs'];return;
   }
   $this->hwDebugProStart(__METHOD__);
   // Ignore absence of hotwire._preferences view
   if (! $this->viewExists('_preferences',$this->hwschema)) {return;}
   if (! $this->dbh) {
    $this->echoDebug('','DBH not set');
   }
   $sql='select preference_name, preference_value, '.
        'preference_type from '.
        $this->hwschema.
        '._preferences';
   $start=microtime(true);
   $dbQuery=$this->sqlexec(get_class($this),__METHOD__,HW_DBG_DBVI,$sql);
   if ($dbQuery===FALSE) {$this->sqlError('loading users preferences',$sql);};
   while ($row=pg_fetch_assoc($dbQuery)) {
    if ($row['preference_type']=='boolean') {
     #$row['preference_value']=preg_replace('/FALSE/i',false,
                                           #$row['preference_value']);
     $this->prefs[$row['preference_name']]=($row['preference_value']=='TRUE');
     settype($this->prefs[$row['preference_name']],$row['preference_type']);
    } else {
     $this->prefs[$row['preference_name']]=$row['preference_value'];
    }
   }
   $end=microtime(true);
   global $hw_sql_timing;
   $hw_sql_timing[$sql]=($end-$start)/1000;
   $_SESSION['prefs']=$this->prefs;
   $this->hwDebugProStop(__METHOD__);
  }

  /**
   * @brief Figure out a default view if none already exists.
   * @date 2012-11-09
   * @version 12
   */
  protected function getDefaultView() {   
    $this->hwDebugProStart(__METHOD__);
    if (array_key_exists('DEFVIEW',$this->prefs)) {
      $_SESSION['defaultview']=$this->prefs['DEFVIEW'];
    }
    $this->hwDebugProStop(__METHOD__);
  }

  /**
   * @brief Convert POSTed variables back to HTML for inclusion in a form.
   * @param[in] vars An array of variable names to include in the form
   * @param[in] prefix A string prefix to add to the names.
   * @return HTML input elements to include in a form.
   * @date 2011-07-11
   * @version 11
   */
  public function setPostVars($vars,$prefix="") {
    $this->hwDebugPubStart(__METHOD__,array($vars,$prefix));
    if (!is_array($vars)) { return; }
    $html = '';
    foreach ($vars as $var) {
      if (!property_exists($this,$var)) { continue ; }
      $html.=$this->hiddeninput('_'.$prefix.$var,$this->$var);
      // Send back the previously posted variables as well
      if ($var == 'action' && $this->propertyEqual('action','search')) {
        foreach (array_keys($this->post) as $key) {
          $html.=$this->hiddeninput($key,$this->post[$key]);
        }
      }
    }
    $this->hwDebugPubStop(__METHOD__);
    return $html;
  }

  /**
   * @brief to generate a hidden input.
   * If value is an array, we generate lots of them
   * @param[in] name The name of the input.
   * @param[in] value The value of the input
   * @param[in] id An optional array giving an ID element
   * @return HTML elements to include in a form.
   * @date 2011-07-11
   * @version 11
   **/
  protected function hiddenInput($name,$value,$id=NULL) {
   $input='';
   if (is_array($value)) {
    $keys=array_keys($value);
    for ($i=0;$i<count($value);$i++) {
     $key=$keys[$i];
     $idfield=(is_array($id) && array_key_exists($id,$key)?
               $id[$key]:$name);
     $input.=$this->hiddeninputscalar($name.'[]',$value[$key],$idfield);
    }
   } else {
     $input.=$this->hiddeninputscalar($name,$value,$id);
   }
   return $input;
  }

  /**
   * @brief To generate a hidden input for scalar data
   * @param[in] name The name of the input.
   * @param[in] val The value of the input
   * @param[in] id An optional string giving an ID element
   * @return HTML element to include in a form
   * @date 2011-07-11
   * @version 11
   **/
  protected function hiddeninputscalar($name,$val,$id=NULL) {
//    $this->hwDebugPriStart(__METHOD__,array($name,$val,$id));
    return "<input type='hidden' name='$name' ".
           ($id !== NULL ? "id='".$this->domName($id)."' ":'').
           "value='".htmlspecialchars($val)."'/>\n";
  }

  /**
   * @brief To generate a submit input - only scalar data
   * @param[in] name The name of the input
   * @param[in] val The value of the input
   * @param[in] id An optional string giving an ID element
   * @return HTML element to include in a form
   * @date 2011-12-13
   * @version 1
   **/

  public function submitInput($name,$value,$id=NULL) {
//    $this->hwDebugPriStart(__METHOD__,array($name,$val,$id));
    return "<input type='submit' name='$name'".
           ($id !== NULL ? "id='".$this->domName($id)."' ":'').
           "value='".htmlspecialchars($val)."'>\n";
  }

  /**
   * @brief To generate a GET string equivalent to the POST form used elsewhere.
   * @param[in] vars An array of variable names to send
   * @return URL encoded representation of the variables.
   * @date 2011-07-11
   * @version 11
   */
  public function setGetVars($vars) {
    $this->hwDebugPubStart(__METHOD__,array($vars));
    $url=array();
    if (is_array($vars)) {
      foreach ($vars as $var) {
        // Don't encode empty variables.
        if (property_exists($this,$var) && isset( $this->$var)) { 
         $url[]="_$var=".urlencode((is_array($this->$var) ?
                                   implode(',', $this->$var) :
                                   $this->$var));
        }
        if ($var == 'action' && $this->propertyEqual($var,'search')) {
          //property_exists($this,$var) && $this->$var == 'search') {
          foreach ($this->post as $key => $value) {
            // Don't encode empty values;
            if ($value) {
             if (is_array($value)) {
              $url[]=urlencode($key).'[]='.join('&'.urlencode($key).'[]=',$value);
             } else {
              $url[]=urlencode($key).'='.urlencode($value);
             }
            }
          }
        }
      }
    }
    $this->hwDebugPubStop(__METHOD__);
    return join('&',$url);
  }


 /**
  * @brief To create arguments to a URL from an array of data
  * @param[in] vars An array of variables to encode
  * @return string to be appended to a GET URL
  * @date 2011-12-13
  * @version 11
  **/
  public function buildGetArgs($vars) {
    $this->hwDebugPubStart(__METHOD__,array($vars));
    $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__,
                        array($vars)); 
    $urls=array();
    if (! is_array($vars)) { return;}
    foreach ($vars as $key=>$value) {
      if (is_array($value)) {
        $urls[]=urlencode($key).'[]='.join('&'.urlencode($key).'[]=',$value);
      } else {
        $urls[]=urlencode($key).'='.urlencode($value);
      }
    }
    $this->hwDebugPubStop(__METHOD__);
    return join('&',$urls);
  }

  /**
   * @brief To get an array of properties from the object
   * @param[in] vars An array of variable names to get
   * @return array of variable names to corresponding values.
   * @date 2011-12-13
   * @version 11
   **/
  public function getVars($vars) {
    $this->hwDebugPubStart(__METHOD__,array($vars));
    $results=array();
    if (is_array($vars)) {
      foreach ($vars as $var) {
        if (property_exists($this,$var) && $this->$var) {
          $results['_'.$var]=$this->$var;
        }
        if ($var == 'action' && $this->propertyEqual('action','search')) {
          foreach ($this->post as $key=>$value) {
            if ($value) {
              $results[$key]=$value;
            }
          }
        }
      }
    }
    $this->hwDebugPubStop(__METHOD__);
    return $results;
  }

  /**
   * @brief To generate a nice, fluffy title for bookmarking.
   * Bookmarks may now be used to save a particular view / search. 
   * By generating a user-friendly name, we hope to prevent saving multiple
   * bookmarks with similar-looking names.
   * @return URL for the page to bookmark
   * @todo: hdrFormat similarity?
   * @date 2011-07-11
   * @version 11
   */
  public function getTitle() {
    $this->hwDebugPubStart(__METHOD__);
    $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
    if (!property_exists($this,'view')) { return; } ;
    $title=$this->hdrFormat(str_replace("_view","",$this->view));
    if (property_exists($this,'id') && $this->id) {
      // Beware of empty arrays
      if ((! is_array($this->id)) || implode(',',$this->id)) {
        $title.=" (id ".(is_array($this->id)?implode(',',$this->id):$this->id).")";
      }
    }
    if ($this->search && $this->filter) {
      $title.=" (".str_replace("_"," ",str_replace("_hid","",$this->search))." ".$this->filter.")";
    }
    if ($this->propertyEqual('action','search')) {
      //property_exists($this,"action") && $this->action=='search') {
      $title.=" (";
      foreach ($this->post as $key => $value) {
        // Don't list empty values;
        if ($value) {
          ## TODO: isn't this just $this->hdrFormat?
          $title.=str_replace("_"," ",str_replace("_hid","",str_replace("so_","",$key)))." '".
                  htmlentities(((is_array($value)?implode(',',$value):$value)))."', ";
        }
      }
      $title=rtrim($title," ,").")";
    }
    $this->hwDebugPubStop(__METHOD__);
    return $title;
  }

  /**
   * @brief Determines whether the user has authenticated against the database
   * Returns a true value if the user can connect to the daabase correctly.
   * @return True if we've authenticated, false otherwise.
   * @date 2011-07-11
   * @version 11 
   */
  public function authenticate() {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
    if (property_exists($this,'dbuser') && property_exists($this,'dbpasswd') &&
        $this->dbuser && $this->dbpasswd) {
      session_start();
      $uids = explode('/', $this->dbuser);

      $_SESSION['dbuser'] = array_shift($uids);
      $_SESSION['dbsetuid'] = array_shift($uids);

      $_SESSION['dbpasswd'] = $this->dbpasswd;
      $_SESSION['dbhost'] = $GLOBALS['PSQL_HOST'];
      $_SESSION['dbname'] = $GLOBALS['PSQL_DB'];
      $_SESSION['javascript']=($_REQUEST['hw_javascript']=='true');

      if ($this->dbInit()) {
        return 1;
      } else {
        ini_set('session.gc_max_lifetime', 0);
        ini_set('session.gc_probability', 1);
        ini_set('session.gc_divisor', 1);
        session_unset();
        session_destroy();
        $this->dbh->disconnect;
        return 0;
      }
    }

    return 0;

  }

  /**
   * @brief Connects to the database
   * Uses the session variables already set to attempt a connection to the database.
   * @return 1 for success, 0 for failure.
   * @date 2011-07-11
   * @version 11
   */
  public function dbInit() {
    $return=0;
    $this->dbConnect();
    if ($this->dbh != FALSE) {
     $this->connected=(pg_connection_status($this->dbh)===PGSQL_CONNECTION_OK);
     $return=1;
    }
    if ($this->connected) {
      $this->getHWSchema();
      $this->getRoles();
      $this->getDefaultView();
    }
    $this->hwDebugPubStop(__METHOD__,$return);
    return $return;
  }

  /**
   * @brief Find out which roles the user can fulfill.
   * Each user may hold a number of roles. Enumerate them! 
   * Sets the 'roles' property in the object and the _SESSION['dbroles'] 
   * variable with the roles this user has.
   * @date 2011-07-11
   * @version 11
   */
  protected function getRoles() {
    if ($_SESSION['dbroles']) {$this->roles=$_SESSION['dbroles'];return;};
    $start=microtime(true);
    $sql = "SELECT role FROM ".$this->hwschema."._role_data WHERE member=current_user";
    $dbQuery=$this->sqlexec(get_class($this),__METHOD__,HW_DBG_DBVI,$sql);
    if (!$dbQuery) {$this->sqlerror('getting list of roles the user has',$sql);}
    $this->roles = pg_fetch_all_columns($dbQuery);
    $end=microtime(true);
    global $hw_sql_timing;
    $hw_sql_timing[$sql]=($end-$start)/1000;
    $_SESSION['dbroles'] = $this->roles;
  }

  /**
   * @brief Test if a user has a role
   * @date 2018-07-26
   * @author RFL
   * @version 1
   */
  protected function hasRole($role) {
    return array_search($role,$this->roles)!==FALSE;
  }

  /**
   * @brief To execute some SQL sending optional debugging info back
   * @param[in] obj  The class of object calling this
   * @param[in] meth The method of the object calling this
   * @param[in] dbug The class of query for debugging
   * @param[in] sql  The SQL to execute
   * @param[in] args An array containing any arguments to that SQL
   * @return HTML element to include in a form
   * @date 2011-12-13
   * @version 1
   */
  public function sqlexec($obj,$meth,$dbug,$sql,$args=NULL) {
   if (pg_connection_status($this->dbh)!==PGSQL_CONNECTION_OK) {
     print "SQL connection bad: $this->dbh\n";
     return;
   }
   //$this->hwDebugPubStart(__METHOD__,array($sql,$args));
   $dbstart=microtime(true);
   if (is_array($args) && count($args)>=1) {
     $result=pg_query_params($this->dbh, $sql, $args);

   } else {
     $result=pg_query($this->dbh,$sql);
   }
   /*$dbend=microtime(true);
   if (array_key_exists('debugval',$_SESSION)
    && is_array($_SESSION['debugval'])
    && array_search($dbug,$_SESSION['debugval'])!==false) {
     $this->hotwireDebug($dbug,$obj.'->'.$meth.' SQL: '.sprintf('%ums',round(1000*($dbend-$dbstart))),$this->nicesql($sql,$args));
   }
   $this->hwDebugPubStop(__METHOD__);
   */
   return $result;
  }

  /**
   * @brief Get a list of views available to this user
   * Sets the 'views' property of this object with the results 
   * @date 2011-07-11
   * @version 11
   */
  protected function getViews() {
    $this->hwDebugProStart(__METHOD__);
   //$this->hotwireDebug(HW_DBG_OPRO,'Called [protected] '.get_class($this).'->'.__METHOD__);
   if (pg_connection_status($this->dbh) !== PGSQL_CONNECTION_OK) {$this->hwDebugProStop(__METHOD__,array('No Conn'));return ; }
   // Only display views we can select from...
   if (array_key_exists('views',$_SESSION) && $_SESSION['views']  && $this->pref('CACHEMENU')) { 
     $this->views=$_SESSION['views']; 
     $this->hwDebugProStop(__METHOD__);
     return;
   }
   $sql='SELECT * from '.$this->hwschema.'._view_data where "select"';
   $start=microtime(true);
   $dbQuery=$this->sqlexec(get_class($this),__METHOD__,HW_DBG_DBVI,$sql);
   # Take the default 
   if (!$dbQuery) { $this->sqlError("looking up the list of views",$sql);}
   while ($row=pg_fetch_assoc($dbQuery)) {
    $this->views[$row['view']]=array('schema'=>$row['schema'],
                                      'primary_table'=>(array_key_exists('primary_table',$row)?$row['primary_table']:NULL),
				      # RFL thinks that we don't actually use this property anywhere...
                                      #'insert_returning'=>($row['insert_returning']=='t'),
                                      'insert'=>((array_key_exists('insert',$row)?$row['insert']:NULL)=='t'),
                                      'insert_rule'=>((array_key_exists('insert_role',$row)?$row['insert_rule']:NULL)=='t'),
                                      'delete'=>((array_key_exists('delete',$row)?$row['delete']:NULL)=='t'),
                                      'update'=>((array_key_exists('update',$row)?$row['update']:NULL)=='t'),
                                      'update_rule'=>((array_key_exists('update_rule',$row)?$row['update_rule']:NULL)=='t'),
                                      'default_sort'=>(array_key_exists('order_by',$row)?$row['order_by']:NULL),
                                     );
    // Store the string, convert the string into parsers later on
    $this->views[$row['view']]['schema']=$row['schema'];
    $this->views[$row['view']]['entity']=(array_key_exists('primary_table',$row)?$row['primary_table']:NULL);
	# TODO - parse the sorting fields more correctly
    // RFL thinks that this code is now redundant.
if (false) {
    $row['definition']=preg_replace('/\).*/','',$row['definition']);
    if (preg_match('/ORDER BY\s*?(\b.+\b)\s*?(LIMIT|OFFSET|FOR|\))?/', 
                   $row['definition'], $match)) {
     $orderby = explode(',', $match[1]);
     $sort = array();
     foreach ($orderby as $order) {
      $ordarr=explode(' ',$order);
      $dir=(isset($ordarr[1])?$ordarr[1]:NULL);
      $order=$ordarr[0];
      // deal with explode weirdness
      if (empty($order)) { $order = $dir; unset ($dir); $dir=""; }
      if (preg_match("/".preg_quote($order,'/')."\s+AS\s+(\w+)/", 
                         preg_quote($row['definition'],'/'), $match)) {
       $sort[] = '"'.$row['view'] . '"."' . $match[1] . '"'." $dir";
      } else {
       $order = preg_replace('/\w+\./', '', $order);
       $sort[] = '"'.$row['view'] . '"."' . $order . '"'." $dir";
      }
     }
    }  
}
    // End redundancy
   }
   $end=microtime(true);
   global $hw_sql_timing;
   $hw_sql_timing[$sql]=($end-$start)/1000;

   $_SESSION['views']=$this->views;
    $this->hwDebugPubStop(__METHOD__);
  }

  /**
   * @brief Parses an "Order by ..." fragment into an array 
   * @author Frank Lee
   * @version 1
   * @date 2012-11-09
   **/
  protected function parse_order_by($sort) {
   $sort=trim($sort);
   if ($sort!='') {
    $sortlist=new dbSortList($sort);
    return $sortlist;
   } else {
    return null;
   }
  }
  
  /**
   * @brief Change a PHP array into a PG array
   * @return A string representation of the array, compatible with postgres
   * @date 2011-11-09
   * @version 12
   */
  public function phpArrayToPG($phparr) {
    //$this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__,
    //                   array($phparr));
    // Be uncharacteristically forgiving: Assume that people calling this on a non-array have
    // the conversion themselves.
    if (is_array($phparr)) {
        return '{'.implode(',',$phparr).'}';
    } else {
        return $phparr;
    }
  }

  /**
   * @brief Change a db array into a PHP array
   * @param $dbarr String representing the DB array
   * @return A PHP array
   * Shamelessly taken from Chris KL's posting at http://php.net/manual/en/ref.pgsql.php
   * @date 2011-07-11
   * @version 11
   */
  public function phpArray($dbarr) {
    //$this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__,
    //                    array($dbarr));
    // Take off the first and last characters (the braces)
    $arr = substr($dbarr, 1, strlen($dbarr) - 2);
    // Hotwire: if the array is empty, return NULL
    if ($arr=='') {
     return NULL;
    }
    // Pick out array entries by carefully parsing.  This is necessary in order
    // to cope with double quotes and commas, etc.
    $elements = array();
    $i = $j = 0;        
    $in_quotes = false;
    while ($i < strlen($arr)) {
    // If current char is a double quote and it's not escaped, then
    // enter quoted bit
    $char = substr($arr, $i, 1);
    if ($char == '"' && ($i == 0 || substr($arr, $i - 1, 1) != '\\')) 
     $in_quotes = !$in_quotes;
    elseif ($char == ',' && !$in_quotes) {
     // Add text so far to the array
     $elements[] = substr($arr, $j, $i - $j);
     $j = $i + 1;
    }
    $i++;
   }
   // Add final text to the array
   $elements[] = substr($arr, $j);
   // Do one further loop over the elements array to remote double quoting
   // and escaping of double quotes and backslashes
   for ($i = 0; $i < sizeof($elements); $i++) {
    $v = $elements[$i];
    if (strpos($v, '"') === 0) {
     $v = substr($v, 1, strlen($v) - 2);
     $v = str_replace('\\"', '"', $v);
     $v = str_replace('\\\\', '\\', $v);
     $elements[$i] = $v;
    }
   }
  return $elements;
  }

  /**  
   * @brief To display an SQL error
   * @param[in] proc A user-friendly definition of the process taking place when
   *                 the error occurred.
   * @param[in] sql A sysadmin-friendly piece of SQL.
   * @date 2011-07-11
   * @version 11
   **/
  public function sqlError($proc,$sql) {
    $this->hwDebugPubStart(__METHOD__,array($proc,$sql));
   # We're about to die; send any buffered data first.
   if (array_key_exists('buffered',$GLOBALS)) {
    $buffer=ob_get_clean();
    unset($GLOBALS['buffered']);
    print $buffer;
   }
   ?>
<script type='text/javascript' src='js/jquery.js'></script>
<script type='text/javascript' src='js/report_error.js'></script>

<h3 class='error _debug_sqlerror'>Database returned an error!</h3>
<p>The system was trying to <?php print $proc; ?> when an error was reported. Please contact your support team with the 
following information (copy and paste into an email would be ideal):
<ul>
 <li>Time: <?php print date(DATE_RFC822); ?></li>
 <li>Task: <?php print $proc;?></li>
 <li>Code: <tt><?php print wordwrap($sql) ;?></tt></li>
 <li>Error: <pre><?php print wordwrap(pg_last_error()); ?></pre></li>
</ul>
<?php
    $this->hwDebugPubStop(__METHOD__);
    print "<pre>\n";
    debug_print_backtrace();
    print "Posted data\n ";
    print_r($this->post);
    print "Request\n";
    if (array_key_exists('_dbpasswd',$_REQUEST)) {
     $_REQUEST['_dbpasswd']='PASSWORD_REMOVED';
    }
    print_r($_REQUEST);
   exit;
  } 

  /**
   * @brief To determine whether a view or table exists (only used for hotwire._preferences)
   * @param[in] viewname The name of the view to search for
   * @param[in] schemaname The schema in which we search.
   * @return true if the view or table exists, false otherwise.
   * @date 2011-07-11
   * @version 11
   */
  public function viewExists($viewname,$schemaname='public') {
    $this->hwDebugPubStart(__METHOD__,array($viewname,$schemaname));
#print "Checking for view $viewname in $schemaname\n";
    if (! $this->dbh) { $this->hwDebugPubStop(__METHOD__); return; }
    $testsql='select count(*) from (select viewname from pg_views where '.
             'viewname=$1 and schemaname=$2 UNION ALL '.
             'select tablename from pg_tables '. 
             'where tablename=$1 and schemaname=$2) a;';
    $args=array($viewname,$schemaname);
    $start=microtime(true);
    $result=$this->sqlexec(get_class($this),__METHOD__,HW_DBG_DBVI,$testsql,$args);
    $row=pg_fetch_array($result);
    $end=microtime(true);
    global $hw_sql_timing;
    $hw_sql_timing[$testsql]=($end-$start)/1000;
    $this->hwDebugPubStop(__METHOD__);
    return ($row[0]>0);
  }

  /**
   * @brief To return a preference or NULL if not set
   * @param[in] p Preference name
   * @param[in] default Default value of the preference if not set.
   * @return The value of the preference, or NULL if not set
   * @date 2011-07-11
   * @version 11
   **/
  public function pref($p,$default=null) {
    if (!array_key_exists($p,$this->prefs)) {
      return $default;
    }
    return $this->prefs[$p];
  }

  /**
   * @brief Dig into the database and get a string back
   * @param[in] sql - the SQL to execute
   * @param[in] args optional array of arguments to the SQL query
   * @return The first row in the first column of the output when the SQL is run.
   * @date 2011-07-11
   * @version 11
   **/
 public function sqltostring($sql,$args=array()) {
  $this->hwDebugPubStart(__METHOD__,array($sql,$args));
  $start=microtime(true);
  $result=$this->sqlexec(get_class($this),__METHOD__,HW_DBG_DBVI,$sql,$args);
  $end=microtime(true);
  //$this->hotwireDebug(HW_DBG_DANY,get_class($this).'->'.__METHOD__.' SQL: '.($end-$start).'s',$this->nicesql($sql,$args));
  if ($result===FALSE) {
   print "<pre>\n";
   print "Database error: ".pg_last_error($this->dbh)."\n";
   print "Query was: $sql\n";
  
   debug_print_backtrace();
   exit;
  }
  if (pg_num_rows($result)==0) {
   print "Odd, query $sql got no results\n";
   $this->hwDebugPubStop(__METHOD__);
   return;
  }  
  $this->hwDebugPubStop(__METHOD__,$result);
  return pg_fetch_result($result,0,0);
 }

 public function nicesql($sql,$args) {
  for ($i=1; $i<=count($args); $i++) {
   if (is_numeric($args[$i-1])) {
    $sql=str_replace('$'.$i,$args[$i-1],$sql);
   } else {
    $sql=str_replace('$'.$i,"'".pg_escape_string($args[$i-1])."'",$sql);
   }
  }
  return $sql;
 }



}
?>
