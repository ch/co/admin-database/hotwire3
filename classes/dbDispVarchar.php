<?php
 /**
  * @brief A class with basic methods to display a varchar field
  * @author Frank Lee
  * @date 2011-07-11
  * @version 11
  **/
class dbDispVarchar extends dbDispText {


 /**
  * @brief Displays an edit control for the varchar field
  * @date 2012-11-14
  * @version 12
  **/
 public function displayEdit() {
  if (substr($this->dbColumn->name(),0,1)=='_') { return;}
  $result="Cannot display this record!";
  // Handle varchars being _id fields
  if (preg_match('/_id$/',$this->getkey())) {
    $subobj=new dbDispInt4($this->dbColumn);
    return $subobj->displayEdit();
  }
  // Is this varchar big enough to get a text area?
  // Policy decision: varchar & text are sufficiently similar
  // that /varchars/ are presented as <input> and text, as <textarea>
  $size=$this->dbColumn->size();
   $result="<input type='".
           (preg_match('/password/', $this->getkey()) ? 'password' : 'text')."' ".
           "name='".$this->getkey()."' ".
           "id='".$this->id()."' ".
           "size='".($size>0?intval($size * 0.6):'')."' ".
           "value='".$this->htmlesc()."'" . 
           ($this->ro() ? " readonly class='readonly'>" : ">");
  return $result;
 }


 public function displaySearch() {
   if (preg_match('/_id$/',$this->key)) {
    $subobj=new dbDispInt4($this->dbColumn,$this->key);
    return $subobj->displaySearch();
   }
   // If this widget has a value, display it. 
   if ($this->val) {
     return '<input type="text" class="varchar" size="10" '.
            'name="'.$this->getkey().'" value="'.$this->val.'"/>';
   } else {
     if (array_key_exists($this->colname,$this->objRecord->post)) {
       return '<input type="text" class="varchar" size="10" '.
              'name="'.$this->getkey().'" value="'.($this->objRecord->post[$this->colname]).'"/>';
     } else {
       return '<input type="text" class="varchar" size="10" '.
              'name="'.$this->getkey().'" value=""/>';
     }
   }
 }

 public function ajaxmmdropdown() {
  $subobj=new dbDispInt4($this->dbColumn);
  return $subobj->ajaxmmdropdown();
 }

}
?>
