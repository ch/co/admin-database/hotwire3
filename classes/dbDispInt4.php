<?php
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

 /** 
  * @file dbDispInt4.php
  * @brief Handles display of Integer fields
  * Part of Hotwire http://hotwire.sourceforge.net/. 
  */

 /**
  * @brief A class with methods to display an integer field
  * @author Frank Lee
  * @date 2012-11-09
  * @version 12
  **/
class dbDispInt4 extends dbDispBase {

 /**
  * @brief Code placed in the HEAD of the page to load CSS/JS.
  * We use JS and CSS to provide a nice interface for many-many lists
  * @return HTML for the HEAD to load appropriate JS files. 
  * @date 2012-11-09
  * @version 12
  **/
 public static function prepareRecord(&$js,&$css) {
   //$this->hwDebugPubStart(__METHOD__);
   //if ($this->dbColumn->ent_hid()) {
   //}
   //$this->hwDebugPubStop(__METHOD__);
 }

 /**
  * @brief Displays an edit UI for integer fields.
  * @date 2011-07-11
  * @version 11
  * @return HTML input element for a form
  **/
 public function displayEdit() {
  if (substr($this->dbColumn->name(),0,1)=='_') { return;}
  # We might have a 'ent_hid' attribute, in which case this gets a bit clever.
  $return='';
  if ($this->dbColumn->ent_hid()) {
   // We might have an array to display, or not.
   if ($this->dbColumn->isarray()) {
     $return=$this->getMMSelect()."<!-- getMMSelect -->";
   } else {
     $return=$this->getSelect($this->getkey(), $this->getval(), NULL)."<!-- getSelect -->";
   }
  } else {
   $return='<input type="text" size="10" name="'.htmlspecialchars($this->getkey()).'" '.
         ($this->ro?' readonly="readonly" ':'').
         'value="'.htmlspecialchars($this->getval()).'" />';
  }
  $this->hwDebugPubStop(__METHOD__);
  return $return;
 }

 /**
  * @brief Displays a search UI for integer fields.
  * @date 2011-07-11
  * @version 11
  * @return HTML input element for a form
  **/
 public function displaySearch(){
  $this->hwDebugPubStart(__METHOD__);
  $return='';
  if ($this->dbColumn->ent_hid()) {
   if ($this->dbColumn->isarray()) {
    $return=$this->getMMSelect();
   } else {
    $return=$this->getMMSelect(true);
   }
  } else {
   $field=$this->getkey();
   if (substr($field,-3,3)=='_id') {
    $field=substr($field,0,strlen($field)-3).'_hid';
    if (array_key_exists($field,$this->objRecord->post)) {
     $this->val=$this->objRecord->post[$field];
    }
   }
   $return='<input type="text" size="10" name="'.htmlspecialchars($field).'" '.
           'value="'.$this->getval().'" '.
           ' />';
  }
  $this->hwDebugPubStop(__METHOD__);
  return $return;
 }
  
 /**
  * @brief Provides a UI for many-many integer lists
  * @date 2011-07-11
  * @version 13
  * @return HTML input element for a form
  **/
  protected function getMMSelect($search=NULL) {
   $this->hwDebugPubStart(__METHOD__);
   $ent_id= $this->dbColumn->ent_id();
   $ent_hid= $this->dbColumn->ent_hid();
   $ro=$this->ro && (! ($this->objRecord->action=='search'));
   // Find an md5 of all the options for this select
   $md5='';
   $src='';
   if ($this->objRecord->pref('MM_UI_AJAX',0)==1) {
     $md5=$this->checksum_of_mm_options();
     $src=sprintf('ajaxmmdropdown.php?_view=%s&column=%s',
                  $this->objRecord->view,
                  $ent_id);
   }
   $select="<select name='".$this->getkey()."[]' view='".$this->objRecord->view."' column='$ent_id' src='$src' md5='$md5' "
          ."class='select_ajax_multi ".($this->objRecord->pref('MM_UI_TAG',0)==1?'select_ajax_multi_invisible':'')."' size='10' "
          ."id='".$this->id()."' multiple='multiple' ".
          ($ro ? "  disabled='disabled'>" : ">");
   //if (is_array($this->getval())) {
//print "<pre>\n";
//print_r($this);
   // We should have an explicit indication that we're going to use AJAX:
   $restrict_to_selected_records=($this->objRecord->pref('MM_UI_AJAX',0)==1);
   if ($this->dbColumn->isarray()) {
    $extra=$this->getoptionsfor($ent_id,$ent_hid,$this->getval(),$restrict_to_selected_records);
    $select.=$extra;
   } else {
    $select.=$this->getoptionsfor($ent_id,$ent_hid,$this->getval(),$restrict_to_selected_records);
   }
   $select .= "</select>";
   $this->hwDebugPubStop(__METHOD__);
   return $select;
  }

 /**
  * @brief Returns the number of options for an integer list.
  * @date 2011-08-08
  * @version 1
  * @return integer
  **/
 protected function countOptionsFor($ent_hid) {
   $this->hwDebugProStart(__METHOD__);
   if (array_key_exists('hid_count_cache',$_SESSION) && array_key_exists($ent_hid,$_SESSION['hid_count_cache'])) {
    $count=$_SESSION['hid_count_cache'][$ent_hid];
    $this->hwDebugProStop(__METHOD__,'[cached] '.$count);
    return $count;
   }
   $sql='select count(*) from '.$this->hwschema.'."'.preg_replace('/^ro_/','',$ent_hid).'";';
   $count=$this->objRecord->sqltostring($sql);
   # Poke this into the session
   $_SESSION['hid_count_cache'][$ent_hid]=$count;
   $this->hwDebugProStop(__METHOD__,$count);
   return 0+$count;
 }

 /**
  * @brief Provides a list of options for an integer list
  * @date 2011-07-11
  * @version 12
  * @return HTML input element for a form
  * @todo Order by, er, something.
  **/
  protected function getoptionsfor($ent_id, $ent_hid, $id=NULL, $individual=NULL) {
   $this->hwDebugProStart(__METHOD__);
   $search=($this->objRecord->action=='search');
   # Handle $id being individual
   $notnull=$this->dbColumn->notnull();
   $notnull=($notnull && $search);
   $array=$this->dbColumn->isarray();
   // detect the special case where id==NULL and individual==true:
   if (($id===NULL) && ($individual) ) {
    # Javascript? Empty list will be translated by css
    //if ($_SESSION['javascript']) {
     //return '';
    //}
    
    if ($search) {
     return "";
    } else {
     return "<option value='' selected class='blank'>blank</option>";
    }
   }
   if ($id !== NULL && ! is_array($id)) {
     $id=array($id);
   }
   $sql = 'SELECT * FROM '.$this->hwschema.'."'.preg_replace('/^ro_/','',$ent_hid).'"';
   $sqlargs=array();
   $options='';
   if ($individual ) {
     if ( ($id !== NULL ) && ($id !== array()) && ($id !== array(0=>''))) {
       $sql.=" WHERE \"".preg_replace('/^ro_/','',$ent_id)."\" in (";
       $holders=array();
       foreach ($id as $i) {
         $holders[]='$'.(string)(count($holders)+1);
         $sqlargs[]=$i;
       }
       $sql.=join(',',$holders).')';
     } else {
       $id=array();
       // id is either NULL or array(), so either way, no options to display
       $sql.="WHERE 'f'::boolean";
     }
     if ((join($id)=='') 
      && !$notnull && !$array && !$search) {
       $options="<option value='' selected>blank</option>";
     }
   } else {
     if (!$this->ro && !$notnull && !$array) {
       $options="<option value=''>blank</option>";
     }
   }
   // Sort our field by *_hid.*_sort if available
   $hidsort=preg_replace('/_id/','_sort',preg_replace('/^ro_/','',$ent_id));
   if ($this->objRecord->database_entity_exists($this->hwschema,preg_replace('/^ro_/','',$ent_hid),$hidsort)) {
     $sql.=' ORDER BY "'.$hidsort.'"';
   } else {
     $sql.=' ORDER BY "'.preg_replace('/^ro_/','',$ent_hid).'"';
   }
   // $id might be empty, in which we just return a blank option 
   $start=microtime(true);
   //$dbQuery=$this->objRecord->sqlexec(get_class($this),__METHOD__,HW_DBG_DBVI,$sql);
   $dbQuery = pg_query_params($this->objRecord->dbh, $sql, $sqlargs);
   if (!$dbQuery) {$this->sqlError('getting list of options for the view '.$ent_id,$sql);}
   while ($row = pg_fetch_assoc($dbQuery)) {
     $options.= "    <option value='".$row[preg_replace('/^ro_/','',$ent_id)]."'";
     if ($id !== NULL) {
       $options.= (in_array($row[preg_replace('/^ro_/','',$ent_id)],$id) ? " selected" : "");
     }
     $options.= ">".substr(strip_tags($row[preg_replace('/^ro_/','',$ent_hid)]), 0, 60) .
                      (strlen($row[preg_replace('/^ro_/','',$ent_hid)]) > 60 ? '...' : '') .
                      "</option>";
   }
   $end=microtime(true);
   global $hw_sql_timing;
   $hw_sql_timing[$sql]=($end-$start)/1000;
   $this->hwDebugProStop(__METHOD__);
   return $options;
  }


 /**
  * @brief Provides a Select list for an integer field
  * @date 2011-07-11
  * @version 11
  * @return HTML input element for a form
  **/
  protected function getSelect() {
    $this->hwDebugProStart(__METHOD__);
    $ent_id = $this->dbColumn->ent_id();
    $ent_hid = $this->dbColumn->ent_hid();
    // How many options are there for this dropdown list?
    $count=$this->countOptionsFor($ent_hid);
    $mindd=$this->objRecord->pref('AJAXMIN',1);
    if (!$_SESSION['javascript']) { $mindd=-1;} 
    // Find an md5 of all the options for this select
    $md5='';
    $src='';
    if ($this->objRecord->pref('MM_UI_AJAX',0)==1) {
      $md5=$this->checksum_of_mm_options();
      $src=sprintf('ajaxmmdropdown.php?_view=%s&column=%s',
                   $this->objRecord->view,
                   $ent_id);
    } else {
      $mindd=-1;
    }
    // Don't use ajax if min dropdown smaller than $count or readonly
    // Use AJAX if readonly && MM_UI_TAG - displays nicely?
    $useajax=(($mindd>0) && ($count>$mindd) && ((! $this->dbColumn->ro()) || $this->objRecord->pref('MM_UI_TAG',0)));
#&& (! $this->objRecord->action=='new'))));
// RFL hack 2017-02-21
 //$useajax=0;
    $select=sprintf('    <select id="%s" name="%s" view="%s" column="%s" src="%s" class="%s" notnull="%s" >',
                         $this->id(),
                         $this->dbColumn->name(),
                         $this->objRecord->view,
                         $ent_id,
                         $src,
                         ($useajax?"select_ajax ".($this->objRecord->pref('MM_UI_TAG',0)==1?'select_ajax_invisible':''):''),
                         (($this->dbColumn->notnull() && $this->objRecord->action=='search')?'1':'0'),
                         ($this->ro?'disabled':'')
                   );
/*
    $select="    <select name="'.$this->dbColumn->name().'" view='".$this->

id="'.$this->id().'" '
           . ($useajax?'class="select_ajax" ':'')
           . (($this->dbColumn->ro() && ($this->objRecord->action!='new')) ? " readonly >" : ">");
*/
    // If we're read only, just display it swiftly.
    if ($this->ro && (! $this->objRecord->action=='new')) {
      // Get individual option for this entry and no more.
      $select .=$this->getoptionsfor($ent_id,$ent_hid, $this->getval(),1);
      $select .= "</select>";
      $this->hwDebugProStop(__METHOD__);
      return $select;
    }

    // Should we use AJAX?
    $select.=$this->getoptionsfor($ent_id,$ent_hid,$this->getval(),($useajax?1:0))
            ."</select>";

    $this->hwDebugProStop(__METHOD__);
    return $select;
  }

 /**
  * @brief To calculate a checksum of options for a field
  * @todo: We no longer use _sort - the view defines its own order
  * @todo: We should use _cur if available
  **/
  private function checksum_of_mm_options() {
    $ent_id=$this->dbColumn->ent_id();
    $ent_id=preg_replace('/^ro_/','',$ent_id);
    $ent_hid=$this->dbColumn->ent_hid();
    $ent_hid=preg_replace('/^ro_/','',$ent_hid);
    $hidview=$this->dbColumn->ent_hid();
    $hidview=preg_replace('/^ro_/','',$hidview);
    $ent_sort=preg_replace('/_id/','_sort',$ent_id);
    $sql = sprintf('SELECT md5(array_agg(concat(a."%s",a."%s")::text)::text) from (select "%s", "%s" from "%s"."%s" ORDER BY "%s") a;',
                   $ent_id,
                   $ent_hid,
                   $ent_id,
                   $ent_hid,
                   $this->hwschema,
                   $hidview,
                   $this->objRecord->database_entity_exists($this->hwschema,$ent_hid,$ent_sort)?$ent_sort:$ent_hid);
    $start=microtime(true);
    $dbQuery = pg_query($this->objRecord->dbh, $sql);
    if ($dbQuery===false) {
      print" Hmm - ".pg_last_error()."\n";
    }
    $row=pg_fetch_array($dbQuery);
    $end=microtime(true);
    global $hw_sql_timing;
    $hw_sql_timing[$sql]=($end-$start)/1000;
    return $row[0];
  }

	
 /**
  * @brief To return an ajax-ready list of options for the given field
  * in a slightly different format.
  * @date 2011-07-11
  * @version 11
  **/
  public function ajaxmmdropdown() {
    $ro=$this->ro;
    $ent_hid = preg_replace('/^ro_/','',$this->dbColumn->ent_hid());
    $ent_id = preg_replace('/^ro_/','',$this->dbColumn->ent_id());
    $ent_dis = preg_replace('/_hid$/','_dis',$ent_hid);
    # Select2 likes to have 'id' and 'text' properties
    if ($this->objRecord->database_entity_exists($this->hwschema,$ent_hid,$ent_dis)) {
      # $sql = "SELECT $ent_id as id, $ent_hid as text, $ent_dis as disabled FROM ".$this->hwschema.".\"$ent_hid\" ";
      $sql = sprintf('SELECT "%s" as id, "%s" as text, "%s" as disabled FROM "%s"."%s"',
                           $ent_id,    $ent_hid,      $ent_dis    , $this->hwschema, $ent_hid);
    } else {
      #$sql = "SELECT $ent_id as id, $ent_hid as text FROM ".$this->hwschema.".\"$ent_hid\" ";
      $sql = sprintf('SELECT "%s" as id, "%s" as text FROM "%s"."%s"',
                           $ent_id,    $ent_hid,      $this->hwschema, $ent_hid);
    }
    $start=microtime(true);
    $dbQuery = pg_query($this->objRecord->dbh, $sql);
    $data=array('method'=>'ajaxmmdropdown');
    if ($dbQuery === false) {
      $data['Error']=pg_last_error();
    }
    $opts=array();
    #while ($row=pg_fetch_assoc($dbQuery)) {
      #$opts[$row[$ent_id]]=$row[$ent_hid];
    #}
    $opts=pg_fetch_all($dbQuery);
    $end=microtime(true);
    global $hw_sql_timing;
    $hw_sql_timing[$sql]=($end-$start)/1000;
    $data['opts']=$opts;
    $data['md5']=$this->checksum_of_mm_options();
    $data['sql_timing']=$hw_sql_timing;
    $data['view']=$this->objRecord->view;
    $data['column']=$ent_id;
    return $data;
  }

  /**
   * We might be a _hid field, in which case we search the _hid column 
   **/
  public function asSQL() {
  }

 /**
  * @brief To give the value of this object (used by subview);
  * @author Frank Lee
  * @version 1
  * @date 2012-09-10
  */
 public function getval() {
  if (array_key_exists(0,$this->objRecord->id)) {
   $val=$this->objRecord->data[$this->objRecord->id[0]][$this->dbColumn->name()];
   if ($val=='{NULL}') { $val=NULL; }; # '{NULL}' represents NULL if we're an array.
   if ($this->dbColumn->isarray()) {
     //$val=explode(',',trim($val,'{}'));
     $val=$this->pg_array_parse($val);
   }
   return $val;
  } else {
   if (is_array($this->objRecord->data[''])) {
     if (array_key_exists($this->colname,$this->objRecord->data[''])) {
      return $this->objRecord->data[''][$this->colname];
     } else if (array_key_exists($this->dbColumn->ent_id(), $this->objRecord->data[''])) {
      return $this->objRecord->data[''][$this->dbColumn->ent_id()];
     }
    } else {
      return '';
    }
  }
 }

  /**
  * @brief to parse a string representation of an SQL array into a PHP array
  * 'cos it's 2018 and PHP still doesn't do this.
  */
  public function pg_array_parse($s, $start = 0, &$end = null) {
    if (empty($s) || $s[0] != '{') return null;
    $return = array();
    $string = false;
    $quote='';
    $len = strlen($s);
    $v = '';
    for ($i = $start + 1; $i < $len; $i++) {
        $ch = $s[$i];

        if (!$string && $ch == '}') {
            if ($v !== '' || !empty($return)) {
                $return[] = $v;
            }
            $end = $i;
            break;
        } elseif (!$string && $ch == '{') {
            $v = pg_array_parse($s, $i, $i);
        } elseif (!$string && $ch == ','){
            $return[] = $v;
            $v = '';
        } elseif (!$string && ($ch == '"' || $ch == "'")) {
            $string = true;
            $quote = $ch;
        } elseif ($string && $ch == $quote && $s[$i - 1] == "\\") {
            $v = substr($v, 0, -1) . $ch;
        } elseif ($string && $ch == $quote && $s[$i - 1] != "\\") {
            $string = false;
        } else {
            $v .= $ch;
        }
    }
    return $return;
  }

}
?>
