<?php
 /**
  * @brief A class with methods to display a column
  * @author Frank Lee
  * @date 2012-11-12
  * @version 1
  * Replaces an entry in the colAttrs array
  **/
class dbColumn extends hwFunctions {
 private $widget; //<@brief Object to display this column
 public $parentobj;
 private $name;   //<@brief Name of column
 private $type;   //<@brief Type of column
 private $islist; //<@brief True if this is a list view
 private $length;
 private $size=-1;//
 private $notnull=false;
 public $isarray=false;
 private $hasdef=false;
 private $ro=false;
 private $ent_hid=null;
 private $ent_id=null;
 private $htmlview=null;
 private $sort=null;
 private $listsql=null;
 private $hidden=false;
 private $search=null;
 private $view;
 public $hwschema;

 public function getid($offset=NULL) {
  if ($offset===NULL) {
   return $this->parentobj->id;
  } else {
   return $this->parentobj->id[$offset];
  }
 }


 public function type() {
  return $this->type;
 }

 public function ent_hid() {
  return $this->ent_hid;
 }

 public function ent_id() {
  return $this->ent_id;
 }

 public function notnull() {
  return $this->notnull;
 }

 public function htmlview() {
  return $this->htmlview;
 }

 public function size() {
  return $this->size;
 }

 // $args is a row from hotwire._column_data, ASSOC.
 // $parentobj might be a dbRecord or a dbList
 public function __construct($args,&$parentobj) {
  if (get_class($parentobj)=='dbMetaList') { return;}
  $this->parentobj=&$parentobj;
  $this->islist=(get_class($this->parentobj)=='dbList');
  $this->hwschema=$this->parentobj->hwschema;
  $this->name=$args['attname'];
  $this->type=$args['typname'];
  if ($this->type=='varchar' && array_key_exists('atttypmod',$args) &&  $args['atttypmod']) {
   $this->size=$args['atttypmod'];
  }
  $this->notnull=((array_key_exists('addnotnull',$args)?$args['attnotnull']:NULL)=='t');
  $this->hasdef=((array_key_exists('atthasdef',$args)?$args['atthasdef']:NULL)=='t');
  $this->isarray=((array_key_exists('array',$args)?$args['array']:NULL)=='t');
  if ($this->isarray) {
   $this->type=$args['elementtype'];
   $this->listsql=$this->getMMSQL();
  }

  // Flags from colName
  if (substr($this->name,0,1)=='_') { $this->hidden=true;}
  if (preg_match('/^(_)?(hl_)?(mm_)?(ro_)?(so_)?(x_)?(.+?)(_id)?(_html)?$/', $this->name, $match)) {
    $this->hidden=(array_key_exists(1,$match) && $match[1]);
    // HL_ is deprecated
    // MM_ is deprecated
    $this->ro=(array_key_exists(4,$match) && $match[4]);
    if (array_key_exists(8,$match) && array_key_exists(7,$match) && $match[8]) {
      $this->ent_id = ($this->ro?'ro_':'').$match[7] . '_id';
      $this->ent_hid = ($this->ro?'ro_':'').$match[7] . '_hid';
    }
    $this->htmlview=(array_key_exists(9,$match) && $match[9]);
  }

  // Might be lining up for a search
  if ($parentobj->propertyEqual('action','search')) { $this->parsesearch(); }
  # Instantiate a widget
  $this->loadWidget();
  // Perhaps we should be hiding this column?
  $this->hidden=$this->hidden || (array_key_exists('hde_'.$this->name,$this->parentobj->post));
  // Or perhaps it's been turned into a _hid column and we should be hiding /that/
  $this->hidden=$this->hidden || ($this->ent_hid!='' && array_key_exists('hde_'.$this->ent_hid,$this->parentobj->post));
  if ($this->name=='id') { $this->hidden=false;}
 }

 public function parsesearch() {
   if ($this->ent_id) {
     # Either post[$ent_id] or post[$ent_hid] must be set
     if ( ! ( $this->arrayKey($this->parentobj->post,$this->ent_id)
           ||$this->arrayKey($this->parentobj->post,$this->name)
           ||$this->arrayKey($this->parentobj->post,$this->ent_hid))) {
       return;
     }
   } else {
     # Need one of post[$fieldname{,_min,_max}] to be set
     $nonro=preg_replace('/^ro_/','',$this->name);
     $ro='ro_'.$nonro;
     if (!($this->arrayKey($this->parentobj->post,$nonro)
        || $this->arrayKey($this->parentobj->post,$ro)
        || $this->arrayKey($this->parentobj->post,"min_".$this->name)
        || $this->arrayKey($this->parentobj->post,"max_".$this->name))) {
       return;
     }
   }
   $this->search=new dbSearch($this);
 }

 public function asGET(&$args) {
  if ($this->search) {
   $this->search->asGet($args);
   if ($this->hidden) {
     $args[]=urlencode('hde_'.$name).'=on';
   }
  }
 }

 // Returns the extra field for 'select' if this has a HID
 // Otherwise just returns our normal field.
 public function selectfield(&$fields) {
   if (!$this->hidden && $this->islist && $this->ent_hid) {
     if ($this->isarray) {
       #$h='"'.$this->ent_hid.'"';
       #$i='"'.$this->ent_id.'"';
       #$v='"'.$this->parentobj->view.'"';
       #$n='"'.$this->name.'"';
       #$fields[]='array_to_string(array(select '.preg_replace('/^ro_/','',$h).' from hotwire.'.preg_replace('/^ro_/','',$h)." where ".preg_replace('/^ro_/','',$h).".$i=any($v.$n)),'; ') as $h ";
       $hidfield=preg_replace('/^ro_/','',$this->ent_hid);
       $hidtable=preg_replace('/^ro_/','',$this->ent_hid);
       $idfield=preg_replace('/^ro_/','',$this->ent_id);
       $hid_name=$this->ent_hid;
       $view=$this->parentobj->view;
       $field=$this->name;
       // Don't merge _hid fields from varchar[] into text. We need to export varchar[] to CSV in a re-importable format
       // $fields[]=sprintf('"%s"."%s",array_to_string(array(select "%s" from hotwire."%s" where "%s"."%s" = any ("%s"."%s")),\';\') as "%s"',$view,$this->ent_id,$hidfield,$hidtable,$hidtable,$idfield,$view,$field,$hid_name);
       $fields[]=sprintf('"%s"."%s",array_to_string(array(select "%s" from %s."%s" where "%s"."%s" = any ("%s"."%s")),$$;$$) as "%s"',$view,$this->ent_id,$hidfield,$this->hwschema,$hidtable,$hidtable,$idfield,$view,$field,$hid_name);
     } else {
       # If field is read only, should alias the hid field to readonly too
       if (substr($this->name,0,3)=='ro_') {
         $fields[]='"'.preg_replace('/^ro_/','',$this->ent_hid).'".'
                  .'"'.preg_replace('/^ro_/','',$this->ent_hid).'" AS "'.$this->ent_hid.'"';
         $fields[]='"'.preg_replace('/^ro_/','',$this->ent_hid).'".'
                  .'"'.preg_replace('/^ro_/','',$this->ent_id). '" AS "'.$this->ent_id.'"';
       } else {
         $fields[]='"'.$this->ent_hid.'"."'.$this->ent_hid.'"';
         $fields[]='"'.$this->ent_hid.'"."'.$this->ent_id.'"';
       }
     }
   } else {
     if (($this->type()!='bytea')&&($this->type()!='oid')) {
       $fields[]='"'.$this->name.'"';
     }
   }
 }

 // Returns the necessary JOIN if this is a HID
 public function join(&$join) {
  if (!$this->islist) { return; }
  if ($this->hidden) { return ; }
  if (!$this->ent_id) { return; }
  if ($this->isarray) {
    if ($this->search !==NULL ) {
    // If it's an array, we've already assembled the _hid data earlier on.
    // So don't add it again here.
    /*
    $join[]=' LEFT JOIN hotwire."'.$this->ent_hid.'" ON '
           .'"'.$this->ent_hid.'"."'.$this->ent_id.'" = ANY ('
           .'"'.$this->parentobj->view.'"."'.$this->name.'")';  
    */
    }
  } else {
    if ($this->ro) {
      $join[]=' LEFT JOIN '.$this->hwschema.'."'.preg_replace('/^ro_/','',$this->ent_hid).'" ON '
             .'"'.$this->name.'"="'.preg_replace('/^ro_/','',$this->ent_id).'"';
    } else {
      if ($this->ent_id == $this->name) {
       $join[]=' LEFT JOIN '.$this->hwschema.'."'.$this->ent_hid.'" USING ("'.$this->ent_id.'")';
      } else {
       $join[]=' LEFT JOIN '.$this->hwschema.'."'.$this->ent_hid.'" ON "'.$this->name.'"="'.$this->ent_id.'"';
      }
    }
  }
 }

 // Returns a JOIN if necessary for counting
 public function countjoin(&$join) {
  // We're building a search on an *_id field:
  if ($this->search !== NULL && $this->ent_id) {
   if ($this->isarray) {
// LEFT JOINs take all the rows from the preceding table and only add to them
// But they bring in fieldnames we might search on.`
     $join[]=' LEFT JOIN '.$this->hwschema.'."'.preg_replace('/^ro_/','',$this->ent_hid).'" ON '
            .'"'.preg_replace('/^ro_/','',$this->ent_hid).'".'
            .'"'.preg_replace('/^ro_/','',$this->ent_id).'" = ANY ('
            .'"'.$this->parentobj->view.'"."'.$this->name.'")';
   } else {
    if ($this->ro) {
// LEFT JOINs take all the rows from the preceding table and only add to them.
// But they bring in fieldnames we might search on.`
      $join[]=' LEFT JOIN '.$this->hwschema.'."'.preg_replace('/^ro_/','',$this->ent_hid).'" ON '
             .'"'.$this->name.'"="'.preg_replace('/^ro_/','',$this->ent_id).'"';
    } else {
      if ($this->ent_id==$this->name) {
       $join[]=' LEFT JOIN '.$this->hwschema.'."'.$this->ent_hid.'" USING ("'.$this->ent_id.'")';
      } else {
       $join[]=' LEFT JOIN '.$this->hwschema.'."'.$this->ent_hid.'" ON "'.$this->name.'"="'.$this->ent_id.'"';
      }
    }
   }
  }
 }

 // Should this appear? Only seems to be used for list-type output, eg list view and CSV, hence
 // suppressing large binary columns here
 public function visible() {
   // We do not want to display these in list view
   if (($this->type=='bytea')||($this->type=='oid')) { 
     return false ;
   } else { 
     return !$this->hidden;
   }
 }

 public function insql() {
   return !$this->hidden;
 }

 public function inhtml() {
   return !$this->hidden && $this->name!='id';
 }

 public function asJSfields() {
   if ($this->search === NULL ) { return; }
   return $this->search->asJSfields();
 }

 private function getMMSQL() {
   $hid_table=str_replace('_id','_hid',$this->name);
   $hid_table=str_replace('ro_','',$hid_table);
   $hid_field=str_replace('_id','_hid',$this->name);
   $id_field=str_replace('ro_','',$this->name);
   $this_table=$this->view;
   $sql="array_to_string(array(select \"$hid_table\" from ".$this->hwschema.".\"$hid_table\" where ".
        " \"$hid_table\".\"$id_field\" = any(\"$this_table\".\"$id_field\"::bigint[])),', ') as \"$hid_field\" ";
   return $sql;
 }

 public function prepareList(&$js,&$css) {
  $this->widget->prepareList($js,$css);
 }

 public function where(&$where, &$args) {
  if ($this->search) {
   $this->search->asSQL($where,$args,$this->isarray);
  }
 }

 public function wherecount(&$wherecount,&$argscount) {
  if ($this->search) {
   $this->search->asSQLforCount($wherecount,$argscount,$this->isarray);
  }
 }

 public function name() {
  return $this->name;
 }

 public function isarray() {
  return $this->isarray;
 }

 public function is_array() {
  return $this->isarray;
 }

 public function ro() {
  # If we're doing a search, it shouldn't be readonly
  if ( isset($this->parentobj->action) && $this->parentobj->action=='search') {
   return false;
  }
  return $this->ro;
 }

 // Returns a GET string representing the sort with *this* column's 
 // sorting iterated (ASC -> DESC -> none)
 public function nextAsGET() {
 print "nextAsGET for $this->name\n";
  print  "<pre>\n";
  foreach ($this->parentobj->colobjs as $col) {
    if ($this->name=='ro_person_id') {
      print_r($col);
    }
  }
  print "</pre>\n";
 }

   /**
   * @brief Loads an object appropriate to the datatype of the field.
   * to load a widget appropriate to the type or a dbDispBase widget as fallback
   * @param[in] type Field type
   * @param[in] fieldname Name of the field
   * @date 2012-11-06
   * @version 13
   * Don't wrap this in timing info, it's a fast operation. (Usually)
   */
  protected function loadWidget() {
   // Handle php<5.3.0 which can't raise exceptions from __autoload
   $class='dbDisp'.ucfirst($this->type);
   if (file_exists('classes/'.$class.'.php')) {
    try {
      $this->widget=new $class($this);
    }
    catch(Exception $e) {
      $this->widget=new dbDispBase($this);
    }
   } else {
    $this->widget=new dbDispBase($this);
   }
  }

  // To produce a table cell for this data
  public function viewcell(&$row) {
    return $this->widget->viewcell($row);
  }
  
  // To get raw data for this table cell
  public function viewcellcsv(&$row) {
    return $this->widget->viewcellcsv($row);
  }

  // To prepare for the display of a dbRecord
  public function prepareRecord(&$js,&$css) {
    return $this->widget->prepareRecord($js,$css);
  }

  public function ajaxdropdown() {
    return $this->widget->ajaxdropdown();
  }

  public function ajaxmmdropdown() {
    return $this->widget->ajaxmmdropdown();
  }

  public function displayField() {
    return $this->widget->displayField();
  }

  // To produce a dbRecord entry for the data
  // TODO: don't display the 'id' column
  public function displayRecordField() {
    # Display the ID field
    if ($this->name=='id') {
     if (array_key_exists(0,$this->parentobj->id)) {
       return '<input id="'.$this->widget->id().'" type="hidden" value="'.$this->parentobj->id[0].'" name="'.$this->widget->id().'"/>';
     } else {
       return null;
     }
    } // End handler for "ID" column;
    # Display a hidden field
    if ($this->hidden) {
      return '<input type="hidden" id="'.$this->widget->id().'" name="'.$this->widget->id().'" readonly="readonly" value="'.htmlspecialchars($this->widget->getval()).'"/>';
    }
    $ui=array();
    $ui[]='<!-- Displaying an entry of type '.$this->type.' -->';
    $ui[]='<!-- Class '.get_class($this->widget).' -->';
    # Trip out of the two-column div if we're a wide widget
    $this->widget->preDisplayEdit($ui);
    $ui[]=$this->widget->displayEdit();
    $this->widget->postDisplayedit($ui);
    return join("\n",$ui);
  }

  public function displaySearchField() {
    if ($this->hidden || $this->name=='id') { return; }
    $ui=array();
    $ui[]='<!-- Displaying a search of type '.$this->type.' for '.$this->name.' -->';
    $ui[]='<!-- Class '.get_class($this->widget).' -->';
    $this->widget->displaySearchField($ui);
/*
    $ui[]=$this->widget->preDisplaySearch();
    $ui[]='<div class="clearwidth '.$this->type.'" id="'.$this->domName($this->name.'_row').'">';
    $ui[]='<div class="search">';
    $ui[]= '<div class="searchkey"><p>'.$this->hdrFormat($this->name).'</p>';
    $ui[]= '<div class="suppress" id="'.$this->domName('hde_'.$this->name).'" >';
    $ui[]= '<input type="checkbox" name="hde_'.$this->name.'" id="'.$this->domName('hde_'.$this->name).'" />';
    $ui[]= '</div> <!-- suppress -->';
    $ui[]= '<div class="display" id="'.$this->domName("dsp_".$this->name).'">';
    $ui[]= '[Show? <input type="checkbox" checked="checked" name="dsp_'.$this->name.'" id="'.$this->domName('dsp_'.$this->name).'" />]';
    $ui[]= '</div>';
    $ui[]= '</div> <!-- searchkey -->';
    $ui[]='<div class="searchvalue">'.$this->widget->displaySearch();
    $ui[]= '<span class="searchxct">'.$this->widget->displayExact().'</span>';
    $ui[]= '<span class="searchxcl">';
    $ui[]=  "Exclude <input type='checkbox' name='xcl_".$this->name."' id='".$this->domName("xcl_".$this->name)."'/>";
    $ui[]= '</span ><!-- searchxcl -->';
    $ui[]= '</div><!-- SearchValue -->';
    $ui[]='</div> <!-- search -->';
    $ui[]='</div> <!-- clearwidth -->';
    $ui[]=$this->widget->postDisplaySearch();
*/
    $ui[]='<!-- End display of '.$this->name.' -->';
    return join("\n",$ui);
  }

  public function permarray() {
    return $this->parentobj->permarray();
  }

}
?>
