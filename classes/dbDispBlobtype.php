<?php
 /**
  * @brief A class with methods to display a blob
  * Actually just inherits everything...
  * @author Frank Lee
  * @date 2012-11-09
  * @version 2
  * @TODO - put icons into a hash somewhere else. They take up much space.
  **/
class dbDispBlobtype extends dbDispBase {

  /**
  * @brief To display a simple edit UI for the general case
  * @date 2011-07-11
  * @version 11
  **/
  public function displayEdit() {
    if (substr($this->dbColumn->name(),0,1)=='_') { return;}
    $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
    $blobinf=explode(',',trim($this->getval(),'()'));
    $mimetype=$blobinf[0];
    $blobid=$blobinf[1];
    $return=array();
    switch ($mimetype) {
      case '': # no mime type? then just provide the file upload form
        $return[]='<div class="hw_blobdisp">'
                 .'<input name="'.$this->getkey().'" type="file"/>'
                 .'</div>';
        break;
      case 'image/jpeg':
        $return[]='<div class="hw_blobdisp">'
                 .' <img src="image.php?_id='.$this->objRecord->id[0].'&'
                 .'_view='.$this->objRecord->view.'&_field='.$this->getkey().'" /><br/>'
                 .'<input name="'.$this->getkey().'" type="file"/>'
                 .'</div>';
        break;
      case 'application/pdf':
        $icon='iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAABGdBTUEAANkE3LLaAgAAGNJJREFU
eNrtnQlwVFXWx8EZBSbKMIq4MHxOImgEJKggRBERdcANRAYdBUdxHBVFpT5BWcaSkf0TIvoVCEKx
BFnFsFiQUGSrkGC2ymoWE5IKJF+WylJ0Ur2kknC++7/p27z3+r1eAvTrTl6q/hXovO5+753fPfec
c5fXq5dnP72ZfpeZmTkrJSWlhv2m3NzcHqnU1FRKTk6mCxcuUGlpadLevXuHsXtzXa9u/APjX3/i
xInX6urqqLW1ldrb23u0rFYrnT9/nkNQUVFh2r59ezgaiP1edbsfXFj/kpKSKsP4l4V7ce7cOS7m
CUwRERGPdlcI+jINQes3DC9Xc3Mz9wKVlZX022+/dVsIbmIa0dDQ0KON3dbW5hBaP9TS0kL5+fmO
390VggFMD/U0AJTGRr9vNpu5YHAIHgCxALwjfiNILCoq6nYQ3Mw0rqd1AVIAYHwlADA+BPePOACv
nz17lrKzs+nXX3/tVhD0WADctf6LFy9STU0NFRQU8PfA+IAAaXJeXl63gcAAQKX1w/gQPAAzNn8P
uoHuCAEHoLa2tse7f2Xrh5AFwOh4H+5RcXGxA4KMjAzEBaYFCxaMsENgABCIAGi1/qamJt7qYWi8
D/8HEEoIfvnll4LJkycPDlQIOADo6wz37wwAAsC0tDT+PvwdXaUSAvw9JSWlYMaMGXf0CsCysQGA
IvIXxkdqXFpayo0sqoP4u0gLpRBgDIFB8OuUKVMCDoIeC4Cy/1e2fgBQUlJCZ86ckZWIXUGQlJQU
cBBwABDtBrRhmVHbG+upvZ6B3MAyGhfjGp72/wAABk5ISHAaJ9CCAAo0CAIegLbyEmrdvoGs08aQ
9bEQsk4ZRW3fraG2ZlOXABDGFwDExsaqDha5goBBEzAQBC4AVgu1HvierOOGkOUvN8hkY6pfNo9M
JhNZLJYuA4Ai0KlTpzRHDLUgwJyC+Pj4gICAA4DINqCMb7NS69efOxleyMrUNCyICmKjqaysjBtK
CwBl/i8AgGExCHTy5EmXw8ZqEAAAxA7Me/g9BAEJQFtqPFnuCdIEAGob2o+Kv/yEp3KAQMx38AYA
VAGPHTvmdu6AEgJUCgFBYmIinT592q8h4ADgxAMGAHML2ea/IjO2eegfyBLSV/Zae0gfyps9left
RUVF3LBdAeDIkSMeTSCRQoCuQwpBTEyM30IQeADUVpEl9CaZu2+cfB9dHHMn/7d4vSP4BsoJv5sP
6MCVi1TXGwAwBHzo0CGPzssVBMgkWFfilxAEHABtZ07JWnorU+0rk6jhhbFOAGSOvoMaGxt5SxaB
rrIO4A6AAwcOeHxuriCIi4ujEydO+B0EHAD0k4ECQOuPO5wCvsYnQqlh5gQy33ujDIzG0YOoKCGW
98swiCfjAFIAENDt27fPu/NzA8HRo0f9CoLAA+DQdicAWkbeTDXzZlHT2MEOL4BU0BI2iIpjjvGS
LgJBpITSUrAAQK0MDAPCcLt37/b+HF1AgLqCP0EQ8B6At3bm7mvnPE2N44bIADADgOhOAMTMHnel
YCUAu3bt6tp5uoGABZd+AQEHADcoYGKA+J9V8/6L4XdRy/ABsmIQB+D0CRkAWoGgtBQsAMBw744d
O7oOqwsIUGCKiorSHYKAA6C9+jxL+fo55/7DgsgsSQXbmFeoevAOqigqpMLCQo8AgOABhBcAAFu3
br0yj+UCgujoaPrpp590hYADgFGvgAGgvoasE+92WQSSZgH19fX8posYQDkpxFUgiOwhIiLiyrst
NxAcPHjw18cee0wXCAIPgGYT2d6Z7hEAOeNDHHUA3Hjp6idX3YDUC6xYscIxJ+BaQYByM0s3dYEg
8ABABL9ns1sA2hkAeS+EU1VVlQMAGF1rYohWPQC1gGXLltGGDRto8+bNtGXLFt4tbNu2jccHO3fu
5IEisgWkjPv37+e1A9aqeRGJ9fO8msgif15Wlur48eM8FrAXivBen0PAAUCeHFBjAcV5LMUb6Hos
gMUD+f98iZeCAYDaiKerWECZEWDCBySGfJVCS9b6m1Tic8RnISMAMAKUvXv3+hSCgASg/WIj2d56
ziUArUP7UcGyD7DCl/fl1dXVmvMDPEkJIUyeRZcCmAAWBM8CSRaT8plEQri37gQYAIDwGl999dWG
Xp3rNnsbAGjMAGrdt8UlADaWFRR+u4YbBf2t1toHb8YGlBAIENQgUIKgBoT0dWQc6BbgDVi38n/M
LgOZrvcJAGL1i/Sm+H03UJhN1kdDNOcDYMCo+FAkv7kYDdRa/9gVALQ8gQBACoEWCErhOIwcIiZA
HMHsEsLUzwDAxYwg2+K31Vs/04XQAVTKbiyuDTdXOinkSmIBqRdQegI1b6CEQUs4DmMP+I2gkNll
FFN/nwCAICngAMB5xh4ji2QASApAxcNDHO4fN1gUgbyFQDlRVIAg9QRacYESBHcSo5aICZhdxjD9
yQDAlRrryfavaaoAlL0wjgeAAKC8vNyja/IUArXuQOoNtGBwJbEMDd9nB2Cc3T4GAC4LLFG7eRlY
GQNUP3gn5WdmcAC8me/gaXeg1SVogeCJUBjC9yA99CkAYgVsIAKAsQHryxOcK4Ehfan0i4+5EeAJ
vLkmd4GhKxCUMCiBcCUAgO8xAPBStg9edi4EBbM4YFIolRcX8SgbU8S9AcDTBSSuQFCDQUsiBsD3
oKhkAOCp6qrJ+lK4aip4aVg/ylr7b14E8nbtgzsIPAFBDQgtIVBFd6ULAKh3Ky88YDKBnDSyPj5M
fTwghKWDk0dQRWHnaKC7TMAbCLS8gRQELSDUJApD+B6sKfApAGITBGn/JwXBn9X68wGy3PdHzYrg
pXuDKHnlEu5mMTDU5e9xsaGEFAQlDGpAKIVzgwdAsIrPR0HIAMBD2b5Z7npUkHmB6qlhVJqd6Vgl
dCUQaG0sJYVBCYQaFFIBABgf3RQ+3wDAGwA++YfboWHEAj8v+Bfva+EFrvT63IHgCRBSiUAQXQE+
314J9B0ASD8CEoAq5jJfmegWAEwatU4OpaxTJ3m+jYzgqnQ/ChCkMGhBoRQAEHEAvIEBgDcGSEsg
y9g7ZcZuDruVrMOc1w1eCulDx//7Xaq0b/96ta9RwKAEQg0KqaSeAEDABlg/YADgyU0/uke2JhCL
QaoW/IMa5j6nWh6+NH4wJR/YwwG4Wl7AEyC0JJ2JBOH/ugAgdsISc9eE/BoAdn62dYud5gHmb1pH
hXEx1KySGXRgweg7L1NxYSHvCvS+RqWnEKk3ZgnpCgBOxu8BqKsh6xtTnIK9pKhDvPRb/d7MztVB
yuLQyP6UsvM7KmcZgehzezwAYiu0QAKgNS+DLA/dITNux8QQSo6P42XVrNPR1PLAINmCUVEibp4Z
Trks80HqZbPZ9Dl/lVhBAIBJIQYA7m7gqSNywzKZ3p5OORnpvKp2rrSUWjetVE8LWVeQtmopFRYW
8NTQH1q/AYA3slnJFvFvJ6NmrFvODN85C6iq8gK11VSS9Zkw9XGCB26lnDMJjskierd+KQBYLOJT
ADABIaAAgGFnhssNGhpEMT/sdkwCqa2p6bzZUbs0awNlc6ZSfl6eY7hYT+MbAHhzEwtzZFPBEOx1
PHUfpZyKdkwDc5R8LzaR7YNZ2msHNq/nXQZq8nq5fiUAWCRiAOAKgAPfy90/a83n579Gxaw1w5iY
BSw9/9bkWLKE3araFTQ+eDvlpyQ5Rgv1av26AiBd++b3ALQ0k23BbEX615cOrFvpeMCDdDcQx5jB
/65QXVWMrqDkb49TSXHRNa0NuDO+FAAsGdMNAFG6FBD4nVj/b3n4z/L0L3wIxR/cxyd/Yn4j3LnT
+zBu8PdJql6g/Z4gKli9hAWOlXw18bU4b1flYCHdAMAMlEABwJYU47QItGnO01SYmcHdPwDA+Wu+
d/QgVS/Q/OBtlHn0Rw4A4oerec7uxgOUAGCFkAGAmuAqFYtBkP4lfPo+VVdVcuMjrUNxRxOgjcvJ
gn0FVWoD+U/eTzlpqRyCq3X9nhrfAMATNZtko398gOeBgXR65/eO5/sgDnD5GSwrsM6erF4gursP
lbz5PJWVlPDt5XxpfCkAWEruUwCke+L7MwA2xf5AHcF9qOGlR6gkJ9vh/j1y3/lZLI4YrF4gGn4T
5a5aTDXVVdqehMUTXOaWK+73DQC8aVEL33BqsTEfzqV6+8bOcP+enrft2A9kCe2vGg+0hQ2k7E3r
qTQpjpoO7aTWrxaTdcEcss55iqyvPkHWKfd3avpY5k2eJOvHs8n284Eut3whsYMJ9gnQFQCMTQsI
/EYs/bM8cJs8+h9zOyXt3MrTvpycHD7dy6vPRDlZIzU0338LmZmXsLLflrv7uZ11hA0r2hbNZZ6j
2u0sIC3pBgAmIfo9ACcOyoK3jpAbqOK5sVRWkM93AUP/j37bq8/MyyTL+CGaK4xt7oyuUlnM2r2F
d0eY4tVVALBZhAGAQhbmZsXsH6t9unf80o9Y9F/F9/FBCVjMqlEVrsd0kawZyWRZPp8sWEswYoD6
lnMqMuM49v02IY19CS7Me5kyMzJ4lwQgAwIA6XNx/BIAZjjL2MGy1tnMWm7WscOOdfVY/eP0PouF
rDWsW8g6S9YvP+40umJbeWE43vcHaz+AombOFKpYOo/yl82nws/epbIZj5J5WJBsvgHO69znH1JW
ZiaHEl2SNwCIaiC2iTEAkGrH17It4i8xI8bOnMzzdfT9fPDHZLp8fEM9d+/WbRvI8vQITReP3P/S
0H7UNuoWqh37Z6oKG8Snlakd3zJqIOWt+5yy0lI7t3pj4JnemCqD5hLrllL37+bnJGISbwGAFzAA
ULp/FnlLDdc2/I+UsfUbPoADV4sZtTjOVpxPVmwmjQdKqBR7rMLow/pROwvwEh8fSckfvklZ276l
MpZKpp08TjXhwdwbaO09aJv5COUf2kPn0s9S+6sTZedlZbAlJyXx1g8Aqr0MCAUA2CzKpwBgFqoA
QMxl9xsAzpfJAjUYoWRiKM/T+SaQWZmUcvQwFa38jNpYaqZpdJYyYswg5flwil34HuX8uI+3UGQQ
Yh8ftOzC2BjqmDpSe/Mp++d1YLdSyfRzeJLULRv5/AJ0SQhKMefQGwDEPdcdABRTxAnprvXLHDca
xrTeE0R5a5ZRPnPFJ9csp9LXn6VLowZwo5gVc/+4ocbcRoUvPkrHF39Eaft3URkzNgyPSB3ZA7wI
RgHFGAC+04L5hizndxUUWhVl5JK501jwl85bP6bZY5KJdGWQJxL3HDuFGQBA9XVkmTFedtNNof2p
Yv5rZHr2AT4MjH5XNrIX3FkgMj//EMUueJvitn9HucwwYgUuugwYHe4Zo4aa311dSeY1i1im8CfX
3oB9V/GcqZSdksxjEcywBljolgIGAOnDEf0KAKz8ZQGa7MYH92FpWB9uaKvCzXeE3UJ1/5xGUV+t
orTTMY5NmmB0GL/SPtzr1bWdPkqWj14j8+hB3NgArjN4ZOfwbBglfLGQctLT+SQUtHwxHO2t8cU9
h+zbxBkAWD78u1s3zA3/xL2U+eUiij24j3KZMTAvQBSH8G+0fqwE6up5tLCswpSWRNG7d/B5AynL
F1HU99/RmegTVMAMDqOj5YupZV0xvq4ASJ+O6RcANDNjbVmrOo1LuN525PMsO0jctIHOJsRTITMC
AjnR2tHPi/V2XTa8YoUvFwOppqaadyHwLvgudCkISLvi9tWETad1A0BctC4AoN9d+QlZHgl2ejAk
DwCZ+zeFDaTyRW9TOov8M1kgiBaI1i76dmkwd1UNr5B0USe8y9UwvPg83QEQJHd1UMMrmVgrPV9O
5tWLOmfraFTqaicMpbxViymdGV1U2iAxB9DblEtL7tby+0KRkZG+BQDz0HUBoOIcmSM+V52mJRv2
DQ2ilKhD3NjIsWF4uN7qKxh580fDi51Euj8AMPwP35FlapislduCO928ci1f+byXKYO1fFFiRX/b
3Qwv3XsQD5/ongCwvrIl5giZZ01wMjKi+YYnR1DdE/fJ0juM+acc/ZH381lZWbzldzfD6w6A9BHp
1wyAnDQyL3xTVqMXZVVr+F10fNH7VHAwkur+GsY9gaiwpSz9kApYOifq610ZZ/dnoysBwPXhETS6
ASAenXa1UhoOUuQmMj81XDELl7XucYMp9b1XKWF/JF+lm7b4fUdJt411BS1TR1EqS/FEkUVs+96V
6NrVLl3+IrR+3AefA4CFCNcEgIoyMs/7m2zunSiflr/+LCXs2UnnWO6O9C113y7qsFf9uOu/fwAl
RqymMqz1Z64fHsDTIosnW7P5MwB4EJXuAEglbqhXrT4misySsXizvdW3P3kvxTLD5tlbNKL6/PQ0
Mj09svNpn2KO/pxnqCA3x1FlQxm3OxpdKrh/BLh+B4C3QLRsXkOWh25XlGxvoPx3ZlF63GnHoAzc
OnL4is/ecUyswG/z48Mo48QxfhyOQQCI7/VkV85ABwCg43F0PgUAK1G8AcAVFC0I9BRLt1vHD6Hk
jWspNzuL1+fRooVLLzuwk9qH97/s+kcOoITVn3PvgOMg5PvdydDdEoCm+jpqXjTXeds2FtVnHDtM
OczVozXDoAjq8J7Gw5FkC7tFNq6ePXc6FeZ1jqlj5xLU23uC8SH0/5ibgIdS6gqAu82NnVRTTaZP
33JatFk4LZyyUpK50bEHAYyK0Tm8pyk1kcwTQmTHV00Jo+zEOB70YcUyvARuitfnE6DSDQAsRboS
AC5uXS8fvAnuS9kvTuDGT09P58bMycnmLo5H8ge+5wsvpIUg0yPBlBZ1kAeFOF4Efj3F+GLXcMRE
eDRtQAHQ8uJ4+XDt0H6UduSQY7sWUM1bfW4mmdctJvO9N8lW4bQ88hdKYWkgWjw8BVw/plT1JOML
AHDPfA4AliJdCQDS1uwYvXt+LEUuX0JHNm2k1Mjt1LJpBTVPDXPqJlAFTP5hJzc+Wn66fUJHTzO+
FAA8oDqgAFCbgs3r+0P7UAcmbIYGkfmuGxQ1gT7U/Nf76QwzPjIDtPyebHwIXR6yn4ADwDzyZo9m
z4rMAJXAzNefo7Mnj3Pjw+VDPdn4AgDMasJj6n0KANaiCQCk+9d7qqaN/3GawaOEoMM+4mebNpai
1yynrPQ0PriDzSkQ8KHP9/Z7u5sQKwUkAHW1NdRw8jBdXPoetT0+lDqYy+czZzFhM/RGapsYQuXz
Z1P0t+spMfok5TLDi+wAQSLI7+nGFwCg+rlp06YAA0CoqoqKsjIpjhk56oc99GPkLjq6fx9FHz1C
KUlJPMVDa4e7RykYF4zc1zC+zgBgMeJVAUAhkdcisEH1D+4NF4nXDcM7C/cKHvGbb77pHgAY8h4A
eEafA4DFiAYA+kusYtq4caN+AIhBCUO+l3h+sAFADxViJVREv/76awMAAwAfAoDlyO0JvbiaevWi
akO6qJgpkykiIsIAoCeqiCldDwCwHNkAwH8A2LBhg34ANLATqNJZ1sOH6dKlS05qTUwk01tvyY5V
O04In4Pjq/v2dfoOV++D9LjuQqY0PQDAcmR/BQD/VgLRvHChkyE7WB4tjlUe31ZQQHUhIaoAKN8n
pBcAZ5nWr19vAKBsiTAgjCWMpjSk0mho9ebVq2XeQw0AvYztCoC1a9caAKi5YrXX3RlS+p7GSZP8
GoA8phQ9AMB69EAAAK5cuHRPDQmju+o6/BGANWvW6AvAeZ0lBQD/rxo4kJol7ryJBXbiWKkh1T6r
knUFaseI12ysa6gdPVomva47lylZDwCwHt1fAVCL7GFUTwHQOsZVBqAnAElMqxnsBgCKLAAeoHHG
DI+M6w0A6E7gUaTqcQBgPbo/dwGu5A4AuPSudB16KIcpkWnVqlUGAFcLABML/MQxNSyV9HcA4vUG
oI6dQIXOskgAcHesOA7vUf6tgXUZWn939T69lG0HYOXKlQYA3gKg7MsR3Uv/doEFjv4OAMYB4vQA
ADtSSAeDAhEANbXX1fHgUWl8fwUAAWAM04oVK/QDoG1bZ3/kLzfFnZQ5vFA16+8D5Roq7BlAjF4A
oPAgAIBav+gMBt0NX3pT5fI2IPLm5pV7oRIvVOyFirxQoULpEuNHMS1ZssTiSwAenjt3bgl/eKTd
AwgBBMj6zGWZB11Wy+8vq7lXpy5K1CRRg0R1dmnBpAaCliG1jCC9wXl25UqUI1G2RJl2pUuUJtFZ
u1IkSpYoSaJEu+IlipMoRqJouyKYpk+f/guzy1hfADCA6cGgoKAVCxcutGHT6POfqBteanw1w2sZ
X83wWsbX8gDdwfDxbgx/iGkt07tMzCbzmUYz9b/WANzIFMo0q3fv3hGTJk0qnjVrVo0hfTR+/HjG
Za/lTNOZhjL94VoD0IfpDqZHmd5gWsq0mul/DPlU65hWMX3K9CrTGKZbma6/1gD8jimI6b/sQcc0
ptlMbzLNNeQzvWk3/LPokpnuZOrH1PtaA9DbDgFczSCmu5mGM40y5HPdxxTMNJCpL9N1vXz4c53d
3fSzxwX9DflcN9oN/3tftHzjpwf8/D/3EKXGD+wE0AAAAABJRU5ErkJggg==';
        $return[]='<div class="hw_blobdisp"><a href="image.php?_id='.$this->objRecord->id[0].'&_view='.$this->objRecord->view.'&_field='.$this->getKey().'"><img src="data:image/png;base64,'.$icon.'"/></a></div>';

        break;
      default:
        $return='Edit dbDispBlobtype to display Blobs of type '.$mimetype;
        break;
    }
    return join("\n",$return);
  }

  /**
  * @brief To display a non-editable edit UI for the general case
  * @date 2011-11-09
  * @version 12
  **/
  public function displayField() {
    $blobinf=explode(',',trim($this->getval(),'()'));
    $mimetype=$blobinf[0];
    $blobid=$blobinf[1];
    pg_query($this->objRecord->dbh,'begin');
    $handle=pg_lo_open($this->objRecord->dbh,$blobid,'r');
    if ($handle===FALSE) {
      print "Bad handle returned for ".$this->getval()."\n".pg_last_error()."\n";
    }
    header('Content-type: '.$mimetype);
    header('Content-Disposition: inline; filename="'.$this->objRecord->view.'_record_'.$this->objRecord->id[0].'"');
    $result=pg_lo_read_all($handle);
    pg_lo_close($handle);
    pg_query($this->objRecord->dbh,'rollback');
    if (!$result) {
      echo base64_decode('R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==');
    }
  }

  public function viewcell(&$row) {
  }

}
?>
