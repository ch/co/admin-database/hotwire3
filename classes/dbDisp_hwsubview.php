<?php
 /**
  * _hwsubview type:
  *   subview_name - name of a subview (must be in hotwire._view_data!)
  *   subview_field - field in subview to match against id of current view
  *   target_view - name of a view to jump to (Null? No jumping)
  *   subview_reference - field from subview to match against target (Null? id)
  *   target_field - field in the target view to search (Null? id)
  **/

 /**
  * @brief A class to display a subview
  * @author Frank Lee
  * @date 2012-11-09
  * @version 12
  **/
class dbDisp_hwsubview extends dbDispBaseWide {

 public function __construct(&$dbColumn) {
   $this->hwDebugCreateStart();
   $this->dbColumn=&$dbColumn;
   $this->fieldname=$this->dbColumn->name();
   # If we're about to display a dbRecord:
   if (is_a($dbColumn->parentobj,'dbRecord')) {
     $this->objRecord=&$dbColumn->parentobj;
     $this->key=$this->fieldname;
     // Individual RO field
     $this->ro = (preg_match('/^ro_|_ro_|_ro$/', $this->dbColumn->name()));
     // No permission to add (shouldn't get here)
     if ($this->objRecord->propertyEqual('action','new') && (!$this->objRecord->perms['insert'])) {
      $this->ro=TRUE;
     }
     // No permission to update
     if (!$this->objRecord->perms['update']) {$this->ro=TRUE;}
   }
   $this->hwDebugCreateStop();
 } 

 public function viewcell(&$row) {}

 /**
  * @brief Displays an editable version of the subview
  * @author Frank Lee
  * @date 2012-11-09
  * @version 12
  * @todo Don't do a separate call to the DB, use the list of views
  **/
 public function displayEdit() {
  if ($this->action=='insert') {
   return;
  }
  $vi=$this->fieldname;
  $list=new dbList($this,$vi);
  $vala=explode(",",trim($this->val,'()'));
  $destview=$vala[1];
  # If we have permission to add to that view, display an 'Add' button
  $sql = 'SELECT "insert", insert_rule from '.
           $this->hwschema."._view_data WHERE view = '$destview'";
  $dbQuery = pg_query($this->objRecord->dbh, $sql);
  $row = pg_fetch_assoc($dbQuery);
  if ($row['insert']=='t') {
    if ($row['insert_rule']!='t') {
     //print "Need insert rule on $destview";
    } else {
     //print "Add a row\n";
    }
  } else {
   //print "No insert permissions on $destview according to $sql";
  }
  return $list->returndisplay();
 }
}
?>
