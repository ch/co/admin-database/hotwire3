<?php
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

 /** 
  * @file dbNavBar.php
  * @brief Provides an interface to uploads CSV files to the database.
  * Part of Hotwire http://hotwire.sourceforge.net/.
  * @author Stuart Taylor
  * @author Frank Lee
  */

 /**
  * @class dbCSVimport
  * @brief Provides CSV import facilities
  * @date 2011-07-11
  * @version 11
  */

class dbCSVimport extends dbList {
  public $sort;
  public $view;
  public $action;
  public $incomingfile;
  public $csv_fieldmap=array();  # Mapping between CSV column names and columns in temporary table
  public $temp_fieldmap=array(); # Mapping between temporary table columns and query columns
  public $sqloutputdebug=0;      # Count our SQL debug-output blocks

  /** 
   * @brief General importing routine.
   * Keeps track of what the state of play is and then performs the import
   * @date 2011-07-11
   * @version 11
   * @todo Split this up into more manageable functions [2011-07-11]
   * @todo handle update rights without import rights.
   **/
  public function import() {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
    error_reporting(E_ALL);
    ini_set('display_errors',1);
    ini_set('auto_detect_line_endings',1);
    if (!$this->view) { return; }
    $this->getPostVars();
    $this->dbInit();
    $this->getViews();
    $this->getColAttrs();
    $this->getPerms();
    $this->getTable();
    # Now what? Need a navbar, surely... ... hmm, maybe later
    if (!isset($this->action) && isset($_REQUEST['action'])) {
      $this->action=$_REQUEST['action'];
    }
    $this->echoDebug('Action is '.$this->action);
    $name='upload';
    $stage='incoming';
    $newdir=dirname($_SERVER["SCRIPT_FILENAME"]).'/'.$stage.'/';
    // Check that the directory exists and is writable
    if (! is_dir($newdir)) {
      $this->crashandburn("Was expecting $newdir to be a directory, but it isn&apos;t");
    }
    if (! is_writable($newdir)) {
      $this->crashandburn("Was expecting $newdir to be writable, but it isn&apos;t");
    }
    $newfile=dirname($_SERVER["SCRIPT_FILENAME"]).'/'.$stage.'/'.basename($this->view).'-'.session_id();
    # Have we just been POSTed a file?
    if (isset($_FILES[$name]) && $_FILES[$name]) {
      $this->echoDebug('Got a file called '.$name);
      if(move_uploaded_file($_FILES[$name]['tmp_name'], $newfile)) {
        $this->incomingfile=$newfile;
        print "<p>Uploaded file received</p>\n";
        # Get the headers from the file
        if (($handle = fopen($newfile, "r")) !== FALSE) {
          $headers=fgetcsv($handle);
        }
      } else {
        $this->echoDebug('Unable to move file');
      }
    }
    # Have we just been told to restart?
    if ($this->action=='restart' &&
      file_exists($newfile)) {
      unlink($newfile);
    }
    if (file_exists($newfile)) {
      print "<p>Using file uploaded at ".
             date ("F d Y H:i:s.", filemtime($newfile))."</p>\n";
      print "<p>If this file is incorrect, you might need to <form method='post'>".
             $this->setPostVars(array('view', 'sort', 'pagelen')).
            "<input type='hidden' name='action' value='restart'>".
            "<input type='submit' value='Remove file and restart'/></form></p>\n";
      # Some setup now
      $querycols=array_keys($this->colAttrs);
      $this->echoDebug($querycols,'Query Cols');
      $csvcols=$this->getcolsfromcsv($newfile);
      $this->mapfields($csvcols,$querycols);
      error_reporting(E_ALL);
      switch ($this->action) {
        case "update":
          $this->csvupdate($csvcols,$querycols);
        break;
        case "update2":
          $this->csvupdatedo($csvcols,$querycols,$newfile);
        break;
        case "insert":
          $this->echoDebug('Calling insert');
          $this->csvinsert($csvcols,$querycols,$newfile);
        break;
        default:
          # No action specified: repeat action bit of form
          ?>
   <form enctype="multipart/form-data" action="csvin.php" method="POST">
<p>You may use the uploaded file to update specfic columns or to insert rows to the view. </p>
 
<p>Please choose one:
<select name='action'>
<option value='update'>Update existing records</option>
<option value='insert'>Add new records</option>
</select></p>
<input type="submit" value="Process File" /> 
<?php echo $this->setPostVars(array('view', 'sort', 'pagelen'));?>
</form>
<?php
      }
    } else {
      # No file has been uploaded, present uploading form
?>
<form enctype="multipart/form-data" action="csvin.php" method="POST">
<p>You may use the uploaded file to update specfic columns or to insert rows to the view. </p>

<p>Please choose one:
<select name='action'>
<option value='update'>Update existing records</option>
<option value='insert'>Add new records</option>
</select></p>
Choose a file to upload: <p>
<input name="<?php print $name;?>" type="file" /><br />
<input type="submit" value="Upload File" />
<?php echo $this->setPostVars(array('view', 'sort', 'pagelen'));?>
</form>
<br/>

<h2>The format of the CSV file</h2>
<p>Please note the format of the CSV file: The first row should have titles for each column. Those titles matching the list below will be imported, others will be ignored. Do watch out for extra spaces and capital letters which will cause the titles not to match! Also watch out for column headings which match the headings below but which you don&apos;t want to import: these will need renaming or deleting before you upload.</p>
<?php
      $querycols=array_keys($this->colAttrs);
      // Remove the Id field from this list.
      $querycols=array_diff($querycols,array('id'));
      $titles=array_map(array($this,'hdrFormat'),$querycols);
      print "Column titles are:\n<ul>\n<li>".
            join("</li>\n<li>",array_unique($titles))."</li>\n</ul>\n";
    } # Check staged file exists
  } # function import


  /**
   * @brief Inserts data from the CSV file into the database
   * Creates a temporary table with datatypes matching the original view
   * and imports the CSV data into that, then runs an update command to update
   * the original view
   * @date 2011-07-11
   * @version 11
   **/
  private function csvinsert($csvcols,$querycols,$newfile) {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__);
    $tmptable='foo';
    $this->createtmptable($tmptable);
    $this->csvsuckb($newfile,$tmptable);

    # Handle HID fields
    $destfields=array();
    $srcfields=array();
    $tables=$tmptable;
    $prefix='';
    # See if this is a hotwire view or not
    if ($this->database_entity_exists($this->hwschema,$this->view)) {
      $this->echoDebug('Hotwire view '.$this->view);
      $prefix=$this->hwschema; # $prefix seems to get changed in places so can't just replace it
    }
    foreach ($this->csv_fieldmap as $src => $dest) {
      if (substr($dest,-4)=='_hid') {
        $hid=$dest;
        $id=$this->temp_fieldmap[$dest];
        if ($this->getColByName($id)->isarray()) {
          $hidfield=$dest;
          $idfield=substr($dest,0,strlen($dest)-4).'_id';
          $hid_hid=preg_replace('/^ro_/','',$hidfield);
          $hidtable=$hidfield;
          $hidtable=preg_replace('/^ro_/','',$hidtable);
          $hidfield=preg_replace('/^ro_/','',$hidfield);
          $hid_id=preg_replace('/^ro_/','',$idfield);
          array_push($destfields,'"'.$id.'"');
          array_push($srcfields,sprintf('array(select "%s" from '.$this->hwschema.'."%s",(select unnest("%s"."%s") as hid) a where a.hid="%s"."%s")',
                                                     $hid_id                         ,$hidtable,        $tmptable,$dest,                 $hidtable,$hid_hid));
        } else {
          array_push($destfields,'"'.$this->temp_fieldmap[$dest].'"');
          array_push($srcfields,sprintf(' "%s"."%s"',$hid,$id));
          $tables.=sprintf(' left join "%s"."%s"   ON   "%s"."%s" =  "%s"."%s"',
                           $this->hwschema,$hid, $tmptable,$dest, $dest,$dest);
        }
        
      } else if (substr($dest,-3)=='_id') {
        # Is this an array?
        if ($this->getColByName($dest)->isarray()) {
          $idfield=$dest;
          $hidfield=substr($dest,0,strlen($dest)-3).'_hid';
          $hid_hid=preg_replace('/^ro_/','',$hidfield);
          $hidtable=$hidfield;
          $hidtable=preg_replace('/^ro_/','',$hidtable);
          $hidfield=preg_replace('/^ro_/','',$hidfield);
          $hid_id=preg_replace('/^ro_/','',$idfield);
          array_push($destfields,'"'.$idfield.'"');
          array_push($srcfields,sprintf('array(select "%s" from '.$this->hwschema.'."%s",(select unnest("%s"."%s") as hid) a where a.hid="%s"."%s")',
                                                $hid_id                         ,$hidtable,        $tmptable,$dest,                 $hidtable,$hid_hid));
        } else {
          $idfield=$dest;
          $hidfield=substr($dest,0,strlen($dest)-3).'_hid';
          $hid_hid=preg_replace('/^ro_/','',$hidfield);
          $hidtable=$hidfield;
          $hidtable=preg_replace('/^ro_/','',$hidtable);
          $hidfield=preg_replace('/^ro_/','',$hidfield);
          $hid_id=preg_replace('/^ro_/','',$idfield);
          array_push($destfields,'"'.$idfield.'"');
          array_push($srcfields,'"'.$hidtable.'"."'.$hid_id.'"');
          #$tables.=" left join ".$this->hwschema.".$hidtable ON $tmptable.$idfield = $hidtable.$hidfield";
          $tables.=sprintf(' left join "%s"."%s" ON "%s"."%s" = "%s"."%s"',
                                    $this->hwschema,$hidtable, $tmptable,$idfield,$hidtable,$hidfield);
        }
      } else {
        array_push($destfields,'"'.$dest.'"');
        array_push($srcfields,'"'.$dest.'"');
      }
    }
    # Transfer from the temporary table;
    # detect _hid fields and ha
    $sql='INSERT INTO '.$prefix.'."'.$this->view.'" ('.join(', ',$destfields).
         ') SELECT '.join(', ',$srcfields).' FROM '.$tables;
    $this->echoDebug("Inserting data into temporary table with this command: <pre>$sql</pre>");
    #print "<p class='debug'>SQL for populating temporary table: <pre>".wordwrap($sql)."</pre>\n";
    $this->output_sql_debug($sql,'SQL for populating table');
    $dbQuery=pg_query($this->dbh,$sql);
    if (!$dbQuery) {
      $this->sqlError('Insert data from temporary table to permanent table',$sql);
      $this->dumpmap($csvcols);
      exit;
    } else {
      #Confirm the number of rows back to the user
      print "<p>Transferred ".pg_affected_rows($dbQuery).
            " rows into permanent table</p>\n";
    }
  }

  /**
   * @brief To obtain the column titles from a CSV file
   * @date 2011-07-11
   * @version 11
   */
  private function getcolsfromcsv($file) {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__,
                        array($file));
    if (($handle = fopen($file, "r")) !== FALSE) {
      $headers=fgetcsv($handle);
      return $headers;
    }
  }

  /** 
   * @brief To display the field mappings in case of problems
   * @date 2011-07-11
   * @version 11
   */
  private function dumpmap($csvcols) {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__,
                        array($csvcols));
    print "<p>Here is the mapping between fields in the CSV file and fields in the query:</p>\n<ul>\n";
    foreach ($csvcols as $ccol) {
      print "<li>$ccol =&gt; ".(isset($this->fieldmap[$ccol])?
                              $this->hdrFormat($this->fieldmap[$ccol]):
                              "&lt;Ignored&gt;")."</li>\n";
    }
    print "</ul>\n";
  }

  /** 
   * @brief To create a mapping between the CSV columns and the query columns
   * @date 2011-07-11
   * @version 11
   */
  public function mapfields($csvcols,$querycols) {
    $missing=array();
    foreach ($csvcols as $ccol) {
      $found=0;
      # Ignore if we have already found a column; "First match"
      if (array_key_exists($ccol,$this->csv_fieldmap)) { continue ; }
      foreach ($querycols as $qcol) {
        # Look for a similarly-named field on the destination view and in the CSV
        if ($this->hdrFormat($ccol) == $this->hdrFormat($qcol)) {
          # Fields in destination view ending _id are renamed _hid in the temporary table
          # to make it plain that the temporary table contains the human-readable form
          if (substr($qcol,-3)=='_id') {
            $this->csv_fieldmap[$ccol]=substr_replace($qcol,'_hid',-3);
            $this->temp_fieldmap[substr_replace($qcol,'_hid',-3)]=$qcol;
          } else {
            $this->csv_fieldmap[$ccol]=$qcol;
            $this->temp_fieldmap[$qcol]=$qcol;
          }
          $found=-1;
        }
      }
      if (!$found) {
        $missing[]=htmlentities($ccol);
      }
    }
#print "CSV_Fieldmap: <pre>".print_r($this->csv_fieldmap,1)."</pre>\n";
#print "temp_Fieldmap: <pre>".print_r($this->temp_fieldmap,1)."</pre>\n";
    if ($missing != array(0=>'')) {
     print '<div class="warning"><p>NB: The following fields in the CSV file don\'t match any updatable columns'.
           ' in the table and will be ignored. This may or may not be what you '.
           'want, so check before you go further...</p></div><ul><li>'.
           join('</li><li>',$missing).'</li></ul>'."\n";
    }
  }

  /**
   * @brief To suck a CSV file into a temporary table
   * @date 2011-07-11
   * @version 11
   */
  private function csvsuckb($file,$table) {
    if (($handle = fopen($file,'r'))===FALSE) {
      print "<p class='error'>Error: Cannot open CSV file $file</p>\n";
      exit;
    }
    $headers=fgetcsv($handle);   # We don't need the headers
    $rows=array(); 
    # Which fields (integers) should we use?
    $colarr=array();
    $i=0;
    $titles=array();
    foreach ($headers as $h) {
      if (isset($this->csv_fieldmap[$h])) {
        $colarr[]=$i;
        $titles[]=$this->csv_fieldmap[$h];
      }
      $i++;
    }
    mb_detect_order('ASCII, UTF-8, ISO-8859-1');
    while ($data=fgetcsv($handle)) {
      # Convert this to UTF-8 
      $row=array(); 
      foreach ($colarr as $id => $colid) {
        # Treat empty cells as NULL 
        $colname=$titles[$id];
        $val=$data[$colid];
        $val=mb_convert_encoding($val,'UTF-8');
        if (array_key_exists('array',$this->colAttrs[$this->temp_fieldmap[$titles[$id]]])) {
          # array case
          array_push($row,"string_to_array('".pg_escape_string($val)."',';')");
        } else {
          # scalar case - RFL wonders about the value of explicitly setting NULL here.
          array_push($row,($data[$colarr[$id]]!==''?"'".pg_escape_string($val)."'":'NULL'));
        }
      }
      array_push($rows,"(".join(',',$row).")");
    }
    if (count($rows)>0) {
      $sql='INSERT INTO '.$table.' ("'.join('","',$titles).'") VALUES '."\n".join(",\n",$rows);
      #print "<p class='debug'>SQL for populating temporary table: <pre>".$sql.";</pre>\n";
      $this->output_sql_debug($sql,'SQL for populating table');
      $dbQuery=pg_query($this->dbh,$sql);
      if (!$dbQuery) { $this->sqlError('import data from CSV file',$sql); }
    }

    $sql='select count(*) as count from '.$table;
    $dbQuery = pg_query($this->dbh,$sql);
    if (!$dbQuery) { $this->sqlError('count rows from temporary table',$sql); }
    $row=pg_fetch_row($dbQuery);
    print "<p class='info'>Inserted ".$row[0]." rows into temporary table</p>\n";
  }
  

  /**
   * @brief Create the temporary table for the CSV import
   * @date 2011-07-11
   * @version 11
   */
  private function createtmptable($tmptable) {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__,
                        array($tmptable));
    # Creates a temporary table with columns matching the name and type
    # of the CSV. We use fieldmap
    $this->echoDebug('Building tmp table');
    $this->echoDebug(__FUNCTION__);
    $csvimp=array();
    $tempcols=array();
#print "Col Attrs: <pre>\n".print_r($this->colAttrs,true)."</pre>\n";
    foreach ($this->csv_fieldmap as $ccol => $qcol) {
      $vcol=$this->temp_fieldmap[$qcol];
      if (!array_key_exists('type',$this->colAttrs[$vcol])) { continue; }

      // Are we updating an _id field?
      if (preg_match('/_id$/',$vcol)) { 
        if (array_key_exists('array',$this->colAttrs[$vcol]) && $this->colAttrs[$vcol]['array']) {
          array_push($tempcols,'"'.$qcol.'" varchar[]');
        } else {
          array_push($tempcols,'"'.$qcol.'" varchar');
        }
        continue;
      }
      # colAttrs applies to the view's columns
      $type=$this->colAttrs[$this->temp_fieldmap[$vcol]]['type'];
      #if ($type=='numeric') {
        #TODO - this should happen correctly!
        #$type='numeric(5,2)';
      #}
      if (array_key_exists('array',$this->colAttrs[$vcol]) && $this->colAttrs[$vcol]['array']) {
        $type=$type.'[]';
      }
      array_push($tempcols,'"'.$qcol.'" '.$type);
    }
    # Now need to create a temporary table with the right types
    $sql='create temporary table '.$tmptable." (\n ".join(",\n ",$tempcols)."\n)";
    $this->output_sql_debug($sql,'SQL for temporary table');
    #print "<p class='debug'>SQL for temporary table: <pre>".$sql.";</pre>\n";
    $dbQuery = pg_query($this->dbh, $sql);
    if (!$dbQuery) { $this->sqlError('create temporary table',$sql); }
  }

 /** 
  * @brief Display a collapsible block of SQL output
  * @param[in] sql SQL to display
  * @param[in] title Heading of block
  * @date 2019-09-12
  * @version 1
  **/
  private function output_sql_debug($sql,$title='SQL') {
    print "<div class='wrap-collapsible'>\n".
          " <input id='collapsible_sql".$this->sqloutputdebug."' class='toggle' type='checkbox'>\n".
          " <label for='collapsible_sql".$this->sqloutputdebug."' class='lbl-toggle'>".$title."</label>\n".
          " <div class='collapsible-content'>\n".
          "  <div class='content-inner'>\n".
          "   <pre>".$sql.";</pre>\n".
          "  </div>\n".
          " </div>\n".
          "</div>\n";
    $this->sqloutputdebug++;
  }

  /**
   * @brief Present the user with a form to specify which columns should be matched and which
   * updated.
   * @param[in] csvcols Columns present in the CSV file
   * @param[in] querycols Columns present in the query
   * @date 2011-07-11
   * @version 12
   */
  public function csvupdate($csvcols,$querycols) {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__,
                        array($csvcols,$querycols));
    # Need to present a form asking which columns should be used as comparisons
    # and which should be used as updates
    print "<form method='post'>\n";
    print "<p>Select columns to match with existing data and which columns to update:</p>\n";
    print "<ul>\n";
    foreach ($this->csv_fieldmap as $key => $qcol) {
      print "<li>$key: ";
      print "<select name='role_".str_replace(' ','_',$this->hdrFormat($key))."'>\n";
      print "<option value='ignore'>Ignore</option>\n";
      print "<option value='compare'>Match</option>\n";
      print "<option value='update'>Update</option>\n";
      print "</select>\n\n";
      print "</li>\n";
    }
    print "</ul>\n";
    print "<input type='hidden' name='action' value='update2' />\n";
    print $this->setPostVars(array('view', 'sort', 'pagelen'));
    print "<input type='submit' value='Do Update'/>\n";
    print "</form>";
  }

 /**
   * @ brief Import the CSV into a temporary table
   * We do now know which columns to compare, update or ignorE
   * @date 2011-07-11
   * @version 11
   */
  public function csvupdatedo($csvcols,$querycols,$filename) {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__,
                        array($csvcols,$querycols,$filename));
    # loop  through columns, figure out what to do with each
    $where=array();
    $set=array(); 
    foreach ($this->csv_fieldmap as $key => $qcol) {
      if (preg_grep('/'.preg_quote($key).'/i',$csvcols)) {
        if (isset($_REQUEST['role_'.str_replace(' ','_',$this->hdrFormat($key))])) {
          switch ($_REQUEST['role_'.str_replace(' ','_',$this->hdrFormat($key))]) {
            case "compare":
              array_push($where,$key);
              break;
            case "update":
              if (substr($qcol,0,3)!='ro_') {
               array_push($set,$key);
              }
              break;
          }
        }
      }
    }
    if (count($set)<1) {
     print "<p>Error: no columns were set to 'Update', so no rows would ".
           "be updated.</p>";
    }
    if (count($where)<1) {
     print "<p>Error: no columns were set to 'Match', so all rows would ".
           "be updated.</p>";

    }
    if (count($set)<1 || count($where)<1) {
     return;
    }
    $tmptable='foo';
    $allfields=array_merge($set,$where);
    $nwhere=array();
    $addtables='';
    foreach ($where as $w) {
     $fld=$this->temp_fieldmap[$this->csv_fieldmap[$w]];
     if (substr($fld,-3)=='_id') {
      $hid=substr($fld,0,strlen($fld)-3)."_hid";
      $hid=preg_replace('/^ro_/','',$hid);
      $addtables.=" LEFT JOIN ".$this->hwschema.".\"$hid\" USING (\"$hid\") ";
      array_push($nwhere,'"'.$this->view.'"."'.$fld.'"="'.$hid.'".'.preg_replace('/^ro_/','',$fld).'');
     } else {
      array_push($nwhere,'"'.$this->view.'"."'.$fld.'"="'.$tmptable.'"."'.$fld.'"');
     }
    }
    $nset=array();
    foreach ($set as $w) {
     $fld=$this->temp_fieldmap[$this->csv_fieldmap[$w]];
$this->echoDebug("Considering field $w which maps to ".$fld);
     if (substr($fld,-4)=='_hid') {
      $hid=$fld;
      $id=substr($hid,0,strlen($hid)-4)."_id";
      array_push($nset,'"'.$id.'"="'.$hid.'"."'.$id.'"');
      $addtables.="\n LEFT JOIN ".$this->hwschema.".\"$hid\" USING (\"$hid\") ";
     } else if (substr($fld,-3)=='_id') {
      if ($this->getColByName($fld)->isarray()) {
       $hid=substr($fld,0,strlen($fld)-3)."_hid";
       array_push($nset,sprintf('"%s"=array(select %s from hotwire.%s where %s.%s =any(%s."%s"))',
                                  $fld,         $fld,              $hid,   $hid, $hid,  $tmptable,$this->csv_fieldmap[$w]));

      } else {
       # w - field in temporary table
       # id - field in destination view
       $hid=substr($fld,0,strlen($fld)-3)."_hid";
       array_push($nset,'"'.$fld.'"="'.$hid.'"."'.$fld.'"');
       $addtables.="\n".sprintf(' LEFT JOIN "%s"."%s" ON "%s"."%s"      ="%s"."%s"',
                                 $this->hwschema,$hid,   $hid,$hid ,$tmptable,$this->csv_fieldmap[$w]);
      }
     } else {
      array_push($nset,'"'.$fld.'"="'.$tmptable.'"."'.$this->csv_fieldmap[$w].'"');
     }
    }
    $prefix='';
    if ($this->database_entity_exists($this->hwschema,$this->view)) {
     $prefix=$this->hwschema;
    }
    $updatesql='UPDATE '.$prefix.'."'.$this->view.'" SET '."\n ".join(",\n ",$nset)." \nFROM ".$tmptable.
               ' '.$addtables."\n WHERE\n ".join(" AND\n ",$nwhere);

    # Need to pull the data into a table - but only fields in $allfields;
    $this->createtmptable($tmptable);
    # Now pull all the stuff from the CSV file into the temporary table
    $this->csvsuckb($filename,$tmptable);
    # We'll verify this by counting the rows
    //$sql='select count(*) as count from '.$tmptable;
    //$dbQuery = pg_query($this->dbh,$sql);
    //if (!$dbQuery) { $this->sqlError("count rows from temporary table",$sql); }
    //$row=pg_fetch_row($dbQuery);
    //print "Inserted - ".$row[0]." rows into temporary table\n";
    //# Now to do the update
    $dbQuery=pg_query($this->dbh,$updatesql);
    #print "<p class='debug'>Update SQL: <pre>".$updatesql.";</pre></p>\n";
    $this->output_sql_debug($updatesql,'SQL to update records');
    # Mostly this always returns zero...
    #print "<p>Updated ".pg_affected_rows($dbQuery)." rows</p>\n";
  }

} # Class
