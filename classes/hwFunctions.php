<?php
 /**
  * @brief A class with non-object functions
  * @author Frank Lee
  * @date 2012-11-13
  * @version 1
  **/
class hwFunctions {

  public $dbh;
  public $message = '';

   /**
   * @brief Returns a DOM-friendly representation of a string.
   * Must start [A-z] then have [-A-z0-9_:\.]*
   * @date 2011-07-11
   * @version 11
   **/
  public function domName($name) {
   # Ensure we start with A-z
   if (!is_null($name)){
    $name=preg_replace('/^([^A-Za-z])/', 'Z\1', $name);
    # Escape underscores with underscores
    $name=preg_replace('/_/','__',$name);
    # Replace non [-A-z0-9_:\.] characters with underscores
    $name=preg_replace('/[^-_A-Za-z0-9:\.]/','_',$name);
   }
   return $name;
  }

  /**
   * @brief Formats the given string as if it were a column header.
   * Columns have various flags in their name, such as _id, which should
   * be removed before displaying to the user.
   * Certain acronyms are also converted to the correct case.
   * Strings are returned in Title Case.
   *
   * @date 2012-11-15
   * @version 12
   */
  protected function hdrFormat ($col) {
    //$this->hotwireDebug(HW_DBG_OPRO,'Called [protected] '.get_class($this).'->'.__METHOD__,
    //               array($col));
    if (strpos($col,'//')!==FALSE) {
     # Remove double_slashes. (could be Menu/MetaView//*)
     $col=preg_replace('/\/\/.*/','',$col);
    }
    $col=preg_replace('/.*\//','',$col);
    if (preg_match('/(.*)_(id|hid|subview|view|report|ro|ssi|html)$/', $col, $match))    {
      $col = $match[1];
    }
    if (preg_match('/^_?(hl_)?(mm_)?(ro_)?(so_)?(x_)?(.*)/', $col, $match)) {
      $col = $match[6];
    }
    //$col = mb_convert_case(str_replace('_', ' ', $col), MB_CASE_TITLE);
    $col = ucfirst(str_replace('_', ' ', $col));
    return $col;
  }


 /**
  * @brief Returns a value from an array, if it exists.
  * @date 2011-07-11
  * @version 12
  * TODO: differentiate between a key which isn't present and a key which is False!
  **/
 public function arrayKey(&$arr,$key) {
  return (is_array($arr) && array_key_exists($key,$arr) && $arr[$key]);
 }

 public function dbConnect() {
   set_error_handler(function($errno, $errstr) { 
     global $LOGIN_ERROR_MESSAGE;
     if ($LOGIN_ERROR_MESSAGE=='POSTGRES') {
       $this->message = $errstr;
     } else {
       $this->message = $LOGIN_ERROR_MESSAGE;
     }
   }, E_WARNING);

   if (isset($_SESSION['dbname'])
   && isset($_SESSION['dbuser'])
   && isset($_SESSION['dbpasswd'])) {
   global $PSQL_HOST;
   $this->dbh = pg_connect(
     "host=$PSQL_HOST  ".
      "dbname   = ".$_SESSION['dbname']." ".
      "sslmode  = require ".
      "user     = ".$this->dbescapepassword($_SESSION['dbuser'])." ".
      "password = ".$this->dbescapepassword($_SESSION['dbpasswd']));
   }
   restore_error_handler();
 }

  /**
   * To escape single quotes and backslashes, then surround in single slashes
   **/
  private function dbescapepassword($pass) {
    $pass=str_replace('\\','\\\\',$pass);
    $pass=str_replace("'","\\'",$pass);
    return "'".$pass."'";
  }



}
?>
