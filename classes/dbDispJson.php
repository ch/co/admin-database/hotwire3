<?php
 /**
  * @brief A class to display JSON fields, which are, more or less, just (hidden) text.
  * @author Frank Lee
  * @date 2018-08-15
  * @version 1
  **/
class dbDispJson extends dbDispText {

 /**
  * @brief Displays an edit control for the text field
  * @date 2018-06-15
  * @version 1
  **/
 public function displayEdit() {
  if (substr($this->dbColumn->name(),0,1)=='_') { return;}
  // fields of type JSON are just text, but we hide them.
  $result="<div id='".$this->id()."' class='readonly jsonhide'>".$this->getval()."</div>\n";
  $result.="<textarea ".($this->dbColumn->htmlview()?'class="hw_htmlview"':'')." name='".$this->getkey()."' ".
           "id='".$this->id()."' >".
           $this->htmlesc()."</textarea>";
  return $result;

 }

 /**
  * @brief Displays an entry in the 'view' table
  * @date 2018-06-15
  * @version 1
  **/
  public function viewcell(&$row) {
   //if (!$this->inhtml) { return; }
   //$view=$this->dbColumn->parentobj->view;
   //$field='id';
   // Replace with _hid?
   //$value=$this->valuefromrow($row);
   return "<td class='jsondata ".$this->classes."'><textarea>"
          .$row[$this->colname]."</textarea></td>\n";
   }
 }


?>
