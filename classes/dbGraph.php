<?php
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

 /** 
  * @file dbGraph.php
  * @brief Handles display of graphs
  * Part of Hotwire http://hotwire.sourceforge.net/.
  * @author Stuart Taylor
  * @author Frank Lee
  */

 /**
  * @brief A class to display graphs from the database
  * @author Frank Lee
  * @date 2011-07-11
  * @version 11
  **/
class dbGraph extends dbBase {
 protected $extras; //!<@brief Meta data for the graph
 /** 
  * @file dbGraph.php
  * @author Frank Lee
  * Displays a graph from the database
  */

 /**
  * @todo Find out why the constant here isn't defined.
  * pChart graphs work by
  * Prepare dataset
  *  Add dataset
  *  Add series / descriptions / axes
  * Defaults are two columns, X:Y, label on X axis is name of X column & similar for Y
  * Prepare chart object - pImage(dataset)
  *  Adjust chart object
  *  Add scale(s?) to chart object
  *  draw chart/graph
  * Render chart object
  */

  public function __construct() {
    $this->hotwireDebug(HW_DBG_OCRE,"Creation of a ".get_class($this)." object");
    $this->dbInit(); // where to abort?
    $this->extras=array();
  }

  /**
   * @brief Produces a plot of a graph
   * @date 2011-07-11
   * @version 11
   * @todo Should use the crashandburn routine
   **/
 public function plot() {
  $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
  // We must have chart_id and chart_view
  if (! array_key_exists('chart_id',$_REQUEST)) {
   print "chart_id was not set.\n";
   exit(1);
  }
  if (! array_key_exists('chart_view',$_REQUEST)) {
   print "chart_view was not set.\n";
   exit(1);
  }
  $chartid=$_REQUEST['chart_id'];
  $chartquery=$_REQUEST['chart_view'];

  // Load information associated with that chart_id
  // Everybody with access to any chart could read
  // all names for charts, so best not to have 
  // "Employees to be fired next month" as a name.
  $chartname=$this->getmetadata('Chart Name',$chartid,$chartquery);
  if (! $chartname) {
   print "Could not find a chart associated with ".$chartid."\n";
   exit(2);
  }
  $chartdata=$this->getmetadataarray('Data Query',$chartid);
  $chartextra=$this->getmetadataarray('Extras Query',$chartid);
  $charttype=$this->getcharttype($chartid,$chartquery);
  $chartcall=strtolower(preg_replace("/[^a-zA-Z0-9]/", "",$charttype));
  
  if (method_exists($this,$chartcall)) {
   call_user_func(array($this,$chartcall),$chartname,$chartextra,$chartdata);
  } else {
   print "Don't know how to handle $charttype charts: if there were a method $chartcall I could do.";
   exit(3);
  }
  
 }

 /**
  * @brief Load metadata for the plot
  * @date 2011-07-11
  * @version 11
  **/
 public function populate_extras($extraqs) {
   $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
  $extras=array();
  foreach ($extraqs as $extraq) {
   if ($extraq) {
    $extras=array_merge($extras,$this->querytodata($extraq,$this->dataqargs($extraq)));
   }
  }
  if ($extras) {
   $extras=$this->columnisedata($extras);
   $keys=array_keys($extras);
   $this->extras=$extras[$keys[0]];
  }
 }

 /**
  * @brief Parse arguments for a chart
  * To parse any general or query-specific arguments from the request array
  * For example, a histogram of marks might need the question number and
  * how many bins to put the data into.
  * REQUEST variable is alldataqargs (for all) or ${QUERY}args.
  * The latter over-rides the former.
  * @date 2011-07-11
  * @version 11
  */
public function dataqargs($qname) {
  $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
    $dataqargs=array();
    if (array_key_exists('alldataqargs',$_REQUEST)) {
     $dataqargs=$_REQUEST['alldataqargs'];
    }
    if (array_key_exists($qname.'args',$_REQUEST)) {
     $dataqargs=$_REQUEST[$qname.'args'];
    }
   return $dataqargs;
}

 /**
  * @brief To re-order the data received.
  * To reformat data from 
  * [0]->array(col=>val, col2=> val) [1]->array(...
  * (col=>array(val,val), col2=>array(val2,val2)
  * @date 2011-07-11
  * @version 11
  **/
public function recolumnisedata($data) {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
 $cols=array_keys($data[0]);
 $newdata=array();
 foreach ($cols as $col) {
  array_values(array_map(function($a) use($col) { return $a[$col]; },$data));
  $newdata[$col]=$column;
 }
 return $newdata;
}


 /**
  * @brief Will we call a method on such an object?
  * @date 2011-07-11
  * @version 11
  * @todo Document this function.
  * @param[in] name ??
  * @param[in] pattern ??
  **/
 public function willcallmethod($name,$pattern) {
  $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
  $keys=preg_grep("/^$pattern/",array_keys($this->extras));
  $textras=array();
  $count=0;
  foreach ($keys as $key) {
   if ($this->extras[$key]==$name) {
    $count++;
   }
  }
  return $count > 0;
 }

 /**
  * @brief To plot a barchart.
  * @todo Make this a little more manageable.
  * @date 2011-07-11
  * @version 11
  **/
 function barchart($name,$extraqs,$dataqs) {
   $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
  include("./classes/pchart/pData.class.php");
  include("./classes/pchart/pDraw.class.php");
  include("./classes/pchart/pImage.class.php"); 
  $defaults=array('title'=>array('X'=>$this->width()/2,
                                 'Y'=>2,),
                  'drawmethod'=>'drawBarChart',
                  'drawoptions'=>array(
                  ),
            );
  $defaults['title']['title']=$name;
  $this->populate_extras($extraqs);

  /* Create and populate the pData object */
  $MyData = new pData();
  $serielabel=0;
  $dataq=$dataqs[0];
  $data=$this->querytodata($dataq,$this->dataqargs($dataq));
  if (!is_array($data)) { $this->baleout("Insufficient data\nreturned by query");}
  $data=$this->recolumnisedata($data);

  /* X data is in the first column; */
  $cols=array_keys($data);
  $xcolumn=array_shift($cols);
  $MyData->addPoints($data[$xcolumn],$xcolumn);
  $MyData->setAbscissa($xcolumn);
  foreach ($cols as $ycolumn){
   $ydata=$data[$ycolumn];
   $MyData->addPoints($ydata,$ycolumn);
  }

  $myPicture = new pImage($this->width(),$this->height(),$MyData);
  $myPicture->setFontProperties(array("FontName"=>"./classes/fonts/verdana.ttf","FontSize"=>11));

  /* Will we call a drawText method? If not, we place a default title on the chart */
  if (! $this->willcallmethod('drawText','pictureaction\d+\.method')) {
   $myPicture->drawText($this->width()/2,$this->height()*0.1,$name,
     array("FontSize"=>20,"Align"=>TEXT_ALIGN_BOTTOMMIDDLE));
  } 

  /* Default draw legend */
  if (! $this->willcallmethod('drawLegend','pictureaction\d+\.method')) {
   $myPicture->drawLegend($this->width()/20,$this->height()*0.1,
     array("Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_HORIZONTAL));
  }
 
  /* Draw the scale and the 1st chart */
  if (! $this->willcallmethod('setGraphArea','pictureaction\d+\.method')) {
   $myPicture->setGraphArea((array_key_exists('lmargin',$this->extras)?$this->extras['lmargin']:0),
                             (array_key_exists('tmargin',$this->extras)?$this->extras['tmargin']:0),
                            $this->width()-(array_key_exists('lmargin',$this->extras)?$this->extras['lmargin']:0),
                            $this->height()-(array_key_exists('bmargin',$this->extras)?$this->extras['bmargin']:0));
  }

  /* Default drawing of the scale */
  if (! $this->willcallmethod('drawScale','pictureaction\d+\.method')) {
   $myPicture->drawScale(array("DrawSubTicks"=>TRUE));
  }

  /* Default drawing of barchart */
  if (! ( ($this->willcallmethod('drawBarChart','pictureaction\d+\.method')) ||
          ($this->willcallmethod('drawAreaChart','pictureaction\d+\.method')) 
        )
     ) {
   $myPicture->drawBarChart(array("DisplayValues"=>0,"Surrounding"=>60));
  }
  
  /* Cycle through any actions and process them */
  $this->applyExtraActions($myPicture,'pictureaction');
  /* DRAW */
  $myPicture->autoOutput("pictures/example.drawBarChart.png");

 }

 /**
  * @brief produce image with error message in 
  * This module must return an image, so we produce an image of the error
  * @date 2011-07-11
  * @version 11
  **/
 public function baleout($msg='Unspecified error') {
  $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
  $obj=new pImage($this->width(),$this->height());
  $obj->setFontProperties(array("FontName"=>"./classes/fonts/pf_arma_five.ttf","FontSize"=>8));
  $obj->drawText($this->width()/2,$this->height()/2,$msg,array('Align'=>TEXT_ALIGN_MIDDLEMIDDLE));
  $obj->autoOutput('pictures/error.png');
 }

 /**
  * @brief Draw an X-Y plot
  * @param[in] name ??
  * @param[in] extraqs ??
  * @param[in] dataqs ??
  **/
 public function xyplot($name,$extraqs,$dataqs) {
  $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
  /* pChart library inclusions */
  include("./classes/pchart/pData.class.php");
  include("./classes/pchart/pDraw.class.php");
  include("./classes/pchart/pImage.class.php"); 
  include("./classes/pchart/pScatter.class.php"); 

  $defaults=array('scale'=>array('XMargin'=>0,
                                 'YMargin'=>0,
                                 'Floating'=>TRUE,
                                 'GridR'=>200,
                                 'GridG'=>200,
                                 'GridB'=>200,
                                 'DrawSubTicks'=>FALSE,
                                 'Mode'=>SCALE_MODE_FLOATING,
                                 'CycleBackground'=>TRUE,
                                 'LabelRotation'=>90),
                  'series'=>array('color'=>'0,0,0'),
                  'grapharea'=>array('left'=>0,
                                     'right'=>$this->width(),
                                     'top'=>0,
                                     'bottom'=>$this->height()),
                  'title'=>array('X'=>$this->width()/2,
                                 'Y'=>2,
                                 'FontSize'=>10,
                                 'Angle'=>0,
                                 'Align'=>TEXT_ALIGN_TOPMIDDLE,
                                ),
                  'drawmethod'=>'drawScatterLineChart',
                  );

   # Set up a graph
   $defaults['title']['title']=$name;
   $this->populate_extras($extraqs);

   /* Create and populate the pData object */
   $myData = new pData();
   $serielabel=0;
   foreach ($dataqs as $dataq) {
    $data=$this->querytodata($dataq,$this->dataqargs($dataq));
    $xlabela=array_keys($data[0]);
    $xlabel=$xlabela[0];
    // Set up X axis
    $myData->setAxisName(0,$xlabel);
    $myData->setAxisXY(0,AXIS_X);
    $myData->setAxisPosition(0,AXIS_POSITION_BOTTOM);
    $data=$this->columnisedata($data);
    $cols=array_keys($data);
    $myData->addPoints(array_keys($data[$cols[0]]),$xlabel);

    foreach (array_keys($data) as $ylabel){
     $ydata=$data[$ylabel];
     $myData->addPoints(array_values($data[$ylabel]),$ylabel);
     // Set up Y axis
     // I guess we ought in the future to have this configurable by the Extras table
     $myData->setSerieOnAxis($ylabel,1);
     $myData->setScatterSerie($xlabel,$ylabel,$serielabel);
     $myData->setScatterSerieDescription($serielabel,$ylabel);
     if (array_key_exists('series'.$serielabel.'.color',$this->extras)) {
      $myData->setScatterSerieColor($serielabel,$this->getcolorspec($this->extras['series'.$serielabel.'.color']));
     } else {
      $myData->setScatterSerieColor($serielabel,array('R'=>0, 'G'=>0, 'B'=>0));
     }
     $serielabel++;
    }
   }

   if (array_key_exists('yaxis',$this->extras)) {
    $myData->setAxisName(1,$this->extras['yaxis']);
   }
   $myData->setAxisXY(1,AXIS_Y);
   $myData->setAxisPosition(1,AXIS_POSITION_LEFT);

   /* Create the pChart object */
   $myPicture = new pImage($this->width(),$this->height(),$myData);

   /* Turn off Anti-aliasing */
   $myPicture->Antialias = (array_key_exists('antialiasing',$this->extras) && $extras['antialiasing']);

   /* Add a border to the picture */
   if (array_key_exists('border',$this->extras)) {
    // To suppress border, have 'border' set but empty.
    if ($this->extras['border']) {
     $myPicture->drawRectangle(0,0,$this->width()-1,$this->height()-1,$this->rectspec($this->extras['border']));
    }
    $myPicture->drawRectangle(0,0,$this->width()-1,$this->height()-1,array("R"=>0,"G"=>0,"B"=>0));
   } else {
    $myPicture->drawRectangle(0,0,$this->width()-1,$this->height()-1,array("R"=>0,"G"=>0,"B"=>0));
   }

  /* Set the default font */
  if (array_key_exists('font',$this->extras)) {
   $myPicture->setFontProperties(array("FontName"=>"./classes/fonts/".$this->extras['font'],
                                       "FontSize"=>(array_key_exists('fontsize',$this->extras)?
                                                    $this->extras['fontsize']:6)));
  } else {
   $myPicture->setFontProperties(array("FontName"=>"./classes/fonts/pf_arma_five.ttf","FontSize"=>6));
  }
 
   /* Set the graph area */
  $myPicture->setGraphArea((array_key_exists('lmargin',$this->extras)?$this->extras['lmargin']:0),
                           (array_key_exists('tmargin',$this->extras)?$this->extras['tmargin']:0),
                           $this->width()-(array_key_exists('lmargin',$this->extras)?$this->extras['lmargin']:0),
                           $this->height()-(array_key_exists('bmargin',$this->extras)?$this->extras['bmargin']:0));
 
  /* Create the Scatter chart object */
  $myScatter = new pScatter($myPicture,$myData);
 
  /* Draw the scale */
  $scaleSettings=$defaults['scale'];
  $scaleSettings=array_merge($scaleSettings,$this->extrastoarray('scale'));
  if ($scaleSettings['Mode']==SCALE_MODE_MANUAL) {
   $scaleSettings['ManualScale']=array(0=>$this->extrastoarray('scale0'),
                                       1=>$this->extrastoarray('scale1'));
  }
  $myScatter->drawScatterScale($scaleSettings);

  /* Draw the legend */
  // Apply methods and variables to the myScatter object
  $this->applyExtraActions($myScatter,'scatteraction');
 
  /* Add a title */
  $titlesettings=$defaults['title'];
  if (array_key_exists('title.title',$this->extras)) {
   $titlesettings=array_merge($titlesettings,$this->extrastoarray('title'));
  }
  $t=$titlesettings['title'];
  $x=$titlesettings['X'];
  $y=$titlesettings['Y'];
  unset($titlesettings['X']);
  unset($titlesettings['Y']);
  unset($titlesettings['title']);
  $myPicture->drawText($x,$y,$t,$titlesettings);

  /* Draw a scatter plot chart */
  $myPicture->Antialias = TRUE;
 
  $drawmethod=$defaults['drawmethod'];
  if (array_key_exists('drawmethod',$this->extras)) {
   $drawmethod=$this->extras['drawmethod'];
  }

  // Cycle through any actions and process them
  $this->applyExtraActions($myPicture,'pictureaction');

  if (method_exists($myScatter,$drawmethod)) {
   call_user_func(array($myScatter,$drawmethod));
  } else {
   print "Drawing method '$drawmethod' was selected, but I don't know how to use that method";
   exit(4);
  }
  // $myScatter->drawScatterLineChart();

  /* Render the picture (choose the best way) */
  $myPicture->autoOutput("pictures/example.example.drawScatterBestFit.png"); 
 }


 /**
  * @brief Apply extra methods to the various objects
  * @todo document this method
  * @date 2011-07-11
  * @version 11
  **/
 public function applyExtraActions($object,$prefix) {
  $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
  $actkeys=preg_grep('/^'.$prefix.'[\d]+\./',array_keys($this->extras));
  $actkeys=array_unique(preg_replace('/^('.$prefix.'[\d]+)\..*/','$1',$actkeys));
  foreach ($actkeys as $key) {
   if (array_key_exists($key.'.method',$this->extras)) {
    $this->applyaction($object,$this->extrastoarray($key));
   } else {
    print "No specified method for '".$this->extras[$key]."' on a ".get_class($object)." object\n";
   }
  }
 }

 /**
  * @brief Ensure a method exists
  * @param[in] object Object 
  * @param[in] args Array containing a 'method' item which is to be tested.
  * @todo Should be lower-down the inheritance tree, surely.
  * @date 2011-07-11
  * @version 11
  **/
 public function ensuremethod($object,$args) {
  $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
  if (! method_exists($object,$args['method'])) {
   print "Method ".$args['method']." not found in this ".get_class($object)." object\n";
   exit (6); 
  }
 }
 
 /**
  * A set of functions to apply methods with various numbers of arguments.
  **/

 /**
  * @brief To call, on a pchart object, a method with 0 mandatory arguments
  * @param[in] object The pchart object
  * @param[in] flat Array which is ignored (included for type compatibility with similar methods)
  * @param[in] args Array passed containing optional arguments for the method.
  * @todo Unify all these applymethodN methods!
  * @date 2011-07-11
  * @version 11
  **/
 public function applymethod0($object,$flat,$args) {
  $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
  call_user_func(array($object,$args['method']),$args);
 }

 /**
  * @brief To call, on a pchart object, a method with 1 mandatory argument
  * @param[in] object The pchart object
  * @param[in] flat Array containing names of arguments to be passed in order
  * @param[in] args Array passed containing optional arguments for the method.
  * @todo Unify all these applymethodN methods!
  * @date 2011-07-11
  * @version 11
  **/
 function applymethod1($object,$flat,$args) {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($object,$flat,$args));
  call_user_func(array($object,$args['method']),$args[$flat[0]],$args);
 }

 /**
  * @brief To call, on a pchart object, a method with 2 mandatory arguments
  * @param[in] object The pchart object
  * @param[in] flat Array containing names of arguments to be passed in order
  * @param[in] args Array passed containing optional arguments for the method.
  * @todo Unify all these applymethodN methods!
  * @date 2011-07-11
  * @version 11
  **/
 private function applymethod2($object,$flat,$args) {
  $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($object,$flat,$args));
  call_user_func(array($object,$args['method']),$args[$flat[0]],
                 $args[$flat[1]],$args);
 }

 /**
  * @brief To call, on a pchart object, a method with 3 mandatory arguments
  * Can omit the options array (required by some objects)
  * @param[in] object The pchart object
  * @param[in] flat Array containing names of arguments to be passed in order
  * @param[in] args Array passed containing optional arguments for the method.
  * @todo Unify all these applymethodN methods!
  * @param[in] omitarray If true, the optional array is not passed
  * @date 2011-07-11
  * @version 11
  **/
 function applymethod3($object,$flat,$args,$omitarray=false) {
  $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($object,$flat,$args,$omitarray));
  # Class 3a
  if ($omitarray) {
   call_user_func(array($object,$args['method']),
                  $args[$flat[0]],$args[$flat[1]],$args[$flat[2]]);

  } else {
   call_user_func(array($object,$args['method']),
                  $args[$flat[0]],$args[$flat[1]],$args[$flat[2]],$args);
  }
 }

 /**
  * @brief To call, on a pchart object, a method with 4 mandatory arguments
  * @param[in] object The pchart object
  * @param[in] flat Array containing names of arguments to be passed in order
  * @param[in] args Array passed containing optional arguments for the method.
  * @todo Unify all these applymethodN methods!
  * @date 2011-07-11
  * @version 11
  **/
 function applymethod4($object,$flat,$args) {
  $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($object,$flat,$args));
  call_user_func(array($object,$args['method']),
                 $args[$flat[0]],$args[$flat[1]],$args[$flat[2]],
                 $args[$flat[3]],$args);
 }

 /**
  * @brief To call, on a pchart object, a method with 5 mandatory arguments
  * @param[in] object The pchart object
  * @param[in] flat Array containing names of arguments to be passed in order
  * @param[in] args Array passed containing optional arguments for the method.
  * @date 2011-07-11
  * @version 11
  **/
 function applymethod5($object,$flat,$args) {
  $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($object,$flat,$args));
  call_user_func(array($object,$args['method']),
                 $args[$flat[0]],$args[$flat[1]],$args[$flat[2]],
                 $args[$flat[3]],$args[$flat[4]],$args);
 }

 /**
  * @brief To call, on a pchart object, a method with 6 mandatory arguments
  * @param[in] object The pchart object
  * @param[in] flat Array containing names of arguments to be passed in order
  * @param[in] args Array passed containing optional arguments for the method.
  * @todo Unify all these applymethodN methods!
  * @date 2011-07-11
  * @version 11
  **/
 function applymethod6($object,$flat,$args) {
  $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($object,$flat,$args));
  call_user_func(array($object,$args['method']),
                 $args[$flat[0]],$args[$flat[1]],$args[$flat[2]],
                 $args[$flat[3]],$args[$flat[4]],$args[$flat[5]],$args);
 }

 /**
  * @brief To call, on a pchart object, a method with 7 mandatory arguments
  * @param[in] object The pchart object
  * @param[in] flat Array containing names of arguments to be passed in order
  * @param[in] args Array passed containing optional arguments for the method.
  * @todo Unify all these applymethodN methods!
  * @date 2011-07-11
  * @version 11
  **/
 function applymethod7($object,$flat,$args) {
  $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($object,$flat,$args));
  call_user_func(array($object,$args['method']),
                 $args[$flat[0]],$args[$flat[1]],$args[$flat[2]],
                 $args[$flat[3]],$args[$flat[4]],$args[$flat[5]],
                 $args[$flat[6]],$args);
 }

 /**
  * @brief To call, on a pchart object, a method with 8 mandatory arguments
  * @param[in] object The pchart object
  * @param[in] flat Array containing names of arguments to be passed in order
  * @param[in] args Array passed containing optional arguments for the method.
  * @todo Unify all these applymethodN methods!
  * @date 2011-07-11
  * @version 11
  **/
 function applymethod8($object,$flat,$args) {
  $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($object,$flat,$args));
  call_user_func(array($object,$args['method']),
                 $args[$flat[0]],$args[$flat[1]],$args[$flat[2]],
                 $args[$flat[3]],$args[$flat[4]],$args[$flat[5]],
                 $args[$flat[6]],$args[$flat[7]],$args);
 }

 /**
  * @brief To call, on a pchart object, a method with 9 mandatory arguments
  * @param[in] object The pchart object
  * @param[in] flat Array containing names of arguments to be passed in order
  * @param[in] args Array passed containing optional arguments for the method.
  * @todo Unify all these applymethodN methods!
  * @date 2011-07-11
  * @version 11
  **/
 function applymethod9($object,$flat,$args) {
  $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($object,$flat,$args));
  call_user_func(array($object,$args['method']),
                 $args[$flat[0]],$args[$flat[1]],$args[$flat[2]],
                 $args[$flat[3]],$args[$flat[4]],$args[$flat[5]],
                 $args[$flat[6]],$args[$flat[7]],$args[$flat[8]],$args);
 }

 /**
  * @brief To apply a method to a pchart object. 
  * Calls the method appropriate to the number of arguments.
  * @todo Surely this can be rationalised? 
  * @param[in] object The pchart object
  * @param[in] flat List of arguments to be applied as positional parameters
  * @param[in] args Array of arguments provided as optional parameters
  * @param[in] omitarray Boolean flag to prevent passing optional parameters
  * @date 2011-07-11
  * @version 11
  **/
 function applymethod($object,$flat,$args,$omitarray=false) {
  $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($object,$flat,$args,$omitarray));
  $flatcount=count($flat);
  switch ($flatcount) {
   case 0:
    $this->applymethod0($object,$flat,$args);
   break;
   case 1:
    $this->applymethod1($object,$flat,$args);
    break;
   case 2:
    $this->applymethod2($object,$flat,$args);
    break;
   case 3:
    $this->applymethod3($object,$flat,$args,$omitarray);
    break;
   case 4:
    $this->applymethod4($object,$flat,$args);
   break;
   case 5:
    $this->applymethod5($object,$flat,$args);
    break;
   case 6:
    $this->applymethod6($object,$flat,$args);
    break;
   case 7:
    $this->applymethod7($object,$flat,$args);
    break;
   case 8:
    $this->applymethod8($object,$flat,$args);
    break;
   case 9:
    $this->applymethod9($object,$flat,$args);
    break;
   default:
    print "The method ".$args['method']." requires more than 9 arguments ".
          "which is unexpected.";
    exit (7);
  }
 }

 /**
  * @brief To apply some action to a pchart object.
  * This is going to get big and rather messy because we need to keep track of 
  * all available methods of all objects, what arguments they take, 
  * which are positional and which are positional. Oh, and scale X-Y arguments
  * appropriately
  * @date 2011-07-11
  * @version 11
  **/
 private function applyaction($object,$args) {
  $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($object,$args));
  $this->ensuremethod($object,$args);
  switch ($args['method']) {
  # Class 0
   case 'drawDerivative':
   case 'drawBarChart':
   case 'drawScale':
   case 'drawAreaChart':
    $this->applymethod($object,array(),$args);
    break;
  # Class 1
   case 'drawThreshold':
    $this->applymethod($object,array('value'),$args);
    break;
   case 'writeBoundaries':
    $this->applymethod($object,array('type'),$args);
    break;
  # Class 2
   case 'drawAntialiasPixel':
   case 'drawLegend':
   case 'drawScatterLegend':
    $this->scalexy($args,'X','Y','scale');
    $this->applymethod($object,array('X','Y'),$args);
    break;
   case 'drawThresholdArea':
    $this->applymethod($object,array('value1','value2'),$args);
    break;
  # Class 3
   case 'drawText':
   case 'drawArrowLabel':
    $this->scalexy($args,'X','Y','scale');
    $this->applymethod($object,array('X','Y','text'),$args);
    break;
   case 'drawFilledCircle':
    $this->scalexy($args,'X','Y','scale');
    $this->applymethod($object,array('X','Y','radius'),$args);
    break;
  # Class 3a doesn't accept an extra array
   case 'drawFromGIF':
   case 'drawFromPNG':
   case 'drawFromJPG':
    $this->scalexy($args,'X','Y','scale');
    $omitarray=true;
    $this->applymethod($object,array('X','Y','radius'),$args,$omitarray);
  # Class 4
   case 'drawLine':
   case 'drawArrow':
   case 'drawFilledRectangle':
   case 'drawRectangle':
    // Might need to adapt this if we have >1 y axis/x axis 
    // graph:axis-N,axis-M should do.
    $this->scalexy($args,'X1','Y1','scale');
    $this->scalexy($args,'X2','Y2','scale');
    $this->applymethod($object,array('X1','Y1','X2','Y2'),$args);
    break;
   case 'drawCircle':
    $this->scalexy($args,'X1','Y1','scale');
    $this->applymethod($object,array('X1','Y1','width','height'),$args);
    break;
  # Class 5
   case 'drawRoundedFilledRectangle':
   case 'drawRoundedRectangle':
    // Might need to adapt this if we have >1 y axis/x axis 
    // graph:axis-N,axis-M should do.
    $this->scalexy($args,'X1','Y1','scale');
    $this->scalexy($args,'X2','Y2','scale');
    $this->applymethod($object,array('X1','Y1','X2','Y2','radius'),$args);
    break;
   case 'drawGradientArea':
    // Might need to adapt this if we have >1 y axis/x axis 
    // graph:axis-N,axis-M should do.
    $this->scalexy($args,'X1','Y1','scale');
    $this->scalexy($args,'X2','Y2','scale');
    $this->applymethod($object,array('X1','Y1','X2','Y2','direction'),$args);
    break;
  # Class 8
   case 'drawBezier':
    $this->scalexy($args,'X1','Y1','scale');
    $this->scalexy($args,'X2','Y2','scale');
    $this->scalexy($args,'Xv1','Yv1','scale');
    $this->scalexy($args,'Xv2','Yv2','scale');
    $this->applymethod($object,array('X1','Y1','X2','Y2',
                                     'Xv1','Yv1','Xv2','Yv2','direction'),$args);
    break;
# TODO scale to graph? Which axis?
    
   default:
    print "Don't know how to apply the method ".$args['method']." to a ".get_class($object)." object\n";
    exit(5);
  }  

 }

 /**
  * @brief To scale a pair of coordinates to some other method.
  * @param[in,out] args A pointer to an array containing the X-Y values to be scaled
  * @param[in] ax Name of the X value in the args array
  * @param[in] ay Name of the Y value in the args array
  * @param[in] amethod Method of mapping from one to the other: 
  *   image% maps to percentage of image, graph maps to graph units. More?
  * @date 2011-07-11
  * @version 11
  **/
 private function scalexy(&$args,$ax,$ay,$amethod) {
  $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($args,$ax,$ay,$amethod));
  // Can scale in a number of ways
  $method=(array_key_exists($amethod,$args)?$args[$amethod]:'image%');
  $x=$args[$ax];
  $y=$args[$ay];
  switch (true) {
   case preg_match('/image%/',$method):
    list ($args[$ax],$args[$ay])=$this->scaletoimage($args[$ax],$args[$ay]);
    break;
   case preg_match('/graph/',$method):
    list ($args[$ax],$args[$ay])=$this->scaletograph($args[$ax],$args[$ay]);
    break;
   case preg_match('/axis:([a-zA-Z0-9]*):([a-zA-Z0-9]*)/',$method):

    break;
   default:

    break;
  }

 }

 /**
  * @brief Scales a pair of coordinates to the image background
  * @param[in] x X co-ordinate to be scaled 
  * @param[in] y Y co-ordinate to be scaled
  * @return (x,y) new scaled co-ordinates
  * @date 2011-07-11
  * @version 11
  **/
 private function scaletoimage($x,$y) {
  $this->hotwireDebug(HW_DBG_OPRI,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($x,$y));
  return array($x/100*$this->width(),(100-$y)/100*$this->height());
 }

 /**
  * @brief Scales a pair of coordinates to the graph axes.
  * @param[in] x X co-ordinate to be scaled 
  * @param[in] y Y co-ordinate to be scaled
  * @return (x,y) new scaled co-ordinates
  * @date 2011-07-11
  * @version 11
  **/
 private function scaletograph($x,$y) {
   $this->hotwireDebug(HW_DBG_OPRI,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($object,$flat,$args));
  // Have to insist on manual scaling being provided.
  if (array_key_exists('scale.Mode',$this->extras)) {
   $lmargin=(array_key_exists('lmargin',$this->extras)?$this->extras['lmargin']:0);
   $rmargin=(array_key_exists('rmargin',$this->extras)?$this->extras['rmargin']:0);
   $wpix=$this->width()-($lmargin+$rmargin);
   $wg=$this->extras['scale0.Max']-$this->extras['scale0.Min'];
   $tmargin=(array_key_exists('tmargin',$this->extras)?$this->extras['tmargin']:0);
   $bmargin=(array_key_exists('bmargin',$this->extras)?$this->extras['bmargin']:0);
   $hpix=$this->height()-($tmargin+$bmargin);
   $hg=$this->extras['scale1.Max']-$this->extras['scale1.Min'];
   $x=$lmargin+(($x-$this->extras['scale0.Min'])/$wg)*$wpix;
   $y=$this->height()-$bmargin-(($y-$this->extras['scale1.Min'])/$hg)*$hpix;
  } else {
   print "Scale is not set to manual: cannot scale=graph for actions\n";
   exit(6);
  }
  return array($x,$y);
 }

 /**
  * @brief To parse an r,g,b => array('R'=>...)
  * @param text A string with comma-separated R,G & B values
  * @return array with R, G and B values
  * @date 2011-07-11
  * @version 11
  **/
 private function getcolorspec($text) {
   $this->hotwireDebug(HW_DBG_OPRI,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($object,$flat,$args));
   $results=explode(',',$text);
   return array('R'=>$results[0],'G'=>$results[1],'B'=>$results[2]);
 }

 /**
  * @brief To dig some settings out of the extras array.
  * Some may be post-processed: 'CONST:name' gives the value of the constant 
  * called Name.
  * @param prefix Search for values with this prefix 
  * @return Array containing matching values
  * @date 2011-07-11
  * @version 11
  **/
 # TODO - need to parse arrays out of extras table
 private function extrastoarray($prefix) {
  $this->hotwireDebug(HW_DBG_OPRI,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($object,$flat,$args));
  $results=array();
  foreach (preg_grep("/^$prefix\./",array_keys($this->extras)) as $key) {
   $nkey=preg_replace("/^$prefix\./",'',$key);
   switch (true) {
    case is_array($this->extras[$key]):
     $results[$nkey]=$this->extras[$key];
    break;
    case preg_match("/^CONST:(.*)$/",$this->extras[$key]):
     $results[$nkey]=preg_replace_callback("/^CONST:(.*)$/",function($matches) { return constant($matches[1]); },$this->extras[$key]);
    break;
    default:
     $results[$nkey]=$this->extras[$key];
   }
  }
  return $results;
 }

 /**
  * @brief To get some metadata about the chart
  * @param type See hotwire.hw_chart_metadata_types 
  * @param id Graph identifier
  * @return String from the database 
  * @date 2011-07-11
  * @version 11
  **/
 function getmetadata($type,$id) {
  $this->hotwireDebug(HW_DBG_OPRI,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($type,$id));
  return $this->sqltostring('select metadata_value from '.$this->hwschema.'.hw_chart_metadata '.
                            'inner join '.$this->hwschema.'.hw_chart_metadata_type_hid '.
                            'using (hw_chart_metadata_type_id) where hw_chart_id=$1 '.
                            'and hw_chart_metadata_type_hid=$2;',array($id,$type));
 }

 /**
  * @brief To get some metadata about the chart
  * @param type See hotwire.hw_chart_metadata_types 
  * @param id Graph identifier
  * @return Array of strings from the database 
  * @date 2011-07-11
  * @version 11
  **/
 function getmetadataarray($type,$id) {
  $this->hotwireDebug(HW_DBG_OPRI,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($type,$id));
  return $this->phpArray(
   $this->sqltostring('select array(select metadata_value from '.$this->hwschema.'.hw_chart_metadata '.
                            'inner join '.$this->hwschema.'.hw_chart_metadata_type_hid '.
                            'using (hw_chart_metadata_type_id) where hw_chart_id=$1 '.
                            'and hw_chart_metadata_type_hid=$2);',array($id,$type)));
 }

 /** 
  * @brief Look up chart types from the database
  * @param[in] id ID of chart to consider
  * @return Description of chart type
  * @date 2011-07-11
  * @version 11
  **/
 private function getcharttype($id) {
   $this->hotwireDebug(HW_DBG_OPRI,'Called [private] '.get_class($this).'->'.__METHOD__,
                       array($object,$flat,$args));
   $typeid=$this->getmetadata('Chart Type',$id);
   return $this->sqltostring('select hw_chart_type_hid from '.$this->hwschema.'.hw_chart_type_hid where hw_chart_type_id=$1',array($typeid));
 } 

 /**
  * @brief Returns the desired width of the image
  * @return Width of image (640 is default)
  * @date 2011-07-11
  * @version 11
  **/
 private function width() {
   $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__);
   return (array_key_exists('width',$_REQUEST)?
           0+$_REQUEST['width']:
           640);
 }

 /**
  * @brief Returns the desired height of the image
  * @return Height of image (480 is default)
  * @date 2011-07-11
  * @version 11
  **/
 private function height() {
  $this->hotwireDebug(HW_DBG_OPRI,'Called [private] '.get_class($this).'->'.__METHOD__);
  return (array_key_exists('height',$_REQUEST)?
          0+$_REQUEST['height']:
          480);
 }

 /**
  * @brief Returns all the data from a query
  * @return array containing data from the query
  * @param[in] query Name of view to use
  * @param[in] args Optional array of parameters for the query
  * @date 2011-07-11
  * @version 11
  **/
 private function querytodata($query,$args=array()) {
  $this->hotwireDebug(HW_DBG_OPRI,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($query,$args));
  $query=pg_escape_string($query);
  $sql='select * from '.$query;
  // sort out quoting: query=>"query", function()=>"function"()
  $query=preg_replace('/\b/','"',$query,2);
  $result=pg_prepare($this->dbh,'',$sql);
  $result=pg_execute($this->dbh,'',$args);
  if (!$result) {$this->sqlError('Getting data for the query - maybe alldataqargs should be set?',$sql);}
  return pg_fetch_all($result);
 }

 /**
  * @brief Convert data into columnar form
  * Convert data of the form [(1->(X->xval, Y1->y1val)),(2->(X->xval, Y1->y1val))...]
  * to data of the form [Y1->[xval->y1val,xval->y1val,...], Y2->[]]
  * @param[in] data data of the form [(1->(X->xval, Y1->y1val)),(2->(X->xval, Y1->y1val))...]
  * @return data of the form [Y1->[xval->y1val,xval->y1val,...], Y2->[]]
  * @date 2011-07-11
  * @version 11
  **/
 private function columnisedata($data) {
  $this->hotwireDebug(HW_DBG_OPRI,'Called [private] '.get_class($this).'->'.__METHOD__,
                      array($object,$flat,$args));
  $ycols=array_keys($data[0]);
  $xcol=array_shift($ycols);
  $coldata=array();
  foreach ($ycols as $ycol) {
   $coldata[$ycol]=array();
   foreach ($data as $row) {
    $coldata[$ycol][$row[$xcol]]=$row[$ycol];
   }
  }
  return $coldata;
 }

}
?>
