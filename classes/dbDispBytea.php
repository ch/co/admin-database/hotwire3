<?php
 /**
  * @brief A class with basic methods to display an array of bytes
  * @author Frank Lee
  * @date 2018-10-15
  * @version 1
  **/
class dbDispBytea extends dbDispBase {

  public function viewcell(&$row) {
  }

  /** 
   * @brief Returns the bytea in all its glory
   * @date 2019-01-30
   * @version 1
   **/
  public function displayField() {
   # Columns matching '_.*' are not displayed.
   if (substr($this->dbColumn->name(),0,1)=='_') { return;}
   $data=pg_unescape_bytea($this->getval());
   $length=strlen($data); # Number of /bytes/, not chars.
   $parts=explode("\n\n",$data,2); # header and data
   if (preg_match("/Content-type: (.*)\n/",$parts[0]."\n",$matches)) {
    $type=$matches[1];
    header("Content-Type: ".$type);
    header('Content-Disposition: inline; filename="'.$this->fieldname.'"');
    header('Content-Transfer-Encoding: binary');
    header('Accept-Ranges: none');
    print $parts[1];
   }
  }

 /**
  * @brief Displays a search UI for binary fields.
  * @date 2011-07-11
  * @version 11
  * @return HTML input element for a form
  **/
 public function displaySearch(){
 }

 public function displaySearchField(&$ui) {}

 /**
  * @brief Displays the OID - just as an image, currently.
  * @date 2012-11-09
  * @version 12
  **/
 public function displayEdit() {
  # Hide columns matching '_.*' (it's a convention)
  if (substr($this->dbColumn->name(),0,1)=='_') { return;} 
  $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
  // How we display an OID largely depends on what's stored in it
  // which we'll have to infer from the name of the field, I guess
   # What sort of file is it?
   $data=pg_unescape_bytea($this->getval());
   $length=strlen($data); # Number of /bytes/, not chars.
   $parts=explode("\n\n",$data,2); # header and data
   #$s=substr(pg_unescape_bytea($this->getval()),0,255); # Mime types should be <127 chars long.
   if (preg_match("/Content-type: (.*)\n/",$parts[0]."\n",$matches)) {
     switch (true) {
       case preg_match('/image/',$matches[1]):
         return "<img src='data:".$matches[1].";base64,".base64_encode($parts[1])."'>".
                ($this->ro?'':"<input type='file' name='".$this->fieldname."' >");
       default:
         # TODO - add an icon based on the content type of the file
         if ($length < 1000) {
           return "<a href='data:".$matches[1].";base64,".base64_encode($parts[1])."'><img src='' alt='File'></a>".
                  ($this->ro?'':"<input type='file' name='".$this->fieldname."' >")."Length $length";
         } else {
           return "<a href='img.php?_view=$this->view&_id=".$this->getid()[0]."&_field=".$this->fieldname."'>Download</a>\n";
           #return "View is ".$this->view." - Field is ".$this->fieldname." - ID is ".$this->getid()[0]."\n";
         }
     }
   } else {
     # Empty field.
    #return "<img src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANwAAAGBCAYAAAANPd8cAAAABmJLR0QA/wD/AP+gvaeTAAAAB3RJTUUH2wUCAyEBLxqamwAAEjJJREFUeJzt3Xms5tVdx/H3ZYZ1xg5LZQoCHUa2shUGXLBYSjCthVIUMaKhxcRqrY2SqP0LmxptgtjWpo3GYk3UtFQtVVxYaiFhbZGytcDAzFAKUwZmA4SZAWZhZvzj3Mm9d+72PM8953yf8zzvV/LNvTTN735/597PnPP7Pb8FJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJAEwEt1AY/YBfhw4EjgcWAS8ZfTrdN8fAMwH9t2r9v7f3gS2A9tGa/z3U/3368DG0dow7uue718pNAaaAwM3ZgQ4GjgeWEIK1RGjX/fUYlJQWrCDiWFcDawCVo5+fXr0/6OKhjFwi4ETSMEa//U40mw0LN4EnmFiCFeO1trAvgbaoAfu7cAy4KxxXw8P7agN64BvA98ZrYdJS1rN0SAFbikTg7UMOCy0o8GxFXiIiSHcGNpRo1oM3AhpGbiMsXCdCRwS2dQQ+gHwP8C/A3cBO2PbaUMLgVsIvAt4D3AOKVxviWxIk7wE/DcpfLeRZkRNoR8D9xbGAnYeaQZr5cygYAtwK3AjcDOwKbad/tIPgVsEnMtYwJYB8yIbUjbbgZuAvwbuCO6lL0QE7mDg3aRwnQecgQEbBo+TgvcV0of2KuzzwCOkg+vd1tDW/wGfI51VHjo1Z7jdFX+W+t8u4BbgC8Dtwb1UY+DUD+4A/oi0Ahpo+0Q3IAHnkz5Y/yfgqOBeinKGU795A/gr4Fpgc3Av2Rk49av1wKeAv2eArmIxcOp3DwG/QbqboXkew6nfnUW6W+G3ohvJwRlOLfkG8Dukz/KaZODUmueAK4C7oxvphUtKteZo0ud2n6bBi9qd4fJaS/oXeN0UtZ50DeFW0kOAtu71/Xxgf9JjHvbf6/sDSXeqLwbeNsXXYxjO61G/CfwKDV2baeB6swZ4Alg++nVPRT0p6wDgJOBU4JRxdSz9cUdISfcBF9HIcZ2Bm91uYAXpmOFu0t3Nz4d21LlDSFdxXDBaJ8a2U8xy4L3AC9GN9JPoq9S7qXXAl4FLgbeWGIwgRwIfAm4gLWWjxzlnPUt69IZGRf9CZqtVwGdId5sPw8mkw4CrgO8RP/a5aj3pERwi/pcxVT0P/AXpeGeYLQOuI92hHf07mWu9Cvxc3uFpU/QvYk9tBb4OvJ/hPLM3k+OAfyXdqxb9e5pLbWRIb3AdL/qXsAb4BHBo6R0dAGeRnr4V/TubSz1JepzH0Ioa+MeAK0kvzFB3LiOdbo8OT691Gw1+OJ5L7cG+n7Rs1NwsIT1pOTo8vdZ12UekEbUG+IfA5Qz+B741zQeuod1juz/MPyT9r/Sgvkwa2P1q7dAQuoQ2P7/bCXygwHj0tdKD+gf1dmWoXUQ60xsdom5rPUP2cpfSA/qz9XZl6P0i6dkj0SHqtr5aYjD6VcmB3Ea6ql71vJc2Q3dhicHoRyUH8YGK+6Exv018gLqt5xiSty+VHMS/qbgfmugrxIeo2/pSkZHoMyUH8MMV90MTLSDdCxgdom5qF+ltTQOt5ACeVHE/NNnJpPfCRQepm3qKAb/6qNTAvYIfcveDPyY+RN3WR4qMRJ8oNWi31dwJTWs/0nu/o0PUTT1N5WstB+FGy/ujGxCQ7qX7RHQTXVpKeqrzQCr1r9QHa+6EZnUn8TNXN7WCwZh4Jik1YItr7oRmdSbtXeT8a0VGIliJgVpddQ/UqW8RH6Ju6lEqnXhrfSr1+K0/tXYhwmnAL9X4Qa0H7rvRDWhKNwE/im6iSx+v8UNaD5wzXH/aSXt3Wr+H9Nj4gZF73f0mcFDVPVA3Dqe9m1WvKjIS47Q8wy2noZc4DKENwK3RTXSp+GdyLQfO5WT/uym6gS79NPCTJX9Ay4HzhEn/u4W0VGvJr0c3kEvu9fZpddtXjx4i/tism1peZhiSVme410j3YKn/tbasPBl4Z6mNtxq4B0mnntX/WgscpCeTFdFq4Dx+a8eDwOboJrp0bqkNtxo4z1C2YzfwcHQTXTqHQtdWtho4Z7i2tPZUtYNJ70vPrsXArSU96kzteDC6gR68q8RGWwycs1t7DNwoA6cania9Z64lRU6ctBg4T5i0qbUTJ0uAI3NvtLXA7aa9A3AlP4huoAfZl5WtBW4FsCm6CfXkh9EN9ODE3BtsLXAev7WrxcAdm3uDrQXO47d2tRi4pbk32FrgnOHa1WLgss9wNZ/JP9f7oraS3uu1I0MvivEycEh0E13YCRxAepxHFi3NcI9g2FrX2iw3Dzgm5wZbCpzHb+3bEN1AD7IuK1sKnMdv7XspuoEeZD1xYuBUU4uByzrD1Xw3li9N1MvRDfTg8Jwba2mGU/tanOEW5NyYgVNNLc5wWZ/ubeBUkzNczo1Js3CGy7kxaRZboxvogTOcmtXilULOcGrW9ugGeuAMp2a1GDhnODWrxcA5w6lZLR7DZWXgVFOLM9y2nBszcKqpxcBl/SjDwKmmXdEN9MDAqVn7RjfQA5eUalaLgXOGU7NaDJwznJq1X3QDPXCGU7NanOFey7kxA6eaWgxc1ieNGTjV1GLg1ufcmIFTTS0ewxk4NevA6AZ6YODUrJbeK7CHgVOzDo5uoAcGTs1qcYZbl3NjBk41tRa4XcDGnBs0cKqptcCtJvNNswZONbUWuBW5N2jgVFNrgVuZe4MGTjUdFt1Al5zh1LSsr++tIPsM5zvbVMsI6VaXli7vOgI/FlCjjqCtsL1K5rCBgVM9b49uoEvZj9/AwKme1gL3QImNGjjV0lrg7iuxUQOnWgwcBk71vCO6gS6sB54psWEDp1rOiG6gC0VmNzBwqmMJbd0L97+lNmzgVENLsxs4w6lxLQVuO/BgqY0bONXQUuDuAF4vtXEDpxrOjG6gC/8Z3YA0F0uA3Q3VTxQZhVHOcCrtgugGuvAw8HzJH2DgVNovRDfQBZeTatoI6aqN6GVip9XSyR1pktOJD1GntbrQGEzgklIltXT89i/RDUhzdS/xM1cntQtYWmgMpCqOJv0hR4epk7q10BhM4pJSpVxOOw+p+tvoBqS5eoj4mauT+hEwr9AYTOIMpxJOAJZFN9GhLwM7o5uQ5uLTxM9cndQO0uP7pGYdCLxIfJg6qa8WGgOpmo8RH6ROZ7fjCo2BVMUI6Xn80WHqpK4rNAZSNR8kPkid1BsUvg1HKm2E9PCd6DB1Up8rNAZSNVcQH6ROahPw1kJjIFWxAFhDfJg6qT8tMwRSPX9OfJA6qdXAwkJjIFWxlHQSIjpMndT7Co2BVMV82jlR8o9lhkCq5xrig9RJrQUOKTQGUhUXkC76jQ5TJ3VpoTGQqjgKeIH4IHVSNxQaA6mKQ4DlxAepk9oALC4zDFJ5BwLfJj5IndQO4LwywyCVty/wX8QHqdP6/TLDIJW3CLid+BB1Wv9QZhik8o4GHiM+RJ3Wd4EDioyEVNhPkV5uER2iTms96Qyq1JR9gT8jnXiIDlGntRX4+RKDIZV0OvA94gPUTW0DLiwxGFIpRwJfoq1ZbTfpndwXFxgPqYhDgWtJ77WODk+3tQP45fxDIuV3KukR31uID04v9SZwWfZRkTJaSHrm/53EB2auYbs879BIeRwMfAj4D9q5UXSmeh341awjJM3BEaQ/yC+SXhLfyi00ndQLpM8Gm9XK64Q0tUOBs0l/hGeP1qB+8PsI6bmXa6IbmQsD145FwFmMBets4NjQjuq5kbQsfi26kbkycP1pIel1T+PDdRzD+fu6BriatKRs3jD+AvvNgcAZTAzXSfjuvi3A7wLXRzeSk4Gra3/SpVPjw3Uy6elXGnMv8GHgmehGcjNw5cwnfbg8PlynAftFNtXntgOfAv4S2BXcSxEGLo99gHcw8WzhO/G+rG48Tno/wfejG1H/a+U1Tf1YO4HPkpbbA89jhzxeim6gUY8AVwH3RDeitpxI/EzRUq0BrsRDGvXoMOL/iFuozcCfkD4KkXq2D+kK9ug/6H6tN4G/w4eyKqONxP9h91vtJF2WdeocxlWa0gri/8D7pV4FPs/wXOupAPcS/4ceXatITzz2LaPT8GOBfF6MbiDQ7cAXgJtJwdM0DFw+wxa4p0ivgroeeCK4l2YYuHyGIXCrSCG7AS/B6omBy2dQrzZZyVjIHg3upXkGLp9BmeE2k94BdydwC+lFHsrEwOXTauBeJZ1hvRO4i7EHD6kAA5dPK4F7ibEZ7C7SewQG8t6zfmTg8unXwD1FCtie2vMBvQIYuHz64aTJduAhJgZsY2hHmsDbI/IZIb1cYl7Fn/kS8B3GwvUA6dVN0lDYQN1LqY6vs1vKZdgfxZZb7eO4zZV/nubIwOVl4DQjA5dXzRMnu0hvklFDDFxeNWe41/D0fnMMXF41A+dyskEGLq+agdtS8WcpEwOXlzOcZmTg8qp50sTANcjA5eWSUjMycHm5pNSMDFxeBk4zMnB5vUJ6ynANLikbZODye7nSz3GGa5CBy6/WstLANcjA5VcrcC4pG2Tg8nOG07QMXH4GTtMycPnVutrEwDXIwOXnMZymZeDyc0mpaRm4/AycpmXg8nNJqWkZuPwMnKZl4PKrcZbyNXwfQJMMXH6vkp7AXJKzW6MMXBmlZzlPmDTKwJVR+jjOwDXKwJVROnAuKRtl4MpwSakpGbgyXFJqSgauDJeUmpKBK8MZTlMycGUYOE3JwJXhSRNNycCV4TGcpmTgynBJqSkZuDIMnKZk4MrYDGwvuH2XlI0ycOWUPHHiDNcoA1dOyWWlgWuUgSunZOBcUjbKwJXjDKdJDFw5Bk6TGLhySp002QrsLLRtFWbgyik1wzm7NczAlWPgNImBK6dU4DxD2TADV44znCYxcOWUOmli4Bpm4MpxhtMkBq6cLaRT+CW2q0YZuLJKLCud4Rpm4Moqsaw0cA0zcGWVmOFcUjbMwJXlDKcJDFxZBk4TGLiySgTOJWXDDFxZznCawMCV5ccCmsDAleWSUhMYuLJcUmoCA1eWgdMEBq4sl5SSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJEmSJElq20h0A0NoBDgFOBc4CTgeWAosAhYCBwHbSK8W3gQ8C6wCngTuAR4DdtduWmrJPOBC4HpgAykwvdZG4GvA+0e3K2nUAuCTwDrmFrLpai1wNWlWlIbWfOBjpECUCNre9QLw0dGfKw2Vy4CV1Ana3rUCuLT8LkrxFgI3EhO0vevfSMtZaSAdy9jZw36p7wNLCu6zFOJ84EXiAzZVbQTeXW7XpbquBHYQH6yZajtwRakBkGo5j/THHB2oTkN3bplhkMpbQlquRQepm1oPHFNgLKSiFpBOSEQHqJd6BD8kV2O+QXxw5lJfzz8kUhmXEB+YHHVx7oGRctsHeJz4sOSox0b3R+pbv0l8UHLWlVlHR8pof2A18SHJWatH90sVuazozMcZvFPqxwC/F92EtLcRBm9221PP4l3/6jPnEx+MknVevqHSbFxSzm7Qr0Mc9P3rKy4nZvc8cGR0EwWtAY6ObmJYOMPN7AQGO2wAR5GeHKYKDNzMhuVeMo/jKjFwMzsluoFKTo5uYFgYuJmdGN1AJSdENzAsDNzMlkY3UMmw7Gc4Azezg6MbqGRY9jOcgZvZwugGKvmx6AaGhZ/DzWwnw/GP0k58anMVw/DHNBdvRDdQybDsZzgDN7Mt0Q1Usjm6gWFh4Gb2SnQDlQzLfoYzcDN7OrqBSoZlP8MZuJmtjG6gklXRDQwLAzez5dENVDIs+6k+dxzxN4jWKK80Ud8Y1Mcr7Klns42UZuWScna3RDdQ2M3RDUjjnUP8LFSyfibfUEl5rCA+GCVqRc5B0uxcUnbmM9ENFHJtdAPSVOaRTp1Hz0g5y/cLqK9dTHxIctYH8g6PlN/dxAclR92Ve2CkEgbljKVnJtWMzxIfmLmUJ0rUlHnAt4gPTi/1TTxRogYdSrqlJTpA3dRT+LAgNew00h3h0UHqpDbhw141AC6k/0O3GXhfqQGQajsDeI74YE1Vq4HTy+26FONtwP3EB2x83QcsLrnTUqQDgH8mPmi7getH+5EG3iXAE8QEbTnpEjRpqMwDPgqspU7Qngc+MvpzpaG1APgksI4yQVsLXA0cVGuHpBbMBy4CvgZsYG4h2zi6nQtxRmuOL/OobwQ4FTgXOIn0fu2lwCLS23oOAraRPj/bRHrIzyrgSeAe4FFS8CRJkiRJkiRJkiRJkiRJkiRJkiRJkiRJUsf+H5seVoqwL/1+AAAAAElFTkSuQmCC' alt='Unknown image' width='150' height='200'>";
  return ($this->ro?'':"<input type='file' name='".$this->fieldname."'>");
   }

 }


}

