<?php
 /**
  * @brief A class with methods to display a numeric field.
  * @author Frank Lee
  * @date 2011-07-11
  * @version 11
  **/
class dbDispNumeric extends dbDispBase {

 public function displaySearch() {
   $min=(array_key_exists('min_'.$this->getkey(),$this->objRecord->post)?
         $this->objRecord->post['min_'.$this->getkey()]:NULL);
   $max=(array_key_exists('max_'.$this->getkey(),$this->objRecord->post)?
         $this->objRecord->post['max_'.$this->getkey()]:NULL);

   return '<input type="text" size="10" name="min_'.htmlspecialchars($this->getkey()).
          '" class="date" '.($min!==NULL?'value="'.$min.'"':'').'/> to '.
          '<input type="text" size="10" name="max_'.htmlspecialchars($this->getkey()).
          '" class="date" '.($max!==NULL?'value="'.$max.'"':'').'/> ';
 }

  // No 'Exact Value' box for numeric, we use ranges
  public function displayExact() {
   return;
 }


}
?>
