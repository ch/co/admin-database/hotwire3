<?php
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

 /**
  * @file dbRecord.php
  * @brief To provide a class to display and manipulate an individual record
  * Part of Hotwire http://hotwire.sourceforge.net/.
  * @author Stuart Taylor
  * @author Frank Lee
  */

 /**
  * @class dbRecord
  * @brief Displays a single record for editing
  */
class dbRecord extends dbView {

  public $id = array();  //<!@brief ID value for this record
  private $nextid;	 //<!@brief ID value of the next record
  public $checksum;      //<!@brief Checksum for this record
  public $data = array();//<!@brief Data for this record.
  public $updatefailed = false; //<!@brief Indicate a failed Update/insert/delete
  private $idlist = array(); //<!@brief List of IDs from the view
  public $listClass;
  public $is_subview;
  public $action;
  
  /**
   * @brief Construct the dbRecord object
   * @date 2011-07-11
   * @version 11
   * @todo Handle a failed Update or Insert method
   **/
  function __construct() {
    $this->hwDebugCreateStart();
    $this->getPostVars();
    $this->dbInit(); // where to abort?
    $this->loadPreferences();
    $this->getViews();
    $this->getPerms();
    $this->getColAttrs();
    $this->getTable();
    if (!isset($this->offset)) { $this->offset = 0; }
    if ($this->propertyEqual('action','update') 
     || $this->propertyEqual('action','insert')) {
      $this->id=$this->dbUpdateOrInsert();
    }
    if (in_array('ids',$_SESSION)) {
     $this->ids=$_SESSION['ids']; 
     $this->nextid();
    }
    #if ($this->propertyEqual('action',constant("BTN_MSG_UPDATE_SHOW_NEXT"))) {
    if (array_key_exists('_action',$_POST) && $_POST['_action']==constant("BTN_MSG_UPDATE_SHOW_NEXT")) {
     //$this->action='update'; // It already was.
     // $this->id=$this->dbUpdateOrInsert(); // We already did.
     if (! $this->updatefailed) {
      $this->msg.=' - Showing next record';
      $this->id=$this->nextid;
      $this->nextid();
     }
    }
    // Todo Handle action UpdateAndNext ; DeleteAndNext
    // If we don't have an ID, don't worry: it'll be action=search
    if ($this->id) {
     if (!is_array($this->id)) { $this->id=array($this->id);}
     if (count($this->id)>0 && join(',',$this->id)) {
      $start_us=microtime(true);
      $sql = 'SELECT * FROM "'.$this->views[$this->view]['schema'].'"."'.
             $this->view."\" WHERE id IN ('".join("','", $this->id) . "')";

      // TODO: Parameterize SQL properly.
      $dbResult=$this->sqlexec(get_class($this),__METHOD__,HW_DBG_DBVI,$sql);
      $end_us=microtime(true);
      global $hw_sql_timing;
      $hw_sql_timing[$sql]=($end_us-$start_us)/1000;
      if (! $dbResult) {
        $this->sqlError('loading rows from the "'.$this->view.' view '
              .'in the '.$this->views[$this->view]['schema'].'schema',$sql); 
      }
      $this->data = $this->collateRows($dbResult);
      $this->logQuery($sql);
      $this->pagelen = 1; 
     }
    } else {
      // No id: if action==insert, that failed so let's try to recover the data
      // to give the user another chance.
      // TODO: Recoverdata
      foreach (array_keys($this->colAttrs) as $col) {
        if (array_key_exists($col,$_REQUEST) 
         && $_REQUEST[$col]!='') {
          $this->data[""][$col]=$_REQUEST[$col];
          $this->id=array();
        }
      }
    }
    $this->hwDebugCreateStop();
  }

  /**
   * @brief Displays the record or a search form
   * calls the displaySearch or displayData methods
   * @date 2011-07-11
   * @version 11
   **/
  public function display() {
    if ($this->propertyEqual('action','search') && !$this->id) {
      $this->displaySearch();
    } else {
      $this->displayData();
    }
  }

  /**
   * @brief To call a more appropriate ajaxdropdown function based on the field
   * we require.
   * @todo This looks very similar to the ajaxmmdropdown function. Do we need both?
   * @param[in] col Field we're producing a list for
   * @version 1
   * @date 2011-07-19
   **/
  public function ajaxdropdown($col) {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'
          .__METHOD__,array($col));
    $keys=array_flip($this->colList);
    return $this->colobjs[$keys[$col]]->ajaxdropdown();
  }

  /**
   * @brief To call a more appropriate ajaxmmdropdown function based on the field
   * we require.
   * @param[in] col Field we're producing a list for
   * @version 1
   * @date 2011-07-19
   **/
  public function ajaxmmdropdown($col) {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'
          .__METHOD__,array($col));
    $keys=array_flip($this->colList);
    return $this->colobjs[$keys[$col]]->ajaxmmdropdown();
  }


  /**
   * @brief Loads an object appropriate to the datatype of the field.
   * to load a widget appropriate to the type or a dbDispBase widget as fallback
   * @param[in] type Field type
   * @param[in] fieldname Name of the field
   * @date 2012-11-06
   * @version 13
   * Don't wrap this in timing info, it's a fast operation. (Usually)
   */
  protected function loadWidget($type,$fieldname) {
   // Handle php<5.3.0 which can't raise exceptions from __autoload
   if (file_exists('classes/'.$type.'.php')) {
    try {
      $widget=new $type($this,$fieldname);
    }
    catch(Exception $e) {
      $widget=new dbDispBase($this,$fieldname);
    }
   } else {
    $widget=new dbDispBase($this,$fieldname);
   }
   return $widget;
  }

  /**
   * @brief Displays the top of the record block.
   * Slightly mis-named, 'cos we don't use tables any more. 
   * @date 2011-07-11
   * @version 11
   **/
  private function displayTableHeader() {
  $this->hwDebugPriStart(__METHOD__);
  # Detect if this->msg contains a pointer, display that message and remove the pointer
  if (substr($this->msg,0,3)=='msg') {
	  $t=$_SESSION['msgs'][$this->msg];
	  unset($_SESSION['msgs'][$this->msg]);
	  $this->msg=$t;
  }
  $this->hotwireDebug(HW_DBG_OPRI,'Called [private] '.get_class($this).'->'
        .__METHOD__);
?>
   <div class='tableHeader'>
    <div class='tableTitle'>
       <?php print $this->hdrFormat($this->entity) . 
        ((property_exists($this,'msg') && isset($this->msg) && $this->msg) ? "<div class='msg'> (" . (isset($this->action)?$this->action:'') . ") ".$this->msg ."</div>": "") ; ?>
    </div> <!-- tableTitle -->
    <div class='tableActions'>
      <form name='altview' action='edit.php' method='get'>
       <?php print $this->setPostVars(array('id','debugval','debugobj' )) . $this->altViews() ;?>
      </form>
    </div> <!-- tableActions -->
    <div class='clearwidth'>
    </div> <!-- clearwidth -->
   </div><!-- tableHeader -->
<?php
$this->hwDebugPriStop(__METHOD__);
  }

  /**
   * @brief To end the main data form, with update and delete buttons
   * @date 2011-07-11
   * @version 11
   **/
  public function displayTableFooter() {
   $this->hwDebugPriStart(__METHOD__);
   // End the "Update" form: this is trickier than it needs to be
   print "   <div class='tableFooter'>\n";
   if ($this->data || (property_exists($this,'action') && 
                       in_array($this->action, array('new', 'search')))) { 
    echo $this->actionButton();
    print "</div><!-- end tableFooter -->\n";
    if ($this->arrayKey($this->id,$this->offset) && $this->perms['delete']) { 
     print "</form>"; //mainform
     // Build a search
     $args=array('_view='.$this->view);
     foreach ($this->colobjs as $col) {
       $col->parsesearch();
       $col->asGET($args);
     }
     // One argument is the _view; more would be a search
     if (count($args)>1) {
       $args[]='_action=search';
     }
     // Build a sort
     $args[]=$this->usortobjs->asGET();
     $url='list.php?'.join('&',$args);
     // Build a delete button
     
     print "<div class='tableFooter'>\n";
# TODO - Pass through view, search, filter options.
     print "<form onsubmit=\"return confirm_delete(this);\" id='deleteform' action='$url"
          ."' method='post'>";
     print $this->setPostVars(array('id','debugval','debugobj'));
     print "<input type='hidden' name='deleteid' value='".$this->id[$this->offset]."'/>";
     // Add sort and search option to the delete action.
     print "       <input type='submit' name='dodelete' value='Delete' />";
     //if ($this->nextid) {
      //print "<input type='submit' name='_action' value='Delete, show next' action='list.php'
        //onClick\"confirm_delete(this);\" />";
     //}
    }
   }
   print "</form>\n";
   print "</div><!-- tableFooter -->\n";
   print "<div class='postFooter'></div>\n";
$this->hwDebugPriStop(__METHOD__);
  }

  /** 
   * To return the next ID from whatever list we had before
   **/
  private function nextid() {
   // We might prefer not to have these buttons
   if (!$this->pref('SHOWNEXT') || (is_null($this->ids))) { return NULL; }
   $id=(is_array($this->id)?$this->id[0]:$this->id);
   $temp=array_flip($this->ids);
   $ord=$temp[$id];
   unset($id);
   unset($temp);
   if ($ord==count($this->ids)) { return NULL; }
   $this->nextid=$this->ids[$ord+1];
  }

  /**
   * @brief To start the main data form
   * @date 2011-07-11
   * @version 11
   **/
  public function beginItemdataForm() {
$this->hwDebugPubStart(__METHOD__);
   if (!property_exists($this,'view')) { return; };
   if (isset($this->action) && $this->action == 'search') { $target='list.php';} else { $target='edit.php';}
   $fieldlist=array_keys($this->colAttrs);
   print "
   <form enctype='multipart/form-data' name='itemdata' id='itemdata' action='$target' method='post' />";
    print "<div class='actionbuttontop'>".$this->actionButton();
    print "</div>\n";
    if (isset($this->action) && $this->action == 'new') {
      print "<div class='dbRecord_hints'><span class='hint_intro'>Hint:</span> <span class='hint_create'>Add data as required; use the '".BTN_MSG_CREATE."' button to save it.</span></div>";
    } else {
      print "<div class='dbRecord_hints'><span class='hint_intro'>Hint:</span> <span class='hint_update'>Make changes as required; use the '".BTN_MSG_UPDATE."' button to save it.</span> <span class='hint_delete'>The '".BTN_MSG_DELETE."' button erases this record.<span> </div>";
    }
    print '<div class="clearwidth"> </div>'."\n";
    //<div class='actionbuttontop'>".$this->actionButton()."&nbsp;</div>\n
    print "
    <div class='tableResults ".(isset($this->action)?$this->action:'')."'>
    <input type='hidden' name='_changed' id='_changed' value='".($this->updatefailed?'1':'0')."' />
    <input type='hidden' name='_view' id='_view' value='".$this->view."' />
    ".$this->setPostVars(array('id','debugval','debugobj'))."
    <script type='text/javascript'>
     window.HWallcols=new Array(\"".join('","',$fieldlist)."\");
    </script>";
$this->hwDebugPubStop(__METHOD__);
  }

  /**
   * @brief To display the advanced search page
   * @date 2011-07-11
   * @version 11
   **/
  public function displaySearch() {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'
          .__METHOD__);
  $js="
var f = document.itemdata; 
for (i = 0; i < f.length; i++) { 
 var s = f.elements[i].name; 
 if (s.substr(0,4) == 'hde_') { 
  f.elements[i].checked = this.checked; 
 } 
} ";
  // Display table header
  print "  <div class='searchtable'>\n";
  $this->displayTableHeader();
  $this->beginItemdataForm();
  print " <div class='itemdata'>\n";
  print "</div>\n";
  print "<div class='clearwidth'>
<div class='searchkey'>
<div class='suppressall' id='hide_all'>Hide All: <input type='checkbox' onclick=\"
$('div.suppress input:checkbox').attr('checked',\$('div.suppressall input:checkbox').attr('checked'));
\"/></div>
<div class='displayall' id='hide_all'></div>
<script type='text/javascript' src='js/advsearch.js'>

</script>
</div>\n";
   // Loop over each field in the record
   foreach ($this->colobjs as $col) {
     print $col->displaySearchField();
   }
   print "    </div><!-- tableResults -->\n";
   $this->displayTableFooter();
   print "  </div><!-- searchtable -->\n";
  }
 


  /**
   * @brief Builds the 'Exclude this field' tickbox
   * @date 2011-07-11
   * @version 11
   * 
   **/
  public function makeExcludeOpt($field) {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'
          .__METHOD__,
                        array($field));
   return "Exclude <input type='checkbox' name='xcl_$field' id='"
          .$this->domName("xcl_".$field)."'/>";
  }

  /**
   * @brief Gets the value of a particular field in this record
   * @date 2011-07-11
   * @version 11
   **/
  public function getDataValue($key) {
    if (is_array($this->id) && 
        array_key_exists($this->offset,$this->id) &&
        is_array($this->data) && 
        array_key_exists($this->id[$this->offset],$this->data) && 
        array_key_exists($key,$this->data[$this->id[$this->offset]])) {
     $dataValue = $this->data[$this->id[$this->offset]][$key];
    } else {
     $dataValue=NULL;
    }
    return $dataValue;
  }

  /**
   * @brief Displays this record.
   * Loops over each field, displays an appropriate widget for each field
   * @date 2011-07-11
   * @version 13
   **/
  public function displayData() {
   $this->hwDebugPubStart(__METHOD__);
   print "<div class='clearwidth'></div>\n";
   print "<div class='resultstable".($this->updatefailed?'changed':'')." ".(isset($this->action)?$this->action:'')."'>\n";
   $this->displayTableHeader();
   $this->beginItemdataForm();
   print "<div class='itemdata'>\n";

   # Loop over all columns
   foreach ($this->colobjs as $col) {
     print $col->displayRecordField();
   }

   print "</div>\n";
   print "    </div> <!-- tableResults -->\n";
   print "   <div class='clearwidth'></div>\n";
   $this->displayTableFooter();
   print "</div><!-- end resultstable -->\n";
   $this->hwDebugPubStop(__METHOD__);
 }

  /**
   * @brief Displays a button to activate some javascript
   * @date 2011-07-11
   * @version 11
   */
  protected function actionButton() {
    //$this->hotwireDebug(HW_DBG_OPRO,'Called [protected] '.get_class($this).'->'.__METHOD__);
    $html = '';
    # In a /record/ we only have one ID - the 0th.
    #if ($this->arrayKey($this->id,$this->offset)) {
    if ($this->arrayKey($this->id,0)) {
      if (isset($this->action) && $this->action=='clone') {
        if ($this->perms['insert']) {
          $html="<input type='submit' name='_action' value='".constant("BTN_MSG_CREATE")."' ".
                "onClick='changes=false;'/>";
        } else {
          $html='No permission to insert a new record.';
        }
      } else {
      if ($this->perms['update']) {
        $html .= "<input type='submit' class='update' name='_action' value='".constant("BTN_MSG_UPDATE")."' " .
                 "onClick='changes=false;'/>";
        if ($this->nextid) {
          $html .="<input type='submit' class='update shownext' name='_action' value='".constant("BTN_MSG_UPDATE_SHOW_NEXT")."' ".
                  "onClick='changes=false;'/>";
          $html .="<a href='edit.php?_view=".$this->view."&_id=".$this->nextid."'><input type='button' value='".constant("BTN_MSG_SHOW_NEXT")."'/></a>";
        }
      }
      }
    } elseif ($this->propertyEqual('action','search')) {
      $html .= "<input type='submit' name='_action' class='search' value='Search' " .
               "onClick='changes=false;'/>";
    } else {
      if (array_key_exists('insert',$this->perms) && $this->perms['insert']) {
        $html .= "<input type='submit' name='_action' class='create' value='".constant("BTN_MSG_CREATE")."' " .
                 "onClick='changes=false;'/>";
      }
    }
    return $html;
  }

  /**
   * @brief Displays a list of views for which this record is a foreign key
   * @todo Review and manage this!
   * @date 2011-07-11
   * @version 11
   */
  public function keyedViews($keyval = NULL) {
    # This all takes a stupid amount of time. Do it with AJAX if possible
    if ($_SESSION['javascript']) {
      $return=array();
      $return[HOTWIREMENU.'/Records referencing this one']='AJAXRELREC';
      return $return;
    } else {
      return ; 
    }
    $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__,
                        array($keyval));
    $menuitem=HOTWIREMENU;
    # What is our underlying table or identifier?
    $view = $this->view;
    # To find the underlying table:
    $sql="select definition, substring(definition from E'([[:alnum:]_]*?).id.* FROM ') as table from pg_views where viewname='".$this->view."'";
    $this->hotwireDebug(HW_DBG_DBVI,'Finding related views '.get_class($this).'->'.__METHOD__,array($sql));
    $dbQuery = pg_query($this->dbh, $sql);
    if (! $dbQuery) {
     print "SQL error: ".pg_last_error($this->dbh)."\n";
    }
    $table="";
    $rows = pg_fetch_all($dbQuery);
    $table=$rows[0]['table'];
    $definition=$rows[0]['definition'];
    #print "Table is $table\n";
    # Find the tables this item could be an FK for:
    $sql="SELECT DISTINCT c.relname, substring(pg_catalog.pg_get_constraintdef(r.oid, true) from E'FOREIGN KEY \\\\((.*)\\\\) REFERENCES') as fk ".
         "FROM pg_catalog.pg_class c ".
         "LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace ".
         "INNER JOIN pg_catalog.pg_constraint r ON r.conrelid=c.oid ".
         "WHERE  pg_catalog.pg_table_is_visible(c.oid) AND ".
                "pg_catalog.pg_get_constraintdef(r.oid, true) LIKE 'FOREIGN KEY % REFERENCES $table(%'".
         "ORDER BY c.relname;";
    $dbQuery = pg_query($this->dbh,$sql);
    $this->hotwireDebug(HW_DBG_DBVI,'Finding related views '.get_class($this).'->'.__METHOD__,array($sql));
    if (! $dbQuery) {
     print "SQL error: ".pg_last_error($this->dbh)."\n";
    }
    $tables=array();
    $results=array();
    $submenu="Records which may reference this record/";
    while ($row=pg_fetch_assoc($dbQuery)) {
     $tables[$row['relname']]=$row['fk'];
     $t=$row['relname'];
     $fk=$row['fk'];
     $sql="select distinct view, definition from ".$this->hwschema."._view_data where ".
          "(definition LIKE '%FROM%$t %' OR definition LIKE '%FROM%$t;%') ".
          " AND view NOT LIKE '%_subview%' ";
     $this->hotwireDebug(HW_DBG_DBVI,'Finding related views '.get_class($this).'->'.__METHOD__,array($sql));
     $dbQy = pg_prepare($this->dbh,'',$sql);
     $dbQy = pg_execute($this->dbh,'',array());
     if (! $dbQy) {
      print "SQL error: ".pg_last_error($this->dbh)."\n";
     }
    $this->echoDebug($sql,"SQL");

     while ($qrow=pg_fetch_assoc($dbQy)) {
      if (($qrow['view'] != $this->view) && $this->arrayKey($this->views,$qrow['view'])) {
       $view=$qrow['view'];
       $def=$qrow['definition'];
       $chunks=explode(' FROM ',$def);
       $sel=$chunks[0];
       # First step - is $fk mentioned in $sel? If so, that's the one we want to use
       # and if not, don't bother with the damn' thing!
$this->echoDebug($sel,'SEL');
$this->echoDebug($fk,'FK');
       if (strpos($sel,$fk)) {
        # Need to check here whether this is like $t.$fk AS alias...
        $searchfield=$fk;
        if (strpos($sel,$t.'.'.$fk.' AS ')) {
         preg_match("/$t\\.$fk AS (\\w*)/",$sel,$matches);
         #print "Alias for $t.$fk observed - ".$matches[1];
         if ($matches[1]) {
          $searchfield=$matches[1];
         }
        }
$this->echoDebug('OK to search '.$view.' with FK '.$fk);
        $results[$menuitem.'/'.$submenu.$this->hdrFormat($view)." (".$this->hdrFormat($searchfield).")"]="list.php?_view=".$view."&_action=search&_filter=".$this->id[0]."&_search=$searchfield";
       }  else {
       }
      }
     }
    }
    ## Now find links to items which are FKs in this table
    $submenu="Records referenced by this record/";
    
    $sql="SELECT substring(pg_catalog.pg_get_constraintdef(r.oid, true) from E'REFERENCES ([^ ]*)') as condef, ".
         "substring(pg_catalog.pg_get_constraintdef(r.oid, true) from E'FOREIGN KEY .([^ ]*)\\\\)') as field ".
         "FROM pg_catalog.pg_class c ".
         "LEFT JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace INNER JOIN ".
         "pg_catalog.pg_constraint r ON r.conrelid=c.oid ".
         "WHERE c.relname ~ '^($table)$' ".
         "AND pg_catalog.pg_table_is_visible(c.oid) AND pg_catalog.pg_get_constraintdef(r.oid, true) LIKE 'FOREIGN%';";
    //TODO: Parameterize this SQL properly
    $dbQuery= $this->sqlexec(get_class($this),__METHOD__,HW_DBG_DBVI,$sql);
    if (! $dbQuery) {
     print "SQL error: ".pg_last_error($this->dbh)."\n";
    }

    if (!is_array($this->id) || !array_key_exists(0,$this->id)) { return;}
    $tdata=$this->data[$this->id[0]];
$this->echoDebug($tdata,'TDATA');
    $chunks=explode(' FROM ',$definition);
    $sel=$chunks[0];
$this->echoDebug($sel,'SEL');
    while ($row=pg_fetch_assoc($dbQuery)) {
     #print "This table is linked to ".$row['condef']." by ".$row['field'];
     # Find a view based on that table
     $tbls=explode('(',$row['condef']);
     $tbl=$tbls[0];
     #print "TBL is $tbl ";
     $sql="select view from ".$this->hwschema."._view_data where definition ~ E'FROM[\\( ]*".$tbl."[; ]' AND view ~ E'_view$';";
     //$dbQ=pg_prepare($this->dbh,'',$sql);
     //$dbQ=pg_execute($this->dbh,'',array());
     $dbQ=$this->sqlexec(get_class($this),__METHOD__,HW_DBG_DBVI,$sql);
     $foundview=0;
 #print "SQL is $sql\n";
     if (!$dbQ) { print "SQL error: ".pg_last_error($this->dbh)."\n";}
     $tfld=$row['field'];
$this->echoDebug($tfld,'Tfld');
     # Search for aliasing
     if (strpos($sel,$table.'.'.$tfld.' AS ')) {
      preg_match("/$table\\.$tfld AS (\\w*)/",$sel,$matches);
      #print "Alias for $table.$tfld observed - ".$matches[1]." - on $tbl";
      if ($matches[1]) {
       $tfld=$matches[1];
      }
     }
     if (array_key_exists('_'.$tfld,$tdata)) {
      $tfld='_'.$tfld;
     }
$this->echoDebug('Tfld');
     if ( array_key_exists($tfld,$tdata)) {
      while ($qrow=pg_fetch_assoc($dbQ)) {
       $results[$menuitem.'/'.$submenu.$this->hdrFormat($qrow['view'])." (".$this->hdrFormat($tbl).")"]="'list.php?_view=".$qrow['view']."&_action=search&_filter=".$tdata[$tfld]."&_search=id'";
       $foundview=1;
      }
     } else {
      #$results['Failed to find field '.$tfld]="";
     }
    } # Loop through tables which are FKs in this table
$this->echoDebug($results,'RelRec menu');
$this->debug=false;
    $this->hotwireDebug(HW_DBG_OPUB,'Left [public] '.get_class($this).'->'.__METHOD__);
    return $results;
  }

  /**
   * @brief Displays a list of alternative views.
   * Look for views which select from the same data table
   * @date 2011-07-11
   * @version 11
   */
  protected function altViews() {
    // Remove to un-cache
    //unset($_SESSION['views'][$this->view]['siblings']);
    if (!array_key_exists('views',$_SESSION)) { return;}
    if (gettype($_SESSION['views'])!=='array') {return;}
    if (!array_key_exists($this->view,$_SESSION['views'])) { return ; }
    
    if (! array_key_exists('siblings',$_SESSION['views'][$this->view])) {
      // Loop over all views which have the same table
      $tbl=$this->views[$this->view]['entity'];
      $select = array("<option value=''>--- Alternative view ---</option>");
      $altviews=array();
      foreach ($this->views as $k => $v) {
        if ($v['entity']==$tbl) {
          $altviews[$this->formatViewName($k)]=$k;
        }
      }
      ksort($altviews);
      foreach ($altviews as $k => $v) {
          $select[]="<option value='".$v."'>".$k."</option>";
      }
      $html = "<select name='_view' "
            .  $this->ifJS('onChange="this.form.submit();"')
            .  (sizeof($select) > 1 ? ">" : " disabled>")
            .  implode(' ', $select) . "</select>";
      $_SESSION['views'][$this->view]['siblings']=$html;
    }
    return $_SESSION['views'][$this->view]['siblings'];
  }

  protected function formatViewName($viewname) {
    return $this->hdrFormat($viewname);
  }
 
  protected function OldaltViews($keyval = NULL) {
    // Only do this if the preference is set
    if (! $this->pref('ALTVIEW')) {
      return;
    }
    // Seek a cached version
    if (array_key_exists('siblings',$_SESSION['views'][$this->view])) {
      if (!$keyval) { print "(no alternative views)"; return;}
      $id = array_shift(array_values($keyval));
      $html=$_SESSION['views'][$this->view]['siblings'];
      $html=str_replace($html,'%ID%',$id);
      return $html;
    }

    $sql = "SELECT * FROM ".$this->hwschema."._view_data WHERE view NOT LIKE '^_%' ";

    if ($keyval) {
      $key = array_shift(array_keys($keyval));
      $id = array_shift(array_values($keyval));
      $sql .= "AND definition LIKE 'SELECT %$key% FROM%' ";
    } else {
      $sql .= "AND definition SIMILAR TO '%FROM \\\(*$this->entity( |;)%' ";
    }

    $sql .= "ORDER BY view";

    //$dbQuery = pg_query($this->dbh, $sql);
    $dbQuery=$this->sqlexec(get_class($this),__METHOD__,HW_DBG_DBVI,$sql);
    if (!$dbQuery) {$this->sqlErrror('looking up list of alternative views',$sql);}
    $rows = pg_fetch_all($dbQuery);
print "Old Alt Views\n";
    $select = array("<option value=''>--- Alternative view ---</option>");
    if (pg_num_rows($dbQuery)>0) {
    foreach ($rows as $row) {

      if (($row['select']=='t') && ($row['view'] != $this->view)) {
        array_push($select, "<option value='".$row['view']."'" .
                            ($this->view == $row['view'] ? " selected" : "") .
                            ">" . $this->hdrFormat($row['view']) . 
                            "</option>");
      }

    }
    }

    $html = "<select name='_view' onChange=\"this.form.submit();\"" .
            (sizeof($select) > 1 ? ">" : " disabled>") .
            implode(' ', $select) . "</select>";
    if ($keyval) {
      $html .= "<input type=hidden name='$key' id='$key' value='%ID%'>";
    }
    $_SESSION['views'][$this->view]['siblings']=$html;
    $html=str_replace($html,'%ID%',$id);
    return $html;
  }

 /**
  * @brief Builds the SQL to perform an update or insert from the posted values
  * @todo Rationalise, use more private methods to make this readable
  * @todo Use placeholder-style SQL queries rather than pg_escape_string
  * @date 2011-07-11
  * @version 11
  */
  private function dbUpdateOrInsert() {
    $this->hotwireDebug(HW_DBG_OPRI,'Called [private] '.get_class($this).'->'.__METHOD__,
                        array());
    // Begin transaction
    pg_query($this->dbh, 'begin');
    $cols=array();
    $vals=array();
    $columns='';
    $values='';
    $sql='';
    // Handle uploaded files: Loop over uploaded files, check to see 
    // whether they match the name of a field.
    $this->echoDebug($_FILES,'Files');
    foreach (array_keys($_FILES) as $filefield) {
     $this->echoDebug($filefield,'File');
     if (array_key_exists($filefield, $this->colAttrs)
     && (array_key_exists('tmp_name',$_FILES[$filefield]))
     && (file_exists($_FILES[$filefield]['tmp_name']))) {
       # It's a posted field.
       $filename=$_FILES[$filefield]['tmp_name'];
       $fp=fopen($filename,'r');
       $buffer=fread($fp,filesize($filename));
       if ($this->colAttrs[$filefield]['type'] === 'oid') {
         $oid=pg_lo_create($this->dbh);
         $handle=pg_lo_open($this->dbh,$oid,'w');
         $w=pg_lo_write($handle,$buffer);
         $o=pg_lo_close($handle);
         $columns.=($columns ? "," : "").$filefield;
         $this->post[$filefield]=$oid;
       } else {
         # Not an oid, so we just cast and push.
         $this->post[$filefield]=pg_escape_bytea($buffer);
       }
     }
    }

    # Build SQL
    foreach(array_keys($this->post) as $key) {
      // Filter out fields which aren't of the record...
      // Or which are hidden or ID fields, or read only.
      if ((! array_key_exists($key,$this->colAttrs)) ||
          (preg_match('/^_|^id$|\bro_/', $key))) {
        if (!array_key_exists($key,$this->colAttrs)) {
         $this->echoDebug('Field not found :'.$key);
         $this->echoDebug($this->colAttrs,'ColAttrs');
        }
       continue;
      }
      // Make a copy of the posted data, sanitize and then update the DB
      $fields=$this->post;
      if (preg_match('/^mm_|_id$/', $key)) { 
        // for arrays
        if (is_array($fields[$key])) {
         if (($this->action == 'update') 
          || ($this->action == 'insert')) {
          $fields[$key]=$this->phpArrayToPG($fields[$key]);
         }
        }
        if (empty($fields[$key])) {
          $fields[$key] = "NULL";
        } 
      } else {
        if ($fields[$key] == '') {
          $fields[$key] = "NULL";
        } 
      }
      if ($this->action == 'update') {
        if ($fields[$key] === 'NULL') {
         array_push($cols,$key);
         array_push($vals,NULL);
         $sql .= ($sql ? ", ": ""). '"'.$key.'"= NULL';
        } else {
         if (! in_array($key,$cols)) {
          array_push($cols,$key);
          array_push($vals,$fields[$key]);
          $sql .= ($sql ? ", " : "") . '"'.$key . '"=' ."'". pg_escape_string($fields[$key])."'";
         }
        }
      } elseif ($this->action == 'insert') {
        if ($fields[$key] != 'NULL') {
          $columns .= ($columns ? "," : "") . '"'.pg_escape_string($key).'"';
          $values .= ($values ? "," : "") . "$$".pg_escape_string($fields[$key])."$$";
          array_push($cols,$key);
          array_push($vals,$fields[$key]);
        }
      }
    }

    // Some fields may not be passed through from the browser for update
    // Fields not shown in the browser get set to NULL here.
    // RFL really isn't convinced this is wise.
    if ($this->action=='update') {
      $extrafields=array_diff(preg_grep("/^_|^ro_|_ro$/",array_keys($this->colAttrs),PREG_GREP_INVERT),array_keys($fields));
      foreach ($extrafields as $k) {
        # Ignore fields of type _hwsubview or OID
        if ($this->colAttrs[$k]['type']!='_hwsubview' 
         && $this->colAttrs[$k]['type']!='blobtype'
         && $this->colAttrs[$k]['type']!='bytea'
         && $this->colAttrs[$k]['type']!='oid') {
          $sql.=($sql ? ", ":"") .'"'.$k.'"=NULL';
          array_push($cols,$k);
          array_push($vals,NULL);
        }
      }
    }
    if ($this->action == 'update') {
      $i=0;
      $sql='';
      $cols2=array();
      foreach ($cols as $col) {
       $i++;
       array_push($cols2,"\"$col\"=\$$i");
      }
      array_push($vals,$fields['id']);
      $sql = "UPDATE \""
            . $this->views[$this->view]['schema'].'"."'
           . $this->view."\" SET ".join(',',$cols2). " WHERE id=\$".(count($vals));
    } elseif ($this->action == 'insert') {
      $sql = "INSERT INTO \""
           . $this->views[$this->view]['schema'].'"."'
           . $this->view."\" (\"".join('","',$cols)."\") VALUES (".join(',',array_map(function($i){ return '$'.$i;} ,range(1,count($vals)))).")";
      $sql .= " RETURNING id";
    }

    $this->echoDebug($sql, __FUNCTION__ . '(): SQL');
    $start=microtime(true);
    $dbResult = pg_query_params($this->dbh, $sql,$vals);
    $this->logQuery($sql);
    $return = NULL; 
    if ($dbResult) {
      if ($this->action == 'insert') {
        $return = pg_fetch_assoc($dbResult);
      }
      $this->status = 'Success';
      $this->updatefailed=false;
      $this->msg = $this->hdrFormat($this->action) . ' OK';
      if (!is_array($_SESSION['msgs'])) {
	      $_SESSION['msgs']=array();
      }
      $uniqid=uniqid('msg');
      $_SESSION['msgs'][$uniqid]=$this->msg;
      unset ($this->action);
      pg_query($this->dbh, 'commit');
      # Find the record id and redirect to it, to prevent people resubmitting the form by refreshing page
      if ($return['id']>0) {
        $myid = $return['id'];
      } else {
        $myid = $this->id[0];
      };
      header('HTTP/1.1 303 See Other');
      header('Location: edit.php?_view='.$this->view.'&_id='.$myid.'&_msg='.$uniqid);
      exit;
    } else {
      $this->echoDebug('Unable to insert');
      $this->status = 'Fail';
      $this->updatefailed=true;
      $msg=array();
      $msg['raw']=pg_last_error();
      if (preg_match('/^(.*)\nHINT: +(.*)/ms',$msg['raw'],$matches)) {
        $msg['raw']=$matches[1];
        $msg['hint']=$matches[2];
      }
      if (preg_match('/^(.*)\nDETAIL: +(.*)/ms',$msg['raw'],$matches)) {
        $msg['raw']=$matches[1];
        $msg['detail']=$matches[2];
      }
      if (preg_match('/(.*)\n?ERROR: +(.*)/ms',$msg['raw'],$matches)) {
        # This should be hit every time.
        $msg['raw']=$matches[1];
        $msg['err']=$matches[2];
      }
      if ($msg['raw']=='') {
       # Format as a structured error block
       $this->msg = "<div class='hw_error_struct'>\n ".
                    "<span class='error'>".htmlspecialchars($msg['err'])."</span>\n ".
                    "<span class='detail'>".htmlspecialchars($msg['detail'])."</span>\n ".
                    "<span class='hint'>".htmlspecialchars($msg['hint'])."</span>\n".
                    "</div>";
      } else {
       $this->msg = "<div class='hw_error_unstruct'>".htmlspecialchars($msg['raw'])."</div>";
      }
      // Leave $this->action==insert so that we don't lose data
      pg_query($this->dbh,'rollback');
    }
    $end=microtime(true);
    global $hw_sql_timing;
    $hw_sql_timing[$sql.print_r($vals,1)]=($end-$start)/1000;
    // end transaction
    if (isset($return)) {
      $ret=$return['id'];
    } else {
      $ret = $fields['id'];
    }
    $this->echoDebug($ret,"Returning from update/insert");
    return $ret;
  }
 
  /**
   * @brief Calculates a checksum for this row
   * Selects this row, converts to text, calculates
   * the MD5sum. Called through AJAX from the frontend.
   * @date 2011-07-11
   * @version 11
   **/
  function calculateChecksum() {
   if (!property_exists($this,'view')) { return;}
   if (!$this->id) { return;}
   $cksumsql='select md5(textin(record_out("'.
             $this->views[$this->view]['schema'].'"."'.
             $this->view.'".*))) from "'.
             $this->views[$this->view]['schema'].'"."'.
             $this->view.'" where id '.
             'in ('.join(',',$this->id).');';
   $this->checksum=$this->sqltostring($cksumsql);
  }

  /** 
   * @brief Gives a list of views which might link to this record
   * @date 2012-11-01
   * @author Frank Lee
   * @version 1
   **/
   public function relatedrecords() {
    $table=$this->views[$this->view]['entity']; 
    $sql="select rtrim(regexp_replace(split_part(definition,' ',2),E'.*\\\\.',''),',') a from pg_catalog.pg_views where schemaname='".$this->hwschema."' and viewname=\$1;";
    $field=$this->sqltostring($sql,array($this->view));
    // Other views based on select $table.$field appear in 'alternative views'
    // Find views which refer to this table as a foreign key
    $sql="
 select trim(trailing '\"' from trim(leading '".$this->hwschema.".\"' from ev_class::text)) as view, regexp_replace(regexp_replace(f,E'.*\\\\.',''),'.*AS ','') as field from (select table_name, column_name, ev_class, regexp_split_to_table(regexp_replace(ltrim(definition,'SELECT '),'FROM.*',''),', ') f from (SELECT pg_get_viewdef(ev_class) as definition, rw.ev_class::regclass, tc.table_name, kcu.column_name 
FROM 
    information_schema.table_constraints AS tc 
   JOIN pg_class c ON c.relname=tc.table_name
   JOIN pg_depend d ON (c.oid=d.refobjid)
    JOIN information_schema.key_column_usage AS kcu using  (constraint_name, constraint_schema)
   JOIN pg_attribute a on (c.oid=a.attrelid and d.refobjsubid=a.attnum and a.attname=kcu.column_name)
    JOIN information_schema.constraint_column_usage AS ccu using (constraint_name, constraint_schema)
   INNER JOIN pg_namespace n ON (c.relnamespace = n.oid)
   INNER JOIN  pg_rewrite rw ON (d.objid = rw.oid)
WHERE constraint_type = 'FOREIGN KEY' AND ccu.table_name=\$1 and ccu.column_name=\$2 and rw.ev_class::regclass::text LIKE '".$this->hwschema.".%' and rw.ev_class::regclass::text NOT LIKE '%//%' and rulename='_RETURN' and has_table_privilege(rw.ev_class,'select'::text)) a ) b where b.f  LIKE table_name||'.'||column_name order by ev_class;";
    $dbResult=pg_query_params($this->dbh, $sql, array($table,$field));
    $return=array();
    while ($row=pg_fetch_assoc($dbResult)) {
     $vn=join(' =&gt; ',explode('/',$row['view']));
     $return[]=array('name'=>$vn.' - '.$this->hdrFormat($row['field']),
                     'url'=>'list.php?_action=search&_view='.$row['view'].'&'.$row['field'].'='.$this->id[0]);
    }
    return $return;
   }

   /**
    *  Yep, we do need this. It provides images (-:
    */
  public function displayField() {
   $keys=array_flip($this->colList);
   $this->colobjs[$keys[$this->field]]->displayField();
  }

 /**
  * @brief To encapsulate the result as a dbAjaxResult object
  * @author Frank Lee
  * @date 2012-11-24
  * @version 1
  **/
  public function ajaxresult() {
    $obj=new dbAjaxResult();
    # Has to return sensible values for delete, update, insert

    # Delete / insert - just boolean and message
    if ($this->updatefailed) {
      $obj->fail($this->msg);
    } else {
      $obj->succeed($this->msg);
    }
    # Insert returns a new ID value
    if ($this->id) {
      $obj->setid($this->id);
    }
  }
}
?>
