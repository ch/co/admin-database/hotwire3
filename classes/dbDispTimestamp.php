<?php
 /**
  * @brief A class with methods to display an Integer field.
  * Inherits everything from dbDispInt8 (BigInt) class
  * @author Frank Lee
  * @date 2011-07-11
  * @version 11
  **/
class dbDispTimestamp extends dbDispBase {
 private $infmt;
 private $outfmt;

 public function __construct($args) {
   parent::__construct($args);
   $this->infmt=$this->pref('DBDATEFORMAT','Y-m-d').' H:i:s';
   $this->outfmt=$this->pref('DATEFORMAT','Y-m-d').' H:i';
 }


 /**
  * @brief Ensure we include the correct JS file for our UI
  * @return Some HTML to include in our HEAD
  * @version 11
  * @date 2011-07-11
  **/
 public static function prepareRecord(&$js,&$css) {
  $js[]="js/datetimepicker/jquery.datetimepicker.full";
  $css[]="css/datetimepicker/jquery.datetimepicker.min";
 }

 /**
  * @brief To display a simple edit UI for the general case
  * @date 2011-07-11
  * @version 11
  **/
 public function displayEdit() {
   // Don't display fields named _FOO by convention
   if (substr($this->dbColumn->name(),0,1)=='_') { return;}
   return '<input type="text" size="10" name="'.htmlspecialchars($this->dbColumn->name()).'" '.
          ($this->ro?' readonly="readonly" class="readonly" ':'').
          'value="'.$this->valuefromrow($this->objRecord->data[$this->objRecord->id[0]]).'" />';
 }

  protected function valuefromrow(&$row) {
     $value=$row[$this->colname];
     $date=DateTime::createFromFormat($this->infmt,$value);
     if ($date === FALSE ) {
       return '';
     }
     $text_date=$date->format($this->outfmt);
  return ($text_date);
 }


}
?>
