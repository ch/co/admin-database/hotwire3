<?php
 // $Id$

/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

 /** 
  * @file dbView.php
  * @brief Handles a view on the database
  * Part of Hotwire http://hotwire.sourceforge.net/. 
  */

 /**
  * @class dbView
  * @brief Handles a view of the database
  * @author Stuart Taylor
  * @author Frank Lee
  */
class dbView extends dbBase {

  public $entity;           //<!@brief Table from which the view is selected
  public $view;		    //<!@brief Name of the current view
  public $status;           //<!@brief Human readable result of last operation
  public $offset;           //<!@brief start displaying from row N
  public $pagelen=NULL;     //<!@brief display N rows
  public $search;           //<!@brief ??
  public $msg;              //<!@brief ??
  public $widgets = array();//<!@brief Array of widgets to display stuff
  public $subview = array();//<!@brief Array of subviews associated with this view
  public $colAttrs = array(); //<!@brief Additional information about columns
  public $colobjs = array(); //<!@brief Array of dbColumn objects
  public $colList = array(); // List of names of columns in order
  public $needstinymce = FALSE; //<!@brief Should be redundant.

 /** 
  */

 protected function ensurePath($paths,$url,&$menu) {
   $thispath=array_shift($paths);
   if (count($paths)==0) {
    $menu[$thispath]=$url;
    return;
   } else {
    if (!array_key_exists($thispath,$menu)) {$menu[$thispath]=array();}
    $this->ensurePath($paths,$url,$menu[$thispath]);
   }
 }

 protected function buildMenuArrayFast($items) {
   $this->hwDebugProStart(__METHOD__);
   $menuarray=array();
   foreach ($items as $p => $url) {
    $paths=explode('/',$p);
     $this->ensurePath($paths,$url,$menuarray);
   }
   $this->hotwireDebug(HW_DBG_OPRO,'Ended [protected] '.get_class($this).'->'.__METHOD__);
   $this->hwDebugProStop(__METHOD__);
   return $menuarray;
 }

 public function permarray() {
  return $this->views[$this->view];
 }

 /**
  * @brief Display an appropriate navigation for this view
  * @date 2011-07-15
  * @version 1
  * @todo: Fix menu caching
  */
  public function echoNavBar() {
    $this->hwDebugPubStart(__METHOD__);
    $navbar=array();
    $navbar[]="<div id='hw_menu'>". $this->navbarViewName();
    // Menu list
 if (false) { //TODO: ensure menu caching works.
    if (! array_key_exists('menu-'.$this->view,$_SESSION)) { 
     $vs=$this->navbarGetAllItems();
     $menuarray=$this->buildMenuarrayFast($vs);
     ksort($menuarray);
     $_SESSION['menu-'.$this->view]=$this->buildmenuformat($menuarray);
    }
    $navbar[]=$_SESSION['menu-'.$this->view];
 } else {
     $vs=$this->navbarGetAllItems();
     $menuarray=$this->buildMenuarrayFast($vs);
     ksort($menuarray);
     $navbar[]=$this->buildmenuformat($menuarray);
 }
    // Search form
    $navbar[]=$this->navbarBuildSearchForm();
    // Nav controls
    $navbar[]=$this->navbarControls();
    // Debugging - no longer handled this way
/*
if (array_search('dev',$this->roles)!==false) {
  $navbar[]='<span id="hw_control_debug">DEBUG';
  if (property_exists($this,'debugval') && is_array($this->debugval)) {
  foreach ($this->debugval as $v) {
   $navbar[]='<input type="hidden" id="_debugval_'.$v.'" value="'.$v.'"/>';
  }
  }
  if (property_exists($this,'debugobj') && is_array($this->debugobj)) {
  foreach ($this->debugobj as $o) {
   $navbar[]='<input type="hidden" id="_debugobj_'.$o.'" value="'.$o.'"/>';
  }
  }
  $navbar[]='</span>';
}*/
    $navbar[]="</div><!-- id=menu -->\n";
    print join("\n",$navbar);
    $this->hwDebugPubStop(__METHOD__);
  }

  /**
   * @brief Produce the bookmark div.
   * @date 2011-07-15
   * @version 1
   */
  private function navbarBookmark() {
    $this->hwDebugPriStart(__METHOD__);
    $html='';
    $url='http';
    if (array_key_exists('HTTPS',$_SERVER) && $_SERVER["HTTPS"] == "on") {$url .= "s";}
    $url.="://".$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME']."?";
    $url.=$this->setGetVars(array('view','pagelen','id','filter','action','search'));
    $url.="&".$this->getSearchVars();
    $html.="<a class='bookmark' target='_blank' href='$url'>".
'
<img id="hw_bookmark" title="Drag this icon to bookmark this page" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAANCAMAAABBwMRzAAAAAXNSR0IArs4c6QAAAJlQTFRF/////v7+9v7+5PX97fX94/T84vP7yOn5v+D5tt/4vt/4td73rt/4rd73pNX2rN32o9z1o9T1nNX2mtP0kcv0f8nyfsjxdsjxd8HybcDxdcfwbL/wZLbwW7XvWr3uSazuUbPtP6rsNqnrLajqLqHrLKfpJaDqLJ/pJJ/pG5/pGp7oI57oCJTmAJTmAJPlAIvlAIvlAIrkAIjibG1MNgAAAAFiS0dEAIgFHUgAAAAJcEhZcwAACxMAAAsTAQCanBgAAAAHdElNRQfcBwUMJAY9R+chAAAAd0lEQVQI12NggABVBhTAZ8SLwteQVUeRVmZRRygQldERYhDWlRThZAbyjIyMxDiYmJh4xOU09YB8ESN2qDouQwEQJagEEeBQ4ofq1wZTRoIw05XAlBY3lC8tzcgrxcugIAHlK0kra0lrqcgpQvlG+iCdwoa6QBIA0ewILR2vgSgAAAAASUVORK5CYII="/>
'."</a>";
    $this->hwDebugPriStart(__METHOD__);
    return $html;
  }

  /**
   * @brief Displays output of any insert or update queries
   * @date 2011-07-15
   * @version 1
   * @todo: is this called?
   */
  protected function displayPreviousMessage() {
    $html='<!-- displayPreviousMessage -->';
    if (property_exists($this,'msg')
     && $this->msg) {
      $html.="<div class='results'>".$this->msg."</div>";
    }
    print $html;
  }

  /** 
   * @brief displays a checksum box
   * @version 1
   * @date 2011-07-11
   * @todo: Is this called? 
   * @todo: If JS is enabled, checksum should be handled earlier
   * @todo: Move checksum code into hw_objs as a JS object and call appropriately.
   **/
  protected function displayChecksumBox() {
    if (method_exists($this,'calculateChecksum')) {
      $this->calculateChecksum();
    } else {
      print "No checksum method. This is odd....";
    }
    if (property_exists($this,'checksum')
     && $this->pref('CHANGE')) {
      $ids=(property_exists($this,'id')?'["'.join('","',$this->id).'"]':"null");
      $cks="'".$this->checksum."'";
      $view="'".$this->view."'";
      ?>   
<script type="text/javascript" src="js/checksum.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    setTimeout(function() {
        var id=<?php print $ids;?>
        comparechecksum('checksum',<?php print $cks;?>,<?php print $view;?>,id);
    }, 1000);
    $('#checksum').hide();
});
</script>
<div id="checksum" class="checksum">
Javascript is not enabled. Unable to check for datachanges.
</div>
<?php
    }
  }

  /**
   * @brief Displays the navigation 'back' button
   * @version 1
   * @param[in] vars An array of variables controlling the view.
   * @date 2011-12-13
   **/
  private function navbarBack($vars) {
    // Build the "<<" link
    if (array_key_exists('_offset',$vars)) {
      if ($vars['_offset']>$this->pagelen) {
        $vars['_offset']=$vars['_offset']-$this->pagelen;
        $title='View results '.($vars['_offset']+1).' - '.
               ($vars['_offset']+$this->pagelen);
      } else {
        unset($vars['_offset']);
        $title='View first '.$this->pagelen.' results';
      }
      $html="<a class='navbuttons' title='$title' href='".
             $this->baseurl().'?'.$this->buildGetArgs($vars).'&'.$this->usortobjs->asGET().
             "'>&laquo;</a>";
    } else {
      # Make the << do nothing (first page of the data)
      $html="<span class='navbuttons' title='Already on the first page'>&laquo;</span>";
    }
    return $html;
  }

  /**
   * @brief Displays the navigation 'All' button
   * @param[in] vars An array of variables controlling the view
   * @return string representing the "All" button
   * @date 2011-12-13
   * @version 1
   **/   
  private function navbarAll($vars) {
    unset($vars['_offset']);
    $vars['_pagelen']=$this->count;
    $title='Display all '.$vars['_pagelen'].' records';
    $html="<a class='navbuttons' title='$title' href='".
           $this->baseurl().'?'.$this->buildGetArgs($vars).'&'.$this->usortobjs->asGET().
           "'>All</a>";
    return $html;
  }

  /**
   * @brief Displays the navigation '>>' button
   * @param[in] vars An array of variables controlling the view
   * @return string representing the '>>' button
   * @date 2011-12-13
   * @version 1
   **/
  private function navbarNext($vars) {
    if (!array_key_exists('_offset',$vars)) { $vars['_offset']=0;}
    if ($vars['_offset']+$this->pagelen>$this->count) {
      $title='Already on the last page';
      $html="<span class='navbuttons' title='$title'>&raquo;</span>";
    } else {
      $vars['_offset']+=$this->pagelen;
      $title='View results '.($vars['_offset']+1).' - '.
             ($vars['_offset']+$this->pagelen>$this->count?
              $this->count:$vars['_offset']+$this->pagelen);
      $html="<a class='navbuttons' title='$title' href='".
             $this->baseurl().'?'.$this->buildGetArgs($vars).'&'.$this->usortobjs->asGET().
             "'>&raquo;</a>";
    }
    return $html;
  }

  /**
   * @brief Returns the base URL (without GET args) for this page
   * @return string URL
   * @date 2011-12-13
   * @version 1
   **/
  private function baseurl() {
    $this->hwDebugPriStart(__METHOD__);
    #$url='http';
    #if (array_key_exists('HTTPS',$_SERVER) && $_SERVER["HTTPS"] == "on") {$url .= "s";}
    #$url.="://".$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME'];
    $url=$_SERVER['SCRIPT_NAME'];
    $this->hwDebugPriStart(__METHOD__);
    return $url;
  }

  /**
   * @brief Displays the navigation controls.
   * @todo Pop-up navbarControls on subviews?
   * @date 2011-12-13
   * @version 12
   */
  private function navbarControls() {
    $this->hwDebugPriStart(__METHOD__);
    $html = "";
    // Don't display navbarcontrols on records. We do, after all,
    // only have one of them.
    if (! property_exists($this,'count')) {
      return;
    }
    # Detect multiple-views and don't display navigation 
    if (($this->count > $this->pagelen) 
     && !(property_exists($this,'viewlist') && $this->viewlist)) {
      $url='http';
      if (array_key_exists('HTTPS',$_SERVER) && $_SERVER["HTTPS"] == "on") {$url .= "s";}
      $url.="://".$_SERVER['SERVER_NAME'].$_SERVER['SCRIPT_NAME']."?";
      $vars=$this->getVars(array('view','pagelen','id','filter','action','search','offset','sort','order'));
      $html .= $this->navbarBack($vars).
               $this->navbarAll($vars).
               $this->navbarNext($vars);
    }

    if ($this->count > $this->offset) {
      $html .= "&nbsp;&nbsp;[" . 
        ($this->count > 1 && $this->pagelen > 1 ? "" : "") .
        ($this->offset + 1) . 
        ($this->count > 1 && $this->pagelen > 1
          ? "-" .
          ($this->count < ($this->offset + $this->pagelen) ?
           $this->count : ($this->offset + $this->pagelen))
          : "") . "] of $this->count" . 
        ((property_exists($this,'action') 
         && $this->action == 'search') 
         ? " (search results)" : "");
    }

    if ($html) {
      $html ="<div id='hw_navbuttons'>".$html."\n         </div>";
    }
    $this->hwDebugPriStart(__METHOD__);
    return $html;
  }

  /**
   * @brief Maybe build the search menu
   * @date 2011-07-15
   * @version 1
   **/
  private function navbarBuildSearchForm() {
    $this->hwDebugPriStart(__METHOD__);
    if (isset($this->view) 
     && !preg_match('/\bsearch/', $this->view) 
     && !(property_exists($this,'viewlist') && $this->viewlist)
     && !(is_a($this, 'dbRecord') 
         && property_exists($this,'action')
         && isset($this->action)
         && in_array($this->action, array('new', 'search')))) { 
      return $this->navbarSearch();
    }
    $this->hwDebugPriStart(__METHOD__);
  } 

  /**
   * @brief Generates the search box.
   * @date 2012-11-15
   * @version 13
   * We display this if the pref OLDSEARCH is set or we don't have JS
   */
  private function navbarSearch() {
    if (array_key_exists('javascript',$_SESSION) && $_SESSION['javascript'] && ! $this->pref('OLDSEARCH')) { return ; }
    $this->hwDebugPriStart(__METHOD__);
    # filter out cols with types we can't do a text search on
    function col_is_searchable($col){
        $unsearchable_types = array("bytea", "oid");
        return !in_array($col["type"],$unsearchable_types);
    }
    $searchable_fields=array_filter($this->colAttrs,'col_is_searchable');
    # filter out cols with certain name patterns (the main id column, old style oid images, many-many cols(?), hidden cols)
    $fields = array_values(preg_grep('/^id$|_oid$|\bmm_|^_/',
			   array_keys($searchable_fields),
                           PREG_GREP_INVERT));
    # Replace any foo_id fields with foo_hid fields
    $fields = preg_replace('/(.*)_id$/','${1}_hid',$fields);
    $fields = array_unique($fields);
    if ($this->search) {
     $selected=$this->search;
    } elseif (array_key_exists(0,$fields)) {
     $selected=$fields[0];
    } else {
     $selected=NULL;
    }
    
    $html = "<form name=qs_form action='list.php' method=get>".
      $this->setPostVars(array('view', 'sort', 'pagelen')) .
      $this->hiddeninputscalar('_action','search').
      $this->hiddeninputscalar('_pagelen',0).
      $this->genSelect('_search',$fields, $selected) . 
      "&nbsp;matches&nbsp;". 
      "<input type=text name='_filter' class='navSearch' " .
      "value='" .(property_exists($this,'filter')?
                  $this->filter:''). "'>" .
      "<input type=submit value='Search' class='btn'>&nbsp;" .
      $this->setSearchVars() . "
     </form>";

    $this->hwDebugPriStop(__METHOD__);
     return "<div class='menutopinactive' "
           ."id='".($this->pref('OLDSEARCH')?"hw_quicksearch_form":"hw_quicksearch_form_hide")."'>"
           .$html."</div>\n";
  }


  /**
   * @brief Creates a selection box from a list.
   * @todo Is this compatible with another, similarly named, routine?
   * @todo: Is this version actually called?
   * @date 2011-07-11
   * @version 11
   */
  private function genSelect($name, $items, $selected, $sort = TRUE) {
    $this->hwDebugPriStart(__METHOD__,array($name,$items,$selected,$sort));
    $html = "<select name=$name>";
    if ($sort) { sort ($items); }
    foreach ($items as $item) {
      $html .= "<option value='$item'" . 
               ($item == $selected ? " selected>" : ">") .
               $this->hdrFormat($item) . "</option>";
    }
    $html .= "</select>";
    $this->hwDebugPriStop(__METHOD__);
    return $html;
  }


  /**
   * @brief Build the menu array structure from a flat array
   * @param[in] path Path to menu item
   * @param[in] value Name of menu item
   * @param[out] r Pointer to array to contain tree structure
   * @date 2011-07-11
   * @version 11
   */  
  private function buildmenuarray($path,$value,&$r) {
    $this->hwDebugPriStart(__METHOD__,array($path,$value,$r));
   $first=array_shift($path);
   if (count($path)==0) {
    $r[$first]=$value;
   } else {
    if (! array_key_exists($first,$r)) {
     $r[$first]=array();
    }
    $this->buildmenuarray($path,$value,$r[$first]);
   }
    $this->hwDebugPriStop(__METHOD__);
  }

  /**
   * @brief Returns the HTML equivalent of the menu array structure
   * @param[in] menuarray Array containing the menu
   * @param[in] top Boolean indicating we're at the top of the tree
   * @return HTML formatted nested-UL list representing menu
   * @date 2011-07-11
   * @version 11
   **/
   private function buildmenuformat($menuarray,$top=true,$depth='') {
    //$this->hwDebugPriStart(__METHOD__,array($menuarray,$top));
    $formatted='';
    foreach ($menuarray as $k =>$v) {
     // Strip off leading numbers
     $nicek=preg_replace('/^\d+_/','',$k);
     // Replace spaces
     $nicek=preg_replace('/_/',' ',$nicek);
     if (is_array($v)) {
      ksort($v);
      $formatted.="<li><a href='#".$nicek."'>".$nicek."</a>".$this->buildmenuformat($v,false,'')."</li>\n";
     } else {
      $formatted.="<li><a href='$v'>".$nicek."</a></li>\n";
     }
    }
    //$this->hwDebugPriStop(__METHOD__);
    return ($top?"<nav id='primary_nav_wrap'>":"").
           "<ul ".($top?"class='sm sm-clean'":"").">".$formatted."\n</ul>".
           ($top?"</nav>":"");
   }

 /**
  * @brief Get the complete list of views (including 'artificial' views)
  * @date 2011-07-15
  * @version 1
  */
  private function navbarGetAllItems() {
    $this->hwDebugPriStart(__METHOD__);
    // Build the menu structure:
    $viewnames=array_unique(preg_replace('/\/\/.*/','//*',array_keys($this->views)));
    $viewnames = preg_grep('/\/_/',$viewnames,PREG_GREP_INVERT);
    $vs=array();
    foreach ($viewnames as $n) {
      $a=preg_replace('/\/\/./','',$n);
      $vs[$a]='list.php?_view='.htmlspecialchars($n);
    }
    // If we don't have HOTWIREMENU defined, these won't get displayed
    if (defined('HOTWIREMENU')) {
      $vs[HOTWIREMENU.'/99_Logout']='logout.php';

      // Need $this->view and not $this->viewlist for Advanced Search
      if (property_exists($this,'view') 
       && $this->view 
       && !property_exists($this,'viewlist')
       && !$this->pref('NOADVSEARCH',false)
      ) {
        $vs[HOTWIREMENU.'/90_Advanced Search']='edit.php?_view='.$this->view.
                                               '&_action='.constant('BTN_MSG_SEARCH');
      }

      // Need to have searched to Amend Search
      if (property_exists($this,'action') 
       && isset($this->action) && $this->action == 'search') {
        // NB - setGetVars includes POSTed data if action=search
        $vs[HOTWIREMENU.'/90_Amend Search']='edit.php?'.
                        $this->setGetVars(array('action','view'));
      }

      // Need to be showing a list to flip records
      if (property_exists($this,'listClass') 
       && $this->listClass == 'view' 
       && $this->data) {
        $ids = array_keys(array_slice($this->data, 0+$this->offset, 
                                      0+$this->pagelen, TRUE));
        $vs[HOTWIREMENU.'/90_Flip records']='list.php?'.
                         $this->setGetVars(array('view','action','filter')).'&'.
                         '_id[]='.join('&_id[]=',$ids);
      }

      // Need to be showing a non-readonly list to add new record
      if (!(property_exists($this,'listClass') && ($this->listClass == 'report'))
       && array_key_exists('insert',$this->perms) 
       && $this->perms['insert']) {
        $vs[HOTWIREMENU.'/00_Add new record']='edit.php?_action=new&'.
                                                  '_view='.$this->view;  
        if ($this->id && (!(isset($this->action))||($this->action != 'clone'))) {
         $vs[HOTWIREMENU.'/99_Clone this entry']='edit.php?_action=clone&'.
						 '_id[]='.(is_array($this->id)?join('&_id[]=',$this->id):'&_id[]='.$this->id).'&'.
                                                 '_view='.$this->view;
        }
      }

      // Need to be showing a single record to dislpay related revords
      if ($this->pref('RELREC') && method_exists($this,'keyedViews')) {
        $morevs=$this->keyedViews();
        if (is_array($morevs)) {
          $vs=array_merge($vs,$morevs);
        }
$this->echoDebug($vs,'Vs');
      }
     
      // Need to have CSV rights to import/export
      if (in_array('csv', $_SESSION['dbroles']) 
       && (is_a($this, 'dbList') || is_a($this,'dbMetaList'))) {  
        if (property_exists($this,'view') && $this->view) {
          $vs[HOTWIREMENU.'/99_Export CSV (UTF8)']='csv.php?_encoding=utf-8&_view='.$this->view
                                           .'&_pagelen='.$this->count
                                           .'&'.$this->setGetVars(array('action'))
                                           ."&".$this->usortobjs->asGET();
          $vs[HOTWIREMENU.'/99_Export CSV (ISO-8859-1)']='csv.php?_encoding=ISO-8859-1&_view='.$this->view
                                           .'&_pagelen='.$this->count
                                           .'&'.$this->setGetVars(array('action'))
                                           ."&".$this->usortobjs->asGET();
        }
        // If we also have insert or update rights, we might want to import
        if ((array_key_exists('insert',$this->perms) 
          || array_key_exists('update',$this->perms))
         && ($this->perms['insert'] 
          || $this->perms['update'])) {
          $vs[HOTWIREMENU.'/99_Import CSV']='csvin.php?_view='.$this->view;
        }
      }
    } // if defined(HOTWIREMENU)
    $this->hwDebugPriStop(__METHOD__);
    return $vs;
  }


 /**
  * @brief Display the name of this view
  * @date 2011-07-11
  * @version 11
  */
  private function navbarViewName() {
    $this->hwDebugPriStart(__METHOD__);
    $result="<div class='pageTitle' id='hw_pagetitle'>No current view</div>\n";
    if (property_exists($this,'view') && $this->view) {
      $view=$this->view;
      #if (substr($view,-1)=='*') {
      #}
      $result="<div class='pageTitle' id='hw_pagetitle'>".
              "<a href='list.php?".$this->setGetVars(array('view'))."'>".
	      #preg_replace('/^\d+ /','',$this->hdrFormat($view))."</a>".
              preg_replace('/(\/)\d+_|^\d+_/','$1',$this->view).'</a>'.
'
<img id="dragtableRemover" onclick="dragtable.clearAbsolutelyAll()" title="Remove re-ordering of columns" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABUAAAAOCAYAAADABlfOAAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB9wGHhMkO8gttlMAAAClSURBVDjLnZNBDsMwCAQXWvXU/z8xL8i1poeUCDA4pCv5YAFjsNdAkACChgQgzRfgaWNcFFyC6YDJiuOY4guchu9Q0GjgTFqBQ2wCcwDa8ShexQBY9xrP6jkDVmACPr/9YzEpqHsn5iDVDuBdNPM3tBQXeaTLAF/mADIrq51eXoKxnbkTVwgurDVZaoTf0rFUZmztbhuLn3LH/OnIDXArkdFUBv0CIztstj1RTBIAAAAASUVORK5CYII="/>'.
$this->navbarBookmark().
              "</div>\n";
    } 
    // Append a debug icon to this if the pref is set
    // This icon sets a cookie via javascript when clicked.
    if ($this->hasRole($this->pref('DBDEV'))) {
      $result.="
<div class='debug-holder'>
 <div class='enable-debug-holder'>
  <span class='ui-icon ui-icon-wrench enable-debug'></span>
 </div>
 <div class='disable-debug-holder'>
  <span class='ui-icon ui-icon-wrench disable-debug'></span>
 </div>
</div>";
    }
    $this->hwDebugPriStop(__METHOD__);
    return $result;
  }

 /**
  * @brief Figures out what table this view looks at. 
  * Use the first table.id field to identify table
  * @return Name of table
  * @date 2012-12-06
  * @version 12
  * Don't use hwDebug methods: exec time<1ms.
  */
  protected function getTable() {
    //$this->hwDebugProStart(__METHOD__);
    if (! property_exists($this,'view')) { return ; } ;
    if (! $this->view) {return; };
    if (array_key_exists($this->view,$this->views)) {
     $this->entity=$this->views[$this->view]['entity'];
    }
    //$this->hwDebugProStop(__METHOD__);
  }

 /**
  * @brief Which other views use this table?
  * @date 2011-07-11
  * @return Array of views, or NULL
  * @TODO: Run over the list of views with similar entity fields
  */
  protected function DEDgetViewFromTable () {
    $this->hwDebugProStart(__METHOD__);
    $sql = "SELECT view FROM _view_data WHERE definition LIKE " .
           "'SELECT $this->entity.id%' AND view LIKE '%basic%'";
    $dbQuery = pg_query($this->dbh, $sql);
    $this->hwDebugProStop(__METHOD__);
    return pg_fetch_result($dbQuery, 'view');
  }

 /**
  * @brief Parses the attributes for the columns in the view.
  * Uses the name of the field to determine many things
  * @todo Could use a spruce-up
  * @date 2011-07-11
  * @version 11
  */
  protected function getColAttrs() {
    $this->hwDebugProStart(__METHOD__);
    if (! property_exists($this,'view')) {return ; }
    if ($this->view == '') { return;};
    $sql = "SELECT * FROM ".$this->hwschema."._column_data WHERE relname='$this->view'";
    $dbQuery = pg_query($this->dbh, $sql);
    if (!$dbQuery) {$this->sqlError('looking up column information for '.$this->view,$sql); }
    while ($row = pg_fetch_assoc($dbQuery)) {
# TODO: This block should be a private method to add a column based on the attrs.
#       Subsequently, a new method to add dbColumn objects based on passed data should be
#       created to wrap it, thus allowing us to simulate creation from the DB for help and testing
      $obj=new dbColumn($row,$this);
      $this->colList[]=$obj->name();
      $this->colobjs[]=$obj;
      $this->colAttrs[$row['attname']]['type'] = $row['typname'];
      # Look for arrays
      if ((array_key_exists('array',$row)?$row['array']:NULL)=='t') {
        $this->colAttrs[$row['attname']]['type'] = $row['elementtype'];
        $this->colAttrs[$row['attname']]['array']=TRUE;
        # RFL has implicitly assumed that all fields of type 'array' are 
        # many-many fields.
        # Perhaps this ought only to work if field is _id
        $this->colAttrs[$row['attname']]['listsql']=
                                               $this->getMMSQL($row['attname']); 
      }
      $this->colAttrs[$row['attname']]['size'] = $row['atttypmod'];
      $this->colAttrs[$row['attname']]['notnull'] = $row['attnotnull'];
      $this->colAttrs[$row['attname']]['hasdefault'] = $row['atthasdef'];
      $this->colAttrs[$row['attname']]=
                                     array_merge($this->colAttrs[$row['attname']],
                                        $this->flagsfromcolname($row['attname']));
      if (is_array($row['attname']) 
       && array_key_exists('htmlview',$row['attname']) 
       && $this->colAttrs[$row['attname']]['htmlview']) { 
        $this->needstinymce = TRUE ; 
      }
       // We use $this->post rather than $_REQUEST, which munges spaces to underscores.
      if ($this->propertyEqual('action','search')) {
        // Display everything unless hde_FOO is on.
        if (array_key_exists('hde_'.$row['attname'],$this->post)) {
          $this->colAttrs[$row['attname']]['nodisplay'] = TRUE;
        }
        if (preg_grep('/xcl_' . $row['attname'] . '/', array_keys($this->post))) {
          $this->colAttrs[$row['attname']]['exclude'] = TRUE;
        }
        if (preg_grep('/xct_' . $row['attname'] . '/', array_keys($this->post))) {
          $this->colAttrs[$row['attname']]['exact'] = TRUE;
        }
      }
    }
    $this->hwDebugProStop(__METHOD__);
  }

 /**
  * @brief Get SQL to insert into query for many-many join tables
  * @todo Does this relate to the current MM code or the obsolete version?
  * @date 2011-07-11
  * @version 12
  * @todo replace str_replace with something bound to the beginning of the string
  */
  public function getMMSQL($field) {
    $this->hwDebugPubStart(__METHOD__,array($field));
   # array(select room_hid from room_hid where array[room_hid.room_id] 
   #  <@ room_tinkering_view.room_id) as room_hid
   $hid_table=str_replace('_id','_hid',$field);
   $hid_table=str_replace('ro_','',$hid_table);
   $hid_field=str_replace('_id','_hid',$field);
   $id_field=str_replace('ro_','',$field);
   $this_table=$this->view;
   $sql="array_to_string(array(select \"$hid_table\" from ".$this->hwschema.".\"$hid_table\" where ".
        " \"$hid_table\".\"$id_field\" = any(\"$this_table\".\"$field\"::bigint[])),', ') as \"$hid_field\" ";
   $this->hwDebugPriStop(__METHOD__);
   return $sql;
  }

 /**
  * @brief Get foreign keys for a table
  * @param[in] table Name of table
  * @return List of column name, table name, column_name for the constraint
  * @date 2011-07-11
  * @version 11
  */
  public function getFKs($table) {
   $this->hwDebugPubStart(__METHOD__,$table);
    $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__,
                        array($field));
   $sql="select k.column_name as mm_column, u.table_name, u.column_name ".
        "from information_schema.key_column_usage k ".
        "inner join information_schema.table_constraints c ".
        "using (constraint_name) ".
	"inner join information_schema.constraint_column_usage u ".
        "using (constraint_name) ".
        "where c.constraint_type='FOREIGN KEY' AND k.table_name='".'$1'."'";
   $result=pg_execute($sql,array(pg_escape_string($table)));
   $this->hwDebugPubStop(__METHOD__);
   return pg_fetch_all($result);
  }
 
 /**
  * @brief Match the column name against the flags
  * @param[in] colname Name of the field we're matching
  * @return Array of flags for the column
  * @date 2012-11-06
  * @version 12
  * Don't use hwDebug functions on this: runs in <1 ms.
  * Other fish are more fryable.
  */
  protected function flagsfromcolname($colname) {
    //$this->hwDebugProStart(__METHOD__,$colname);
    $flags=array();
    if (preg_match('/^(_)?(hl_)?(mm_)?(ro_)?(so_)?(x_)?(.+?)(_id)?(_html)?$/', 
                   $colname, $match)) {
      if (array_key_exists(1,$match) && $match[1]) { $flags['hidden']=TRUE; }
      if (array_key_exists(2,$match) && $match[2]) { $flags['highlight']=TRUE; }
      if (array_key_exists(3,$match) 
       && array_key_exists(8,$match) 
       && $match[3] && $match[8]) { $flags['mm']=TRUE ; }
      if (array_key_exists(4,$match) && $match[4]) { $flags['readonly'] = TRUE; }
      //RFL trying to speed up things by removing sort.
      //if ($match[5]) { $flags['sort'] = TRUE; }
      if (array_key_exists(8,$match) && array_key_exists(7,$match) && $match[8]) {
        $flags['ent_id'] = $match[7] . '_id';
        $flags['ent_hid'] = $match[7] . '_hid';
      }
      // If we get _html, we should provide the means to edit it
      // Perhaps all flags should be set thus:?
      $flags['htmlview']=(array_key_exists(9,$match) && $match[9]);
    }
    //$this->hwDebugProStop(__METHOD__);
    return $flags;
  }

 /**
  * This used to collate table rows when we represented many-many relations
  * by returning multiple rows with the same 'id' column value. We now provide
  * many-many values as array columns, so the collation functionality isn't
  * here any more, but the code still requires this function's rearrangement
  * of the data in $result
  */
  protected function collateRows($result) {

    $data = array();
    while ($row = pg_fetch_assoc($result)) {
      foreach (array_keys($row) as $col) {
          $data[$row['id']][$col] = $row[$col];
      }
    }
    return $data;
  }

  /**
   * @brief Produces the HTML form fields required for this search 
   * @return HTML representation of the search variables
   * @date 2011-07-11
   * @version 11
   */
  public function setSearchVars() {
$this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
    $html = '';
    foreach (array_values(preg_grep('/^id$|^_|_hid$/',
                          array_keys($this->colAttrs),
                          PREG_GREP_INVERT)) as $key) {
      if (array_key_exists('nodisplay',$this->colAttrs[$key]) 
       && $this->colAttrs[$key]['nodisplay']) {
        $html .= "<input type=hidden name='hde_$key' value=on>";
      }
      if (array_key_exists('exact',$this->colAttrs[$key]) 
       && $this->colAttrs[$key]['exact']) {
        $html .= "<input type=hidden name='xct_$key' value=on>";
      }
      if (array_key_exists('exclude',$this->colAttrs[$key]) 
       && $this->colAttrs[$key]['exclude']) {
        $html .= "<input type=hidden name='xcl_$key' value=on>";
      }
    }
    return "<!-- setSearchVars -->".$html."<!-- end setSearchVars -->";
  }

  /** 
   * @brief Encodes the posted informtion as a URL string
   * @returns URL-encoded representation of the search variables
   * @todo Recode this using array representation.
   * @date 2011-07-11
   * @version 11
   */
  public function getSearchVars() {
$this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
    $get='';
    foreach (array_values(preg_grep('/^id$|^_|_hid$/',
                          array_keys($this->colAttrs),
                          PREG_GREP_INVERT)) as $key) {
      if (array_key_exists('nodisplay',$this->colAttrs[$key]) 
       && $this->colAttrs[$key]['nodisplay']) {
        $get.="hde_".$key."=on&";
      }
      if (array_key_exists('exact',$this->colAttrs[$key]) 
       && $this->colAttrs[$key]['exact']) {
        $get.="xct_".$key."=on&";
      }
      if (array_key_exists('exclude',$this->colAttrs[$key]) 
       && $this->colAttrs[$key]['exclude']) {
        $get.="xcl_".$key."=on&";
      }
    }
    return rtrim($get,"&");
  }

 /**
  * @brief Produce a URL encoding of the sort settings
  * @date 2011-07-19
  * @version 1
  **/
  public function getSort($sort=NULL) {
$this->hotwireDebug(HW_DBG_OPRO,'Called [protected] '.get_class($this).'->'.__METHOD__,
                    array($sort));

    if ($sort===NULL) {$sort=$this->sort;}
    $result='';
    foreach ($sort as $s) {
      $result.='_sort[]='.urlencode($s).'&';
    }
    return rtrim($result,'&');
  }

 /**
  * @brief Provides relevant information for the HEAD of the page.
  * Gets some information into the HEAD of the page
  * Create an instance of each type and call their prepare methods
  * @date 2011-07-25
  * @version 12
  * @return Links to include in our HEAD element
  */
  public function prepareHead() {
    $this->hwDebugPubStart(__METHOD__);
    $headinfo=array('<!-- Head info from '.__FILE__.' -->');
    $headjs=array();
    $headcss=array();
    if (is_a($this,'dbRecord')) {
      foreach (array_keys($this->colobjs) as $k) {
        $this->colobjs[$k]->prepareRecord($headjs,$headcss);
      }
    } else {
      foreach (array_keys($this->colobjs) as $k) {
        $this->colobjs[$k]->prepareList($headjs,$headcss);
      }
    }
     
    if ($this->arrayKey($this->prefs,'RESIZECOLS')) {
      $headjs[]='js/resizable-tables'; 
    }
    
    // Optionally provide floating table header
    if ($this->pref('FLOATHEAD')) {
      //$headjs[]='js/jquery.floatThead'; 
    }

    if ($this->pref('COLREORDER')) {
      $headjs[]='js/dragtable';
    }
    // And this
    // If there's a custom CSS for our class type, use that
    if (! $this->arrayKey($this->prefs,'CSSPATH')) {
      $headinfo[]="<!-- Could set a preference 'CSSPATH' to have additional CSS for this view; separate paths in here with ".DIRECTORY_SEPARATOR." -->";
    } else {
      $usercss=$this->prefs['CSSPATH'];
      $headinfo=array_merge($headinfo,$this->extracss($usercss));
    }

    $headjs=array_unique($headjs);
    $headcss=array_unique($headcss);
    // Add the head JS and head CSS
    if (array_key_exists('javascript',$_SESSION) && $_SESSION['javascript']) {
     foreach ($headjs as $js) {
       $headinfo[]='<!-- Making headinfo for '.$js.' -->';
       $headinfo[]=$this->makejs($js);
     }
     if ($this->arrayKey($this->prefs,'JSPATH')) {
       $userjs=$this->prefs['JSPATH'];
       $headinfo=array_merge($headinfo,$this->extrajs($userjs));
     } else {
       $headinfo[]="<!-- Could set a preference 'HW_PREF_JSPATH' to have additional JS for this view -->";
     }
    }
    foreach ($headcss as $css) {
      $headinfo[]=$this->makecss($css);
    }

    $this->hwDebugPubStop(__METHOD__);
    return array_unique($headinfo);
  }

 /**
  * @brief Provides additional CSS relevant to this view
  * @date 2012-06-25, 2018-07-17
  * @author RFL
  * @version 2
  * @return Links to include in our HEAD element
  */
  private function extracss($usercsspath) {
    $this->hwDebugPriStart(__METHOD__);
    $headinfo=array();
    // Optionally provide additional CSS info
    $csspath=implode(DIRECTORY_SEPARATOR,array(dirname(realpath($_SERVER['SCRIPT_FILENAME'])),'..','local','www',''));
    # usercsspath might be multiply-valued: split on PATH_SEPARATOR
    $usercsspaths=explode(PATH_SEPARATOR,$usercsspath);
    # Look for a plain 'style.css' in the system css path
    if (file_exists(dirname(realpath($_SERVER['SCRIPT_FILENAME'])).'/css/style.css')) {
      $headinfo[]='<!-- (0) -->'.$this->makecss('css/style');
    } else {
      $headinfo[]='<!-- (0) Could create '.dirname(realpath($_SERVER['SCRIPT_FILENAME'])).'/css/style.css for CSS on all pages -->';
    }
    # Look for a plain 'style.css' in the user-css path
    foreach ($usercsspaths as $usercss) {
      # The canonical location is dirname(realpath($_SERVER['SCRIPT_FILENAME']))/../local/$usercsspath
      $usercsspath=$csspath.$usercss.DIRECTORY_SEPARATOR;
      if (($usercss) && file_exists($usercsspath.'style.css')) {
        $headinfo[]="<!-- A -->".$this->makecss('local'.DIRECTORY_SEPARATOR.$usercss.DIRECTORY_SEPARATOR.'style');
      } else {
        $headinfo[]="<!-- (a) Could create ".realpath($usercsspath).DIRECTORY_SEPARATOR."style.css for CSS on all pages -->";
      }
    }
    # Look for css associated with the object types
    foreach ($usercsspaths as $usercss) {
      $usercsspath=$csspath.$usercss.DIRECTORY_SEPARATOR;
      // Perhaps we use a custom CSS based on our class
      if (($usercss) && file_exists($usercsspath.get_class($this).'.css')) {
        $headinfo[]=$this->makecss('css'.DIRECTORY_SEPARATOR.$usercss.DIRECTORY_SEPARATOR.get_class($this));
      } else {
        $headinfo[]="<!-- (b) Could create ".realpath($usercsspath).DIRECTORY_SEPARATOR.get_class($this).".css for CSS on all such objects-->";
      }
    }
    // If we have no view, go no further.
    if (!property_exists($this,'view')) {
      return $headinfo;
    }
    if (!$this->view) {
      return $headinfo;
    }
    $headinfo[]="<!-- View ".$this->view." -->\n";
    $dirs=explode('/',str_replace('//','/',$this->view));
    $headinfo[]="<!-- Dirs : ".join(', ',$dirs)."  -->\n";
    # Look for css associated with the path to the views
    for ($i=1;$i<=count($dirs);$i++) {
      $path=join(DIRECTORY_SEPARATOR,array_slice($dirs,0,$i));
      foreach ($usercsspaths as $usercss) {
        $usercsspath=$csspath.$usercss.DIRECTORY_SEPARATOR;
        if (file_exists($usercsspath.$path.'.css')) {
          $headinfo[]=$this->makecss('local'.DIRECTORY_SEPARATOR.$usercss.DIRECTORY_SEPARATOR.$path);
        } else {
          $headinfo[]="<!-- (c) Could create ".realpath($usercsspath).DIRECTORY_SEPARATOR.$path.".css for extra CSS -->";
        }
      }
    }
    $this->hwDebugPriStop(__METHOD__);
    #return array_unique($headinfo);
    return $headinfo;
  }

 /**
  * @brief Provides additional JS relevant to this view
  * @date 2012-06-25, 2018-07-17
  * @version 2
  * @return Links to include in our HEAD element
  */
  private function extrajs($userjspath) {
    $this->hwDebugPriStart(__METHOD__,array($userjspath));
    $headinfo=array();
    // Optionally provide additional JS info
    $jspath=implode(DIRECTORY_SEPARATOR,array(dirname(realpath($_SERVER['SCRIPT_FILENAME'])),'..','local','www',''));
    # userjspath might be multi-valued
    $userjspaths=explode(PATH_SEPARATOR,$userjspath);
    # look for a plain 'javascript.js'
    foreach ($userjspaths as $userjs) {
      $userjspath=$jspath.$userjs.DIRECTORY_SEPARATOR;
      if (($userjs) && file_exists($userjspath.'javascript.js')) {
        $headinfo[]=$this->makejs('local'.DIRECTORY_SEPARATOR.$userjs.DIRECTORY_SEPARATOR.'javascript');
      } else {
        $headinfo[]='<!-- Could create '.realpath($userjspath).DIRECTORY_SEPARATOR.'javascript.js for JS on all pages -->';
      }
    }
    # Look for js associated with the objects
    foreach ($userjspaths as $userjs) {
      $userjspath=$jspath.$userjs.DIRECTORY_SEPARATOR;
      // Perhaps we use a custom CSS based on our class
      if (($userjs) && file_exists($userjspath.get_class($this).'.js')) {
        $headinfo[]=$this->makejs('local'.DIRECTORY_SEPARATOR.$userjs.DIRECTORY_SEPARATOR.get_class($this));
      } else {
        $headinfo[]="<!-- Could create ".realpath($userjspath).DIRECTORY_SEPARATOR.get_class($this).".js for JS on all such objects-->";
      }
    }
    // If we have no view, go no further.
    if (!property_exists($this,'view')) {
      return $headinfo;
    }
    if (!$this->view) {
      return $headinfo;
    }

    $headinfo[]="<!-- View ".$this->view." -->\n";
    $dirs=explode('/',str_replace('//','/',$this->view));
    $headinfo[]="<!-- Dirs : ".join(', ',$dirs)."  -->\n";
    for ($i=1;$i<=count($dirs);$i++) {
      $path=join(DIRECTORY_SEPARATOR,array_slice($dirs,0,$i));
      foreach ($userjspaths as $userjs) {
        $userjspath=$jspath.$userjs.DIRECTORY_SEPARATOR;
        if (file_exists($userjspath.$path.'.js')) {
          $headinfo[]=$this->makejs('local'.DIRECTORY_SEPARATOR.$userjs.DIRECTORY_SEPARATOR.$path);
        } else {
          $headinfo[]="<!-- Could create ".realpath($userjspath).DIRECTORY_SEPARATOR.$path.".js for extra JS -->";
        }
      }
    }
    $this->hwDebugPriStop(__METHOD__);
    #return array_unique($headinfo);
    return $headinfo;
  }


 /**
  * @brief Record the query for posterity. In case posterity should want it.
  * @date 2011-07-11
  * @version 11
  */
  protected function logQuery($data) {
   $this->hwDebugProStart(__METHOD__,array($data));
    if (!is_writable($GLOBALS['QUERY_LOG'])) {
      $this->echoDebug($GLOBALS['QUERY_LOG'].' cannot be written.');
    }
    if ($fh = fopen($GLOBALS['QUERY_LOG'], 'a')) {
      $output = $_SESSION['dbuser'] . "\t" . date('d/m/Y H:i') . "\t" . 
                $this->msg . "\t" . $data . "\n";
      fwrite($fh, $output);
      fclose($fh);
    }
   $this->hwDebugProStop(__METHOD__);
  }

  /**
   * result = dbAjaxResult object
   **/
  public function ajaxinsert(&$req,&$result) {
    if (!array_key_exists('newrecord',$req)) {
      $result->fail('Need a newrecord for inserts');
      return;
    }
    $result->succeed('Stub insert');
    
  }

  /**
   * result = dbAjaxResult object
   **/
  public function ajaxdelete(&$req,&$result) {
    if (!array_key_exists('id',$req)) {
      $result->fail('Need an id for delete');
      return;
    }
    $result->succeed('Stub delete');

  }

  /**
   * result = dbAjaxResult object
   **/
  public function ajaxupdate(&$req,&$result) {
    if (!array_key_exists('id',$req)) {
      $result->fail('Need an id for update');
      return;
    }
    if (!array_key_exists('newrecord',$req)) {
      $result->fail('Need a newrecord for update');
      return;
    }
    $result->succeed('Stub update');

  }

  /**
   * result = dbAjaxResult object
   **/
  public function ajaxselect(&$obj) {
    $req=$obj->currentreq();
    if (!array_key_exists('id',$req)) {
      $obj->currentfail('Need an id for select');
      return;
    }
    # Do we have a list of fields to retrieve?
    if (!array_key_exists('fields',$req)) {
      $obj->currentfail('Need a list of fields');
      return;
    }
    # Need to return some data. Do we have some data yet?
#print "<pre>\n";
#print_r($this);
#print "</pre>\n";
    $obj->currentsucceed('Stub select');

  }

  /**
   * result = dbAjaxResult object
   **/
  public function ajaxsearch(&$req,&$result) {
    if (!array_key_exists('search',$req)) {
      $result->fail('Need a search for searching');
      return;
    }
    $result->succeed('Stub search');

  }

  

}

?>
