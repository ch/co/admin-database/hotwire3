<?php
 /**
  * @brief A class with methods to handle sorting
  * @author Frank Lee
  * @date 2012-11-12
  * @version 1
  **/
class dbSort extends hwFunctions {
  private $fieldname; //<@brief Name of field
  private $direction; //<@brief Direction 
  private $nulls=NULL;     //<@brief NULLS LAST

  /**
   * @brief To construct
   * Call with $arg=fieldname (defaults ASC, null handling from prefs)
   * _or_ $arg='field [ASC|DESC] [NULLS FIRST|NULLS LAST]'
   */
  public function __construct($args=NULL) {
    if (is_array($args)) {
      $this->fieldname=(array_key_exists('col',$args)?$args['col']:NULL);
      $this->direction=(array_key_exists('dir',$args)?$args['dir']:NULL);
      $this->nulls=(array_key_exists('nul',$args)?$args['nul']:NULL);
    } else {
      $matches=array();
      $pattern='/^"?([\w\.\"]+)"?\s*([ADESC]*)?\s*(.*)?/i';
      if (preg_match($pattern, trim($args), $matches)) {
        $a=explode('.',$matches[1]);
        $this->fieldname=trim(array_pop($a),'"');
        $this->direction=!(strtoupper($matches[2])=='DESC');
        if ($matches[3]) {
          $n=strtoupper($matches[3]);
          if (strpos('NULLS',$n)) {
            $this->nulls=(strpos('LAST',$n)>0);
          }
        }
      }
    }
   $this->dbConnect();
  }

  public function asGET() {
    $html='_sort[]='.urlencode(trim($this->fieldname))
         .'&_order[]='.urlencode(trim($this->directionAsSQL().$this->nullsAsSQL()));
    return $html;
  }

  public function nextAsGet() {
    $nd=$this->nextDirectionAsSQL();
    if ($nd) {
    $html='_sort[]='.urlencode(trim($this->fieldname))
         .'&_order[]='.urlencode(trim($nd.$this->nullsAsSQL()));
     return $html;
    }
  }

  public function asPOST() {
    $html='<input type="hidden" name="_sort[]" value="'.htmlentities(trim($this->fieldname)).'"/>'
         .'<input type="hidden" name="_order[]" value="'.htmlentities(trim($this->directionAsSQL()
                                                                          .$this->nullsAsSQL())).'"/>';
    return $html;
  }

  public function asSQL() {
   if ($this->direction===NULL) { return; }
   if (substr($this->fieldname,-3)==='_id') {
     $sql='"'.substr($this->fieldname,0,-3).'_hid"'.
          $this->directionAsSQL().
          $this->nullsAsSQL();
   } else {
     $sql=$this->fieldnameAsSQL().
          $this->directionAsSQL().
          $this->nullsAsSQL();
   }
   return rtrim($sql);
  }

  public function fieldnameAsSQL() {
   if (function_exists('pg_escape_identifier')) {
    return pg_escape_identifier($this->dbh,$this->fieldname).' ';
   } else {
    return '"'.$this->fieldname.'" '; // poor-man's escaping.
   }
  }

  public function nullsAsSQL() {
   // If we have no ordering, return null
   if ($this->direction===NULL) { return;}
   if ($this->nulls===NULL) { return ;}
   if ($this->nulls) {
     return ($this->direction?'NULLS FIRST':'NULLS LAST');
   } else {
     return ($this->direction?'NULLS LAST':'NULLS FIRST');
   }
  }

  public function directionAsSQL() {
   switch (true) {
    case  ($this->direction===true):
      return 'ASC ';
      break;
    case ($this->direction===false):
      return 'DESC ';
      break;
   }
  }

  public function directionAsArrow() {
   switch (true) {
    case  ($this->direction===true):
      return '&uArr;';
      break;
    case ($this->direction===false):
      return '&dArr;';
      break;
   }
  }

  public function nextDirectionAsSQL() {
   switch (true) {
    case ($this->direction===true):
      return 'DESC ';
      break;
    case ($this->direction===false):
      return '';
      break;
    case ($this->direction===NULL):
      return 'ASC ';
      break;
   }
  }

  public function fieldname() {
    return $this->fieldname;
  }

  public function fieldForJS() {
    $return=array();
    $return[]='<input name="_sort[]" class="sort" value="'.$this->fieldname.'" type="text" size="3"/>';
    $return[]='<input name="_order[]" class="order" value="'.($this->direction===false?'Desc':'Asc').'" type="text" size="3"/>';
    $return[]='<span class="hw_sort_field">'.$this->hdrFormat($this->fieldname).'</span>'.
               '<span class="hw_sort_order">'.(($this->direction===false)?'Descending':'Ascending').'</span>';
    return join("\n",$return);
  }

}
?>
