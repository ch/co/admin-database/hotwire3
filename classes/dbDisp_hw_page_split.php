<?php
 /**
  * @brief A class with methods to display a numeric field.
  * Actually just inherits everything...
  * @author Frank Lee
  * @date 2011-07-11
  * @version 11
  **/
class dbDisp_hw_page_split extends dbDispBaseWide {

 /**
  * @brief To display a div to split up the page a bit.
  * @date 2018-05-31
  * @version 1
  **/
 public function displayEdit() {
  // Don't display fields named _FOO by convention
  if (substr($this->dbColumn->name(),0,1)=='_') { return;}
  return '<div id="'.$this->domName($this->dbColumn->name()).'" class="_hw_page_split">'.
         htmlspecialchars($this->getval()?$this->getval():$this->dbColumn->name()).
         '</div>';
 }

 public function displaySearch() {
   return $this->displayEdit();
 }

 public function preDisplaySearch(&$ui) {
   $this->preDisplayEdit($ui);
 }

 public function preDisplayEdit(&$ui) {
  $ui[]='</div><!-- Exit itemdata -->';
  $ui[]='<div class="wideitemdata">';
 }

 public function postDisplaySearch(&$ui) {
   $this->postDisplayEdit($ui);
 }

 public function postDisplayEdit(&$ui) {
  $ui[]='</div> <!-- class=wideitemdata -->';
  $ui[]='<div class="itemdata">';
 }

 protected function displaySearchClearWidthStart(&$ui) {}
 protected function displaySearchClearWidthEnd(&$ui) {}
 protected function displaySearchKeyStart(&$ui) {}
 protected function displaySearchHideStart(&$ui) {}
 protected function displaySearchHideBox(&$ui) {}
 protected function displaySearchHideEnd(&$ui) {}
 protected function displaySearchDisplay(&$ui) {}
 protected function displaySearchKeyEnd(&$ui) {}
 protected function displaySearchExact(&$ui) {}
 protected function displaySearchExcludeStart(&$ui) {}
 protected function displaySearchExcludeBox(&$ui) {}
 protected function displaySearchExcludeEnd(&$ui) {}

 /** 
  * @brief We don't display cells of this type
  * @author Frank Lee
  * @date 2018-05-31
  * @version 1
  **/
 public function viewcell(&$row) {
 }

}
?>
