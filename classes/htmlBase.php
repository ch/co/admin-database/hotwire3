<?php
 // $Id$
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

 /** 
  * @file htmlBase.php
  * @brief To provide a base class to produce HTML
  * Part of Hotwire http://hotwire.sourceforge.net/.
  * @author Stuart Taylor
  * @author Frank Lee
  */

 /**
  * @class htmlBase
  * @author Stuart Taylor
  * @author Frank Lee
  * @brief Displays HTML tables nicely
  */
class htmlBase extends hwFunctions {
  private $buffered=0; // <@brief Retain debug info before headers are sent
  private $callstack=array();  // <@brief keep track of when we call routines
  private $js=array();    // The javascript files we load by default

 /**
  * @brief Displays some debug information depending on global debug flags
  * @date 2011-07-15
  * @auth Frank Lee
  * @todo Needs to be replaced with the new _debug methods
  * @version 2
  **/
  public function hotwireDebug($level,$head,$body=NULL) {
    // Don't display debugging information through the image.php script
    $a=explode('/',$_SERVER["SCRIPT_FILENAME"]);
    $phpfile=array_pop($a);
    if ($phpfile=='image.php') { return ; } 
    $objtype=get_class($this);
    if (array_key_exists('debugobj',$GLOBALS)
     && is_array($GLOBALS['debugobj'])
     && array_key_exists('debugval',$GLOBALS)
     && is_array($GLOBALS['debugval'])
     && array_search($objtype,$GLOBALS['debugobj'])!==false 
     && array_search($level,$GLOBALS['debugval'])!==false) {
          $this->displayDebug($body,$head,$level);
    }
  }


 /**
  * @brief Do we have a JS enabled/activated browser? 
  * @todo Should be a property of htmlBase, really.
  **/
  protected function ifJS($text) {
   return ($_SESSION['javascript']?$text:'');
  }

 /**
  * @brief Record the creation of an object
  * @author Frank Lee
  * @date 2012-11-24
  * @version 1
  **/
  public function hwDebugCreateStart($args=NULL) {
    $method='__construct';
    $this->setcalltime($method);
    $this->hwDebugGenStart($method,HW_DBG_OCRE,'creation',$args);
  }

 /**
  * @brief Record the time when a method was called on a stack
  * @author Frank Lee
  * @date 2012-11-24
  * @version 1
  * @param[in] method: Name of the method being called.
  **/
  public function setcalltime($method) {
    if (!( array_key_exists($method,$this->callstack) && is_array($this->callstack[$method]))) {
      $this->callstack[$method]=array();
    }
    $this->callstack[$method][]=microtime(true);
  }

 /**
  * @brief Record & display calling a Protected method
  * @author Frank Lee 
  * @date 2012-11-24
  * @version 1
  * @param[in] method: Name of the method to debug
  * @param[in] args:
  **/
  public function hwDebugProStart($method,$args=NULL) {
    $this->setcalltime($method);
    $this->hwDebugGenStart($method,HW_DBG_OPRO,'protected',$args);
  }

 /**
  * @brief Record & display calling a Private method
  * @author Frank Lee
  * @date 2012-11-24
  * @version 1
  * @param[in] method: Name of the method to debug
  * @param[in] args: 
  **/
  public function hwDebugPriStart($method,$args=NULL) {
    $this->setcalltime($method);
    $this->hwDebugGenStart($method,HW_DBG_OPRI,'private',$args);
  }

 /**
  * @brief Record & display calling a Public method
  * @author Frank Lee
  * @date 2012-11-24
  * @version 1
  * @param[in] method: Name of the method to debug
  * @param[in] args: 
  **/
  public function hwDebugPubStart($method,$args=NULL) {
    $this->setcalltime($method);
    $this->hwDebugGenStart($method,HW_DBG_OPUB,'public',$args);
  }

 /**
  * @brief Display debugging when starting a method
  * @author Frank Lee
  * @date 2012-11-24
  * @version 1
  * @todo Need we have HW_DBG_OPRI and 'private' both supplied?
  * @param[in] method: Name of the method to debug
  * @param[in] level: HW_DBG_OPRI/HW_DBG_OPUB/HW_DBG_OPRO
  * @param[in] label: private    / public    / protected
  **/
  protected function hwDebugGenStart($method,$level,$label,$args=NULL) {
    $a=explode('/',$_SERVER["SCRIPT_FILENAME"]);
    $phpfile=array_pop($a);
    if ($phpfile=='image.php') { return ; }
    $objtype=get_class($this);
    // If we're not debugging calls at this level, quit
    if (!( array_key_exists('debugobj',$GLOBALS)
        && is_array($GLOBALS['debugobj'])
        && array_key_exists('debugval',$GLOBALS)
        && is_array($GLOBALS['debugval'])
        && array_search($objtype,$GLOBALS['debugobj'])!==false 
        && array_search($level,$GLOBALS['debugval'])!==false)) {
      return;
    }
    // Display some useful log space
    print "<div class='debug dbglevel-$level dbgobject-$objtype'><h5>"
          .sprintf("%01.3f",(microtime(true)-$GLOBALS['starttime']))
          .": Called [$label] "
          .$objtype."->".$method."(".$this->fmtargs($args).")</h5></div>\n";
  }

 /**
  * @brief Record ending of a public method
  * @date 2011-07-15
  * @version 2
  * @params[in] method: Name of the method being closed
  * @params[in] result: Data returned by the method (optional)
  **/
  public function hwDebugPubStop($method,$result=NULL) {
    $this->hwDebugGenStop($method,HW_DBG_OPUB,'public',$result);
  }
  public function hwDebugProStop($method,$result=NULL) {
    $this->hwDebugGenStop($method,HW_DBG_OPRO,'protected',$result);
  }
  public function hwDebugPriStop($method,$result=NULL) {
    $this->hwDebugGenStop($method,HW_DBG_OPRI,'private',$result);
  }
  public function hwDebugCreateStop($result=NULL) {
    $this->hwDebugGenStop('__construct',HW_DBG_OCRE,'creation',$result);
  }


 /**
  * @brief Displays some debug information depending on global debug flags
  * @date 2011-07-15
  * @version 2
  * @todo Limit output depending on how deep the call stack is.
  **/
  public function hwDebugGenStop($method,$level,$label,$result=NULL) {
    $a=explode('/',$_SERVER["SCRIPT_FILENAME"]);
    $phpfile=array_pop($a);
    if ($phpfile=='image.php') { return ; }
    // Perhaps we're not debugging this object or method-type?
    $objtype=get_class($this);
    if (!( array_key_exists('debugobj',$GLOBALS)
     && is_array($GLOBALS['debugobj'])
     && array_key_exists('debugval',$GLOBALS)
     && is_array($GLOBALS['debugval'])
     && array_search($objtype,$GLOBALS['debugobj'])!==false 
     && array_search($level,$GLOBALS['debugval'])!==false)) {
      return;
    } 
    $maxarglen=20;
    $starttime=array_pop($this->callstack[$method]);
    // Display some useful log space
    print "<div class='debug dbglevel-$level dbgobject-$objtype'><h5>"
          .sprintf("%01.3f",(microtime(true)-$GLOBALS['starttime']))
          .": Completed [$label] ".$objtype."->".$method." in "
          .sprintf("%01.3f",(microtime(true)-$starttime))."s</h5>\n";
    // Perhaps we display a backtrace?
    if (array_search(HW_DBG_BACK,$GLOBALS['debugval'])!==false) {
      $backtrace=debug_backtrace();
      array_shift($backtrace); # Don't tell about this function
      array_shift($backtrace); 
      print "<ul>\n";
      foreach (array_reverse($backtrace) as $i => $b) {
        print "<li><tt>".$b['class'].$b['type'].$b['function'].'('.$this->fmtargs($b['args']).') '.
               (array_pop(explode('/',$b['file']))).':'.$b['line']."</tt></li>\n";
      }
      print "</ul>\n";
    }
    if (is_array($src)) { 
      print "<pre>\n";
      print_r($src);
      print "</pre>\n";
    } elseif (is_object($src)) {
      print "<pre>\n";
      foreach ($src as $k => $v) {
        if ($k != 'data') {
          echo "\n[$k] => ";
          print_r($v);
        } 
      }
      print "</pre>\n";
    } elseif ($src!==NULL) {
      echo '<textarea cols="100">'.$src.'</textarea>';
    }
    print "</div>\n";
  }

  /* To format an argument list */
  protected function fmtargs($args=NULL) {
    $result=array();
    $maxarglen=20;
    foreach ($args as $arg) {
      $type=gettype($arg);
      if ($type=='object') { $type=get_class($arg); $arg='OBJECT';};
      if (strlen($arg)) {$arg=substr($arg,0,$maxarglen-3).'...';}
      $result[]="'".$arg."'::".$type;
    }
    return join(',',$result);
  }


  protected function hwxxDebugGenStop($method,$level,$result=NULL) {
    $objtype=get_class($this);
    // If we're not debugging calls at this level, quit
    if (!( array_key_exists('debugobj',$GLOBALS)
     && is_array($GLOBALS['debugobj'])
     && array_key_exists('debugval',$GLOBALS)
     && is_array($GLOBALS['debugval'])
     && array_search($objtype,$GLOBALS['debugobj'])!==false 
     && array_search($level,$GLOBALS['debugval'])!==false)) {
      return;
    }
    $backtrace=debug_backtrace();
  }

  /**
   * @brief Just keep track of destruction for debugging
   * @date 2011-07-15
   * @version 1
   * @todo Find out why the constant here isn't defined
   */
  public function __destruct() {
    #$this->hotwireDebug(HW_DBG_OCRE,'Destruction of a '.get_class($this).' object');
  }
 
 /**
  * @brief Keep track of creationg for debugging
  * @date 2011-07-15
  * @version 1
  */
  public function __construct() {
    #$this->hotwireDebug(HW_DBG_OCRE,'Creation of a '.get_class($this).' object');
  }

 /**   
  * @brief Displays some debugging information
  * @date 2011-07-11
  * @version 11
  */  
  public function echoDebug ($src = NULL, $hdr = NULL) {
    if (is_null($src)) {
      $hdr.=' (Automatically set to THIS)';
      $src = $this;
    } 
    if (isset($_SESSION['debug']) && $_SESSION['debug']) {
      // TODO: delete this method and pass everything through the object debugging tool
       // $this->displayDebug($src,$hdr);
    }
  }

 /**
  * @brief Display some debug information
  * @date 2011-07-15
  * @version 1
  **/
  public function displayDebug($src=NULL,$hdr=NULL,$level=NULL) {
    if ((!headers_sent()) && 
        (!array_key_exists('buffered',$GLOBALS))) {
      $GLOBALS['buffered']=1;
      ob_start();
    }
    if (!array_key_exists('toggledebug'.$level,$GLOBALS)) {
      $consts=get_defined_constants();
      $keys=preg_grep('/HW_DBG_/',array_keys($consts));
      $label='Anon'.$level;
      foreach ($keys as $key) {
        if ($consts[$key]==$level) {
          $label=$key;
        }
      }
      print "<span class='level-$level toggledebug'>Debug $label ".
            "<span class='hide'>Hide</span>:<span class='show'>Show</span></span>\n";
      $GLOBALS['toggledebug'.$level]=1;
    }
    print "<div class='debug debug-$level'><h5>".sprintf("%01.3f",(microtime(true)-$GLOBALS['starttime'])).":$hdr</h5>\n<pre>\n";
    if (array_search(HW_DBG_BACK,$GLOBALS['debugval'])!==false) {
     print "<pre>\n";
     debug_print_backtrace();
     print "</pre>\n";
    }
    if (is_array($src)) { 
      print_r($src);
    } elseif (is_object($src)) {
      foreach ($src as $k => $v) {
        if ($k != 'data') {
          echo "\n[$k] => ";
          print_r($v);
        } 
      }
    } elseif ($src!==NULL) {
      echo '<textarea cols="100">'.$src.'</textarea>';
    }
    echo "</pre></div>\n";
  }

 /**
  * @brief Handle some errors - or rather, not.
  * @date 2011-07-11
  * @version 11
  **/
  public function crashandburn($msg) {
   die ("<p class='error'>$msg</p>");
  }

  /**
   * @brief A nice way of checking if a property is equal to a value.
   * @date 2011-07-11
   * @version 11
   */
  public function propertyEqual($prop,$val) {
   return (property_exists($this,$prop) && isset($this->$prop) && $this->$prop == $val);
  }
 
  /**
   * @brief Build a CSS link from the given file
   * @date 2011-07-09
   * @version 11
   **/
  public function makecss($file) {
   # Get the path right first, please
   #$css="local".DIRECTORY_SEPARATOR;
   global $HOTWIRE_VERSION;
   return "<link type='text/css' href='$file.css?version=$HOTWIRE_VERSION' rel='Stylesheet'>";
  }
  
  /**
   * @brief Build a JS link from the given file
   * @date 2011-07-09
   * @version 11
   **/
  public function makejs($file) {
   # Get the path right first, please
   #$js='local'.DIRECTORY_SEPARATOR;
   global $HOTWIRE_VERSION;
   return "<script type='text/javascript' src='$file.js?version=$HOTWIRE_VERSION'></script>";
  }

  /**
   * @brief Display an HTML header 
   * @date 2011-07-15
   * @version 1
   * @todo Move that javascript info hotwire.js!
   * @todo Include userfiles/head.html if it exists
   **/
  public function echoHead() {
   $extraheadinfo=array();
   global $DBTITLE;
   global $HW_DEFAULT_JS_FILES;
   global $HW_DEFAULT_CSS_FILES;
   if (method_exists($this,'prepareHead')) {
    $extraheadinfo=$this->prepareHead();
   }
   // turn off output buffering
   $buffer=ob_get_clean();
   unset($GLOBALS['buffered']);
   ?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
      "http://www.w3.org/TR/html4/loose.dtd">
<html>
 <head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
  <title><?php print $DBTITLE;?></title>
  <!-- This JS is generated dynamically and serves to get some info from PHP into the JS layer -->
  <script type="text/javascript">
//<![CDATA[
 
<?php
if ((!is_null($this->msg)) && (preg_match('/ERROR/',$this->msg))){
  print('var changes = true;');
} else {
  print('var changes = false;');
}
?>

<?php print 'window.hw_prefs='.json_encode($this->prefs).";\n";?>
//]]>
  </script>

<?php
 global $HOTWIRE_VERSION;
 print "\n<!-- CSS files defined in HW_DEFAULT_CSS_FILES -->\n";
 foreach ($HW_DEFAULT_CSS_FILES as $css) {
  printf("<link type='text/css' href='%s?version=$HOTWIRE_VERSION' rel='Stylesheet'>\n",$css);
 }
 print "\n<!-- JS files defined in HW_DEFAULT_JS_FILES -->\n";
 foreach ($HW_DEFAULT_JS_FILES as $js) {
  printf("<script type='text/javascript' src='%s?version=$HOTWIRE_VERSION'></script>\n",$js);
 }
?>
  <!-- Dynamically loaded options -->
<?php 
print join("\n",$extraheadinfo)."\n";
?> 
 </head>
 <body>
 <!-- echoHead() end -->
  <?php
/*
  <div id='hw_prefs' style='display: none'><?php print json_encode($this->prefs).";";?></div>
*/
  print $buffer;
  }

/**
 * @brief Display the top of the page.
 * @todo I guess we ought to include this from a PHP/HTML file
 * so that new sites can configure the UI a bit.
 * @date 2011-07-15
 * @version 1
 **/
public function echoTop() {
 $file='../local/www/top.html';
 global $DBTITLE;
 ?>
 <!-- echoTop() begin -->
 <div class='hw_banners'>
  <div class='bannerleft'>
 <?php if (file_exists($file)) {readfile($file);}?>
  </div>
  <div class='bannertitle'>
  
   <a href='index.php'><?php echo $DBTITLE;?></a>
  </div>
  <div class='bannerright'>
   <?php 
    if (isset($_SESSION['dbuser']) && $_SESSION['dbuser']) {
     echo "User: <span id='_debug_dbuser'>".$_SESSION['dbuser']."</span> <a href='logout.php'>Logout</a>";
    }
   ?>
  </div>
 </div><!-- banners -->
 
 <div class='clearwidth'></div>
 <!-- echoTop() end -->
 <?php
}

/**
 * @brief Display the bottom of the page.
 * @date 2012-11-15
 * @version 2
 **/
public function echoBottom() {
 $file='../local/www/bottom.html';
 if (file_exists($file)) {
  readfile($file);
 }
?>
 </body>
 </html>
 <?php
}

}

?>
