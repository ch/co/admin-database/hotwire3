<?php 
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

 /** 
  * @file dbMetaList.php
  * @brief Displays a view as a list, or a meta-view as a list of lists
  * Part of Hotwire http://hotwire.sourceforge.net/. 
  */

  /** 
   * @class dbMetaList
   * @author Frank Lee
   * @date 2011-07-15
   * @brief Displays a view (or set of views) in list format
   */
class dbMetaList extends dbView {

  public $data = array();  //!<@brief Data returned from the database
  public $listClass;       //!<@brief Type of view - 'report's are read only
  public $is_subview;      //!<@brief Set if we're included in another view
  public $checksum;        //!<@brief Checksum calculated from the data
  public $view;            //!<@brief View relevant for the object
  public $viewname;        //!<@brief Name of the view(s) to display
  public $viewlist;        //!<@brief List of views, if more than one
  public $oViews=array();  //<!@brief Array of view objects
  public $changed;

  /**
   * @brief Create the dbList object
   * @param[in] obj Optional database object
   * @todo Sort this function out - it's *huge*
   * @todo Default View ought really to come from Preferences
   * @date 2011-07-11
   * @version 13
   **/
  function __construct($obj = NULL) {
    $this->hotwireDebug(HW_DBG_OCRE,'Creation of a '.get_class($this).' object');

    ini_set("display_errors",1);
    $this->dbInit(); 
    $this->loadPreferences();
    $this->getPostVars();

    if (is_a($obj, 'dbView')) { 
      $this->view = array_shift($obj->subview);
      $this->is_subview = TRUE;
    }
 
    
    if (property_exists($this,'view') && 
      !$this->view &&
      array_key_exists('defaultview',$_SESSION)) {
      $this->view = $_SESSION['defaultview'];
    }
 
    $this->getViews();
    $this->getColAttrs();
    $this->getPerms();
    $this->getTable();
    $this->getRoles();

    if (property_exists($this,'view')
     && (! is_null($this->view))) {
      if (substr($this->view,-1)=='*') {
       $this->viewname='';
       // Handle this as a multi-view report
       $base=rtrim($this->view,'*');
       $match=preg_quote($base,'/');
       $this->viewname=basename(preg_replace('/\/\//','',$base));
       $views=array_values(preg_grep('/^'.$match.'/',array_keys($this->views)));
       $views=preg_replace('/^'.$match.'/','',$views);
       sort($views);
       $fullviews=array();
       foreach ($views as $v) {
         array_push($fullviews,$base.$v);
         $nicek=preg_replace('/^\d+_/','',$v);
       }
       $this->viewlist=$fullviews;
     } else {
       $this->viewname=basename($this->view);
     }
    }

    // set the listClass to 'report' if we're readonly
    if (property_exists($this,'view') 
     && (! is_null($this->view))
     && (is_array($this->view) || preg_match('/_ro$/', $this->view)) ) {
      $this->listClass = 'report';
    } else { $this->listClass='view'; }
    $this->hotwireDebug(HW_DBG_OCRE,'Finished creation of a '.get_class($this).' object');
  }

  private function sendDebugDiv() {
    // Send some debugging info 
    if (array_search('dev',$this->roles)!==false) {
 print '<div id="hw_debugtools">
<form>';
print '
<input type="hidden" name="_view" value="'.$this->view.'"/>
<select name="_debugval[]" multiple="multiple">';
$debugvals=array(
'Object creation'=>1,
'Public method calls'=>2,
'Protected method calls'=>4,
'Private method calls'=>8,
'View-related SQL'=>16,
'Hotwire-related SQL'=>32,
'Unknown SQL'=>64,
'Display backtrace'=>128,
);
foreach ($debugvals as $str => $i) {
 print '<option value="'.$i.'" '
       .(false!==array_search($i,array_values($this->debugval))?'selected':'')
       .'>'.$str.'</option>'."\n";
}
$debugobjs=array('dbBase','dbCSV','dbCSVimport','dbDispBase','dbDispBool','dbDispDate','dbDispFloat4','dbDispInt4','dbDispInt8','dbDispMacaddr','dbDispNumeric','dbDispOid','dbDispText','dbDispTime','dbDispVarchar','dbGraph','dbList','dbMetaList','dbNavBar','dbRecord','dbView','htmlBase');

print '
</select>
<select name="_debugobj[]" multiple="multiple">
';
foreach ($debugobjs as $str) {
 print '<option value="'.$str.'" '
      .(false!==array_search($str,$this->debugobj)?'selected="selected"':'')
      .'>'.$str.'</option>'."\n";
}
print '</select>
<img id="hw_debug_submit" src="images/icon-hammerSpanner.jpg" width="70" height="70"/>
</form>
</div>
 ';

 
    }
  }

  public function prepareHead() {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
    $extras=array();
    if (!(property_exists($this,'viewlist') 
        && is_array($this->viewlist))) {
      $list=new dbList();
      $extras=array_merge($extras,$list->prepareHead());
      array_push($this->oViews,$list);
    } else {
      foreach($this->viewlist as $vi) {
        $obj=NULL;
        $list=new dbList($obj,$vi);
        $extras=array_merge($extras,$list->prepareHead());
        array_push($this->oViews,$list);
      }
    }
    $this->hotwireDebug(HW_DBG_OPUB,'Left [public] '.get_class($this).'->'.__METHOD__);
    return array_unique($extras);
  }

  /**
   * @brief Produces the navigation bar.
   * 
   * @date 2011-08-04
   * @version 1
   */
  public function echoNavBar() {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
    if (!(property_exists($this,'viewlist') && is_array($this->viewlist))) {
     $list=$this->oViews[0];
     //$list=new dbList(NULL,NULL);
     $list->echoNavBar();
    } else {
     // Any list will produce a navbar of the appropriate formatting
     $vi=$this->viewlist[0];
     //$list=new dbList(NULL,$vi);
     $list=$this->oViews[0];
     $list->echoNavBar();
    }
    //$this->sendDebugDiv();
  }

  public function displaylist() {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__);
    // Display the results of any previous queries
    if (!(property_exists($this,'viewlist') && is_array($this->viewlist))) {
      $list=$this->oViews[0];
      //$list=new dbList(NULL,NULL);
      $list->displayPreviousMessage();
      $list->display();
    } else {
      //foreach ($this->viewlist as $vi) { 
        //#$list=new dbList(NULL,$vi);
        //$list->display($vi);
      //}
      foreach ($this->oViews as $list) {
        $list->displayPreviousMessage();
        $list->display();
      }
    }
  }
}

?>
