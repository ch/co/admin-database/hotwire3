<?php
 /**
  * @brief A class with basic methods to display an OID
  * @author Frank Lee
  * @date 2011-07-11
  * @version 11
  **/
class dbDispOid extends dbDispBase {

 /**
  * @brief Displays the OID - just as an image, currently.
  * @date 2012-11-09
  * @version 12
  **/
 public function displayEdit() {
  if (substr($this->dbColumn->name(),0,1)=='_') { return;}
  $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
  // How we display an OID largely depends on what's stored in it
  // which we'll have to infer from the name of the field, I guess
  switch (true) {
   case preg_match('/image/',$this->key):
    return "<img src='image.php?_id=".$this->objRecord->id[0].
           "&_view=".$this->objRecord->view."&_field=".$this->fieldname."' class='photo' " .
           "width=150 height=200><input type='file' name='".$this->fieldname."' >";
    break;
   default:
    return "No idea how to display this field.\n";
  }
 }

 /**
  * @brief: Dump the OID contents out with the right mimetype
  **/
 public function displayField() {
  $this->val=$this->objRecord->data[$this->objRecord->id[0]][$this->fieldname];
  pg_query($this->objRecord->dbh,'begin');
  $handle=pg_lo_open($this->objRecord->dbh,$this->val,'r');
  if ($handle===FALSE) {
   print "Bad handle returned for ".$this->val."\n".pg_last_error()."\n";
  } else {
   header('Content-type: image/jpeg');
   pg_lo_read_all($handle);
   pg_lo_close($handle);
   pg_query($this->objRecord->dbh,'commit');
  }
 }

 public function viewcell(&$row) {
 }


}
?>
