<?php
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

 /** 
  * @file dbSearch.php
  * @brief Handles details related to a search
  * Part of Hotwire http://hotwire.sourceforge.net/. 
  */

 /**
  * @brief A class with basic methods to handle search options
  * @author Frank Lee
  * @date 2012-11-13
  * @version 1
  **/

/*
   * Search objects: $field => {val => text}  gives "$field eq 'text'"
   *                 $field => {max => MA  gives "$field >=MI" 
   *                 $field =>  min => MI} gives "$field <=MA"
   *                 $field => {after => DATE} "$field > DATE"
   *                 $field => {before => DATE } "$field < DATE"
   *                 $field => {after & before) "$field BETWEEN DATE AND DATE"
   *                 $field => { in => array()} "$field IN (a,b,c,)"
   *                 $field => {invert => true} "NOT ...."
*/

class dbSearch extends hwFunctions {
 private $hidden=false;
 private $invert=false;
 private $exact=false;
 private $fieldname=NULL;
 private $value=NULL;
 private $max=NULL;
 private $min=NULL;
 private $hidtbl=NULL;
 private $ent_hid;
 private $ent_id;
 private $searchhid;
 private $displayvals;
 private $prefs;
 private $unaccent=false; // The name of the unaccent function
 public $type;
 public $viewname;
 public $hwschema;

 /**
  * @brief To provide a GET representation of the object 
  * @author Frank Lee
  * @date 2018-07-30
  * @version 1
  * Accepts an array reference and appends to it
  **/
 public function asGET(&$sofar) {
  if (! is_null($this->value)) {
   if (is_array($this->value)) {
     foreach ($this->value as $val) {
       $sofar[]=urlencode(($this->searchhid?$this->end_hid:$this->fieldname)).'[]='.urlencode($val);
     }
   } else {
     $sofar[]=urlencode(($this->searchhid?$this->ent_hid:$this->fieldname)).'='.urlencode($this->value);
   }
  }
  if (!is_null($this->min)) {
   $sofar[]=urlencode('min_'.$this->fieldname).'='.urlencode($this->min);
  }
  if (!is_null($this->max)) {
   $sofar[]=urlencode('max_'.$this->fieldname).'='.urlencode($this->max);
  }
  if ($this->exact) {
   $sofar[]=urlencode('xct_'.$this->fieldname).'=on';
  }
  if ($this->invert) {
   $sofar[]=urlencode('xcl_'.$this->fieldname).'=on';
  }
 }

 
 /**
  * @brief To construct a search object
  * @author Frank Lee
  * @date 2012-11-13
  * @version 1
  **/
  
 public function __construct(&$columnobj) {
  $this->prefs=$columnobj->parentobj->prefs;
  $this->type=$columnobj->type();
  $this->fieldname=$columnobj->name();
  $this->viewname=$columnobj->parentobj->view;
  $this->hwschema=$columnobj->parentobj->hwschema;
  // In case this is a HID field, search _hid first, _id if that doesn't exist
  // and remove ro_ prefices if the exist
  $possiblevalues=array($this->fieldname,$columnobj->name());
  $possiblevalues=array_merge($possiblevalues,preg_replace('/^ro_/','',$possiblevalues));
  $possiblevalues=array_unique($possiblevalues);
  $this->value=$this->arrayvalue($possiblevalues
                                 ,$columnobj->parentobj->post);
  if ($columnobj->ent_hid()) {
    $this->ent_hid=$columnobj->ent_hid();
    $this->ent_id=$columnobj->ent_id();
    // Should we search the _id or the _hid? Always present as searchng 
    // the _hid field for human readability
    if ((array_key_exists($this->ent_id,$columnobj->parentobj->post)) 
     || (array_key_exists('ro_'.$this->ent_id,$columnobj->parentobj->post))){
      // Convert posted _ids into _hids
      $this->ent_id=preg_replace('/^ro_/','',$this->ent_id);
      # TODO: Quote these fields more portably
      $sql='select array(select "'.preg_replace('/^ro_/','',$this->ent_hid).'" from '
          .$this->hwschema.".\"".preg_replace('/^ro_/','',$this->ent_hid).'" where '
          .(array(0=>'')!==$this->value?(
            '"'.$this->ent_id.'"'." IN ('"
            .(is_array($this->value)?join("','",array_unique($this->value))
                                  :$this->value)
            ."'))"
          ):'false)');
      $results=$columnobj->parentobj->sqltostring($sql);
      $this->displayvals=trim($columnobj->parentobj->sqltostring($sql),'{}');
      if (!$this->displayvals) {
        $this->displayvals=$this->value;
      }
      $this->exact=$this->arrayvalue('xct_'.$this->fieldname,$columnobj->parentobj->post)=='on';
      $this->invert=($this->arrayvalue('xcl_'.$this->fieldname,$columnobj->parentobj->post)=='on');
    } else if (array_key_exists($this->ent_hid,$columnobj->parentobj->post)) {
      $this->value=$columnobj->parentobj->post[$this->ent_hid];
      $this->displayvals=(is_array($this->value)
                          ?join(',',array_unique($this->value))
                          :$this->value);
      $this->searchhid=true;
      $this->exact=$this->arrayvalue('xct_'.$this->ent_hid,$columnobj->parentobj->post)=='on';
      $this->invert=($this->arrayvalue('xcl_'.$this->ent_hid,$columnobj->parentobj->post)=='on');

    } else {
      $this->searchhid=true;
      $this->displayvals=(is_array($this->value)
                          ?join(',',array_unique($this->value))
                          :$this->value);
      $this->exact=$this->arrayvalue('xct_'.$this->fieldname,$columnobj->parentobj->post)=='on';
      $this->invert=($this->arrayvalue('xcl_'.$this->fieldname,$columnobj->parentobj->post)=='on');
    }
  } else { 
    $this->displayvals=$this->value;
    $this->exact=$this->arrayvalue('xct_'.$this->fieldname,$columnobj->parentobj->post)=='on';
    $this->invert=($this->arrayvalue('xcl_'.$this->fieldname,$columnobj->parentobj->post)=='on');
  }
  if ($this->type=='date') {
   $this->max=$this->dateParse($this->arrayvalue('max_'.$this->fieldname,$columnobj->parentobj->post));
   if ($this->max===null) { unset($columnobj->parentobj->post['max_'.$this->fieldname]);}
   $this->min=$this->dateParse($this->arrayvalue('min_'.$this->fieldname,$columnobj->parentobj->post));
   if ($this->min===null) { unset($columnobj->parentobj->post['min_'.$this->fieldname]);}
  } else {
    $this->max=$this->arrayvalue('max_'.$this->fieldname,$columnobj->parentobj->post);
    $this->min=$this->arrayvalue('min_'.$this->fieldname,$columnobj->parentobj->post);
  }
  #$this->value=$this->arrayvalue(array($this->fieldname,$columnobj->name()),$columnobj->parentobj->post);
  // Set the boolean according to the UNACCENT preference
  $this->unaccent=($this->pref('UNACCENT',FALSE));
 }

 public function asSQL(&$sql,&$args,$isarray=false) {
   if ($this->value=='' && $this->max===NULL && $this->min===NULL) { return ; }
   if (method_exists($this,'asSQL'.$this->type) && $this->fieldname !='Xid') {
     $method='asSQL'.$this->type;
     $this->$method($sql,$args,$isarray);
   } else {
    # We shouldn't even be creating a dbSearch object if the value's empty, surely?
    if ($this->value) {
     if (is_array($this->value)) {
       $args[]=join('DELIMITER',$this->value);
       $sql[]=$this->SQLnot()
             .($this->SQLfield()).'= ANY (string_to_array($'.(count($args)).",'DELIMITER'))";

     } else {
       // Might need to handle arrays here
	## UNACCENT - if set, use the 'unaccent' function first.
#print_r($_SESSION);
       $sql[]=$this->SQLnot()
             .($this->exact?$this->SQLfield():
             	($this->unaccent?'unaccent('.$this->SQLfield().'::text)':$this->SQLfield().'::text')
             )
             .($this->exact?' = ':' ILIKE ')
             .($this->unaccent?'unaccent($'.(count($args)+1).')':'$'.(count($args)+1));
       $args[]=($this->exact?'':'%')
              .$this->value
              .($this->exact?'':'%');
      }
    }
   }
 }

 public function asSQLforCount(&$sql,&$args,$isarray=false) {
   if ($this->value=='' && $this->max===NULL && $this->min===NULL) { return ; }
   if (method_exists($this,'asSQLforCount'.$this->type) && $this->fieldname !='Xid') {
     // Surely this should end up in a dbDisp$TYPE object?
     $method='asSQLforCount'.$this->type;
     $this->$method($sql,$args,$isarray);
   } else if (method_exists($this,'asSQL'.$this->type) && $this->fieldname !='Xid') {
     $method='asSQL'.$this->type;
     $this->$method($sql,$args,$isarray);
   } else {
    # We shouldn't even be creating a dbSearch object if the value's empty, surely?
    if ($this->value) {
     if (is_array($this->value)) {
       $args[]=join('DELIMITER',$this->value);
       $sql[]=$this->SQLnot()
             .($this->SQLfield()).'= ANY (string_to_array($'.(count($args)).",'DELIMITER'))";

     } else {
       // Might need to handle arrays here
        ## UNACCENT - if set, use the 'unaccent' function first.
       $sql[]=$this->SQLnot()
             .($this->exact?$this->SQLfield():
                ($this->unaccent?'unaccent('.$this->SQLfield().'::text)':$this->SQLfield().'::text')
             )
             .($this->exact?' = ':' ILIKE ')
             .($this->unaccent?'unaccent($'.(count($args)+1).')':'$'.(count($args)+1));
       $args[]=($this->exact?'':'%')
              .$this->value
              .($this->exact?'':'%');
      }
    }
   }
 }



 private function SQLnot() {
  return ($this->invert?'NOT ':'');
 }

 private function SQLfield() {
  if ($this->searchhid) {
   return '"'.$this->ent_hid.'"';
  } else {
   return '"'.$this->fieldname.'"';
  }
 }

 private function SQLfullfield() {
  if ($this->searchhid) {
   return '"'.$this->ent_hid.'"';
  } else {
   return '"'.$this->viewname.'"."'.$this->fieldname.'"';
  }
 }

 private function asSQLint8(&$sql,&$args,$isarray=false) {
  $hid=$this->ent_hid; // this->hidtbl seemed to be empty
  $field=$this->fieldname;
  if ($this->exact || ($field=='id') || (substr($field,-3)=='_id'))  {
   if ($this->searchhid) {
    if ($isarray) {
     # The /field/ we are seraching is an array. 
     $h=str_replace('ro_','',$hid);
     $i=str_replace('_hid','_id',$h);
     $o=($this->exact?'=':'ILIKE');
     # Is the posted argument?
     if (is_array($this->value)) {
       # Arrays are logical OR
       # WHERE $hid_table.$hid_column LIKE any ('%foo%','%bar%','%baz%');
       #  - or, if $this->exact,
       # WHERE $hid_table.$hid_column = any ('foo','bar','baz');
       # which we encode as 
       # SQL: WHERE $hid_table.$hid_column LIKE/= any ($N, $N+1, $N+2...)
       # ARGS: %foo%, %bar%, %baz%
       $t=implode(',', array_map(function($i) { return '$'.$i; } , 
                                 range(count($args)+1,count($args)+count($this->value))));
       # Slightly complicated array overlap code:
       $sql[]=sprintf("( %s is distinct from '%s'::integer[])",
                         $field, '{null}');
       $sql[]=sprintf("( a.%s && array(select %s from %s.%s where %s %s ANY(ARRAY[%s])))",
                      $field,      $i, $this->hwschema,$h,      $h,$o,          $t);
       if ($this->exact) {
         foreach ($this->value as $a) { $args[]=$a; };
       } else {
         foreach ($this->value as $a) { $args[]='%'.$a.'%'; };
       }
     } else {
       # Field is an array, and we're passing a single argument
       $sql[]=sprintf("( a.%s && array(select %s from %s.%s where %s %s %s))",
                      $field,        $i, $this->hwschema, $h,  $h, $o, '$'.(count($args)+1));
       $args[]=($this->exact?$this->value:'%'.$this->value.'%');
     }
     return;
    } else {
     $sql[]=$this->SQLnot().
           ($this->unaccent?'unaccent('.$this->SQLfield().'::text)':$this->SQLfield().'::text').
           ' ILIKE '.
           ($this->unaccent?'unaccent($'.(count($args)+1).')':'$'.(count($args)+1));
     $args[]='%'.$this->value.'%';
     return;
    }
   } else {
    if ($isarray) {
     if (is_array($this->value)) {
      # We're searching an array by an array.
      $args[]=join('DELIMITER',$this->value);
      $op='ANY';
      $sql[]=$this->SQLnot().$this->SQLfield().'::bigint[] && ((sTRing_to_array($'.count($args).",'DELIMITER'))::bigint[])";
      return;
     } else {
      $args[]='%'.$this->value.'%';
      $op='=';
     }
    } else { // The field is not an array
     if (is_array($this->value)) {
      # Our record will be one of the items in the search array:
      $args[]=join('DELIMITER',$this->value);
      $sql[]=$this->SQLnot().$this->SQLfield().' = ANY ((sTring_to_array($'.count($args).",'DELIMITER'))::bigint[])";
      return;
     } else { // Neither is the search value
      if ($this->exact) {
       $args[]=$this->value;
       $op='=';
      } else {
       $args[]='%'.$this->value.'%';
       $op='=';
      }
     }
    }
   }
   // Operate on hid fields by default, but don't forget original
   $sql[]=$this->SQLnot().'"'.($hid?$hid:$this->fieldname).'" '.$op .' $'.(count($args));
  } else {
   $args[]='%'.$this->value.'%';
   $op='ILIKE';
   $sql[]=$this->SQLnot().($hid?'"'.$hid.'".':'').'"'.$field.'"::text '.$op
        .' $'.(count($args));
  }
 }

 private function asSQLforCountint8(&$sql,&$args,$isarray=false) {
  $hid=$this->ent_hid; // this->hidtbl seemed to be empty
  $field=$this->fieldname;
  if ($this->exact || ($field=='id') || (substr($field,-3)=='_id'))  {
   if ($this->searchhid) {
    if ($isarray) {
     # The /field/ we are searching is an array. Is the posted argument?
     $h=str_replace('ro_','',$hid);
     $i=str_replace('_hid','_id',$h);
     $o=($this->exact?'=':'LIKE');
     # Is the posted argument an array?
     if (is_array($this->value)) {
       # Arrays are logical OR
       # WHERE $hid_table.$hid_column LIKE any ('%foo%','%bar%','%baz%');
       #  - or, if $this->exact,
       # WHERE $hid_table.$hid_column = any ('foo','bar','baz');
       # which we encode as 
       # SQL: WHERE $hid_table.$hid_column LIKE/= any ($N, $N+1, $N+2...)
       # ARGS: %foo%, %bar%, %baz%
       # $t=implode(',', array_fill(count($args)+1,count($args)+count($this->value)));
       # Slightly complicated array overlap code:
       # Ensure that the array isn't empty
       $t=implode(',', array_map(function($i) { return '$'.$i; } ,
                               range(count($args)+1,count($args)+count($this->value))));
       $sql[]=sprintf("( %s is distinct from '%s'::integer[])",
                         $field, '{null}');
       # And see if there is some overlap between the two arrays
       $sql[]=sprintf("( \"%s\".\"%s\".%s && array(select %s from %s.%s where %s %s ANY(ARRAY[%s])))",
                      $this->hwschema,$this->viewname,$field,      $i, $this->hwschema,$h,      $h,$o,          $t);
       if ($this->exact) {
         foreach ($this->value as $a) { $args[]=$a; };
       } else {
         foreach ($this->value as $a) { $args[]='%'.$a.'%'; };
       }
     } else {
       # Field is an array, and we're passing a single argument
       $sql[]=sprintf("( \"%s\".\"%s\".%s && array(select %s from %s.%s where %s %s %s))",
                      $this->hwschema,$this->viewname,$field,        $i, $this->hwschema, $h,  $h, $o, '$'.(count($args)+1));
       $args[]=($this->exact?$this->value:'%'.$this->value.'%');
     }
     return;
    } else {
     $sql[]=$this->SQLnot().
           ($this->unaccent?'unaccent('.$this->SQLfield().'::text)':$this->SQLfield().'::text').
           ' ILIKE '.
           ($this->unaccent?'unaccent($'.(count($args)+1).')':'$'.(count($args)+1));
     $args[]='%'.$this->value.'%';
     return;
    }
   } else {
    if ($isarray) {
     if (is_array($this->value)) {
      # We're searching an array by an array.
      $args[]=join('DELIMITER',$this->value);
      $op='ANY';
      $sql[]=$this->SQLnot().$this->SQLfullfield().'::bigint[] && ((sTRing_to_array($'.count($args).",'DELIMITER'))::bigint[])";
      return;
     } else {
      $args[]='%'.$this->value.'%';
      $op='=';
     }
    } else { // The field is not an array
     if (is_array($this->value)) {
      # Our record will be one of the items in the search array:
      $args[]=join('DELIMITER',$this->value);
      $sql[]=$this->SQLnot().$this->SQLfullfield().' = ANY ((sTring_to_array($'.count($args).",'DELIMITER'))::bigint[])";
      return;
     } else { // Neither is the search value
      if ($this->exact) {
       $args[]=$this->value;
       $op='=';
      } else {
       $args[]='%'.$this->value.'%';
       $op='LIKE';
      }
     }
    }
   }
   // Preferentially search a _hid field, but don't ignore the original
   $sql[]=$this->SQLnot().'"'.($hid?$hid:$this->fieldname).'" '.$op .' $'.(count($args));
  } else {
   $args[]='%'.$this->value.'%';
   $op='ILIKE';
   $sql[]=$this->SQLnot().($hid?'"'.$hid.'".':'').'"'.$field.'"::text '.$op
        .' $'.(count($args));
  }
 }


 private function asSQLint4(&$sql,&$args,$isarray=false) {
  $this->asSQLint8($sql,$args,$isarray);
 }

 private function asSQLforCountint4(&$sql,&$args,$isarray=false) {
  $this->asSQLforCountint8($sql,$args,$isarray);
 }

 private function asSQLdate(&$sql,&$args) {
  # Might have set max and min: BETWEEN
  if ($this->max !== NULL && $this->max !=='' && $this->min !== NULL && $this->min !== '') {
   $sql[]=$this->SQLnot().$this->SQLfield()
         .' BETWEEN ' .'$'.(count($args)+1).' AND $'.(count($args)+2);
   $args[]=$this->min;
   $args[]=$this->max;
  } elseif ($this->max !== NULL && $this->max !=='') {
   $sql[]=$this->SQLnot().$this->SQLfield()
         .' <= $'.(count($args)+1);
   $args[]=$this->max;
  } elseif ($this->min !== NULL && $this->min !=='') {
   $sql[]=$this->sqlnot().$this->SQLfield() 
         .' >= $'.(count($args)+1);
   $args[]=$this->min;
  } else {
   $sql[]=$this->SQLnot().$this->SQLfield()."=$".(count($args)+1);
   $args[]=$this->value;
  }
 }

 private function asSQLnumeric(&$sql,&$args) {
  $this->asSQLdate($sql,$args);
 }

 // To return an ISO date string
 private function dateParse($d) {
  if(is_null($d)) {
   return null;
  } else {
   $date=null;
   if (array_key_exists('DATEFORMAT',$this->prefs)) {
     $date=DateTime::createFromFormat($this->prefs['DATEFORMAT'],$d);
   }
   if ($date==false) {
     $date=DateTime::createFromFormat('U',strtotime($d));
   }
   // Okay, failed to parse that.
   if ($date==false) {
     return null; 
   }
   return $date->format('Y-m-d');
  }
 }

 public function asSQLbool(&$sql,&$args,$isarray=false) {
   if ($this->value=='' && $this->max===NULL && $this->min===NULL) { return ; }
    # We shouldn't even be creating a dbSearch object if the value's empty, surely?
    if ($this->value) {
     if (is_array($this->value)) {
       $args[]=join('DELIMITER',$this->value);
       $sql[]=$this->SQLnot()
             .($this->SQLfield()).'= ANY (string_to_array($'.(count($args)).",'DELIMITER'))";

     } else {
       // Might need to handle arrays here
	## UNACCENT - if set, use the 'unaccent' function first.
#print_r($_SESSION);
       if ((strcasecmp($this->value[0],'y'))==0) {
         $matchagainst='t';
       } elseif ((strcasecmp($this->value[0],'n'))==0) {
         $matchagainst='f';
       } else {
         $matchagainst=$this->value;
       }
       $sql[]=$this->SQLnot()
             .($this->exact?$this->SQLfield():
             	($this->unaccent?'unaccent('.$this->SQLfield().'::text)':$this->SQLfield().'::text')
             )
             .($this->exact?' = ':' ILIKE ')
             .($this->unaccent?'unaccent($'.(count($args)+1).')':'$'.(count($args)+1));
       $args[]=($this->exact?'':'%')
              .$matchagainst
              .($this->exact?'':'%');
      }
    }
 }


 // Represent this search as fields for JS to interact with
 // TODO: this feels like a method for dbDisp* objects; it's type dependent
 public function asJSfields() {
  $fld=(substr($this->fieldname,-3)=='_id'
        ?(substr($this->fieldname,0,-3)."_hid")
        :$this->fieldname);
  #$fld=$this->fieldname;
  if ($this->type=='date' || $this->type=='time' || $this->type=='numeric') {
    $return=array();
    if (($this->min)&&(!($this->max))) {
      $return[]="<input class='hw_search_val' name='min_".$fld."' value='".$this->min."' "
               ."size='3' type='text'/>"
               ."<span class='clause_argument'>"
               .$this->hdrFormat($this->fieldname)."</span> "
               ."<span class='clause_operator'>"
               .($this->invert?"not ":"")
               .(($this->type=='numeric')?'at least':'after')
               ."</span>"
               ."<span class='clause_value'>".$this->min
               ."</span>";
    }
    if (($this->max)&&(!($this->min))) {
      $return[]="<input class='hw_search_val' name='max_".$fld."' value='".$this->max."' "
               ."size='3' type='text'/>"
               ."<span class='clause_argument'>".
                 $this->hdrFormat($this->fieldname)."</span> "
               ."<span class='clause_operator'>"
               .($this->invert?"not ":"")
               .(($this->type=='numeric')?'at most':'before')
               ."</span>"
               ."<span class='clause_value'>".$this->max
               ."</span>";
    }
    if (($this->min)&&($this->max)) {
      $return[]="<span class='clause_argument'>".
                 $this->hdrFormat($this->fieldname)."</span> "
               ."<span class='clause_operator'>"
               .($this->invert?"not ":"")
               ."between "
               .$this->min
               ." and "
               .$this->max
               ."</span>";
    }
    if ((!($this->min))&&(!($this->max))) {
      return "<input class='hw_search_val' name='".$fld."' value='".$this->displayvals."' "
            ."size='3' type='text'/>"
            ."<span class='clause_argument'>".
              $this->hdrFormat($this->fieldname)."</span> "
            ."<input type='hidden' name='xcl_".$this->fieldname."' value='".($this->invert?'on':'')."'/>"
            ."<span class='clause_operator'>".($this->invert?"!=":"=")."</span>"
            ."<span class='clause_value'>"
            .$this->displayvals
            ."</span> ";
    }
    return $return;
  } else {
    // _id fields require exact matches
    if (substr($this->fieldname,-3)=='_id') {
      $dispval=(is_array($this->displayvals)?implode(',',$this->displayvals):$this->displayvals);
      return "<input class='hw_search_val' name='".$fld."' value='".$dispval."' "
            ."size='3' type='text'/>"
            ."<input type='hidden' class='hw_search_xct' name='xct_".$fld."' value='".($this->exact?'on':'off')."'/>"
            ."<span class='clause_argument'>".
              $this->hdrFormat($this->fieldname)."</span> "
            ."<span class='clause_operator'>".($this->exact?($this->invert?"!=":"="):($this->invert?'not like':'like'))."</span>"
            ."<span class='clause_value'>"
            .$dispval
            ."</span> ";
    } else {
      if (is_array($this->displayvals)) {
        $display=implode(',',$this->displayvals);
      } else {
        $display=$this->displayvals;
      }
      return "<input class='hw_search_val' name='".$fld."' value='".$display."' "
            ."size='3' type='text'/>"
            ."<span class='clause_argument'>".
              $this->hdrFormat($this->fieldname)."</span> "
            ."<input type='hidden' name='xcl_".$this->fieldname."' value='".($this->invert?'on':'')."'/>"
            ."<span class='clause_operator'>".($this->invert?"not like":"like")."</span>"
            ."<span class='clause_value'>"
            .$display
            ."</span> ";
    }
  }
 }

 public function arrayvalue($key,$array) {
  if (is_array($key)) {
   foreach ($key as $k) {
     # First match wins
     if (array_key_exists($k,$array)) {
       return $array[$k];
     }
   }
   return null;
  } else {
   return (array_key_exists($key,$array)?$array[$key]:NULL);
  }
 }

   /**
   * @brief To return a preference or NULL if not set
   * @param[in] p Preference name
   * @param[in] default Default value of the preference if not set.
   * @return The value of the preference, or NULL if not set
   * @date 2011-07-11
   * @version 11
   **/
  public function pref($p,$default=null) {
    if (!array_key_exists($p,$this->prefs)) {
      return $default;
    }
    return $this->prefs[$p];
  }


}
