<?php 
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

 /** 
  * @file dbList.php
  * @brief Displays a view as a list
  * Part of Hotwire http://hotwire.sourceforge.net/. 
  */

  /** 
   * @class dbList
   * @author Stuart Taylor
   * @author Frank Lee
   * @date 2011-07-11
   * @brief Displays a view in list format.
   */
class dbList extends dbView {

  public $data = array();    //!<@brief Data returned from the database
  public $view;		     //!<@brief Name of the current view
  public $count;             //!<@brief Total number of rows returned by the view
  public $listClass='view';  //!<@brief Type of view - 'report's are read only
  public $is_subview;        //!<@brief Set if we're included in another view
  public $checksum;          //!<@brief Checksum calculated from the data
  public $is_metaview=false; //!<@brief Set if we're part of a meta-view
  public $scharg = array();  //!<@brief An array of searches
  private $selectsql;
  private $countsql;
  private $sqlargs;
  private $url;			//!<@brief GET equivalent of this search
  private $subviewname;		//!<@brief name of subview
  private $subviewfield;	//!<@brief field in subview to match to this.id
  private $subviewtarget;	//!<@brief View to open on click
  private $subviewref;		//!<@brief Field in subview containing value
  private $subviewtargetfield;  //!<@brief Field in target view to search
  private $subviewwhere;	//!<@brief Where clause if subview
  private $subviewargs;		//!<@brief Args for clause if subview
  private $subviewidfield;      //!<@brief If set, match this field rather than this.id
  private $searchasget;         //!<@brief GET representation of the search (array)
  protected $debugval=array();
  protected $debugobj=array();
  public $action;
  public $id;
  public $changed;

  public function subviewtarget() {
   return $this->subviewtarget;
  }
 
  public function subviewref() {
   return $this->subviewref;
  }

  public function subviewtargetfield() {
   return $this->subviewtargetfield;
  }

  /**
   * Search objects: $field => {val => text}  gives "$field eq 'text'"
   *                 $field => {max => MA  gives "$field >=MI" 
   *                 $field =>  min => MI} gives "$field <=MA"
   *                 $field => {after => DATE} "$field > DATE"
   *                 $field => {before => DATE } "$field < DATE"
   *                 $field => {after & before) "$field BETWEEN DATE AND DATE"
   *                 $field => { in => array()} "$field IN (a,b,c,)"
   *                 $field => {invert => true} "NOT ...."
   **/

  /**
   * @brief Create the dbList object
   * @param[in] obj Optional database object
   * @date 2011-07-11
   * @version 13
   **/
  function __construct(&$obj = NULL,$manualview=NULL, $searchfield=NULL, $searchval=NULL) {
    $this->hotwireDebug(HW_DBG_OCRE,"Creation of a ".get_class($this)." object");
    ini_set("display_errors",1);
    $reqStart = microtime(TRUE);
    $this->dbInit(); 
    $this->loadPreferences();
    $this->getDefaultView();
    $this->getPostVars();  // Also sets sort order.
    
    $where='';

    $this->getViews();

    // Set the view name to the default view unless it's already specified
    if (property_exists($this,'view') && 
      !$this->view &&
      array_key_exists('defaultview',$_SESSION)) {
      $this->view = $_SESSION['defaultview'];
    }

    if ($manualview) {
      $this->view=$manualview;
      $this->is_metaview=true;
    };

    // Handle this being a subview. hwsubviewc inherits from hwsubviewb
    // This over-rides the view name which might have been set above.
    if (is_a($obj, 'dbDisp_hwsubviewb')) { 
      $vala=explode(",",trim($obj->getval(),'()'));
      $this->subviewname=$vala[0];          /// Display rows from this view ...
      $this->subviewfield=$vala[1];         /// ... where this column matches ID (see [5]) in the original view
      $this->subviewtarget=$vala[2];        /// Open this view ...
      $this->subviewref=$vala[3];           /// ... by looking for this value from the displayed view ...
      $this->subviewtargetfield=$vala[4];   /// ... in this field in the opened view
      $this->subviewidfield=$vala[5];       /// If set, over-ride use of the field 'id' and use this field instead.
      $this->view=$this->subviewname;
      $this->is_subview=TRUE;
    };

    // However we got the viewname, if it's not in the list of views, it doesn't exist.
    if (!array_key_exists($this->view,$this->views)) {
      $this->view=NULL;
    }
    
    $this->getPerms();
    $this->getColAttrs();
    $this->getTable();


    // If we're in a subview, then set up our search correctly
    if ($this->is_subview) {
      if ($this->subviewname && $this->subviewfield) {
        $c=$this->getColByName($this->subviewfield);
        if ($c && $c->isarray()) {
          $this->subviewwhere=array('$1=ANY("'.$this->subviewfield.'")');
        } else {
          $this->subviewwhere=array('"'.$this->subviewfield.'"=$1');
        }
        if ($this->subviewidfield) {
          $r=$obj->get_data_value($this->subviewidfield);
          if ($r[0]) {
            $this->subviewargs=array($r[1]);
          } else {
 print "Could not find field ".$this->subviewidfield."\n";
          }
        } else {
         $this->subviewargs=array($obj->getid(0));
        }
      }
    }

    // Append any sorts from the view onto the user-defined sorts
    if ($this->view) {
      if (is_string($this->views[$this->view]['default_sort'])) {
        $this->views[$this->view]['default_sort']=$this->parse_order_by($this->views[$this->view]['default_sort']);
      }
      if (is_object($this->views[$this->view]['default_sort'])) {
        $this->usortobjs->append($this->views[$this->view]['default_sort']->export());
      }

    // set the listClass to 'report' if we're readonly or multiple   
    if (property_exists($this,'view') 
     && (is_array($this->view) || preg_match('/_ro$/', $this->view)) ) {
      $this->listClass = 'report';
    } else { 
      $this->listClass='view';
      $this->pagelen=($this->pagelen?$this->pagelen:$GLOBALS['PAGE_LENGTH']);
    }
 
    // Do a deletion
    if (array_key_exists('dodelete',$_REQUEST) && $_REQUEST['dodelete']=='Delete') {
      $this->dbDelete();
      $this->action='postdelete';
    }

    // Figure out the SQL and count the rows
    $this->makeselectsql();
    $this->countrows();
    $this->loaddata();
    }
    # No longer display time at request of admin staff
    //if (empty($this->msg)) {
      //$this->msg = sprintf('%1.2fs', (microtime(TRUE) - $reqStart));
    //}
    // Create a URL for bookmarking
    $this->url=$this->geturl();

    // Perhaps we jump straight to the single returned record.
    $this->maybejump();
    $this->hotwireDebug(HW_DBG_OCRE,"Ended creation of a ".get_class($this)." object");
  }

  private function countrows() {
    $start=microtime(true);
    $dbResult=$this->sqlexec(get_class($this),__METHOD__,HW_DBG_DBVI,$this->countsql,$this->sqlargs);
    if (!$dbResult) {$this->sqlError('Counting rows in the view',$this->countsql,$this->sqlargs);};
    $acount=pg_fetch_array($dbResult);
    $end=microtime(true);
    global $hw_sql_timing;
    $hw_sql_timing[$this->countsql]=($end-$start)/1000;
    $this->count=$acount[0];
  }

  // To return a pointer to the column object identified by the given name
  // If our colobj array were instead a hash, this would be quite easy. Ho Hum.
  // TODO: Make $this->colobj a hash?
  protected function getColByName($name) {
    $res=NULL;
    foreach ($this->colobjs as $col) {
      if ($col->name() === $name) {
        $res=$col;
      }
    }
    return $res;
  }

  private function loaddata() {
    $start=microtime(true);
    $dbResult=$this->sqlexec(get_class($this),__METHOD__,HW_DBG_DBVI,$this->selectsql,$this->sqlargs);
    if (!$dbResult) {$this->sqlError('Loading data for the view',$this->selectsql,$this->sqlargs);};
    $firstrow=pg_fetch_assoc($dbResult);
    if (!$firstrow) {return;}
    $keys=array_keys($firstrow);
    pg_result_seek($dbResult,0);
    if (count($keys)>0 && !in_array('id',$keys)) {
     if ($this->listClass != 'report') { // Reports without an ID field are fine.
       $this->listClass='report';
       $this->msg='This view has no ID column. Please ask your system administrator to add one.';
     }
     $this->data=pg_fetch_all($dbResult);
    } else {
      // Does fetch_all work any faster? Yes, apparently it does.
      // https://stackoverflow.com/questions/18144782/performance-of-foreach-array-map-with-lambda-and-array-map-with-static-function suggests
      // foreach is faster than array_map.
      //while ($row = pg_fetch_assoc($dbResult)) {
      //  $this->data[$row['id']]=$row;
      //}
      $t=pg_fetch_all($dbResult);
      foreach ($t as $row) {
        $this->data[$row['id']]=$row;
      }
      $_SESSION['ids']=array_keys($this->data);
    }
    $end=microtime(true);
    global $hw_sql_timing;
    $hw_sql_timing[$this->selectsql]=($end-$start)/1000;
    $this->logQuery($this->selectsql);
  }


  // Create a TH for the column. In this object because it depends on everything *else* as
  // well as the individual column.
  private function th($col) {
    // Don't display OIDs, _hwsubview
    if ($col->type()=='blobtype') {return;}
    if ($col->type()=='oid') {return;}
    if ($col->type()=='_hwsubview') {return;}
    if ($col->type()=='_hwsubviewb') {return;}
    if ($col->type()=='_hwsubviewc') {return;}
    if ($col->type()=='_hw_page_split') {return;}
    if ($col->type()=='bytea') {return;}
    
    if (! $this->is_subview) {
      $classes=array($this->listClass,
                     $this->domName($this->view.'_'.$col->name()),
      $this->domName(preg_replace('/.*\/\/\d*_{0,1}/','',
                     $this->view).'_'.$col->name()),
                     $this->domName($col->name()),
                     $this->domName(preg_replace('/.*\/\/\d*_{0,1}/','',
                     $this->view)));
      $return[]= "      <th id='".$this->domName($col->name())."' class='".join(' ',$classes)."'>\n" ;
      $return[]="       <div class='hw_header' id='hw_colhead_".$this->domName($col->name())."'>\n";
      $return[]="        <a id='sort_".$this->domName($col->name())."' href='list.php?"
               .$this->setGetVars(array('view','filter','action','search','pagelen'))."&"
               .$this->getSearchVars()."&"
               .$this->usortobjs->nextAsGET($col->name())."'>";
      $colname=$col->name();
      #if ($col->ent_hid()) {
        #$colname=$col->ent_hid();
      #}
      if (substr($colname,-3)=='_id') {
        $colname=substr($colname,0,-3)."_hid";
      }
      $return[]="<input type='hidden' class='colname' value='".$colname."'/>"
               ."<input type='hidden' class='direction' value=''/>" // Apparently this doesn't matter
               .$this->usortobjs->sortFormat($col->name())."\n"
               ."        </a>\n       </div>\n      </th>\n";
    } else {
      $return[]="<th id='".$this->domName($col->name())."'>".$this->usortobjs->sortFormat($col->name())."</th>";
    }
    return join("\n",$return);
  }

  // Build the SQL associated with this view
  private function makeselectsql() {
   $this->hwDebugPubStart(__METHOD__);
   if (! array_key_exists($this->view,$this->views)) { print "No such view ".$this->view."\n"; exit(2);}
   $fields=array();
   $join=array();
   $where=array();
   $wherecount=array(); # Need to have a slightly different WHERE clause on the count
   $args=array();
   $argscount=array();  # This /should/ end up being the same set of args...
   $sort=array();
   $countjoin=array();
   if ($this->is_subview) {
     $where=$this->subviewwhere;
     $wherecount=$this->subviewwhere;
     $args=$this->subviewargs;
     $argscount=$this->subviewargs;
     unset($this->pagelen);
   }
   foreach ($this->colobjs as $col) {
     $col->selectfield($fields);
     $col->join($join);
     $col->countjoin($countjoin);
   }
   $schema=($this->views[$this->view]['schema']?'"'.$this->views[$this->view]['schema'].'"':'');
   $offset=($this->offset?' OFFSET '.$this->offset:'');
   $limit=($this->pagelen>0?' LIMIT '.$this->pagelen:'');
   $selectsql='SELECT '.join(', ',$fields).' FROM '.$schema.'."'.$this->view.'" '.join(' ',$join);
   $countsql='SELECT count(*) FROM '.$schema.'."'.$this->view.'" '.join(' ',$countjoin);
   // Sorting
   $allsort=array();
   if (! $this->is_subview) {
     $allsort[]=$this->usortobjs->asSQL();
   }
   if (is_string($this->views[$this->view]['default_sort'])) {
     $this->views[$this->view]['default_sort']=$this->parse_order_by($this->views[$this->view]['default_sort']);
   }
   if (is_object($this->views[$this->view]['default_sort'])) {
     $allsort[]=$this->views[$this->view]['default_sort']->asSQL();
   }
   // Handle any searches
   if ($this->action=='search') {
    foreach ($this->colobjs as $col) {
      $col->where($where,$args);
      $col->wherecount($wherecount,$argscount);
    }
   }
   if ($this->action=='search' || $this->is_subview) {
    if (count($where)>0) {
     #$selectsql.=' WHERE '.join(' AND ',$where);
     # Do it this way around to ensure that the WHERE clause operates on renamed columns
     $selectsql='SELECT * from ('.$selectsql.') a WHERE '.join(' AND ',$where);
     #$countsql.=preg_replace('/\bro_/','',' WHERE '.join(' AND ',$where));
     $countsql.=preg_replace('/\bro_(\\w*_hid)/','\\1',' WHERE '.join(' AND ',$wherecount));
     #$countsql='SELECT count(*) from ('.$countsql.') a WHERE '.join(' AND ',$where));
    }
   }
   // Remove empty filters
   $allsort=(array_filter($allsort));
   if (count($allsort)>0) {
     $selectsql.=' ORDER BY '.join(', ',$allsort);
   }
   $this->selectsql=$selectsql.$offset.$limit;
   $this->countsql=$countsql;
   $this->sqlargs=$args;
   $this->hwDebugPubStop(__METHOD__);
  }

  /**
   * @brief Returns a 302 Moved if required
   * @date 2012-01-12
   * @version 1
   **/
  private function maybejump() {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__);
    if (($this->count==1) &&
        !$this->is_metaview &&
        !headers_sent() &&
        $this->listClass!='report' &&
        (($this->pref('JUMPSEARCH') &&
         (property_exists($this,'action')) &&
         ($this->action == 'search')) ||
        ($this->pref('JUMPVIEW')))) {
      $id=array_keys($this->data);
      header('Location: edit.php?_view='.$this->view.'&_id='.$id[0]);
      exit;
    }
  }

  /**
   * @brief Returns a URL for this page
   * @date 2012-01-12
   * @version 1
   * @todo Un-inherit this back up the stack a bit
   * @todo Is this called?
   * @todo - yep, this is called. It generates the bookmarkable link.
   */
  private function geturl() {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__);
    $this->hotwireDebug(HW_DBG_OPRI,'Called '.get_class($this).'->'.__METHOD__);
    $url=$_SERVER['SCRIPT_NAME']."?";
    foreach ($this->post as $key => $value) {
      if (is_array($value)) {
        foreach ($value as $v) {
          $url.="$key"."[]=$v&";
        }
      } else {
        $url.=($value?"$key=$value&":'');
      }
    }
  }

  /**
   * @brief Handles deletion from the database.
   * Handles a deletion, returning an error message if required.
   * @date 2011-07-11
   * @version 11
   * @todo Should this remove the 'id' property from the object to ensure we get 
   * back to the complete list?
   */
  private function dbDelete() {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__);
    if (! $this->post['deleteid']) {return;}
    $sql = "DELETE FROM ".$this->views[$this->view]['schema'].".\"$this->view\" WHERE id = '" . $this->post['deleteid']."'";
    $dbResult = pg_query($this->dbh, $sql);

    if ($dbResult) {
      $this->status = 'Success';
      $this->msg = 'Delete OK';
    } else {
      $this->status = 'Fail';
      $this->msg = pg_last_error();
    }

  }
 
  /** 
   * @brief Displays the sort info bar at the top of the result table
   * @date 2012-09-11
   * @version 1
   **/
  private function displaySortInfo() {
    $this->hotwireDebug(HW_DBG_OPRI,'Called [private] '.get_class($this).'->'.__METHOD__);
    print $this->returnSortInfo();
  }

  /**
   * @brief Produces the sort info bar at the top of the results table
   * @date 2011-07-11
   * @version 11
   **/
  private function returnSortInfo() {
   $this->hotwireDebug(HW_DBG_OPRI,'Called [private] '.get_class($this).'->'.__METHOD__);
   if (! $this->sort) {return;}
   return "";
  }

  /**
   * @brief Displays the big table of results
   * @date 2012-09-11
   * @author Frank Lee
   * @version 1
   */
  public function display($idprefix='') {
   print $this->returndisplay($idprefix);
  }
  
  /**
   * @brief List of column headers for CSV
   * @version 1
   * @date 2013-01-31
   * @author Frank Lee
   **/
  private function returnthscsv() {
    $return=array();
    foreach ($this->colobjs as $col) {
      if ($col->name()!=='id' && $col->visible()) {
        $name=$col->name();
        if (substr($name,-3,3)=='_id') {
          $name=substr($name,0,-2).'hid';
        }
        $return[]=$this->hdrFormat($name);
      }
    }
    return $return;
  }

  private function returnths() {
    $return=array();
    foreach ($this->colobjs as $col) {
      if ($col->name()!=='id' && $col->visible()) {
        $return[]=$this->th($col);
      }
    }
    return join("\n",$return);
  }

  /**
   * @brief Returns a CSV-formatted table of results
   * @version 2
   * @date 2013-01-31, 2018-02-22
   * @author Frank Lee
   * Add stream filter to adjust \n to \r\n
   **/
  public function displaycsv() {
    $data=$this->returncsv();
    $outstream = fopen("php://output",'w');
    stream_filter_register("newlines", "StreamFilterNewlines");
    stream_filter_append($outstream, "newlines");
    if (property_exists($this,'encoding')) {
      stream_filter_append($outstream, 'convert.iconv.utf-8.'.$this->encoding.'//TRANSLIT//IGNORE');
    }
    foreach ($data as $row) {
      fputcsv($outstream,$row);
    }
    fclose($outstream);
  }

  public function csvheader() {
    if (property_exists($this,'encoding')) {
      header('Content-Type: text/csv;charset='.$this->encoding);
      header('Content-Disposition: attachment; filename="' . $this->view . '.'.$this->encoding. '.csv"');
    } else {
      header('Content-Type: text/csv;charset=utf-8');
      header('Content-Disposition: attachment; filename="' . $this->view . '.csv"');
    }
  }

  /**
   * @brief Produces a CSV-formatted table of results.
   * @date 2013-01-31
   * @version 1
   * @author Frank Lee
   **/
  public function returncsv(){ 
    $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__);
    # Build up an array of data
    $return=array();
    $return[]=$this->returnthscsv();
    foreach ($this->data as $row) {
      $line=array();
      foreach ($this->colobjs as $col) {
        if ($col->inhtml()) { $line[]=$col->viewcellcsv($row);}
      }
      $return[]=$line;
    }
    return $return;
  }

  /**
   * @brief Produces a big table of results.
   * Returns an HTML table of results, with fields appropriately formatted.
   * @date 2012-11-15
   * @version 14
   */
  public function returndisplay($idprefix='') {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [public] '.get_class($this).'->'.__METHOD__,
                      array($idprefix));
    $return=array();
    $classes=array('hw_resulttable');
    if ($this->pref('FLOATHEAD')) {
      $classes[]='hw_floatheader';
    }
    if ((property_exists($this,'view')) and (!is_null($this->view))) {
      $classes[]=$this->domName($this->view);
      $classes[]=preg_replace('/.*\/\/\d*_{0,1}/','',
                              $this->view);
    }
    $return[]="
    <div class='clearwidth'></div>
    <!-- echoList() begin -->
    <div class='".join(' ',$classes)."' id='".$this->domName($idprefix.'_resulttable')."'>";
    $return[]=$this->sortAndSearchForJS();
    // display some UI hints
    if (array_key_exists('SHOWHINTS',$this->prefs) && $this->prefs['SHOWHINTS'] && $this->view) {
      $return[]=$this->showListHints();
    }
    $return[]="
    <table id='".$this->domName($this->view)."' class='hw_resulttable resizable hideable draggable' width='100%'>";
// TODO
    $return[]=$this->returnSortInfo();
    $return[]="

     <thead>
     <tr class='tableHeader'>\n";
    $return[]=$this->returnths();

    $return[]="
       </tr>
       </thead>
        <tbody class='tableTbody'>";
    $odd=false;
    foreach ($this->data as $row) {
      $return[]="<tr class='".($odd?'odd ':'even ')
                             .(array_key_exists('_cssclass',$row)?$row['_cssclass']:'')
                             ." inactive'>";
      $odd=!$odd;
      // Refectored: Use dbDisp widgets to provide the data
      foreach ($this->colobjs as $col) {
        $return[]=$col->viewcell($row);
      }
      # RFL: Don't display as a form in subview
      $return[]="</tr>\n";
    }
    // Removed this spacer to enable dragtable 
    // 	<tr class='tableSpacer'><td colspan='0'></td></tr>
    $return[]="
    </tbody>
    </table>
    </div><!-- resulttable -->

    <!-- dbList::__contruct() end -->";
    return join("\n",$return);
  }

  /**
   * @brief Show some hints for using the list
   * @date 2018-07-12
   * @version 1
   * @author RFL
   */
  private function showListHints() {
   $r=array();
   $r[]="<div class='dbList_hints'>";
   $r[]="<span class='hint_intro'>Hint:</span>";
   if ($this->perms['update']) {
     if ($this->perms['delete']) {
       $r[]="Click on any text to open that row in the editor. Delete an unwanted row there.";
     } else {
       $r[]="Click on any text to open that row in the editor. ";
     }
   } else {
     $r[]="Click on any text to view that row.";
   }
   if ($this->perms['insert']) {
     $r[]=" Add new rows by selecting 'Action' &mdash; 'Add new record', or open a record and 'Action' &mdash; 'Clone this entry'";
   }
   $r[]='</div>';
   return join("\n",$r);
  }

  /**
   * @brief Produce an HTML form which can be manipulated by JS for searching
   * @date 2012-07-29
   * @version 1
   * @author RFL
   */
  public function sortAndSearchForJS() {
   // At the very least, we need a well known DIV
   if ($this->is_subview) {return;}
   $result="<div class='hw_sortandsearchforjs'>".
           "<div id='hw_sortandsearch'>".
           "<form method='get' action='list.php' name='hw_sortandsearchforjs'>".
           "<div class='hiddenfields'>".
           $this->hiddenFieldsForJS().
           "</div>\n".
           "<div class='sortedfields'>".
           $this->sortedFieldsForJS().
           "</div>\n".
           "<div class='searchfields'>";
    if ($this->action != 'postdelete') {
           $result.=$this->searchFieldsForJS().
                    "</div>\n";
    }
    $result.="<div class='hw_sortandsearchsubmit'>\n".
           "<input type='submit' value='Apply search/sort'>\n".
           "</div>\n".
           "</form>\n".
           "</div>\n".
           "</div>\n";
   return $result;
  }

  /**
   * @brief Produce an HTML array of hidden fields
   * @date 2012-07-29
   * @version 1
   * @author RFL
   */
  private function hiddenFieldsForJS() {
   return  "<div id='hw_hidden'>"
          ."<input type='text' name='_view' id='_view' value='".$this->view."'/>"
          .($this->pagelen?"<input type='text' name='_pagelen' value='".$this->pagelen."'/>":'')
          .($this->offset?"<input type='text' name='_offset' value='".$this->offset."'/>":'')
          ."</div>";
  }

  /**
   * @brief Produce an HTML array of sorted fields
   * @date 2012-11-13
   * @version 2
   * @author RFL
   */
  private function sortedFieldsForJS() {
   $return=array();
   $return[]='<div id="hw_sort" '.($this->usortobjs->count()>0 ?'':'class="invisible"').'>';
   $return[]='<span>Sorting records by: <ul id="hw_sort_id" class="sortable ui-sortable">';
   $i=1;
   $return[]=$this->usortobjs->fieldForJS();
   $return[]="</ul></span></div>";
   return join("\n",$return);
  }

  /**
   * @brief Produce an HTML array of searched fields
   * @date 2012-11-13
   * @version 2
   * @author RFL
   */
  private function searchfieldsForJS() {
   $return=array();
   $i=1;
   #$return[]='<div id="hw_search" >'; // class=invisible?
   foreach ($this->colobjs as $col) {
     $ret=$col->asJSfields();
     if ($ret) {
       if (is_array($ret)) {
         foreach ($ret as $r) {
           $return[]="<li id='hw_search_".$i."' class='hw_clause' >";
           $return[]=$r;
           $return[]="</li>";
           $i++;
         }
       } else {
         $return[]="<li id='hw_search_".$i."' class='hw_clause' >";
         $return[]=$ret;
         $return[]="</li>";
         $i++;
       }
     }
   }
   $searchcount=count($return);
   if ($searchcount>0) {
    $return[]="<input type='text' name='_action' value='search'/>";
   }
    array_unshift($return,'<span>Showing records where: <ul id="hw_search_id">');
    $return[]="</ul></span>";
  
   return '<div id="hw_search" '.($searchcount>0?'':' class="invisible"').' >' 
         .join("\n",$return)
         .'</div>';
  }

  /**
   * @brief Calculates the checksum of the view.
   * @todo Bale nicely if the function doesn't work.
   * @date 2011-07-11
   * @version 11
   */
  public function calculateChecksum() {
    $this->hotwireDebug(HW_DBG_OPUB,'Called [private] '.get_class($this).'->'.__METHOD__);
   if (!property_exists($this,'view')) { return;}
   $cksumsql='select md5(array_to_string(array(select md5(textin(record_out("'.
	     $this->views[$this->view]['schema'].'"."'.
             $this->view.'".*))) from "'.
             $this->views[$this->view]['schema'].'"."'.
             $this->view.'"),'."'-'));";
   $this->checksum=$this->sqltostring($cksumsql);
  }

 /**
  * @brief returns the result of an ajax search request
  * @author Frank Lee
  * @date 2012-11-24
  * @version 1
  **/
  public function ajaxresultsearch($fields=NULL) {
    $obj=new dbAjaxResult();
    # Has to return sensible values for search
    $obj->succeed($this->msg);
    # Insert returns a new ID value
    $obj->setid($this->id);
    return $obj;
  }

 /**
  * @brief returns the result of an ajax select request
  * @author Frank Lee
  * @date 2012-11-24
  * @version 1
  **/
  public function ajaxresultselect($fieldlist) {
    $obj=new dbAjaxResult();
    # Has to return sensible values for select
    $obj->succeed($this->msg);
    # Insert returns a new ID value
    $obj->setid($this->id);
    $result=array();
    foreach (array_keys($this->data) as $i) {
      $line=array();
      foreach ($fieldlist as $field) {
        if (array_key_exists($field,$this->data[$i])) {
          $line[$field]=$this->data[$i][$field];
        }
      }
      $result[]=$line;
    }
    $obj->setdata($result);
    return $obj;
  }

 /**
  * @brief Returns the GET representation of the search
  * @author Frank Lee
  * @date 2018-07-30
  * @version 2
  **/
  public function searchAsGET() {
    if (is_null($this->searchasget)) {
      $gets=array();
      foreach ($this->colobjs as $col) {
        $col->asGET($gets);
      }
      $this->searchasget=$gets;
    }
    return $this->searchasget;
  }

 /**
  * @brief Returns the GET representation of the pager
  * @author Frank Lee
  * @date 2018-07-30
  * @version 1
  **/
  public function pagerAsGET() {
    $gets=array();
    if ($this->pagelen != $GLOBALS['PAGE_LENGTH']) {
      $gets[]='_pagelen='.$this->pagelen;
    }
    if ($this->offset) {
      $gets[]='_offset='.$this->offset;
    }
    return $gets;
  }

 /**
  * @brief To set the encoding - only used for CSV files currently
  * @author Frank Lee
  * @date 2019-03-26
  * @version 1
  **/
 public function setencoding() {
  if (array_key_exists('_encoding',$_REQUEST)) {
    $this->encoding=$_REQUEST['_encoding'];
  }
 }

}

?>
