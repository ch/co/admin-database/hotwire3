<?php
 /**
  * @brief A class with basic methods to display a field
  * @author Frank Lee
  * @date 2011-07-11
  * @version 11
  * @todo Understand and implement fieldColours correctly
  **/
class dbDispText extends dbDispBase {

 /**
  * @brief Code placed in the HEAD of the page to load CSS/JS.
  * If the field can contain HTML, include a WYSIWYG editor
  * @date 2011-07-11
  * @version 11
  **/
 public static function prepareRecord(&$js,&$css) {
  #if ($this->dbColumn->htmlview()) {
   $js[]="js/tiny_mce/tiny_mce";
   $js[]="js/tinymce.init";
  #}
 }

 /**
  * @brief Displays an edit control for the text field
  * @todo Handle fieldCols array
  * @date 2011-07-11
  * @version 11
  **/
 public function displayEdit() {
  if (substr($this->dbColumn->name(),0,1)=='_') { return;}
  $result="Cannot display this record!";
  // Handle varchars being _id fields
  if (preg_match('/_id$/',$this->getkey())) {
    $subobj=new dbDispInt4($this->dbColumn);
    return $subobj->displayEdit();
  }
  // Is this varchar big enough to get a text area?
  // Policy decision: varchar & text are sufficiently similar
  // that /varchars/ are presented as <input> and text, as <textarea>
  $size=$this->dbColumn->size();
   if ($this->ro()) {
    $result="<textarea id=\"".$this->id()."\" name=\"".$this->getkey()."\" readonly=\"true\">".$this->htmlesc()."</textarea>";
   } else {
    $result="<textarea ".($this->dbColumn->htmlview()?'class="hw_htmlview"':'')." name='".$this->getkey()."' ".
           "id='".$this->id()."' >".
           $this->htmlesc()."</textarea>";
   }
  return $result;
 }


 public function non_displayEdit() {
  /*
  if (is_array($pobject->id) && 
      array_key_exists($pobject->offset,$pobject->id) &&
      array_key_exists($pobject->id[$pobject->offset],$pobject->data)) {
   $fieldCols = $pobject->fieldColours($pobject->data[$pobject->id[$pobject->offset]]);
  } else {
   $fieldCols=array();
  }
  */
  $fieldCols=array();
  $result="Cannot display this record!";
  // If htmlview, display as textarea
  $domkey=$this->domName($this->key);
  if (array_key_exists('htmlview',$this->objRecord->attrs) && $this->objRecord->attrs['htmlview']) {
   // If RO, just display.
   if ($this->ro) {
    $result="<div id='".$this->id()."' ".
            "class='rawhtmlfield'>".
            $val."</div>\n";
   } else {
    $result="<textarea name='".$this->id()."' ".
            "id='".$domkey."' wrap='off'".
            htmlentities($val)."</textarea>".
            "<script type=\"text/javascript\">
  tinyMCE.execCommand('mceAddControl',false,'".$domkey."');
             </script>"; 
   }
  } else {
   // Not html, so display as text area.
   $result="<input type='text' name='".$this->id()."' ".
           "id='".$this->id()."' size='80'" .
           (array_key_exists($this->key,$fieldCols) && $fieldCols[$this->key] 
            ? " style='color: ".$fieldCols[$this->key]."'" : "") .
           " value='".htmlentities($this->getval())."' ".
           ($this->ro ? " readonly class='readonly'>" : ">");
  }
  return $result;
 }

 public function displaySearch() {
   if (preg_match('/_id$/',$this->key)) {
    $subobj=new dbDispInt4($this->dbColumn,$this->key);
    return $subobj->displaySearch();
   }
   // If this widget has a value, display it. 
   if ($this->val) {
     return '<input type="text" class="varchar" size="10" '.
            'name="'.$this->getkey().'" value="'.$this->val.'"/>';
   } else {
     if (array_key_exists($this->colname,$this->objRecord->post)) {
       return '<input type="text" class="varchar" size="10" '.
              'name="'.$this->getkey().'" value="'.($this->objRecord->post[$this->colname]).'"/>';
     } else {
       return '<input type="text" class="varchar" size="10" '.
              'name="'.$this->getkey().'" value=""/>';
     }
   }
 }


 public function ajaxmmdropdown() {
  $subobj=new dbDispInt4($this->dbColumn);
  return $subobj->ajaxmmdropdown();
 }

}
?>
