#!/bin/bash
# To apply all the database changes
set -xv
DATABASE="$1"

function processfile() {
 FILE="$1"
 APPLIED="$2"
 ERRORS="$3"
 OUTPUT=`tempfile`
 if sudo -u postgres psql -v ON_ERROR_STOP=1 -X --single-transaction --file "$FILE" "$DATABASE" >$OUTPUT 2>&1 ; then
  mv $OUTPUT  "$APPLIED"
 else
  mv $OUTPUT "$ERRORS"
 fi 
}

function processdir() {
 DIR="$1"
 echo Processing $DIR
 mkdir -p "$DIR"/applied
 mkdir -p "$DIR"/errors
 for FILE in "$DIR"/*.sql ; do
  F=`basename "$FILE"`
  if [ ! -f "$DIR"/applied/$F ] ; then
   echo Processing "$FILE" "$DIR"/applied/"$F"
   processfile "$FILE" "$DIR"/applied/"$F" "$DIR"/errors/"$F"
  fi
 done
}

BASEDIR=`dirname $0`
for SUBDIR in "$BASEDIR"/*.d; do
 processdir "$SUBDIR"
 
done
