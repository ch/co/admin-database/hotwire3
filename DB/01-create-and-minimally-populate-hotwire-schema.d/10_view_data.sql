-- Information about views - can we read them? write them?
\echo Creating view hotwire._view_data (Rights on views)
DROP VIEW IF EXISTS hotwire._view_data;

CREATE OR REPLACE VIEW hotwire._view_data AS 
 SELECT pgns.nspname AS schema,
    pg_class.relname AS view,
    pg_class.relacl AS perms,
    has_table_privilege(((('"'::text || pgns.nspname::text) || '"."'::text) || pg_class.relname::text) || '"'::text, 'insert'::text) AS insert,
    has_table_privilege(((('"'::text || pgns.nspname::text) || '"."'::text) || pg_class.relname::text) || '"'::text, 'update'::text) AS update,
    has_table_privilege(((('"'::text || pgns.nspname::text) || '"."'::text) || pg_class.relname::text) || '"'::text, 'delete'::text) AS delete,
    has_table_privilege(((('"'::text || pgns.nspname::text) || '"."'::text) || pg_class.relname::text) || '"'::text, 'select'::text) AS "select",
    (( SELECT count(*) AS count
           FROM pg_rewrite r
             JOIN pg_class c ON c.oid = r.ev_class
             LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
          WHERE r.ev_type = '4'::"char" AND r.rulename <> '_RETURN'::name AND c.relname = pg_class.relname)) > 0 AS delete_rule,
    (( SELECT count(*) AS count
           FROM pg_rewrite r
             JOIN pg_class c ON c.oid = r.ev_class
             LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
          WHERE r.ev_type = '3'::"char" AND r.rulename <> '_RETURN'::name AND c.relname = pg_class.relname)) > 0 AS insert_rule,
    true OR (( SELECT count(*) AS count
           FROM pg_rewrite r
             JOIN pg_class c ON c.oid = r.ev_class
             LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
          WHERE r.ev_type = '3'::"char" AND r.rulename <> '_RETURN'::name AND c.relname = pg_class.relname AND r.ev_action::text ~~ '%returningList:%'::text)) > 0 AS insert_returning,
    (( SELECT count(*) AS count
           FROM pg_rewrite r
             JOIN pg_class c ON c.oid = r.ev_class
             LEFT JOIN pg_namespace n ON n.oid = c.relnamespace
          WHERE r.ev_type = '2'::"char" AND r.rulename <> '_RETURN'::name AND c.relname = pg_class.relname)) > 0 AS update_rule,
    pg_views.definition,
    hotwire._primarytable(pg_class.relname::character varying) AS primary_table,
        CASE
            WHEN regexp_replace(pg_views.definition, '.*\)'::text, ''::text) ~~ '%ORDER BY%'::character varying::text THEN regexp_replace(regexp_replace(regexp_replace(pg_views.definition, '.*\)'::text, ''::text), '.* ORDER BY'::text, ''::text), 'LIMIT.*|OFFSET.*|FOR.*|;'::text, ''::text)
            ELSE NULL::text
        END AS order_by
   FROM pg_class
     JOIN pg_namespace pgns ON pg_class.relnamespace = pgns.oid
     JOIN pg_views ON pg_class.relname = pg_views.viewname
  WHERE pg_class.relname ~~ '%/%'::text AND (pgns.nspname = ANY (ARRAY['hotwire'::name, 'public'::name]));

ALTER TABLE hotwire._view_data OWNER TO postgres;
GRANT ALL ON TABLE hotwire._view_data TO postgres;
GRANT SELECT ON TABLE hotwire._view_data TO public;
\echo Created view hotwire._view_data

