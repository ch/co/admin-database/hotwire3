\set QUIET
\echo Creating view preference_hid
CREATE OR REPLACE VIEW hotwire.preference_hid AS 
 SELECT "Preferences".id AS preference_id, "Preferences".hw_preference_name AS preference_hid
   FROM hotwire."hw_Preferences" "Preferences";

ALTER TABLE hotwire.preference_hid OWNER TO postgres;
GRANT ALL ON TABLE hotwire.preference_hid TO postgres;
\echo Created view
