\set QUIET
\echo Removing old "All Possible Preferences" view
DROP VIEW IF EXISTS hotwire."90_Action/Hotwire/All Possible Preferences";
\echo Creating "All Possible Preferences" view

CREATE OR REPLACE VIEW hotwire."90_Action/Hotwire/All Possible Preferences" AS 
 SELECT "hw_Preferences".id, "hw_Preferences".hw_preference_name AS preference_name, "hw_Preferences".hw_preference_type_id
   FROM hotwire."hw_Preferences";
ALTER TABLE hotwire."90_Action/Hotwire/All Possible Preferences" OWNER TO postgres;

\echo Creating 'on delete' rule...
CREATE OR REPLACE RULE hw_super_admin_preferences_del AS
    ON DELETE TO hotwire."90_Action/Hotwire/All Possible Preferences" DO INSTEAD  DELETE FROM hotwire."hw_Preferences"
  WHERE "hw_Preferences".id = old.id;

\echo Creating 'on insert' rule...
CREATE OR REPLACE RULE hw_super_admin_preferences_ins AS
    ON INSERT TO hotwire."90_Action/Hotwire/All Possible Preferences" DO INSTEAD  INSERT INTO hotwire."hw_Preferences" (hw_preference_name, hw_preference_type_id) 
  VALUES (new.preference_name, new.hw_preference_type_id);

\echo Creating 'on update' rule...
CREATE OR REPLACE RULE hw_super_admin_preferences_upd AS
    ON UPDATE TO hotwire."90_Action/Hotwire/All Possible Preferences" DO INSTEAD  UPDATE hotwire."hw_Preferences" SET hw_preference_name = new.preference_name, hw_preference_type_id = new.hw_preference_type_id
  WHERE "hw_Preferences".id = old.id;

\echo "All Possible Preferences" view created
