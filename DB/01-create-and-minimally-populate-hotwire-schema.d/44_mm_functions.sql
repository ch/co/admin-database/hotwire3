-- A function to insert an array into a many-many table

DROP FUNCTION IF EXISTS hotwire.fn_mm_ins(character varying, character varying, character varying, integer, integer[]);

CREATE OR REPLACE FUNCTION hotwire.fn_mm_ins(
	tablename character varying,
	keycol character varying,
	valcol character varying,
	keyval integer,
	valval integer[])
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
declare
 val integer;
begin
 for val in select unnest(valval) loop
  execute 'insert into '||tablename||' ('||keycol||','||valcol||') values ('||keyval||','||val||');';
 end loop;
  return 'OK';
end
$BODY$;

ALTER FUNCTION hotwire.fn_mm_ins(character varying, character varying, character varying, integer, integer[])
    OWNER TO postgres;

COMMENT ON FUNCTION hotwire.fn_mm_ins(character varying, character varying, character varying, integer, integer[])
    IS 'To insert into a generic MM table';

-- A function to set (update or insert) entries in a many-many table where old values are known
DROP FUNCTION IF EXISTS hotwire.fn_mm_set(character varying, character varying, character varying, bigint, bigint[], bigint[]);

CREATE OR REPLACE FUNCTION hotwire.fn_mm_set(
	tablename character varying,
	keycol character varying,
	valcol character varying,
	keyval bigint,
	newval bigint[],
	oldval bigint[])
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
declare
 val bigint;
begin
 -- Delete where id matches and value in old list but not in new list
 if oldval is distinct from newval then
   execute 'delete from '||tablename||' where '||keycol||'='||keyval||
           coalesce(' AND '||valcol||'=ANY('''||oldval::varchar||'''::integer[]) ','')||
           coalesce(' AND NOT ('||valcol||'=ANY('''||newval::varchar||'''::integer[]))','');

 -- 
 for val in select unnest(newval) except select unnest(oldval) loop
  execute 'insert into '||tablename||' ('||keycol||','||valcol||') values ('||keyval||','||val||');';
 end loop;
  end if;
  return 'OK';
end
$BODY$;

ALTER FUNCTION hotwire.fn_mm_set(character varying, character varying, character varying, bigint, bigint[], bigint[])
    OWNER TO postgres;

COMMENT ON FUNCTION hotwire.fn_mm_set(character varying, character varying, character varying, bigint, bigint[], bigint[])
    IS 'To update a generic MM table';

-- A function to set (update or insert) entries in a many-many table where old values are not known

DROP FUNCTION IF EXISTS hotwire.fn_mm_set(character varying, character varying, character varying, integer, integer[]);

CREATE OR REPLACE FUNCTION hotwire.fn_mm_set(
	tablename character varying,
	keycol character varying,
	valcol character varying,
	keyval integer,
	newval integer[])
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
declare
 oldval integer[];
begin
 -- For use when the oldvalue is not known and we must look it up
 execute 'select array_agg('||valcol||') from '||tablename||' where  '||keycol||'='||keyval into oldval;
 perform hotwire.fn_mm_set(tablename,keycol,valcol,keyval,newval,oldval);
 return null ;
end
$BODY$;

ALTER FUNCTION hotwire.fn_mm_set(character varying, character varying, character varying, integer, integer[])
    OWNER TO postgres;

-- A function to set (update or insert) entries in a many-many table where old values are known (ints)

DROP FUNCTION IF EXISTS hotwire.fn_mm_set(character varying, character varying, character varying, integer, integer[], integer[]);

CREATE OR REPLACE FUNCTION hotwire.fn_mm_set(
	tablename character varying,
	keycol character varying,
	valcol character varying,
	keyval integer,
	newval integer[],
	oldval integer[])
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
declare
 val integer;
begin
 -- Delete where id matches and value in old list but not in new list
 if newval is distinct from oldval then
 execute 'delete from '||tablename||' where '||keycol||'='||keyval||
         coalesce(' AND '||valcol||'=ANY('''||oldval::varchar||'''::integer[]) ','')||
         coalesce(' AND NOT ('||valcol||'=ANY('''||newval::varchar||'''::integer[]))','');

 -- 
 for val in select unnest(newval) except select unnest(oldval) loop
  execute 'insert into '||tablename||' ('||keycol||','||valcol||') values ('||keyval||','||val||');';
 end loop;
  end if;
  return 'OK';
end
$BODY$;

ALTER FUNCTION hotwire.fn_mm_set(character varying, character varying, character varying, integer, integer[], integer[])
    OWNER TO postgres;

COMMENT ON FUNCTION hotwire.fn_mm_set(character varying, character varying, character varying, integer, integer[], integer[])
    IS 'To update  a generic MM table';

-- A function to set (update or insert) entries uniquely in a many-many table where old values are known (ints)

DROP FUNCTION IF EXISTS hotwire.fn_mm_set_uniq(character varying, character varying, character varying, integer, integer[], integer[]);

CREATE OR REPLACE FUNCTION hotwire.fn_mm_set_uniq(
	tablename character varying,
	keycol character varying,
	valcol character varying,
	keyval integer,
	newval integer[],
	oldval integer[])
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
declare
 val integer;
begin
 -- Delete where id matches and value in old list but not in new list
 execute 'delete from '||tablename||' where '||keycol||'='||keyval||
         coalesce(' AND '||valcol||'=ANY('''||oldval::varchar||'''::integer[]) ','')||
         coalesce(' AND NOT ('||valcol||'=ANY('''||newval::varchar||'''::integer[]))','');
 -- 
 for val in select unnest(newval) except select unnest(oldval) loop
  execute 'insert into '||tablename||' ('||keycol||','||valcol||') select ' ||keyval||','||val|| '
  where not exists (select 1 from ' ||tablename|| ' where ' ||keycol||'='||keyval|| ' AND '
 ||valcol||'='||val|| ');';
 end loop;
  return 'OK';
end
$BODY$;

ALTER FUNCTION hotwire.fn_mm_set_uniq(character varying, character varying, character varying, integer, integer[], integer[])
    OWNER TO postgres;

-- A function to set entries in a many-many table where the values are of type MACaddr.

DROP FUNCTION IF EXISTS hotwire.fn_mm_set_uniq(character varying, character varying, character varying, integer, macaddr[], macaddr[]);

CREATE OR REPLACE FUNCTION hotwire.fn_mm_set_uniq(
	tablename character varying,
	keycol character varying,
	valcol character varying,
	keyval integer,
	newval macaddr[],
	oldval macaddr[])
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
declare
 val macaddr;
 sql varchar;
begin

 for val in select unnest(oldval) except select unnest(newval) loop
  if val is distinct from null then
    execute 'delete from "'||tablename||'" where "'||keycol||'"='''||keyval||
            ''' AND "'||valcol||'""='''||val||''';';
			raise notice 'SQL %',sql;
  end if;
 end loop;
 
 for val in select unnest(newval) except select unnest(oldval) loop
   if val is distinct from null then
     execute 'insert into '||tablename||
	         ' ("'||keycol||'","'||valcol||'") '||
			 'select '||keyval||','''||val|| '''
     where not exists ('||
	 'select 1 from "'||tablename|| '" '||
	 'where "'||keycol||'"='''||keyval|| ''' '||
	 'AND "'||valcol||'"='''||val||''');';
  end if;
 end loop;
  return 'OK';
end
$BODY$;

ALTER FUNCTION hotwire.fn_mm_set_uniq(character varying, character varying, character varying, integer, macaddr[], macaddr[])
    OWNER TO postgres;

-- Another function to update a many-many table

DROP FUNCTION IF EXISTS hotwire.fn_mm_upd(character varying, character varying, character varying, integer, integer[], integer[]);

CREATE OR REPLACE FUNCTION hotwire.fn_mm_upd(
	tablename character varying,
	keycol character varying,
	valcol character varying,
	keyval integer,
	newval integer[],
	oldval integer[])
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
AS $BODY$
declare
 val integer;
begin
 -- Delete where id matches and value in old list but not in new list
 execute 'delete from '||tablename||' where '||keycol||'='||keyval||
         coalesce(' AND '||valcol||'=ANY('''||oldval::varchar||'''::integer[]) ','')||
         coalesce(' AND NOT ('||valcol||'=ANY('''||newval::varchar||'''::integer[]))','');
 -- 
 for val in select unnest(newval) except select unnest(oldval) loop
  execute 'insert into '||tablename||' ('||keycol||','||valcol||') values ('||keyval||','||val||');';
 end loop;
  return 'OK';
end
$BODY$;

ALTER FUNCTION hotwire.fn_mm_upd(character varying, character varying, character varying, integer, integer[], integer[])
    OWNER TO postgres;

COMMENT ON FUNCTION hotwire.fn_mm_upd(character varying, character varying, character varying, integer, integer[], integer[])
    IS 'To update  a generic MM table';

