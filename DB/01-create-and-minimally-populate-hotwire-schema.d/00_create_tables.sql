\echo Beginnng hotwire schema setup
-- Just have to try to create the schema. No "IF NOT EXISTS" clause.
\set QUIET
SET client_min_messages to FATAL;
create schema "hotwire";
SET client_min_messages to WARNING;
\echo Created schema hotwire.
\echo Creating Preference types setup.
-- Preference types
DROP VIEW IF EXISTS hotwire._preferences;
DROP TABLE IF EXISTS hotwire.hw_preference_type_hid;
DROP SEQUENCE IF EXISTS hotwire.hw_preference_type_seq;
CREATE SEQUENCE hotwire.hw_preference_type_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER SEQUENCE hotwire.hw_preference_type_seq OWNER TO postgres;
\echo Don't worry if there's an objection to the previous line - probably an older postgres version.
\echo Created sequence hw_preference_type_seq

CREATE TABLE hotwire.hw_preference_type_hid (
    hw_preference_type_id integer DEFAULT nextval('hotwire.hw_preference_type_seq'::regclass) NOT NULL,
    hw_preference_type_hid character varying
);
ALTER TABLE hotwire.hw_preference_type_hid OWNER TO postgres;
\echo Created table hw_preference_type_hid

INSERT INTO hotwire.hw_preference_type_hid VALUES (1, 'boolean');
INSERT INTO hotwire.hw_preference_type_hid VALUES (2, 'integer');
INSERT INTO hotwire.hw_preference_type_hid VALUES (3, 'string');

ALTER TABLE ONLY hotwire.hw_preference_type_hid
    ADD CONSTRAINT hw_preference_type_pkey PRIMARY KEY (hw_preference_type_id);

GRANT ALL ON TABLE hotwire.hw_preference_type_hid TO postgres;
\echo Populated table hw_preference_type_hid

-- Preferences
\echo Creating Preferences table
DROP VIEW IF EXISTS hotwire."90_Action/Hotwire/All Possible Preferences";
DROP VIEW IF EXISTS hotwire.preference_hid;
DROP TABLE IF EXISTS hotwire."hw_Preferences";
DROP SEQUENCE IF EXISTS hotwire.hw_pref_seq;
CREATE SEQUENCE hotwire.hw_pref_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;

ALTER TABLE hotwire.hw_pref_seq OWNER TO postgres;
\echo Created sequence hw_pref_seq
CREATE TABLE hotwire."hw_Preferences"
(
  id integer NOT NULL DEFAULT nextval('hotwire.hw_pref_seq'::regclass),
  hw_preference_name character varying,
  hw_preference_type_id integer,
  hw_preference_const varchar,
  CONSTRAINT "Preferences_pkey" PRIMARY KEY (id),
  CONSTRAINT hw_pref_type_fkey FOREIGN KEY (hw_preference_type_id)
      REFERENCES hotwire.hw_preference_type_hid (hw_preference_type_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION

)
WITH (
  OIDS=FALSE
);
ALTER TABLE hotwire."hw_Preferences" OWNER TO postgres;
GRANT ALL ON TABLE hotwire."hw_Preferences" TO postgres;
\echo Created table hw_Preferences

INSERT INTO hotwire."hw_Preferences" VALUES (1, 'Floating table headers', 1, 'FLOATHEAD');
INSERT INTO hotwire."hw_Preferences" VALUES (9, 'Default view', 3, 'DEFVIEW');
INSERT INTO hotwire."hw_Preferences" VALUES (2, 'Jump on a single search result', 1, 'JUMPSEARCH');
INSERT INTO hotwire."hw_Preferences" VALUES (3, 'Jump on a single view result', 1, 'JUMPVIEW');
INSERT INTO hotwire."hw_Preferences" VALUES (4, 'CSS path', 3, 'CSSPATH');
INSERT INTO hotwire."hw_Preferences" VALUES (7, 'JS path', 3, 'JSPATH');
INSERT INTO hotwire."hw_Preferences" VALUES (5, 'Warn if record data changes', 1, 'CHANGE');
INSERT INTO hotwire."hw_Preferences" VALUES (10, 'Warn if view data changes', 1, 'CHECKSUM');
INSERT INTO hotwire."hw_Preferences" VALUES (6, 'AJAX min dd size', 2, 'AJAXMIN');
INSERT INTO hotwire."hw_Preferences" VALUES (11, 'Alternative views enabled', 1, 'ALTVIEW');
INSERT INTO hotwire."hw_Preferences" VALUES (12, 'Related Records enabled', 1, 'RELREC');
INSERT INTO hotwire."hw_Preferences" VALUES (13, 'Resizable Table columns', 1, 'RESIZECOLS');
INSERT INTO hotwire."hw_Preferences" VALUES (14, 'Sort NULLS in numerical order', 1, 'NUMNULL');
INSERT INTO hotwire."hw_Preferences" VALUES (15, 'Hideable Table columns', 1, 'HIDECOLS');
INSERT INTO hotwire."hw_Preferences" VALUES (16, 'Reorderable Table columns', 1, 'COLREORDER');
INSERT INTO hotwire."hw_Preferences" VALUES (17, 'Show old search', 1, 'OLDSEARCH');
INSERT INTO hotwire."hw_Preferences" VALUES (18, 'Include "... show next" buttons', 1, 'SHOWNEXT');
INSERT INTO hotwire."hw_Preferences" VALUES (19, 'Warn on missing rules', 1, 'WARNRULES');
INSERT INTO hotwire."hw_Preferences" VALUES (20, 'Cache menu structure', 1, 'CACHEMENU');
INSERT INTO hotwire."hw_Preferences" VALUES (21, 'Remove accents from search', 1, 'UNACCENT');
INSERT INTO hotwire."hw_Preferences" VALUES (23, 'Date format', 3, 'DATEFORMAT');
INSERT INTO hotwire."hw_Preferences" VALUES (24, 'Show web interface hints', 1, 'SHOWHINTS');
INSERT INTO hotwire."hw_Preferences" VALUES (25, 'Show post-update options', 1, 'POSTUPDATEOPT');
INSERT INTO hotwire."hw_Preferences" VALUES (26, 'Show post-insert options', 1, 'POSTINSERTOPT');
INSERT INTO hotwire."hw_Preferences" VALUES (27, 'Offer xdebug option to developers', 1, 'XDEBUG');
INSERT INTO hotwire."hw_Preferences" VALUES (28, 'DB Development group', 3, 'DBDEV');
INSERT INTO hotwire."hw_Preferences" VALUES (29, 'Database internal date format', 3, 'DBDATEFORMAT');
INSERT INTO hotwire."hw_Preferences" VALUES (30, 'Use Ajax many-many selects', 1, 'MM_UI_AJAX');
INSERT INTO hotwire."hw_Preferences" VALUES (31, 'Use many-many tag interface', 1, 'MM_UI_TAG');
INSERT INTO hotwire."hw_Preferences" VALUES (32, 'No JS on many-many selects', 1, 'MM_UI_NOJS');
INSERT INTO hotwire."hw_Preferences" VALUES (33, 'First day of the week', 2, 'JS_UI_DAYSTART');
INSERT INTO hotwire."hw_Preferences" VALUES (34, 'Open record in own tab', 1, 'OWNTAB');


\echo Populated table hw_Preferences
SELECT setval('hotwire.hw_pref_seq',(select max(id) from hotwire."hw_Preferences"));

-- All user preferences
\echo Creating table hw_User Preferences
DROP VIEW IF EXISTS hotwire."90_Action/Hotwire/User Preferences";
DROP TABLE IF EXISTS hotwire."hw_User Preferences";
DROP SEQUENCE IF EXISTS hotwire.hw_user_pref_seq;
CREATE SEQUENCE hotwire.hw_user_pref_seq
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE hotwire.hw_user_pref_seq OWNER TO postgres;
CREATE TABLE hotwire."hw_User Preferences" (
    id integer DEFAULT nextval('hotwire.hw_user_pref_seq'::regclass) NOT NULL,
    preference_id integer,
    preference_value character varying,
    user_id bigint
);
\echo Created table hw_User Preferences
ALTER TABLE hotwire."hw_User Preferences" OWNER TO postgres;
INSERT INTO hotwire."hw_User Preferences" VALUES (1, 1, 'FALSE', NULL);
INSERT INTO hotwire."hw_User Preferences" VALUES (2, 2, 'TRUE', NULL);
INSERT INTO hotwire."hw_User Preferences" VALUES (3, 3, 'TRUE', NULL);
INSERT INTO hotwire."hw_User Preferences" VALUES (4, 4, 'user-css', NULL);
INSERT INTO hotwire."hw_User Preferences" VALUES (5, 5, 'TRUE', NULL);
INSERT INTO hotwire."hw_User Preferences" VALUES (6, 6, '500', NULL);
INSERT INTO hotwire."hw_User Preferences" VALUES (7, 7, 'user-js', NULL);
INSERT INTO hotwire."hw_User Preferences" VALUES (9, 10, 'TRUE', NULL);
INSERT INTO hotwire."hw_User Preferences" VALUES (10, 11, 'TRUE', NULL);
INSERT INTO hotwire."hw_User Preferences" VALUES (11, 12, 'TRUE', NULL);
INSERT INTO hotwire."hw_User Preferences" VALUES (12, 13, 'TRUE', NULL);
INSERT INTO hotwire."hw_User Preferences" VALUES (13, 14, 'FALSE', NULL);
INSERT INTO hotwire."hw_User Preferences" VALUES (14, 15, 'TRUE', NULL);
INSERT INTO hotwire."hw_User Preferences" VALUES (15, 16, 'TRUE', NULL);
INSERT INTO hotwire."hw_User Preferences" VALUES (16, 30, 'TRUE', NULL);

ALTER TABLE ONLY hotwire."hw_User Preferences"
    ADD CONSTRAINT "hw_User Preferences.pkey" PRIMARY KEY (id);
SELECT setval('hotwire.hw_user_pref_seq',(select max(id) from hotwire."hw_User Preferences"));

\echo Populated table hw_User Preferences

