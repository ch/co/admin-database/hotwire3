\set QUIET
\echo Creating view hotwire._column_data (info about records)
-- To determine meta-information about columns (size, length etc)

DROP VIEW IF EXISTS hotwire._column_data;

CREATE OR REPLACE VIEW hotwire._column_data AS
 SELECT pg_attribute.attname,
    pg_type.typname,
    pg_attribute.atttypmod,
    pg_class.relname,
    pg_attribute.attnotnull,
    pg_attribute.atthasdef,
    pg_type.typelem > 0::oid AS "array",
    ( SELECT child.typname
           FROM pg_type child
          WHERE child.oid = pg_type.typelem) AS elementtype
   FROM pg_attribute
     JOIN pg_type ON pg_attribute.atttypid = pg_type.oid
     JOIN pg_class ON pg_attribute.attrelid = pg_class.oid
     JOIN pg_namespace ON pg_class.relnamespace = pg_namespace.oid
  WHERE (pg_namespace.nspname = 'public'::name OR pg_namespace.nspname = 'hotwire'::name) AND pg_attribute.attnum > 0;



ALTER TABLE hotwire._column_data OWNER TO postgres;
GRANT ALL ON TABLE hotwire._column_data TO postgres;
\echo hotwire._column_data created 
