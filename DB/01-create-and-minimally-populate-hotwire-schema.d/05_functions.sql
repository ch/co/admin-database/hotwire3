-- FUNCTION: hotwire._primarytable(character varying)

DROP FUNCTION IF EXISTS hotwire._primarytable(character varying);

CREATE OR REPLACE FUNCTION hotwire._primarytable(
	_view character varying)
    RETURNS character varying
    LANGUAGE 'plpgsql'
    COST 100
    IMMUTABLE STRICT PARALLEL UNSAFE
AS $BODY$
 declare
  nextview varchar;
  basetable varchar:=null;
 begin
 -- Meh, this is rather broken.
 if 1=0 then
  -- Loop over possible views/tables.
  for nextview in select regexp_replace(regexp_replace(regexp_split_to_table(definition,E'[\\.AS\ ]+id[, ]'::text),'^.* ',''),E'\\..*','') from pg_views where viewname=_view loop
   if basetable is null then
    -- return next 'Considering '||nextview;
    perform * from pg_tables where schemaname!='cache' and tablename=nextview;
    if found then
     -- return next 'Accept '||nextview;
     basetable=nextview;
    else
     perform * from pg_views where schemaname !='cache' and viewname=nextview and viewname!=_view;
     if found then
      -- basetable='Recursing for '||nextview;
      basetable=_primarytable(nextview);
     else
--      return next 'Rejecting '||nextview;
     end if;
    end if;
   end if;
    -- Might be a view    
  end loop;
  return basetable;
  else
   return null;
  end if;
 end
$BODY$;

ALTER FUNCTION hotwire._primarytable(character varying)
    OWNER TO postgres;

GRANT EXECUTE ON FUNCTION hotwire._primarytable(character varying) TO PUBLIC;

GRANT EXECUTE ON FUNCTION hotwire._primarytable(character varying) TO postgres;

