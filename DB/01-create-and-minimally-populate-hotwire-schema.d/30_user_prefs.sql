\set QUIET
\echo Creating "User Preferences" view to edit everybody's preferences

DROP VIEW IF EXISTS hotwire."90_Action/Hotwire/User Preferences";

CREATE OR REPLACE VIEW hotwire."90_Action/Hotwire/User Preferences" AS 
 SELECT "hw_User Preferences".id, "hw_User Preferences".preference_id, "hw_User Preferences".preference_value, "hw_User Preferences".user_id as "Postgres_User_id"
   FROM hotwire."hw_User Preferences";

ALTER TABLE hotwire."90_Action/Hotwire/User Preferences" OWNER TO postgres;
GRANT ALL ON TABLE hotwire."90_Action/Hotwire/User Preferences" TO postgres;

CREATE OR REPLACE RULE hw_user_preferences_del AS
    ON DELETE TO hotwire."90_Action/Hotwire/User Preferences" DO INSTEAD  DELETE FROM hotwire."hw_User Preferences"
  WHERE "hw_User Preferences".id = old.id;

CREATE OR REPLACE RULE hw_user_preferences_ins AS
    ON INSERT TO hotwire."90_Action/Hotwire/User Preferences" DO INSTEAD  INSERT INTO hotwire."hw_User Preferences" (preference_id, preference_value, user_id) 
  VALUES (new.preference_id, new.preference_value, new."Postgres_User_id");

CREATE OR REPLACE RULE hw_user_preferences_upd AS
    ON UPDATE TO hotwire."90_Action/Hotwire/User Preferences" DO INSTEAD  UPDATE hotwire."hw_User Preferences" SET preference_id = new.preference_id, preference_value = new.preference_value, user_id = new."Postgres_User_id"
  WHERE "hw_User Preferences".id = old.id;

\echo Created "User Preferences" View
