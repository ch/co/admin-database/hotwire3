-- What permissions/roles do we have?
\set QUIET
\echo Creating hotwire._role_data (Which roles/permissions apply to this user?)
DROP VIEW IF EXISTS hotwire._role_data;

CREATE OR REPLACE VIEW hotwire._role_data AS 
  SELECT pg_roles.rolname AS member,
    pg_get_userbyid(a.oid) AS role
   FROM pg_roles,
    pg_authid a
  WHERE pg_has_role(pg_roles.rolname, a.oid, 'member'::text) AND pg_roles.rolcanlogin AND pg_roles.rolname <> pg_get_userbyid(a.oid);

ALTER TABLE hotwire._role_data OWNER TO postgres;
GRANT ALL ON TABLE hotwire._role_data TO postgres;
GRANT SELECT ON TABLE hotwire._role_data TO public;
\echo hotwire._role_data created
