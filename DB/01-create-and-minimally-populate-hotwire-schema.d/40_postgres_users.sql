\set QUIET
\echo Creating Postgres_User_hid

DROP VIEW IF EXISTS hotwire."Postgres_User_hid";

CREATE OR REPLACE VIEW hotwire."Postgres_User_hid" AS 
 SELECT pg_roles.oid::bigint AS "Postgres_User_id", pg_roles.rolname AS "Postgres_User_hid"
   FROM pg_roles;

ALTER TABLE hotwire."Postgres_User_hid" OWNER TO postgres;
GRANT ALL ON TABLE hotwire."Postgres_User_hid" TO postgres;
\echo Created
