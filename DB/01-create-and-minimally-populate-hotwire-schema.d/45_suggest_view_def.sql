-- FUNCTION: hotwire.suggest_view(character varying, character varying, character varying, character varying)

DROP FUNCTION IF EXISTS hotwire.suggest_view(character varying, character varying, character varying, character varying);

CREATE OR REPLACE FUNCTION hotwire.suggest_view(
	itablename character varying,
	ischemaname character varying,
	omenuname character varying,
	oviewname character varying)
    RETURNS SETOF character varying 
    LANGUAGE 'plpgsql'
    COST 100
    VOLATILE PARALLEL UNSAFE
    ROWS 1000

AS $BODY$
declare
  hw_schema varchar:='hotwire'; -- name of schema
  colcount bigint;
  keycount bigint;
  colinf record;
  query text;
  pkeyfield varchar;
  fullname varchar;
  colalias varchar;
  -- Type of table/view
  qtype varchar;
  updrule text;
  delrule text;
  insrule text;
  insruleb text:=') values (';
  insrulec text:=') RETURNING ';
  hidtable varchar;
  viewname varchar:=coalesce(oviewname,itablename);
  menuname varchar:=coalesce(omenuname,'Menu');
  testschema varchar;
  istable boolean;
  roid integer;
  rtable varchar;
  rfieldnum integer;
  s_name varchar;
  t_name varchar;
  c_name varchar;
  o_column varchar; 
  qfields varchar[]; -- Fields to go into the query
  qfields_late varchar[]; -- Fields to go at the /end/ of the query
  upd_rule_fields varchar[]; -- To be handled in the main part of the update rule
  upd_rule_mm varchar[]; -- To be handled in the later part of the update rule
  seen_fields varchar[]; -- Keep track of fields which have to be automatically aliased because we have two of them. Somehow.
  field_count integer:=0; -- A unique integer for each field. Meh.
  ins_rule_fields varchar[]; -- To handle the fields in the main part of the trigger function.
  ins_rule_values varchar[]; -- the 'new.X' values to be inserted
  ins_trig_sql varchar[]; -- A list of SQL to go into the insert trigger function.
begin
 return next '-- [II] Using suggest_view2 '::varchar; 
 -- Is this a table or a view?
 select count(*) from pg_catalog.pg_tables 
  where pg_tables.schemaname=ischemaname 
    and pg_tables.tablename=  itablename 
   into colcount;
 istable=(colcount=1);
 if (istable) THEN
  qtype='table';
 else
  qtype='view';
 END IF;

 return next '-- [II]  Hotwire suggesting a view definition for the '||qtype||' "'||itablename||'" ';
 return next '-- [II]  in the "'||ischemaname||'" schema'::varchar;
 -- Check that this table exists
 select count(*) from information_schema.columns 
  where table_name=itablename 
    and table_schema=ischemaname 
   into colcount;
 if (colcount<1) THEN
  return next '-- [EE]  Hotwire was unable to find a definition for that '||qtype||'. Sorry.';
  raise notice ' Hotwire was unable to find a definition for that %', qtype ;
  return;
  -- EXIT;
 END IF;
 return next '-- [II]  Found '||colcount||' fields in that '||qtype;

-- Check that this table has a primary key
if (istable) THEN
  SELECT kcu.column_name FROM information_schema.table_constraints tc
             LEFT JOIN information_schema.key_column_usage kcu 
                    ON tc.constraint_catalog = kcu.constraint_catalog 
                   AND tc.constraint_schema = kcu.constraint_schema 
                   AND tc.constraint_name = kcu.constraint_name 
             LEFT JOIN information_schema.referential_constraints rc 
                    ON tc.constraint_catalog = rc.constraint_catalog
                   AND tc.constraint_schema = rc.constraint_schema
                   AND tc.constraint_name = rc.constraint_name
             LEFT JOIN information_schema.constraint_column_usage ccu
                    ON rc.unique_constraint_catalog = ccu.constraint_catalog
                   AND rc.unique_constraint_schema = ccu.constraint_schema
                   AND rc.unique_constraint_name = ccu.constraint_name
                 WHERE lower(tc.constraint_type) in ('primary key') 
                   AND tc.constraint_schema = ischemaname
                   AND tc.table_name=itablename into pkeyfield;
  if (pkeyfield IS NULL) THEN
   return next '-- [EE]  Hotwire was unable to find a primary key for that '||qtype||'. Perhaps you need to add one?';
   return;
   -- EXIT;
  else
   return next '-- [II] Primary key on this '||qtype||' is '||pkeyfield;
  END IF;
ELSE
  -- How do we find a primary key for a view?
  return next '-- [II]  Attempting to identify primary key on the view '||itablename;
  -- Find the _RETURN rule for this view
  select distinct objid from pg_depend 
                  inner join pg_rewrite on pg_depend.objid=pg_rewrite.oid 
                   where refobjid=itablename::regclass 
                         and classid='pg_rewrite'::regclass  
                         and rulename='_RETURN'into roid;
  select count(*) from (select distinct refobjid::regclass 
                                   from pg_depend 
                                  where objid=roid 
                                    and deptype='n' 
                                    and refobjsubid!=0) a into colcount;
  if (colcount>1) THEN
    return next '-- [EE]  Sorry, hotwire cannot (yet) suggest definitions for views coming from more';
    return next '--       than one table.';
    return;
    -- exit;
  else 

    select distinct refobjid::regclass from pg_depend where objid=roid and deptype='n' and refobjsubid!=0 limit 1 into rtable;
    
    return next '-- [II]  Good, this view uses a single table ('||rtable||')';
    SELECT kcu.column_name FROM information_schema.table_constraints tc
             LEFT JOIN information_schema.key_column_usage kcu 
                    ON tc.constraint_catalog = kcu.constraint_catalog 
                   AND tc.constraint_schema = kcu.constraint_schema 
                   AND tc.constraint_name = kcu.constraint_name 
             LEFT JOIN information_schema.referential_constraints rc 
                    ON tc.constraint_catalog = rc.constraint_catalog
                   AND tc.constraint_schema = rc.constraint_schema
                   AND tc.constraint_name = rc.constraint_name
             LEFT JOIN information_schema.constraint_column_usage ccu
                    ON rc.unique_constraint_catalog = ccu.constraint_catalog
                   AND rc.unique_constraint_schema = ccu.constraint_schema
                   AND rc.unique_constraint_name = ccu.constraint_name
                 WHERE lower(tc.constraint_type) in ('primary key') 
                   AND tc.table_name=rtable into pkeyfield;
    return next '--       Checking for presence of that table''s primary key ('||pkeyfield||')';
    return next '         in the columns of the view';
    -- Find column number
    select attnum from pg_attribute where attrelid='person'::regclass and attname='id' into rfieldnum;
    -- Is column number in view?
    select count(*) from pg_depend where objid=roid and deptype='n' and refobjsubid!=0 and refobjsubid=rfieldnum into colcount;
    if (colcount=1) then
      return next '-- [II]  Good, the primary key ('||pkeyfield||') on that table appears in the view.';
    else
      return next '-- [EE]  Oh dear, the primary key ('||pkeyfield||') doesn''t appear in the view. ';
      return next '--       Try adding it to the view and repeating this operation...';
      return;
      -- exit;
    end if;
  END IF;
--   execute select distinct objid from pg_depend inner join pg_rewrite on 
-- pg_depend.objid=pg_rewrite.oid where refobjid='tview'::regclass 
-- and classid='pg_rewrite'::regclass  and rulename='_RETURN';
END IF;
fullname=menuname||'/'||viewname;

query='CREATE VIEW hotwire."'||fullname||'" AS SELECT ';
updrule='CREATE RULE '||quote_ident(hw_schema||'_'||fullname||'_upd')||' AS '||
        'ON UPDATE TO '||quote_ident(hw_schema)||'.'||quote_ident(fullname)||' DO INSTEAD ( '||
        'UPDATE '||quote_ident(ischemaname)||'.'||quote_ident(itablename)||' SET ';
delrule='CREATE RULE '||quote_ident(hw_schema||'_'||fullname||'_del')||' AS ON DELETE TO hotwire."'||fullname||'" DO INSTEAD '||
        'DELETE FROM "'||ischemaname||'"."'||itablename||'" WHERE "'||pkeyfield||'" = old.id;';
insrule='CREATE RULE "hotwire_'||fullname||'_ins" AS ON INSERT TO hotwire."'||fullname||'" DO INSTEAD '||
        'INSERT INTO "'||ischemaname||'"."'||itablename||'" (';

-- Iterate over all fields in the table
for colinf in (select * from information_schema.columns where table_name=itablename and table_schema=ischemaname) LOOP
 colalias=NULL;
 -- return next '-- Considering field '||colinf.column_name;
  -- Only tables have foreign keys. (Well, views sort of do but...)
  IF (istable) THEN
    -- Is this a foreign key? 
    SELECT count(*) FROM information_schema.table_constraints tc 
                      LEFT JOIN information_schema.key_column_usage kcu 
                        ON tc.constraint_catalog = kcu.constraint_catalog 
                          AND tc.constraint_schema = kcu.constraint_schema 
                          AND tc.constraint_name = kcu.constraint_name 
                      LEFT JOIN information_schema.referential_constraints rc 
                        ON tc.constraint_catalog = rc.constraint_catalog 
                          AND tc.constraint_schema = rc.constraint_schema 
                          AND tc.constraint_name = rc.constraint_name 
                      LEFT JOIN information_schema.constraint_column_usage ccu 
                        ON rc.unique_constraint_catalog = ccu.constraint_catalog 
                          AND rc.unique_constraint_schema = ccu.constraint_schema 
                          AND rc.unique_constraint_name = ccu.constraint_name
                  WHERE lower(tc.constraint_type) in ('foreign key') 
                    AND kcu.column_name=colinf.column_name
                    AND tc.table_name=itablename into keycount;
    IF (keycount>0) THEN
      if ((length(colinf.column_name)>3) AND 
          (substr(colinf.column_name,length(colinf.column_name)-(3-1),3)!='_id')) then
        colalias=colinf.column_name||'_id';
      end if;
      hidtable=regexp_replace(coalesce(colalias,colinf.column_name),'_id$','_hid');
      testschema='hotwire';
      select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema 
        into colcount;
      IF (colcount<1) THEN
        -- There is no _hid table in hotwire or public.
        return next '-- [EE]  No HID table or view hotwire.'||hidtable;
        return next '--       This table or view should exist and is used by hotwire to translate ';
        return next '--       identifiers into human-readable form. Create this view/table before';
        return next '--       trying to use Hotwire on your view!';
      ELSE
        return next '-- [II]  The HID table/view hotwire."'||hidtable||'" was found';
        -- Verify that both _hid._hid and _hid._id exist
        select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema 
          and column_name=coalesce(colalias,colinf.column_name) into colcount;
        if (colcount<1) THEN
          return next '-- [EE]  Although the HID table/view '||hidtable||' exists in the hotwire schema,';
          return next '--       it does not contain the column '||coalesce(colalias,colinf.column_name);
          return next '--       This column is required and should contain the unique identifier for the row';
        END IF;
        select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema and column_name=hidtable 
          into colcount; 
        IF (colcount<1) THEN
          return next '-- [EE]  Although the HID table/view '||hidtable||' exists in the hotwire schema,';
          return next '--       it does not contain the column '||coalesce(hidtable);
          return next '--       This column is required and should contain human-readable text identifying';
          return next '--       the row';
        END IF;
      END IF;
    END IF; -- Is this a foreign key?  
  ELSE
    if (substr(colinf.column_name,length(colinf.column_name)-(3-1),3)='_id') THEN
      return next '-- [II]  The column '||colinf.column_name||' will be treated as a foreign key';
      hidtable=regexp_replace(coalesce(colalias,colinf.column_name),'_id$','_hid');
      testschema='hotwire';
      select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema into colcount;
      IF (colcount<1) THEN
        -- There is no _hid table in hotwire or public.
        return next '-- [EE]  No HID table or view hotwire.'||hidtable;
        return next '--       This table or view should exist and is used by hotwire to translate ';
        return next '--       identifiers into human-readable form. Create this view/table before';
        return next '--       trying to use Hotwire on your view!';
      ELSE
        return next '-- [II]  The HID table/view hotwire."'||hidtable||'" was found';
        -- Verify that both _hid._hid and _hid._id exist
        select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema 
          and column_name=coalesce(colalias,colinf.column_name) into colcount;
        if (colcount<1) THEN
          return next '-- [EE]  Although the HID table/view '||hidtable||' exists in the hotwire schema,';
          return next '--       it does not contain the column '||coalesce(colalias,colinf.column_name);
          return next '--       This column is required and should contain the unique identifier for the row';
        END IF;
        select count(*) from information_schema.columns where table_name=hidtable and table_schema=testschema 
          and column_name=hidtable into colcount; 
        IF (colcount<1) THEN
          return next '-- [EE]  Although the HID table/view '||hidtable||' exists in the hotwire schema,';
          return next '--       it does not contain the column '||coalesce(hidtable);
          return next '--       This column is required and should contain human-readable text identifying';
          return next '--       the row';
        END IF;
      END IF;
    END IF;
  END IF;
 -- Is this the primary key?
 if (colinf.column_name=ANY(seen_fields)) THEN
  colalias=colinf.column_name||'_'||field_count;
  seen_fields=array_append(seen_fields,colalias);
 else
  seen_fields=array_append(seen_fields,colinf.column_name::varchar);
 end if;
 if (colinf.column_name=pkeyfield) THEN
  if (pkeyfield!='id') THEN
   colalias='id';
  END IF;
 ELSE
  -- We don't insert to the primary key of a table.
  ins_rule_fields=array_append(ins_rule_fields,quote_ident(colinf.column_name)::varchar);
  ins_rule_values=array_append(ins_rule_values,('new.'||quote_ident(coalesce(colalias,colinf.column_name)))::varchar);
  insrule=insrule||' "'||colinf.column_name||'", ';
  insruleb=insruleb||' new."'||coalesce(colalias, colinf.column_name)||'", ';
 END IF;
 insrulec=insrulec||' "'||itablename||'"."'||colinf.column_name||'", ';
 -- DEL ME query=query||' "'||colinf.column_name||'"'||coalesce(' AS "'||colalias||'"','')||',';
 qfields=array_append(qfields,(quote_ident(colinf.column_name)||coalesce(' AS '||quote_ident(colalias),''))::varchar);
 -- Let's not update the primary key
 if (colinf.column_name!=pkeyfield) then
  upd_rule_fields=array_append(upd_rule_fields,(quote_ident(colinf.column_name)||'=new.'||quote_ident(coalesce(colalias,colinf.column_name)))::varchar);
 end if;
 -- updrule=updrule||'"'||colinf.column_name||'"=new."'||coalesce(colalias,colinf.column_name)||'", ';
 field_count=field_count+1;
END LOOP;
-- Our first part of the insert trigger is built from the field names:
ins_trig_sql=array_append(ins_trig_sql,(
 'INSERT INTO '||quote_ident(ischemaname)||'.'||quote_ident(itablename)||' ('||array_to_string(ins_rule_fields,',')||') '||
 'VALUES ('||array_to_string(ins_rule_values,',')||') RETURNING '||quote_ident(pkeyfield)||' into _new_id')::varchar);
-- Look for foreign keys into this table.
for s_name,t_name,c_name in (SELECT
    tc.table_schema,  tc.table_name, kcu.column_name 
FROM 
    information_schema.table_constraints AS tc 
    JOIN information_schema.key_column_usage AS kcu
      ON tc.constraint_name = kcu.constraint_name
    JOIN information_schema.constraint_column_usage AS ccu
      ON ccu.constraint_name = tc.constraint_name
WHERE constraint_type = 'FOREIGN KEY' AND ccu.table_schema=ischemaname and ccu.table_name=itablename and ccu.column_name=pkeyfield) loop
 colalias=null;
 return next '-- [II] '||t_name||'.'||c_name||' is a key'; 
 -- Is this table more than an mm_ table (i.e. has > two columns?
 if (select count(*) from information_schema.columns where table_schema=s_name and table_name=t_name)>2 then
  return next '-- [II] '||t_name||' is a record';
  -- Create a dummy hw_subviewc
  qfields_late=array_append(qfields_late,($$hotwire._to_hwsubviewb('A_hotwire_summary_view_on_$$||t_name||$$','$$||itablename||$$_id','A_hotwire_detailed_view_on_$$||t_name||$$',NULL,NULL) as ro_$$||t_name)::varchar);
 else
  -- the /other/ field is:
  return next '-- [II] '||t_name||' is an MM table';
  select column_name from information_schema.columns where table_schema=s_name and table_name=t_name and column_name !=c_name into o_column;
  if (o_column=ANY(seen_fields)) THEN
   colalias=o_column||'_'||field_count;
  end if;
  seen_fields=array_append(seen_fields,coalesce(colalias,o_column));
  return next '-- [II] '||o_column||' is the other column';
  qfields=array_append(qfields,('array(select '||quote_ident(o_column)||' from '||quote_ident(s_name)||'.'||quote_ident(t_name)||' where '||quote_ident(c_name)||'='||quote_ident(itablename)||'.id) as '||quote_ident(coalesce(colalias,o_column)))::varchar);
  -- Create an update rule
  --upd_rule_mm=array_append(upd_rule_mm,(
-- 'DELETE FROM '||quote_ident(t_name)||' WHERE '||quote_ident(c_name)||' = old.id and NOT ('||quote_ident(o_column)||' = ANY (COALESCE(new.'||quote_ident(coalesce(colalias,o_column))||$$,'{}'$$||'::integer[])))')::varchar);
  --upd_rule_mm=array_append(upd_rule_mm,(
-- $$INSERT INTO $$||quote_ident(t_name)||$$ ($$||quote_ident(c_name)||$$, $$||quote_ident(o_column)||$$) SELECT old.id, dummy.col FROM ( SELECT unnest(new.$$||quote_ident(coalesce(colalias,o_column))||$$) AS col) dummy WHERE NOT (dummy.col=ANY(COALESCE(old.$$||quote_ident(coalesce(colalias,o_column))||$$, '{}'::integer[])))$$)::varchar);
  -- Create an update rule
   upd_rule_mm=array_append(upd_rule_mm,
    ('select hotwire.fn_mm_set('||quote_literal(t_name)||','||quote_literal(c_name)||','||quote_literal(o_column)||',old.id,new.'||quote_ident(coalesce(colalias,o_column))||',old.'||quote_ident(coalesce(colalias,o_column))||')')::varchar);
  -- Create some SQL to handle inserts
  ins_trig_sql=array_append(ins_trig_sql,('INSERT INTO '||quote_ident(t_name)||' ('||quote_ident(c_name)||', '||quote_ident(o_column)||') SELECT _new_id, dummy.col FROM (SELECT unnest(new.'||quote_ident(coalesce(colalias,o_column))||') as col) dummy'
)::varchar);
 end if;
 field_count=field_count+1;
end loop;

updrule=updrule||array_to_string(upd_rule_fields,',')||' WHERE "'||itablename||'"."'||pkeyfield||'"=old.id;'||array_to_string(upd_rule_mm,';')||';);';
insrule=rtrim(insrule,', ')||rtrim(insruleb,', ')||rtrim(insrulec,', ')||';';
query='CREATE VIEW '||quote_ident(hw_schema)||'.'||quote_ident(fullname)||' AS SELECT '||array_to_string(qfields||qfields_late,',')||' FROM '||quote_ident(ischemaname)||'.'||quote_ident(itablename)||';';
return next '-- Hotwire suggests the following query definition:';
return next 'begin;';
return next query::varchar;
return next '-- Rules:';
return next updrule::varchar;
return next delrule::varchar;
-- return next insrule::varchar;
return next 'ALTER TABLE '||quote_ident(hw_schema)||'.'||quote_ident(fullname)||' OWNER TO cos;';

return next 'CREATE OR REPLACE FUNCTION '||quote_ident(hw_schema||'_'||fullname||'_ins_trig_fn')||'() RETURNS trigger as $RULE$ DECLARE _new_id integer; BEGIN ' || array_to_string(ins_trig_sql,';')||' ; new.id=_new_id;return new;END;$RULE$ LANGUAGE plpgsql VOLATILE COST 100;';
return next 'ALTER FUNCTION '||quote_ident(hw_schema||'_'||fullname||'_ins_trig_fn')||'() OWNER TO cos;';
return next 'CREATE TRIGGER '||quote_ident(hw_schema||'_'||fullname||'_ins_trig')||' INSTEAD OF INSERT ON '||quote_ident(hw_schema)||'.'||quote_ident(fullname)||' FOR EACH ROW EXECUTE PROCEDURE '||quote_ident(hw_schema||'_'||fullname||'_ins_trig_fn')||'();';
return next 'commit;';
end
$BODY$;

ALTER FUNCTION hotwire.suggest_view(character varying, character varying, character varying, character varying)
    OWNER TO postgres;

COMMENT ON FUNCTION hotwire.suggest_view(character varying, character varying, character varying, character varying)
    IS 'To suggest a hotwire view from a table''s definition';

