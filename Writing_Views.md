How to write views for hotwire to display

Building a simple view, based on a table

The function hotwire.suggest_view will produce a boiler-plate, hotwire-compatible view as a start. There are rules for handling updates and deletions and a trigger function to handle inserts. (Some views need to use a trigger functions in order to handle foreign-keys without horrible race conditions resulting from currval.) The view takes all the fields from the specified table, the results of looking for foreign-keys in mm_ tables and the results of other foreign keys in more complicated tables. This may well be /more/ information than required, but it's easier to delete fields from a view than it is to add them.

To create a hotwire view based on SCHEMA.TABLE which appears as MENUPATH/NAME, run:

select hotwire.suggest_view('TABLE','SCHEMA','MENUPATH','NAME');

to see the suggested code. Perhaps copy-and-paste into an editor with syntax highlighting to help parsing it. If you're happy that it's going to do something sensible, try

\t on \o /tmp/i.sql
select hotwire.suggest_view('TABLE','SCHEMA','MENUPATH','NAME');
\t off \o
\i /tmp/i.sql

to write the output of the function to a file and execute it. 

Building something more complicated

Hotwire has no way to discover which (if any!) fields in a view is guaranteed to be unique, so it imposes this as a constraint on the definer of the view: The first field will be called "id" and will be unique. It doesn't get displayed to the user (but it does appear in URLs, so it's visible by the user and not hidden from them). Conventionally, id is an integer but it's not required to be, and there are some use-cases where integers contatenated with separators are the only sensible way forward. Hotwire will handle non-integer id fields correctly.

SELECT my_unique_field as id, surname, forenames, favourite_colour from my_table;

would be a fairly minimal example of a query. Convert that into a view with

CREATE VIEW hotwire."Heading/My Thoughts" as SELECT my_unique_field as id, surname, forenames, favourite_colour from my_table;

and grant some rights on it. (For the sake of argument, let us assume that this view contains nothing contentious and that anybody with access to the postgresql server should be able to read it:)

GRANT SELECT on hotwire."Heading/My Thoughts" to public;

Reload your hotwire page (if the menu caching preference is set, log out and log back in) and you should see a menu heading "Heading" appearing with "My Thoughts" as an option underneath.

