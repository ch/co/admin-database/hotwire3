<?php
/*
 * License: This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version. This program is distributed in the hope that it
 * will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty
 * of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
 * Public License for more details.
 */

/*
 * This file is part of Hotwire
 * http://hotwire.sourceforge.net/
 * and holds the main configuration
 */

 /** 
  * @file config.php
  */

mb_http_output("UTF-8") ;

// Define connection to host and database
$PSQL_HOST      = 'localhost';
$PSQL_DB        = 'database';

// 
$PAGE_LENGTH    = 50;

$QUERY_LOG	= '/var/www/db/log/query';

/**
 * Prefices of field
 */
$PREFIX		= array	(
			'ro'	=>	'Read-only',
			'so'	=>	'Sort order',
			'hl'	=>	'Highlight'
			);

/**
 * Suffices of field names 
 */
$SUFFIX         = array (
			'id'		=>	'Record ID',
			'hid'		=>	'Record HID',
			'view'		=>	'View as list',
			'report'	=>	'View as report',
			'ssi'		=>	'Self-service view'
                        );

// TODO: self-service instance should use a different config file.
$SELF_SERV = array('dbuser'   => 'selfservice',
                   'dbpasswd' => 'password');

$SSI_MSG = "Please amend your details in the fields below, and click 'Update' ".
           " once complete.<br>Greyed-out data is shown for informational ".
           "purposes only - if you feel it is incorrect, please contact ".
           "Julie Lee in the Academic Secretary's office in the first instance.";

# Instructions about logging in
$LOGINMSG = 'Log in using the default credentials: user=user,password=pass';

# Database title
$DBTITLE = 'Hotwire demonstration database';



?>
